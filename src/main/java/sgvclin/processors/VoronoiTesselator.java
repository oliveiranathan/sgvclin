/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors;

import java.awt.Dimension;
import java.awt.Polygon;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import kn.uni.voronoitreemap.datastructure.OpenList;
import kn.uni.voronoitreemap.diagram.PowerDiagram;
import kn.uni.voronoitreemap.j2d.PolygonSimple;
import kn.uni.voronoitreemap.j2d.Site;
import sgvclin.data.GeographicPoint;
import sgvclin.data.Map;
import sgvclin.data.MapPoint;

/**
 * @author Nathan Oliveira
 */
public class VoronoiTesselator {

	private final Map map;
	private Dimension mapSize;
	private ConcurrentHashMap<GeographicPoint, Point2D.Double> localitiesToPosition;
	private ConcurrentHashMap<Point2D.Double, MapPoint> positionToMapPoints;
	private final PowerDiagram pd;
	private OpenList sites;

	public VoronoiTesselator(Map map) throws IllegalArgumentException, IllegalStateException {
		this.pd = new PowerDiagram();
		this.map = map;
		if (map.getTemplate() == null) {
			throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE THE MAP TEMPLATE FIRST"));
		}
		if (!map.isInvalid()) {
			throw new IllegalStateException("There is no need to regenerate the tesselation.");
		}
		mapSize = new Dimension(map.getMapImage().getWidth(), map.getMapImage().getHeight());
	}

	public void generate() throws SQLException, IOException {
		getPointsFromDB();
		sites = new OpenList(localitiesToPosition.size());
		localitiesToPosition.entrySet().stream().map((entry) -> entry.getValue()).forEach((point) -> sites.add(new Site(point.x, point.y)));
		PolygonSimple rootPolygon = new PolygonSimple();
		rootPolygon.add(0, 0);
		rootPolygon.add(mapSize.width, 0);
		rootPolygon.add(mapSize.width, mapSize.height);
		rootPolygon.add(0, mapSize.height);
		pd.setSites(sites);
		pd.setClipPoly(rootPolygon);
		pd.computeDiagram();
		for (int i = 0; i < sites.size; i++) {
			Site site = sites.array[i];
			PolygonSimple polygon = site.getPolygon();
			int[] xpoints = new int[polygon.length];
			int[] ypoints = new int[polygon.length];
			for (int j = 0; j < polygon.length; j++) {
				xpoints[j] = (int) Math.round(polygon.getXPoints()[j]);
				ypoints[j] = (int) Math.round(polygon.getYPoints()[j]);
			}
			Area aa = new Area(new Polygon(xpoints, ypoints, polygon.length));
			Point2D.Double p = null;
			for (Point2D.Double p2d : positionToMapPoints.keySet()) {
				if (aa.contains(p2d)) {
					p = p2d;
					break;
				}
			}
			if (p == null) {
				LoggerManager.getInstance().log(Level.SEVERE, "I do not think this should happen...", null);
				continue;
			}
			aa.intersect(map.getTemplate());
			MapPoint mp = positionToMapPoints.get(p);
			if (mp != null) {
				mp.setArea(aa);
				MapPoint.updateMapPointArea(mp);
			} else {
				LoggerManager.getInstance().log(Level.SEVERE, "I do not think this should happen...", null);
			}
		}
		map.setInvalid(false);
		Map.updateIsMapInvalid(map);
	}

	private void getPointsFromDB() throws SQLException {
		List<GeographicPoint> ps = MapPoint.getMapPointsOfMap(map);
		localitiesToPosition = new ConcurrentHashMap<>(ps.size());
		positionToMapPoints = new ConcurrentHashMap<>(ps.size());
		for (GeographicPoint gp : ps) {
			try {
				MapPoint pm = MapPoint.getMapPoint(gp.getCode(), map);
				localitiesToPosition.put(gp, pm.getPosition());
				positionToMapPoints.put(pm.getPosition(), pm);
			} catch (ClassNotFoundException | IOException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		}
	}
}
