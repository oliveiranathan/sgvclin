/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author Nathan Oliveira
 */
public class LoggerManager {

	private static LoggerManager instance = null;
	private static final Logger logger = Logger.getLogger("");

	public static LoggerManager getInstance() {
		if (instance == null) {
			instance = new LoggerManager();
		}
		return instance;
	}

	private LoggerManager() {
		logger.setLevel(Level.CONFIG);
		FileHandler file;
		try {
			file = new FileHandler("sgvclin.log");
		} catch (IOException | SecurityException ex) {
			Logger.getLogger(LoggerManager.class.getName()).log(Level.SEVERE, null, ex);
			return;
		}
		SimpleFormatter format = new SimpleFormatter();
		file.setFormatter(format);
		logger.addHandler(file);
	}

	public void log(Level level, String string, Throwable ex) {
		logger.log(level, string, ex);
	}
}
