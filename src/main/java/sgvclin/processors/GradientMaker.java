/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.SwingUtilities;
import sgvclin.UI.StatusBarImpl;

/**
 * @author Nathan Oliveira
 */
public class GradientMaker {

	private final List<DataPoint> pointsOfData = new ArrayList<>(50);
	private final Color[] colors;

	public GradientMaker(int max) {
		colors = new Color[max + 1];
		colors[0] = Color.white;
		for (int i = 1; i <= max; i++) {
			colors[i] = Color.getHSBColor(5.f / 9.f + i / (9.f * max), 0.5f + 0.5f * i / max, 1.f - 0.5f * i / max);
			colors[i] = new Color(colors[i].getRed(), colors[i].getGreen(), colors[i].getBlue(), 200);
		}
	}

	public void addData(int x, int y, int value) {
		pointsOfData.add(new DataPoint(x, y, value));
	}

	public BufferedImage getGradient(int width, int height) {
		BufferedImage grads = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		List<Thread> thList = new ArrayList<>(height);
		SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().showProgress(height));
		for (int y = 0; y < height; y++) {
			final int yf = y;
			Thread thr = new Thread(() -> {
				int[] values = values(yf, width);
				synchronized (grads) {
					grads.setRGB(0, yf, width, 1, values, 0, width);
				}
			});
			thr.start();
			thList.add(thr);
			SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().setProgress(yf));
		}
		if (!SwingUtilities.isEventDispatchThread()) {
			SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().showIndProgress());
		} else {
			SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().hideProgress());
		}
		while (!thList.isEmpty()) {
			if (!thList.get(0).isAlive()) {
				thList.remove(0);
			}
		}
		return grads;
	}

	private int[] values(int y, int width) {
		int[] values = new int[width];
		for (int x = 0; x < width; x++) {
			values[x] = getValue(x, y);
		}
		return values;
	}

	private int getValue(int x, int y) {
		double dTotal = 0;
		double result = 0;
		for (DataPoint dp : pointsOfData) {
			double d = distance(dp.x, dp.y, x, y);
			d = Math.pow(d, 4);
			if (d > 0) {
				d = 1.0 / d;
			} else {
				d = 1.e20;
			}
			result += dp.value * d;
			dTotal += d;
		}
		if (dTotal > 0) {
			return getColor(result / dTotal);
		} else {
			return getColor(0);
		}
	}

	private double distance(double x1, double y1, double x2, double y2) {
		double dx = x1 - x2;
		double dy = y1 - y2;
		return Math.sqrt(dx * dx + dy * dy);
	}

	private int getColor(double value) {
		int index = (int) Math.round(value);
		if (index >= colors.length) {
			index = colors.length - 1;
		}
		if (index < 0) {
			index = 0;
		}
		return colors[index].getRGB();
	}

	public Color[] getListOfColors() {
		return Arrays.copyOf(colors, colors.length);
	}

	private class DataPoint {

		public int x;
		public int y;
		public int value;

		DataPoint(int x, int y, int value) {
			this.x = x;
			this.y = y;
			this.value = value;
		}
	}
}
