/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import javax.imageio.ImageIO;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.constant.ComponentPositionType;
import net.sf.dynamicreports.report.constant.PageOrientation;
import net.sf.dynamicreports.report.exception.DRException;
import sgvclin.MainController;
import sgvclin.data.MapSaver;

/**
 * @author Nathan Oliveira
 */
public class MapMaker {

	private static MapMaker instance;

	public static MapMaker getMapMaker() {
		if (instance == null) {
			instance = new MapMaker();
		}
		return instance;
	}

	public static void saveImage(BufferedImage image, File file) throws IOException, DRException {
		if (file.getAbsolutePath().endsWith(".png")) {
			ImageIO.write(image, "PNG", file);
		} else if (file.getAbsolutePath().endsWith(".pdf")) {
			JasperReportBuilder report = DynamicReports.report();
			report.setPageFormat(image.getWidth(), image.getHeight(), PageOrientation.PORTRAIT);
			report.setPageMargin(DynamicReports.margin(0));
			report.addSummary(DynamicReports.cmp.image(image).setDimension(image.getWidth(), image.getHeight())
					.setPositionType(ComponentPositionType.FLOAT));
			export(file, report);
		} else {
			throw new IOException("File format not known.");
		}
	}

	@SuppressWarnings("AssignmentToMethodParameter")
	private static void export(File file, JasperReportBuilder report) throws DRException {
		if (!file.getName().endsWith(".pdf")) {
			file = new File(file.getAbsoluteFile() + ".pdf");
		}
		report.toPdf(DynamicReports.export.pdfExporter(file));
	}

	private static BufferedImage trim(BufferedImage img) {
		if (img == null) {
			return null;
		}
		int xI = 0, yI = 0, xF = 0, yF = 0;
		int w = img.getWidth(null);
		int h = img.getHeight(null);
		xIni:
		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				if (img.getRGB(x, y) != Color.WHITE.getRGB()) {
					xI = x;
					break xIni;
				}
			}
		}
		xFim:
		for (int x = w - 1; x >= xI; x--) {
			for (int y = 0; y < h; y++) {
				if (img.getRGB(x, y) != Color.WHITE.getRGB()) {
					xF = x;
					break xFim;
				}
			}
		}
		xF += 10;
		yIni:
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				if (img.getRGB(x, y) != Color.WHITE.getRGB()) {
					yI = y;
					break yIni;
				}
			}
		}
		yFim:
		for (int y = h - 1; y >= yI; y--) {
			for (int x = 0; x < w; x++) {
				if (img.getRGB(x, y) != Color.WHITE.getRGB()) {
					yF = y;
					break yFim;
				}
			}
		}
		yF += 10;
		BufferedImage newImg = new BufferedImage(xF - xI + 1, yF - yI + 1, BufferedImage.TYPE_INT_RGB);
		newImg.getGraphics().setColor(Color.WHITE);
		newImg.getGraphics().fillRect(0, 0, newImg.getWidth(), newImg.getHeight());
		newImg.getGraphics().drawImage(img, 0, 0, newImg.getWidth(), newImg.getHeight(), xI, yI, xF, yF, null);
		return newImg;
	}

	public static BufferedImage createTitle(String title) {
		BufferedImage img = new BufferedImage(6000, 500, BufferedImage.TYPE_INT_ARGB);
		Graphics g = img.getGraphics();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 6000, 500);
		g.setFont(new Font("Arial", Font.BOLD, 50));
		g.setColor(Color.BLACK);
		g.drawString(title, 0, 40);
		return trim(img);
	}

	public static BufferedImage createCopyright() {
		BufferedImage img = new BufferedImage(250, 35, BufferedImage.TYPE_INT_ARGB);
		Graphics g = img.getGraphics();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 600, 70);
		g.setColor(Color.BLACK);
		g.setFont(new Font("Arial", Font.BOLD, 20));
		g.drawString(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DEVELOPED BY..."), 5, 20);
		return img;
	}
	private HashMap<String, MapInterface> maps;

	private MapMaker() {
		maps = new HashMap<>(5);
	}

	public void clear() {
		maps = new HashMap<>(5);
		MainController.getInstance().getMainFrame().disablePrinting();
	}

	public void addMap(String title, MapInterface map) {
		maps.put(title, map);
		MainController.getInstance().getMainFrame().enablePrinting();
	}

	public void removeMap(String title) {
		maps.remove(title);
		if (maps.isEmpty()) {
			MainController.getInstance().getMainFrame().disablePrinting();
		}
	}

	public String[] getMapsTitles() {
		return maps.keySet().toArray(new String[maps.size()]);
	}

	public MapInterface getMap(String title) {
		return maps.get(title);
	}

	public void saveMap(String title) throws SQLException, IOException {
		MapSaver ms = maps.get(title).getMapSaver();
		MapSaver ms_old = null;
		try {
			ms_old = MapSaver.getMapSaver(title);
		} catch (SQLException ex) {
			// Does nothing. If it fails it's probably because there is no map with that title...
		}
		if (ms_old != null) {
			ms.setId(ms_old.getId());
			MapSaver.deleteMapSaver(ms_old);
		} else {
			ms.setId(MapSaver.getNewMapSaverId());
		}
		MapSaver.saveMapSaver(ms);
	}

}
