/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors.dataprocessor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import sgvclin.data.Datasheet;
import sgvclin.data.DatasheetVariant;
import sgvclin.data.GeographicPoint;
import sgvclin.data.Question;

/**
 *
 * @author Nathan Oliveira
 */
public class FilteredData {

	private final HashMap<Filter, HashMap<VariantGroup, Integer>> data;
	private final GeographicPoint point;
	private final List<Integer> NK;
	private final List<Integer> TP;

	public FilteredData(GeographicPoint point) {
		this.point = point;
		data = new HashMap<>(5);
		NK = new ArrayList<>(1);
		TP = new ArrayList<>(1);
	}

	public GeographicPoint getPoint() {
		return point;
	}

	public List<Integer> getNK() {
		return Collections.unmodifiableList(NK);
	}

	public List<Integer> getTP() {
		return Collections.unmodifiableList(TP);
	}

	boolean hasDataFor(Filter filter) {
		if (!data.containsKey(filter)) {
			return false;
		} else if (data.get(filter) == null) {
			return false;
		} else {
			return !data.get(filter).isEmpty();
		}
	}

	void trim() {
		List<Filter> filters = new ArrayList<>(5);
		data.entrySet().stream().forEach((entry) -> {
			if (entry.getValue().isEmpty()) {
				filters.add(entry.getKey());
			}
		});
		filters.stream().forEach((filter) -> data.remove(filter));
	}

	void removeFilter(Filter filter) {
		data.remove(filter);
	}

	public HashMap<VariantGroup, Integer> getVariantGroups(Filter filter) {
		if (filter != null) {
			if (data.get(filter) == null) {
				return null;
			} else {
				return new HashMap<>(data.get(filter));
			}
		}
		HashMap<VariantGroup, Integer> map = new HashMap<>(data.size());
		data.keySet().stream().forEach((f) -> data.get(f).keySet().forEach((vg) -> map.merge(vg, data.get(f).get(vg), Integer::sum)));
		return map;
	}

	void add(Datasheet datasheet, Filter filter, Question question, List<VariantGroup> variantGroups) throws SQLException {
		DatasheetVariant dsv = DatasheetVariant.getDatasheetVariant(question, datasheet);
		if (dsv == null) {
			return;
		}
		Filter f = filter == null ? new Filter() : filter;
		if (!data.containsKey(f)) {
			data.put(f, new HashMap<>(5));
		}
		if (dsv.isNotKnown()) {
			NK.add(datasheet.getInformant());
		} else if (dsv.isTechnicalProblem()) {
			TP.add(datasheet.getInformant());
		} else {
			dsv.getVariants().stream().forEach((variant) -> {
				VariantGroup group = null;
				for (VariantGroup vg : variantGroups) {
					if (vg.contains(variant)) {
						group = vg;
						break;
					}
				}
				if (data.get(f).containsKey(group)) {
					data.get(f).put(group, 1 + data.get(f).get(group));
				} else {
					data.get(f).put(group, 1);
				}
			});
		}
	}
}
