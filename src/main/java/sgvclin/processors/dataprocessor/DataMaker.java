/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors.dataprocessor;

import java.io.InvalidClassException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import sgvclin.data.Datasheet;
import sgvclin.data.GeographicPoint;
import sgvclin.data.Map;
import sgvclin.data.MapPoint;
import sgvclin.data.MapSaver;
import sgvclin.data.Question;
import sgvclin.data.Sheet;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class DataMaker {

	private final HashMap<GeographicPoint, FilteredData> data;
	private final Sheet sheet;
	private final Map map;
	private final List<Question> questions;
	private final List<VariantGroup> variantGroups;
	private final List<Filter> filters;

	public DataMaker(Sheet sheet, List<Filter> filters, List<Question> questions, List<VariantGroup> variantGroups, Map map) throws SQLException, InvalidClassException {
		this.sheet = sheet;
		this.questions = questions;
		this.map = map;
		this.variantGroups = variantGroups;
		this.filters = filters != null ? filters : new ArrayList<>(0);
		data = new HashMap<>(MapPoint.getMapPointsOfMap(map).size());
		createData();
		trim();
	}

	private void createData() throws SQLException, InvalidClassException {
		for (Question question : questions) {
			createQuestionData(question);
		}
	}

	private void createQuestionData(Question question) throws SQLException, InvalidClassException {
		List<Datasheet> datasheets = map != null ? Datasheet.getListDatasheets(MapPoint.getMapPointsOfMap(map), sheet) : Datasheet.getListDatasheets(sheet);
		datasheets.stream().forEach((datasheet) -> {
			Filter filter = null;
			for (Filter f : filters) {
				if (f.match(datasheet)) {
					filter = f;
					break;
				}
			}
			try {
				if (!data.containsKey(datasheet.getLocality())) {
					data.put(datasheet.getLocality(), new FilteredData(datasheet.getLocality()));
				}
				data.get(datasheet.getLocality()).add(datasheet, filter, question, variantGroups);
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		});
	}

	private void trim() {
		List<Filter> empty = new ArrayList<>(filters.size());
		filters.forEach((filter) -> {
			data.entrySet().forEach((entry) -> {
				entry.getValue().trim();
			});
		});
		filters.stream().forEach((filter) -> {
			if (!data.entrySet().stream().anyMatch((entry) -> entry.getValue().hasDataFor(filter))) {
				empty.add(filter);
			}
		});
		empty.stream().forEach((filter) -> {
			data.entrySet().stream().forEach((entry) -> entry.getValue().removeFilter(filter));
			filters.remove(filter);
		});
	}

	public String[] getVariantsGroupsRepresentation() {
		List<String> list = new ArrayList<>(variantGroups.size());
		variantGroups.stream().forEach((group) -> list.add(group.getRepresentation()));
		return list.toArray(new String[list.size()]);
	}

	public Map getMap() {
		return map;
	}

	List<VariantGroup> getVariantGroups() {
		return Collections.unmodifiableList(variantGroups);
	}

	List<Filter> getFilters() {
		return Collections.unmodifiableList(filters);
	}

	List<Question> getQuestions() {
		return Collections.unmodifiableList(questions);
	}

	Sheet getSheet() {
		return sheet;
	}

	public List<GeographicPoint> getPoints() {
		List<GeographicPoint> list = new ArrayList<>(data.size());
		data.keySet().stream().forEach((gp) -> list.add(gp));
		list.sort((GeographicPoint o1, GeographicPoint o2) -> o1.getCode() - o2.getCode());
		return list;
	}

	public FilteredData getFilteredData(GeographicPoint point) {
		return data.get(point);
	}

	MapSaver createMapSaver() {
		MapSaver ms = new MapSaver();
		ms.setMap(map);
		ms.setSheet(sheet);
		ms.setQuestions(questions);
		ms.setVariantGroups(variantGroups);
		ms.setFilters(filters);
		return ms;
	}

	public List<String> getQuestionsRepresentations() {
		List<String> list = new ArrayList<>(questions.size());
		questions.stream().forEach(q -> list.add(q.getNumber() + " - " + q.getQuestion()));
		return list;
	}
}
