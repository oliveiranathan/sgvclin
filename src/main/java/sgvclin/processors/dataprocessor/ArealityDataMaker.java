/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors.dataprocessor;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InvalidClassException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import javax.swing.SwingUtilities;
import sgvclin.UI.StatusBarImpl;
import sgvclin.UI.areality.ArealityPanel;
import sgvclin.data.ColorPattern;
import sgvclin.data.Datasheet;
import sgvclin.data.DatasheetVariant;
import sgvclin.data.GeographicPoint;
import sgvclin.data.Map;
import sgvclin.data.MapPoint;
import sgvclin.data.MapSaver;
import sgvclin.data.Question;
import sgvclin.data.Sheet;
import sgvclin.processors.GradientMaker;
import sgvclin.processors.LoggerManager;
import sgvclin.processors.labelmaker.LabelGradPanel;
import sgvclin.processors.labelmaker.LabelPanel;
import sgvclin.processors.labelmaker.LabelUser;

/**
 *
 * @author Nathan Oliveira
 */
public class ArealityDataMaker extends DataMaker implements LabelUser {

	@SuppressWarnings({"fallthrough", "AssignmentToMethodParameter"})
	private static ColorPattern[] getColors(int size) {
		if (size == 0) {
			return null;
		}
		if (size > 10) {
			size = 10;
		}
		ColorPattern[] colors = new ColorPattern[size];
		switch (size) {
			case 10:
				colors[9] = new ColorPattern(new Color(64, 255, 64, 150), Color.BLACK, ColorPattern.Pattern.plain_color, ColorPattern.DEFAULT_SIZE);
			case 9:
				colors[8] = new ColorPattern(new Color(156, 93, 82, 150), Color.BLACK, ColorPattern.Pattern.plain_color, ColorPattern.DEFAULT_SIZE);
			case 8:
				colors[7] = new ColorPattern(new Color(255, 165, 0, 150).darker(), Color.BLACK, ColorPattern.Pattern.plain_color, ColorPattern.DEFAULT_SIZE);
			case 7:
				colors[6] = new ColorPattern(new Color(0, 255, 255, 150), Color.BLACK, ColorPattern.Pattern.plain_color, ColorPattern.DEFAULT_SIZE);
			case 6:
				colors[5] = new ColorPattern(new Color(255, 0, 255, 150), Color.BLACK, ColorPattern.Pattern.plain_color, ColorPattern.DEFAULT_SIZE);
			case 5:
				colors[4] = new ColorPattern(new Color(0, 0, 0, 150), Color.BLACK, ColorPattern.Pattern.plain_color, ColorPattern.DEFAULT_SIZE);
			case 4:
				colors[3] = new ColorPattern(new Color(255, 255, 0, 150), Color.BLACK, ColorPattern.Pattern.plain_color, ColorPattern.DEFAULT_SIZE);
			case 3:
				colors[2] = new ColorPattern(new Color(0, 192, 0, 150), Color.BLACK, ColorPattern.Pattern.plain_color, ColorPattern.DEFAULT_SIZE);
			case 2:
				colors[1] = new ColorPattern(new Color(0, 0, 255, 150), Color.BLACK, ColorPattern.Pattern.plain_color, ColorPattern.DEFAULT_SIZE);
			case 1:
				colors[0] = new ColorPattern(new Color(255, 0, 0, 150), Color.BLACK, ColorPattern.Pattern.plain_color, ColorPattern.DEFAULT_SIZE);
		}
		return colors;
	}

	private final boolean isGradient;
	private ColorPattern[] colors;
	private boolean hadColors;
	private HashMap<Point2D.Double, Area> polygons;
	private HashMap<GeographicPoint, Point2D.Double> points;
	private HashMap<String, List<Area>> groups;
	private final String[] varsNames;
	private boolean[] lastSelected = null;
	private BufferedImage mask;
	private LabelPanel labelMaker;
	private final ArealityPanel arealityPanel;

	public ArealityDataMaker(Sheet sheet, List<Filter> filters, List<Question> question, List<VariantGroup> variantGroups, Map map, boolean isGradient, ColorPattern[] colors, ArealityPanel arealityPanel)
			throws SQLException, InvalidClassException {
		super(sheet, filters, question, variantGroups, map);
		this.isGradient = isGradient;
		this.colors = colors;
		hadColors = colors != null;
		this.arealityPanel = arealityPanel;
		varsNames = getVariantsGroupsRepresentation();
		generateData(map);
	}

	public ArealityDataMaker(Sheet sheet, List<Question> question, List<VariantGroup> variantGroups, Map map, boolean isGradient, ColorPattern[] colors, ArealityPanel arealityPanel)
			throws SQLException, InvalidClassException {
		this(sheet, Arrays.asList(new Filter()), question, variantGroups, map, isGradient, colors, arealityPanel);
	}

	public String[] getLabelsTexts(int point) {
		String[] retVal = new String[3];
		retVal[0] = "<html>" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OBTAINING THE POINT");
		retVal[1] = "";
		retVal[2] = "";
		GeographicPoint gp;
		try {
			gp = GeographicPoint.getGeoPoint(point);
		} catch (SQLException | NullPointerException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			return retVal;
		}
		retVal[0] = "<html>" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("POINT NUMBER") + ": " + gp.getCode() + "<br>"
				+ java.util.ResourceBundle.getBundle("texts/Bundle").getString("LOCALITY") + ": " + gp.getName() + "<br>"
				+ java.util.ResourceBundle.getBundle("texts/Bundle").getString("STATE") + ": " + gp.getState();
		boolean hasObs = false;
		String NK = null;
		String TP = null;
		try {
			if (getQuestions().size() == 1) {
				for (Datasheet ds : Datasheet.getListDatasheets(getPoints(), getSheet())) {
					if (ds.getLocality().getId() == point) {
						DatasheetVariant dsv;
						try {
							dsv = DatasheetVariant.getDatasheetVariant(getQuestions().get(0), ds);
						} catch (SQLException ex) {
							LoggerManager.getInstance().log(Level.SEVERE, null, ex);
							continue;
						}
						if (dsv == null) {
							continue;
						}
						if (dsv.isNotKnown()) {
							if (NK != null) {
								NK += ", " + dsv.getSheet().getInformant();
							} else {
								NK = java.util.ResourceBundle.getBundle("texts/Bundle").getString("NK: ") + dsv.getSheet().getInformant();
							}
						}
						if (dsv.isTechnicalProblem()) {
							if (TP != null) {
								TP += ", " + dsv.getSheet().getInformant();
							} else {
								TP = java.util.ResourceBundle.getBundle("texts/Bundle").getString("TP: ") + dsv.getSheet().getInformant();
							}
						}
						if (dsv.getObservation() != null && !dsv.getObservation().isEmpty()) {
							if (!hasObs) {
								retVal[1] += "<html><p>" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("NOTES:");
								hasObs = true;
							}
							retVal[1] += java.util.ResourceBundle.getBundle("texts/Bundle").getString("INF.") + " " + dsv.getSheet().getInformant()
									+ ": " + dsv.getObservation();
						}
					}
				}
			}
		} catch (InvalidClassException | SQLException ex) {
			return retVal;
		}
		if (NK != null) {
			retVal[0] += "<br>" + NK;
		}
		if (TP != null) {
			retVal[0] += "<br>" + TP;
		}
		if (!retVal[1].isEmpty()) {
			retVal[1] += "</p>";
		}
		if (groups == null || groups.isEmpty()) {
			return retVal;
		}
		for (String var : groups.keySet()) {
			int indexOcc = groups.get(var).indexOf(polygons.get(points.get(gp)));
			if (indexOcc == -1) {
				try {
					countOccs(null);
					retVal[2] += "0% (0)";
				} catch (UnsupportedOperationException ex_) {
				}
				return retVal;
			}
			try {
				int[] counts = countOccs(var);
				int max = counts[0];
				for (int i : counts) {
					if (max < i) {
						max = i;
					}
				}
				int count = counts[indexOcc];
				int perc = 100 * count / max;
				retVal[2] += perc + "% (" + count + ")";
			} catch (UnsupportedOperationException | ArrayIndexOutOfBoundsException ex) {
				try {
					countOccs(null);
					retVal[2] += "0% (0)";
				} catch (UnsupportedOperationException ex_) {
				}
				return retVal;
			}
		}
		return retVal;
	}

	public ConcurrentHashMap<Point2D.Double, Integer> countValuesForOGLGradient(List<Point2D.Double> allPoints) {
		if (groups.size() != 1) {
			return null;
		}
		String[] vars = groups.keySet().toArray(new String[groups.size()]);
		ConcurrentHashMap<Point2D.Double, Integer> values = new ConcurrentHashMap<>(allPoints.size());
		int index = 0;
		List<Point2D.Double> lacking = new ArrayList<>(allPoints);
		for (String var : vars) {
			int[] occs = countOccs(var);
			int max = 0;
			for (int i : occs) {
				if (max < i) {
					max = i;
				}
			}
			for (Area b : groups.get(var)) {
				int value = occs[index];
				index++;
				Point2D.Double p2d = null;
				for (Point2D.Double p : polygons.keySet()) {
					if (polygons.get(p) == b) {
						p2d = p;
						break;
					}
				}
				if (p2d == null) {
					continue;
				}
				lacking.remove(p2d);
				values.put(p2d, value);
			}
		}
		lacking.stream().forEach((point) -> values.put(point, 0));
		return values;
	}

	public boolean isGradient() {
		return isGradient;
	}

	public List<Point2D.Double> getListPoint2D() {
		List<Point2D.Double> list = new ArrayList<>(points.size());
		points.values().stream().forEach((p2d) -> list.add(p2d));
		return list;
	}

	private List<String> getSelected(boolean[] selecteds) {
		if (selecteds == null || selecteds.length != varsNames.length) {
			return null;
		}
		List<String> list = new ArrayList<>(4);
		for (int i = 0; i < varsNames.length; i++) {
			if (selecteds[i]) {
				list.add(varsNames[i]);
			}
		}
		return list;
	}

	@SuppressWarnings("fallthrough")
	private HashMap<String, List<Area>> createGroups(List<String> vars, List<List<Area>> ocurrencies) {
		HashMap<String, List<Area>> map = new HashMap<>(vars.size());
		if (vars.size() > 4) {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle")
					.getString("IT IS NOT POSSIBLE TO PROCESS MORE THAN FOUR VARIANTS"));
		}
		switch (vars.size()) {
			case 4:
				List<Area> quads = new ArrayList<>(ocurrencies.get(0));
				String str = vars.get(0);
				for (int idx = 1; idx < 4; idx++) {
					quads.retainAll(ocurrencies.get(idx));
					str += ", " + vars.get(idx);
				}
				if (!quads.isEmpty()) {
					map.put(str, quads);
				}
			case 3:
				for (int idx1st = 0; idx1st < vars.size() - 2; idx1st++) {
					for (int idx2nd = idx1st + 1; idx2nd < vars.size() - 1; idx2nd++) {
						for (int idx3rd = idx2nd + 1; idx3rd < vars.size(); idx3rd++) {
							List<Area> trios = new ArrayList<>(ocurrencies.get(idx1st));
							trios.retainAll(ocurrencies.get(idx2nd));
							trios.retainAll(ocurrencies.get(idx3rd));
							for (int idxOth = 0; idxOth < vars.size(); idxOth++) {
								if (idxOth != idx1st && idxOth != idx2nd && idxOth != idx3rd) {
									for (int idxTest = 0; idxTest < ocurrencies.get(idxOth).size(); idxTest++) {
										Area p = ocurrencies.get(idxOth).get(idxTest);
										if (trios.contains(p)) {
											trios.remove(p);
										}
									}
								}
							}
							if (!trios.isEmpty()) {
								map.put(vars.get(idx1st) + ", " + vars.get(idx2nd) + ", " + vars.get(idx3rd), trios);
							}
						}
					}
				}
			case 2:
				for (int idx1st = 0; idx1st < vars.size() - 1; idx1st++) {
					for (int idx2nd = idx1st + 1; idx2nd < vars.size(); idx2nd++) {
						List<Area> pairs = new ArrayList<>(ocurrencies.get(idx1st));
						pairs.retainAll(ocurrencies.get(idx2nd));
						for (int idxOth = 0; idxOth < vars.size(); idxOth++) {
							if (idxOth != idx1st && idxOth != idx2nd) {
								for (int idxTest = 0; idxTest < ocurrencies.get(idxOth).size(); idxTest++) {
									Area p = ocurrencies.get(idxOth).get(idxTest);
									if (pairs.contains(p)) {
										pairs.remove(p);
									}
								}
							}
						}
						if (!pairs.isEmpty()) {
							map.put(vars.get(idx1st) + ", " + vars.get(idx2nd), pairs);
						}
					}
				}
			case 1:
				for (int idx1st = 0; idx1st < vars.size(); idx1st++) {
					List<Area> uniques = new ArrayList<>(ocurrencies.get(idx1st));
					for (int idxOth = 0; idxOth < vars.size(); idxOth++) {
						if (idxOth != idx1st) {
							for (int idxTest = 0; idxTest < ocurrencies.get(idxOth).size(); idxTest++) {
								Area p = ocurrencies.get(idxOth).get(idxTest);
								if (uniques.contains(p)) {
									uniques.remove(p);
								}
							}
						}
					}
					if (!uniques.isEmpty()) {
						map.put(vars.get(idx1st), uniques);
					}
				}
			case 0:
			default:
				return map;
		}
	}

	public void setVars(boolean[] selectedB) {
		if (selectedB == null) {
			return;
		}
		lastSelected = Arrays.copyOf(selectedB, selectedB.length);
		List<String> selectedS = getSelected(selectedB);
		List<List<Area>> polys = new ArrayList<>(selectedS.size());
		selectedS.stream().forEach((selected) -> {
			try {
				polys.add(calcOverlay(selected));
			} catch (Exception ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		});
		groups = createGroups(selectedS, polys);
		String[] vars = groups.keySet().toArray(new String[groups.size()]);
		if (!hadColors || colors == null || colors.length != vars.length) {
			colors = new ColorPattern[groups.size()];
			for (int idx = 0; idx < vars.length; idx++) {
				String var = vars[idx];
				boolean done = false;
				for (int idxAux = 0; idxAux < varsNames.length; idxAux++) {
					if (var.equals(varsNames[idxAux])) {
						colors[idx] = (idxAux < 10) ? getColors(varsNames.length)[idxAux] : new ColorPattern(new Color(150, 150, 150, 150), Color.BLACK, ColorPattern.Pattern.plain_color, ColorPattern.DEFAULT_SIZE);
						done = true;
						break;
					}
				}
				if (!done) {
					Random r = new Random();
					colors[idx] = new ColorPattern(new Color(r.nextInt(256), r.nextInt(256), r.nextInt(256), 150), Color.BLACK, ColorPattern.Pattern.plain_color, ColorPattern.DEFAULT_SIZE);
				}
			}
		} else {
			hadColors = false;
		}
		createMask();
		if (!SwingUtilities.isEventDispatchThread()) {
			try {
				SwingUtilities.invokeAndWait(() -> createLabels(vars, colors));
			} catch (InterruptedException | InvocationTargetException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		} else {
			createLabels(vars, colors);
		}
		calcOverlay();
	}

	private void calcOverlay() {
		BufferedImage overlay = new BufferedImage(getMap().getMapImage().getWidth(), getMap().getMapImage().getHeight(), BufferedImage.TYPE_INT_ARGB);
		if (mask != null) {
			Area cut = new Area();
			groups.values().stream().forEach((list) -> list.stream().forEach((area) -> cut.add(area)));
			Graphics2D g2d = overlay.createGraphics();
			g2d.setClip(cut);
			g2d.drawImage(mask, 0, 0, null);
			g2d.dispose();
		} else {
			if (groups == null || groups.isEmpty()) {
				arealityPanel.setOverlay(overlay);
				return;
			}
			Graphics2D g2d = overlay.createGraphics();
			g2d.setColor(new Color(255, 255, 255, 0));
			g2d.fillRect(0, 0, getMap().getMapImage().getWidth(), getMap().getMapImage().getHeight());
			String[] vars = groups.keySet().toArray(new String[groups.size()]);
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.78125f));
			for (int i = 0; i < vars.length; i++) {
				TexturePaint paint = new TexturePaint(colors[i].getTexture(), new Rectangle(colors[i].getSize(), colors[i].getSize()));
				g2d.setStroke(new BasicStroke(3));
				for (Area p : groups.get(vars[i])) {
					g2d.setPaint(paint);
					g2d.fill(p);
					g2d.setColor(Color.BLACK);
					g2d.draw(p);
				}
			}
			g2d.dispose();
		}
		arealityPanel.setOverlay(overlay);
	}

	private void createMask() {
		mask = maskHelper();
	}

	protected BufferedImage maskHelper() {
		try {
			countOccs(null);
		} catch (UnsupportedOperationException ex) {
			return null;
		}
		if (groups.size() != 1) {
			return null;
		}
		String[] vars = groups.keySet().toArray(new String[groups.size()]);
		GradientMaker gm = null;
		int index = 0;
		for (String var : vars) {
			int[] occs = countOccs(var);
			int max = 0;
			for (int i : occs) {
				if (max < i) {
					max = i;
				}
			}
			gm = new GradientMaker(max);
			for (Area b : groups.get(var)) {
				int value = occs[index];
				index++;
				Point2D.Double p2d = null;
				for (Point2D.Double p : polygons.keySet()) {
					if (polygons.get(p) == b) {
						p2d = p;
						break;
					}
				}
				if (p2d == null) {
					continue;
				}
				gm.addData((int) Math.round(p2d.x), (int) Math.round(p2d.y), value);
			}
		}
		colors = gm == null ? null : ColorPattern.toPatterns(gm.getListOfColors());
		SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().hideProgress());
		return gm == null ? null : gm.getGradient(getMap().getMapImage().getWidth(), getMap().getMapImage().getHeight());
	}

	private void createLabels(String[] vars, ColorPattern[] colors) {
		if (!isGradient) {
			labelMaker = new LabelPanel(vars, colors, null, 295, this, true);
		} else if (vars.length != 0) {
			int[] counts = new int[groups.size()];
			int i = 0;
			for (String str : groups.keySet()) {
				int[] cnt = countOccs(str);
				for (int c : cnt) {
					if (counts[i] < c) {
						counts[i] = c;
					}
				}
				i++;
			}
			labelMaker = new LabelGradPanel(vars[0], colors, counts, 295, this);
		}
		arealityPanel.setLabels(labelMaker.getLabel());
		if (isGradient && vars.length == 0) {
			arealityPanel.setLabels(null);
		}
	}

	public HashMap<GeographicPoint, Point2D.Double> getPointsToPoints2D() {
		return new HashMap<>(points);
	}

	private void generateData(Map m) throws NullPointerException, SQLException {
		List<GeographicPoint> ps = getPoints();
		points = new HashMap<>(ps.size());
		polygons = new HashMap<>(ps.size());
		for (GeographicPoint p : ps) {
			try {
				MapPoint pm = MapPoint.getMapPoint(p.getCode(), m);
				points.put(p, pm.getPosition());
				polygons.put(pm.getPosition(), pm.getArea());
			} catch (ClassNotFoundException | IOException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		}
	}

	@Override
	public void notifyColorChange(ColorPattern[] colors) {
		this.colors = Arrays.copyOf(colors, colors.length);
		calcOverlay();
		arealityPanel.setLabels(labelMaker.getLabel());
	}

	private int[] countOccs(String var) throws UnsupportedOperationException {
		if (!isGradient) {
			throw new UnsupportedOperationException("Does not uses this...");
		}
		if (var == null) {
			return null;
		}
		List<Area> areas = groups.get(var);
		List<GeographicPoint> ps = new ArrayList<>(areas.size());
		int[] counts = new int[areas.size()];
		// Reverts its way through the maps to obtain the point based on the polygon.
		for (Area b : areas) {
			if (!polygons.containsValue(b)) {
				throw new UnsupportedOperationException("Polygon does not exists.");
			}
			Point2D.Double val = null;
			for (Point2D.Double d : polygons.keySet()) {
				if (b == polygons.get(d)) {
					val = d;
					break;
				}
			}
			for (GeographicPoint p : this.points.keySet()) {
				if (val != null && val.equals(this.points.get(p))) {
					ps.add(p);
					break;
				}
			}
		}
		// Counts the qtties of the vars in the point.
		VariantGroup variantGroup = null;
		for (VariantGroup vg : getVariantGroups()) {
			if (vg.getRepresentation().equals(var)) {
				variantGroup = vg;
				break;
			}
		}
		for (GeographicPoint gp : getPoints()) {
			if (ps.contains(gp)) {
				FilteredData filteredData = getFilteredData(gp);
				HashMap<VariantGroup, Integer> variantGroups = filteredData.getVariantGroups(null);
				if (variantGroups.containsKey(variantGroup)) {
					counts[ps.indexOf(gp)] += variantGroups.get(variantGroup);
				}
			}
		}
		return counts;
	}

	private List<Area> calcOverlay(String variant) throws Exception {
		List<Area> list = new ArrayList<>(points.size());
		VariantGroup selection = null;
		for (VariantGroup vg : getVariantGroups()) {
			if (vg.getRepresentation().equals(variant)) {
				selection = vg;
				break;
			}
		}
		if (selection == null) {
			return list;
		}
		ConcurrentHashMap<GeographicPoint, Boolean> have = new ConcurrentHashMap<>(points.size());
		points.keySet().stream().forEach((i) -> {
			have.put(i, Boolean.FALSE);
		});
		for (GeographicPoint gp : points.keySet()) {
			FilteredData filteredData = getFilteredData(gp);
			HashMap<VariantGroup, Integer> variantGroups = filteredData.getVariantGroups(null);
			if (variantGroups.containsKey(selection)) {
				have.put(gp, Boolean.TRUE);
			}
		}
		points.keySet().stream().filter((i) -> (have.get(i))).forEach((i) -> {
			list.add(polygons.get(points.get(i)));
		});
		return list;
	}

	public LabelPanel getLabelMaker() {
		return labelMaker;
	}

	@Override
	public MapSaver createMapSaver() {
		MapSaver ms = super.createMapSaver();
		ms.setSelected(lastSelected);
		ms.setColors(colors);
		return ms;
	}
}
