/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors.dataprocessor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Paint;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.InvalidClassException;
import java.sql.SQLException;
import java.text.AttributedString;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StackedBarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.TextAnchor;
import sgvclin.UI.diatopic.ResultPanel;
import sgvclin.data.ColorPattern;
import sgvclin.data.Datasheet;
import sgvclin.data.DatasheetVariant;
import sgvclin.data.GeographicPoint;
import sgvclin.data.Map;
import sgvclin.data.MapPoint;
import sgvclin.data.MapSaver;
import sgvclin.data.Question;
import sgvclin.data.Sheet;
import sgvclin.processors.LoggerManager;
import sgvclin.processors.labelmaker.LabelPanel;
import sgvclin.processors.labelmaker.LabelUser;

/**
 *
 * @author Nathan Oliveira
 */
public class PieDataMaker extends DataMaker implements LabelUser {

	@SuppressWarnings({"AssignmentToMethodParameter", "fallthrough"})
	private static Color[] createColors(int size) {
		if (size == 0) {
			return null;
		}
		if (size > 10) {
			size = 10;
		}
		Color[] cores = new Color[size];
		switch (size) {
			case 10:
				cores[9] = new Color(64, 255, 64);
			case 9:
				cores[8] = new Color(156, 93, 82);
			case 8:
				cores[7] = Color.ORANGE.darker();
			case 7:
				cores[6] = Color.CYAN;
			case 6:
				cores[5] = Color.MAGENTA;
			case 5:
				cores[4] = Color.BLACK;
			case 4:
				cores[3] = Color.YELLOW;
			case 3:
				cores[2] = new Color(0, 192, 0);
			case 2:
				cores[1] = Color.BLUE;
			case 1:
				cores[0] = Color.RED;
		}
		return cores;
	}

	private boolean[] selected = null;
	private Color[] colors = null;
	private LabelPanel label = null;
	private ChartPanel histogramChartPanel = null;
	private HashMap<VariantGroup, Integer> histogram;
	private List<VariantGroup> histogramOrder;
	private HashMap<Point2D.Double, List<Plot>> plots;
	private int plotSize;
	private HashMap<VariantGroup, Integer> mostProductive = null;
	private final int noVars;
	private final ResultPanel resultPanel;
	private GraphType type = GraphType.PIE;

	public PieDataMaker(Sheet sheet, List<Filter> filters, List<Question> questions, List<VariantGroup> variantGroups, Map map,
			int numberOfVariants, int plotSize, ResultPanel resultPanel, Color[] colors, GraphType type)
			throws SQLException, InvalidClassException {
		super(sheet, filters, questions, variantGroups, map);
		this.plotSize = plotSize;
		noVars = numberOfVariants;
		this.resultPanel = resultPanel;
		this.colors = colors;
		this.type = type;
		process();
	}

	public void setSelection(boolean[] selected) throws SQLException {
		// if (the class var is) null, does not create selections (aka. normal plots),
		// else, create with only the n-th group if is selected...
		this.selected = Arrays.copyOf(selected, selected.length);
		colors = null;
		label = null; // This "resets" the label, so that when the user asks for it again, we recreate it with the new parameters
		histogramChartPanel = null; // the same...
		createPlots();
		setPlotsColors();
		calcSize();
	}

	public ChartPanel getHistogramChartPanel() {
		if (histogramChartPanel == null) {
			int total = histogram.keySet().stream().map((set) -> histogram.get(set)).reduce(0, Integer::sum);
			DefaultCategoryDataset dataset = new DefaultCategoryDataset();
			int oth = 0;
			for (VariantGroup vg : histogramOrder) {
				int pos = getVariantGroups().indexOf(vg);
				if (vg != null && selected == null || vg != null && pos != -1 && selected[pos]) {
					String text = vg.getRepresentation();
					dataset.addValue((double) histogram.get(vg) / total * 100.0, text, "");
				} else {
					oth += histogram.get(vg);
				}
			}
			if (histogram.containsKey(null) && histogram.get(null) != 0 || oth != 0) {
				dataset.addValue((double) (histogram.getOrDefault(null, 0) + oth) / total * 100,
						java.util.ResourceBundle.getBundle("texts/Bundle").getString("OTHER"), "");
			}
			JFreeChart hist = ChartFactory.createBarChart(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OVERALL PRODUCTIVITY"),
					java.util.ResourceBundle.getBundle("texts/Bundle").getString("VARIANTS"), "%", dataset, PlotOrientation.VERTICAL, false, true, false);
			hist.getTitle().setFont(new Font("Arial", Font.BOLD, 14));
			hist.setBackgroundPaint(Color.white);
			hist.setBackgroundImageAlpha(0.0f);
			BarRenderer renderer = (BarRenderer) ((CategoryPlot) hist.getPlot()).getRenderer();
			((CategoryPlot) hist.getPlot()).getRangeAxis().setLowerBound(0);
			((CategoryPlot) hist.getPlot()).getRangeAxis().setUpperBound(100);
			hist.getPlot().setBackgroundAlpha(0.0f);
			renderer.setBarPainter(new StandardBarPainter());
			((CategoryPlot) hist.getPlot()).getRenderer().setBaseToolTipGenerator(new StandardCategoryToolTipGenerator("{0}: {2}%", NumberFormat.getInstance()));
			if (colors == null) {
				colors = createColors(histogramOrder.size());
			}
			for (int i = 0; i < histogramOrder.size(); i++) {
				renderer.setSeriesPaint(i, (i < 10 && i < colors.length) ? colors[i] : Color.GRAY);
			}
			if (oth != 0 || histogram.getOrDefault(null, 0) != 0) {
				renderer.setSeriesPaint(dataset.getRowCount() - 1, Color.GRAY);
			}
			histogramChartPanel = new ChartPanel(hist);
			histogramChartPanel.setPreferredSize(new Dimension(300, 250));
			histogramChartPanel.setMaximumSize(histogramChartPanel.getPreferredSize());
		}
		return histogramChartPanel;
	}

	public LabelPanel getLabel() {
		if (label == null) {
			boolean regenerateColors = false;
			if (colors == null) {
				int cSize = 0;
				if (selected != null) {
					for (boolean sel : selected) {
						if (sel) {
							cSize++;
						}
					}
					cSize++;
				} else {
					cSize = histogram.size() - (histogram.getOrDefault(null, 0) != 0 ? 0 : 1);
				}
				colors = new Color[cSize];
				regenerateColors = true;
			}
			Color[] generated = createColors(colors.length);
			String[] texts = new String[histogram.size()];
			Color[] lblColors = new Color[texts.length];
			int i = 0;
			int oth = 0;
			if (histogram.containsKey(null) && histogram.get(null) != 0) {
				oth += histogram.getOrDefault(null, 0);
			}
			if (selected == null) {
				for (VariantGroup vg : histogramOrder) {
					texts[i] = vg.getRepresentation();
					if (regenerateColors) {
						colors[i] = i < 10 ? generated[i] : Color.GRAY;
					}
					lblColors[i] = i < colors.length ? colors[i] : Color.GRAY;
					i++;
				}
			} else {
				for (VariantGroup vg : histogramOrder) {
					boolean found = false;
					for (int j = 0; j < getVariantGroups().size(); j++) {
						if (selected[j] && getVariantGroups().get(j).equals(vg)) {
							texts[i] = vg.getRepresentation();
							if (regenerateColors) {
								colors[i] = i < 10 ? generated[i] : Color.GRAY;
							}
							lblColors[i] = i < colors.length ? colors[i] : Color.GRAY;
							i++;
							found = true;
						}
					}
					if (!found) {
						oth += histogram.get(vg);
					}
				}
			}
			if (oth != 0) {
				texts[i] = java.util.ResourceBundle.getBundle("texts/Bundle").getString("OTHER");
				lblColors[i] = Color.GRAY;
				if (regenerateColors) {
					colors[i] = Color.GRAY;
				}
				i++;
			}
			texts = Arrays.copyOf(texts, i);
			lblColors = Arrays.copyOf(lblColors, i);
			if (colors.length != lblColors.length) {
				colors = Arrays.copyOf(lblColors, lblColors.length);
			}
			String[] pieTypes = new String[getFilters().size()];
			i = 0;
			for (Filter pieType : getFilters()) {
				pieTypes[i] = pieType.toString();
				i++;
			}
			label = new LabelPanel(texts, ColorPattern.toPatterns(lblColors), getFilters().size() == 1 ? null : pieTypes, 295, this, false);
		}
		return label;
	}

	@Override
	public void notifyColorChange(ColorPattern[] patterns) {
		updateColors(ColorPattern.toColors(patterns));
		resultPanel.updatePlotsOnMap();
		resultPanel.setLabels(getLabel().getLabel());
		resultPanel.setHistogram(getHistogramChartPanel());
	}

	public void updateColors(Color[] newColors) {
		colors = Arrays.copyOf(newColors, newColors.length);
		label = null;
		histogramChartPanel = null;
		setPlotsColors();
	}

	public HashMap<Point2D.Double, List<Plot>> getPlots() {
		return new HashMap<>(plots);
	}

	public int getSize() {
		return plotSize;
	}

	public String[] getLabelsTexts(Point2D.Double point) throws SQLException, IOException, ClassNotFoundException {
		String[] retVal = new String[2];
		retVal[0] = java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OBTAINING THE POINT");
		retVal[1] = "";
		for (GeographicPoint locality : getPoints()) {
			MapPoint pm = MapPoint.getMapPoint(locality.getCode(), getMap());
			if (pm.getPosition().equals(point)) {
				retVal[0] = "<html>" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("POINT NUMBER") + ": " + locality.getCode() + "<br>"
						+ java.util.ResourceBundle.getBundle("texts/Bundle").getString("LOCALITY") + ": " + locality.getName() + "<br>"
						+ java.util.ResourceBundle.getBundle("texts/Bundle").getString("STATE") + ": " + locality.getState();
				boolean hasObs = false;
				String NK = null;
				String TP = null;
				if (getQuestions().size() == 1) {
					for (Datasheet fd : Datasheet.getListDatasheets(Arrays.asList(locality), getSheet())) {
						DatasheetVariant vfd;
						try {
							vfd = DatasheetVariant.getDatasheetVariant(getQuestions().get(0), fd);
						} catch (SQLException ex) {
							LoggerManager.getInstance().log(Level.SEVERE, null, ex);
							continue;
						}
						if (vfd == null) {
							continue;
						}
						if (vfd.isNotKnown()) {
							if (NK != null) {
								NK += ", " + vfd.getSheet().getInformant();
							} else {
								NK = java.util.ResourceBundle.getBundle("texts/Bundle").getString("NK: ") + vfd.getSheet().getInformant();
							}
						}
						if (vfd.isTechnicalProblem()) {
							if (TP != null) {
								TP += ", " + vfd.getSheet().getInformant();
							} else {
								TP = java.util.ResourceBundle.getBundle("texts/Bundle").getString("TP: ") + vfd.getSheet().getInformant();
							}
						}
						if (vfd.getObservation() != null && !vfd.getObservation().isEmpty()) {
							if (!hasObs) {
								retVal[1] += "<html>" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("NOTES:");
								hasObs = true;
							}
							retVal[1] += "<br>" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("INF.") + " "
									+ vfd.getSheet().getInformant() + ": " + vfd.getObservation();
						}
					}
				}
				if (NK != null) {
					retVal[0] += "<br>" + NK;
				}
				if (TP != null) {
					retVal[0] += "<br>" + TP;
				}
				break;
			}
		}
		if (!retVal[1].isEmpty()) {
			retVal[1] += "</p>";
		}
		return retVal;
	}

	public String[] getFiltersLabels() {
		List<String> list = new ArrayList<>(getFilters().size());
		getFilters().stream().forEach((filter) -> list.add(filter.toString()));
		return list.toArray(new String[list.size()]);
	}

	public JFreeChart createChart(Plot plot, String label) {
		JFreeChart chart = null;
		switch (type) {
			case PIE:
				chart = ChartFactory.createPieChart(label, ((PiePlot) plot).getDataset(), false, true, false);
				setPlotColors(chart.getPlot());
				((PiePlot) chart.getPlot()).setCircular(true);
				chart.getPlot().setBackgroundAlpha(0.0f);
				((PiePlot) chart.getPlot()).setShadowXOffset(0.0);
				((PiePlot) chart.getPlot()).setShadowYOffset(0.0);
				((PiePlot) chart.getPlot()).setLabelGenerator(new PieSectionLabelGenerator() {
					@Override
					@SuppressWarnings("rawtypes")
					public String generateSectionLabel(PieDataset dataset, Comparable key) {
						double total = 0;
						for (int i = 0; i < dataset.getItemCount(); i++) {
							total += dataset.getValue(i).doubleValue();
						}
						return dataset.getValue(key).intValue() + ", " + String.format("%.2f", dataset.getValue(key).doubleValue()
								/ total * 100.0) + "%";
					}

					@Override
					@SuppressWarnings("rawtypes")
					public AttributedString generateAttributedSectionLabel(PieDataset dataset, Comparable key) {
						return null;
					}
				});
				break;
			case HISTOGRAM:
				chart = ChartFactory.createStackedBarChart(label, null, "%", ((CategoryPlot) plot).getDataset(), PlotOrientation.VERTICAL,
						false, true, false);
				setPlotColors(chart.getPlot());
				CategoryPlot newPlot = chart.getCategoryPlot();
				((StackedBarRenderer) newPlot.getRenderer()).setRenderAsPercentages(true);
				((NumberAxis) newPlot.getRangeAxis()).setNumberFormatOverride(NumberFormat.getPercentInstance());
				newPlot.getRenderer().setBaseToolTipGenerator(new StandardCategoryToolTipGenerator() {
					private static final long serialVersionUID = 1L;

					@Override
					protected String generateLabelString(CategoryDataset dataset, int row, int column) {
						if (row == 0) {
							double value = dataset.getValue(row, column).doubleValue();
							double total = value + dataset.getValue(1, column).doubleValue();
							return value + ", " + (value / total) * 100.0 + "%";
						}
						return null;
					}
				});
				newPlot.getRenderer().setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator() {
					private static final long serialVersionUID = 1L;

					@Override
					protected String generateLabelString(CategoryDataset dataset, int row, int column) {
						if (row == 0) {
							return String.valueOf(dataset.getValue(row, column).intValue());
						}
						return null;
					}

				});
				newPlot.getRenderer().setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER, TextAnchor.CENTER, 3 * Math.PI / 2));
				newPlot.getRenderer().setBaseItemLabelsVisible(true);
				Color transparent = new Color(255, 255, 255, 0);
				newPlot.setBackgroundPaint(transparent);
				newPlot.setDomainGridlinePaint(transparent);
				newPlot.setRangeGridlinePaint(transparent);
				newPlot.setOutlineVisible(false);
				chart.getPlot().setBackgroundAlpha(0.0f);
				break;
			default:
				throw new UnsupportedOperationException("FORGOT TO ADD IMPLEMENTATION HERE :)");
		}
		return chart;
	}

	private void setPlotColors(Plot plot) {
		if (plot == null) {
			return;
		}
		if (colors == null) {
			colors = createColors(histogramOrder.size());
		}
		if (selected == null) {
			switch (type) {
				case PIE:
					histogramOrder.stream().forEach((vg) -> {
						String text = vg.getRepresentation();
						((PiePlot) plot).setSectionPaint(text, histogramOrder.indexOf(vg) < colors.length ? colors[histogramOrder.indexOf(vg)] : Color.GRAY);
					});
					break;
				case HISTOGRAM:
					((CategoryPlot) plot).setRenderer(new StackedBarRenderer() {
						@Override
						public Paint getItemPaint(int row, int column) {
							if (row == 0) {
								String thisVariantGroup = (String) getPlot().getDataset().getColumnKey(column);
								for (int i = 0; i < histogramOrder.size(); i++) {
									if (thisVariantGroup.equals(histogramOrder.get(i).getRepresentation())) {
										return i < colors.length ? colors[i] : Color.GRAY;
									}
								}
								return Color.GRAY;
							} else if (row == 1) {
								return Color.WHITE;
							} else {
								return Color.GRAY;
							}
						}
					});
					((BarRenderer) ((CategoryPlot) plot).getRenderer()).setGradientPaintTransformer(null);
					((BarRenderer) ((CategoryPlot) plot).getRenderer()).setDrawBarOutline(true);
					((BarRenderer) ((CategoryPlot) plot).getRenderer()).setBarPainter(new StandardBarPainter());
					((BarRenderer) ((CategoryPlot) plot).getRenderer()).setMaximumBarWidth(.2);
					((BarRenderer) ((CategoryPlot) plot).getRenderer()).setShadowVisible(false);
					break;
				default:
					throw new UnsupportedOperationException("FORGOT TO ADD IMPLEMENTATION HERE :)");
			}
		} else {
			switch (type) {
				case PIE:
					int i = 0;
					for (VariantGroup vg : histogramOrder) {
						for (int j = 0; j < getVariantGroups().size(); j++) {
							if (selected[j] && getVariantGroups().get(j).equals(vg)) {
								((PiePlot) plot).setSectionPaint(vg.getRepresentation(), i < colors.length ? colors[i] : Color.GRAY);
							}
						}
						i++;
					}
					break;
				case HISTOGRAM:
					((CategoryPlot) plot).setRenderer(new StackedBarRenderer() {
						@Override
						public Paint getItemPaint(int row, int column) {
							if (row == 0) {
								String thisVariantGroup = (String) getPlot().getDataset().getColumnKey(column);
								for (int i = 0; i < histogramOrder.size(); i++) {
									if (thisVariantGroup.equals(histogramOrder.get(i).getRepresentation())) {
										return i < colors.length ? colors[i] : Color.GRAY;
									}
								}
								return Color.GRAY;
							} else if (row == 1) {
								return Color.WHITE;
							} else {
								return Color.GRAY;
							}
						}
					});
					((BarRenderer) ((CategoryPlot) plot).getRenderer()).setGradientPaintTransformer(null);
					((BarRenderer) ((CategoryPlot) plot).getRenderer()).setDrawBarOutline(true);
					((BarRenderer) ((CategoryPlot) plot).getRenderer()).setBarPainter(new StandardBarPainter());
					((BarRenderer) ((CategoryPlot) plot).getRenderer()).setMaximumBarWidth(.2);
					((BarRenderer) ((CategoryPlot) plot).getRenderer()).setShadowVisible(false);
					break;
				default:
					throw new UnsupportedOperationException("FORGOT TO ADD IMPLEMENTATION HERE :)");
			}
		}
		switch (type) {
			case PIE:
				((PiePlot) plot).setSectionPaint(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OTHER"), Color.GRAY);
				break;
			case HISTOGRAM:
				if (((CategoryPlot) plot).getDataset().getRowKey(((CategoryPlot) plot).getDataset().getRowCount() - 1).equals(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OTHER"))) {
					((CategoryPlot) plot).getRenderer().setSeriesPaint(((CategoryPlot) plot).getDataset().getRowCount() - 1, Color.GRAY);
				}
				break;
			default:
				throw new UnsupportedOperationException("FORGOT TO ADD IMPLEMENTATION HERE :)");
		}
	}

	private void createPlots() throws SQLException {
		List<Point2D.Double> points = MapPoint.getPointsPositionsOfMap(getMap());
		plots = new HashMap<>(points.size());
		points.stream().forEach((point) -> plots.put(point, new ArrayList<>(getFilters().size())));
		getFilters().stream().forEach((filter) -> createPlots(filter, points));
	}

	private void createPlots(Filter filter, List<Point2D.Double> pointsIn) {
		List<Point2D.Double> points = new ArrayList<>(pointsIn);
		getPoints().stream().forEach((locality) -> {
			Plot plot = createPlot(filter, locality);
			try {
				MapPoint pm = MapPoint.getMapPoint(locality.getCode(), getMap());
				plots.get(pm.getPosition()).add(plot);
				points.remove(pm.getPosition());
			} catch (ClassNotFoundException | IOException | SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		});
		points.stream().forEach((point) -> plots.get(point).add(null));
	}

	private List<HashMap<String, Integer>> calculateData(HashMap<VariantGroup, Integer> plotData, int total) {
		List<HashMap<String, Integer>> data = new ArrayList<>(2);
		data.add(new HashMap<>());
		data.add(new HashMap<>());
		int oth = 0;
		while (!plotData.isEmpty()) {
			VariantGroup[] keySet = plotData.keySet().toArray(new VariantGroup[plotData.size()]);
			VariantGroup vg = keySet[0];
			int max = plotData.get(vg);
			for (int i = 1; i < keySet.length; i++) {
				if (max < plotData.get(keySet[i])) {
					vg = keySet[i];
					max = plotData.get(vg);
				}
			}
			String text = vg.getRepresentation();
			int pos = getVariantGroups().indexOf(vg);
			if (isDisplayed(vg) && selected == null || isDisplayed(vg) && pos != -1 && selected[pos]) {
				data.get(0).put(text, max);
				data.get(1).put(text, total - max);
			} else {
				oth += max;
			}
			plotData.remove(vg);
		}
		if (oth != 0) {
			String text = java.util.ResourceBundle.getBundle("texts/Bundle").getString("OTHER");
			data.get(0).put(text, oth);
			data.get(1).put(text, total - oth);
		}
		return data;
	}

	private Plot createPlot(Filter filter, GeographicPoint locality) {
		HashMap<VariantGroup, Integer> plotData = getFilteredData(locality).getVariantGroups(filter);
		if (plotData == null) {
			return null;
		}
		final int total = plotData.values().stream().reduce(0, Integer::sum);
		if (total == 0 && !plotData.isEmpty()) {
			/// This should not be possible, since we clear the empty stuff in the filtered data...
			throw new UnsupportedOperationException("Will not be able to divide by zero later.");
		}
		List<HashMap<String, Integer>> processedData = calculateData(plotData, total);
		Plot plot = null;
		switch (type) {
			case PIE:
				DefaultPieDataset pieDataset = new DefaultPieDataset();
				processedData.get(0).keySet().stream().forEach((key) -> pieDataset.setValue(key, processedData.get(0).get(key)));
				PiePlot piePlot = new PiePlot(pieDataset);
				piePlot.setBackgroundAlpha(0.0f);
				piePlot.setCircular(true);
				piePlot.setShadowXOffset(0.0);
				piePlot.setShadowYOffset(0.0);
				piePlot.setLabelGenerator(null);
				piePlot.setOutlinePaint(null);
				plot = piePlot;
				break;
			case HISTOGRAM:
				DefaultCategoryDataset barDataset = new DefaultCategoryDataset();
				processedData.get(0).keySet().stream().forEach((key) -> {
					double value = processedData.get(0).get(key);
					barDataset.addValue(value, "", key);
					barDataset.addValue((total - value), "_", key);
				});
				CategoryPlot categoryPlot = new CategoryPlot(barDataset, new CategoryAxis(), new NumberAxis(), new StackedBarRenderer());
				Color transparent = new Color(255, 255, 255, 0);
				categoryPlot.setBackgroundPaint(transparent);
				categoryPlot.setDomainGridlinePaint(transparent);
				categoryPlot.setRangeGridlinePaint(transparent);
				categoryPlot.setOutlineVisible(false);
				((BarRenderer) categoryPlot.getRenderer()).setGradientPaintTransformer(null);
				((BarRenderer) categoryPlot.getRenderer()).setDrawBarOutline(true);
				((BarRenderer) categoryPlot.getRenderer()).setBarPainter(new StandardBarPainter());
				((BarRenderer) categoryPlot.getRenderer()).setMaximumBarWidth(.2);
				((BarRenderer) categoryPlot.getRenderer()).setShadowVisible(false);
				categoryPlot.getDomainAxis().setVisible(false);
				categoryPlot.getRangeAxis().setVisible(false);
				categoryPlot.setBackgroundAlpha(0.0f);
				plot = categoryPlot;
				break;
			default:
				throw new UnsupportedOperationException("FORGOT TO ADD IMPLEMENTATION HERE :)");
		}
		return plot;
	}

	private boolean isDisplayed(VariantGroup vg) {
		return mostProductive.keySet().stream().anyMatch((key) -> key.equals(vg));
	}

	private void setPlotsColors() {
		plots.values().stream().forEach((List<Plot> list) -> list.stream().forEach((plot) -> setPlotColors(plot)));
	}

	private void calcSize() throws SQLException {
		if (plotSize == 0) {
			List<Point2D.Double> points = MapPoint.getPointsPositionsOfMap(getMap());
			double minDistBetweenPoints = Double.MAX_VALUE;
			double dist;
			for (int i = 0; i < points.size() - 1; i++) {
				for (int j = i + 1; j < points.size(); j++) {
					dist = points.get(i).distance(points.get(j));
					if (dist < minDistBetweenPoints) {
						minDistBetweenPoints = dist;
					}
				}
			}
			minDistBetweenPoints -= 5;
			plotSize = (int) Math.round(minDistBetweenPoints);
		}
	}

	private void process() throws SQLException {
		mapProductivity();
		createPlots();
		calcSize();
		setPlotsColors();
	}

	private void mapProductivity() throws SQLException {
		mostProductive = new HashMap<>(getVariantGroups().size());
		getPoints().forEach((point) -> {
			FilteredData fd = getFilteredData(point);
			HashMap<VariantGroup, Integer> variantGroups = fd.getVariantGroups(null);
			variantGroups.keySet().forEach((vg) -> mostProductive.merge(vg, variantGroups.get(vg), Integer::sum));
		});
		histogram = new HashMap<>(mostProductive);
		if (noVars > 0) {
			while (mostProductive.size() > noVars) {
				VariantGroup lesser = null;
				int qtde = Integer.MAX_VALUE;
				for (java.util.Map.Entry<VariantGroup, Integer> entry : mostProductive.entrySet()) {
					if (entry.getValue() < qtde) {
						qtde = entry.getValue();
						lesser = entry.getKey();
					}
				}
				histogram.remove(lesser);
				if (histogram.containsKey(null)) {
					histogram.put(null, histogram.get(null) + mostProductive.remove(lesser));
				} else {
					histogram.put(null, mostProductive.remove(lesser));
				}
			}
		}
		histogramOrder = new ArrayList<>(histogram.size());
		HashMap<VariantGroup, Integer> temp = new HashMap<>(histogram);
		while (!temp.isEmpty()) {
			int max = 0;
			VariantGroup maxVG = null;
			for (VariantGroup v : temp.keySet()) {
				if (temp.get(v) > max) {
					maxVG = v;
					max = temp.get(v);
				}
			}
			if (max == 0) {
				break;
			}
			histogramOrder.add(maxVG);
			temp.remove(maxVG);
		}
		histogramOrder.remove(null);
	}

	public MapSaver getMapSaver() {
		MapSaver ms = super.createMapSaver();
		ms.setQttsVariants(noVars);
		ms.setSelected(selected);
		ms.setColors(ColorPattern.toPatterns(colors));
		return ms;
	}

	public void setType(GraphType type) throws SQLException {
		this.type = type;
		createPlots();
		setPlotsColors();
		calcSize();
		resultPanel.updatePlotsOnMap();
	}

	public GraphType getGraphType() {
		return type;
	}

	@SuppressWarnings("PublicInnerClass")
	public enum GraphType {
		PIE,
		HISTOGRAM
	}
}
