/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors.dataprocessor;

import java.io.InvalidClassException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.logging.Level;
import sgvclin.data.AgeGroup;
import sgvclin.data.AgeRange;
import sgvclin.data.Data;
import sgvclin.data.Datasheet;
import sgvclin.data.MultivaluedData;
import sgvclin.data.SheetField;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan Oliveira
 */
public class Filter {

	private static final String CONCATENATION_STRING = " - ";
	private static final String LIST_INI = "{";
	private static final String LIST_SEP = ", ";
	private static final String LIST_END = "}";

	private final HashMap<SheetField, List<Object>> details;
	private final List<SheetField> ordenation;

	public Filter() {
		details = new HashMap<>(2);
		ordenation = new ArrayList<>(2);
	}

	public Filter(Filter f) {
		details = f.getDetails();
		ordenation = f.getOrdenation();
	}

	private HashMap<SheetField, List<Object>> getDetails() {
		HashMap<SheetField, List<Object>> map = new HashMap<>(details.size());
		details.keySet().stream().forEach((sf) -> map.put(sf, new ArrayList<>(details.get(sf))));
		return map;
	}

	public List<SheetField> getOrdenation() {
		return new ArrayList<>(ordenation);
	}

	public boolean isEmpty() {
		return ordenation.isEmpty();
	}

	public String getRepresentation(SheetField sf) {
		return print(details.get(sf));
	}

	@Override
	public String toString() {
		String ret = "";
		ret = ordenation.stream().map((sf) -> details.get(sf)).filter((value) -> !(value == null || value.isEmpty()))
				.map((value) -> print(value) + CONCATENATION_STRING).reduce(ret, String::concat);
		return ret.isEmpty() ? ret : ret.substring(0, ret.length() - CONCATENATION_STRING.length());
	}

	private String print(List<Object> list) {
		String ret = list.size() == 1 ? "" : LIST_INI;
		if ((list.get(0) instanceof String) || (list.get(0) instanceof MultivaluedData) || (list.get(0) instanceof AgeRange)) {
			ret = list.stream().map((obj) -> obj.toString() + LIST_SEP).reduce(ret, String::concat);
		} else if (list.get(0) instanceof Date) {
			ret = list.stream().map((obj) -> new SimpleDateFormat(Data.DATE_PATTERN, Locale.getDefault()).format((Date) obj) + LIST_SEP).reduce(ret, String::concat);
		} else if (list.get(0) instanceof Integer) {
			ret = list.stream().map((obj) -> Integer.toString((int) obj) + LIST_SEP).reduce(ret, String::concat);
		} else if (list.get(0) instanceof Double) {
			ret = list.stream().map((obj) -> Double.toString((double) obj) + LIST_SEP).reduce(ret, String::concat);
		}
		ret = ret.substring(0, ret.length() - LIST_SEP.length());
		if (list.size() > 1) {
			ret += LIST_END;
		}
		return ret;
	}

	public void addField(SheetField sf, String str, AgeGroup group) {
		Object value = null;
		switch (sf.getType()) {
			case TEXT:
				value = str;
				break;
			case DATE:
				try {
					value = new SimpleDateFormat(Data.DATE_PATTERN, Locale.getDefault()).parse(str);
				} catch (ParseException ex) {
					LoggerManager.getInstance().log(Level.INFO, str, ex);
				}
				break;
			case INTEGER:
				if (group == null) {
					value = Integer.parseInt(str);
				} else {
					try {
						boolean ok = false;
						for (AgeRange rs : AgeRange.getListOfAgeRanges(group)) {
							if (rs.getName().equals(str)) {
								value = rs;
								ok = true;
								break;
							}
						}
						if (!ok) {
							value = new AgeRange();
							((AgeRange) value).setName(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OTHER"));
							((AgeRange) value).setBegin(-1);
							((AgeRange) value).setEnd(-1);
						}
					} catch (SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
					}
				}
				break;
			case DOUBLE:
				if (group == null) {
					value = Double.parseDouble(str);
				} else {
					try {
						boolean ok = false;
						for (AgeRange rs : AgeRange.getListOfAgeRanges(group)) {
							if (rs.getName().equals(str)) {
								value = rs;
								ok = true;
								break;
							}
						}
						if (!ok) {
							value = new AgeRange();
							((AgeRange) value).setName(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OTHER"));
							((AgeRange) value).setBegin(-1);
							((AgeRange) value).setEnd(-1);
						}
					} catch (SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
					}
				}
				break;
			case MULTIVALUED_UNIQUE:
			case MULTIVALUED_MULTIPLE:
				try {
					for (MultivaluedData md : MultivaluedData.getListOfMultivaluedData(sf)) {
						if (md.getText().equals(str)) {
							value = md;
							break;
						}
					}
				} catch (SQLException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				}
				break;
			default:
				throw new AssertionError(sf.getType().name());
		}
		if (value == null) {
			return;
		}
		if (details.containsKey(sf)) {
			details.get(sf).add(value);
		} else {
			details.put(sf, new ArrayList<>(Arrays.asList(value)));
			ordenation.add(sf);
		}
	}

	public boolean match(Datasheet ds) {
		if (details.isEmpty()) {
			return true; // An empty filter matches everything...
		}
		boolean matches = true;
		try {
			for (SheetField sf : ordenation) {
				Data property = ds.getProperty(sf.getName());
				List<Object> values = details.get(sf);
				switch (sf.getType()) {
					case TEXT: {
						String value = property.getText();
						matches = values.contains(value);
						break;
					}
					case DATE: {
						Date value = property.getDate();
						matches = values.contains(value);
						break;
					}
					case INTEGER: {
						int value = property.getInt();
						if (values.get(0).getClass().equals(AgeRange.class)) {
							for (Object obj : values) {
								AgeRange range = (AgeRange) obj;
								matches = range.getBegin() <= value && value <= range.getEnd();
								if (matches) {
									break;
								}
							}
						} else {
							matches = values.contains(value);
						}
						break;
					}
					case DOUBLE: {
						double value = property.getDouble();
						if (values.get(0).getClass().equals(AgeRange.class)) {
							for (Object obj : values) {
								AgeRange range = (AgeRange) obj;
								matches = range.getBegin() <= value && value <= range.getEnd();
								if (matches) {
									break;
								}
							}
						} else {
							matches = values.contains(value);
						}
						break;
					}
					case MULTIVALUED_UNIQUE: {
						MultivaluedData value = property.getSelected();
						matches = values.contains(value);
						break;
					}
					case MULTIVALUED_MULTIPLE: {
						List<MultivaluedData> selectedValues = property.getSelectedValues();
						for (MultivaluedData md : selectedValues) {
							matches = values.contains(md);
							if (matches) {
								break;
							}
						}
						break;
					}
					default:
						throw new AssertionError(sf.getType().name());
				}
				if (!matches) {
					break;
				}
			}
		} catch (AssertionError | InvalidClassException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			return false;
		}
		return matches;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 79 * hash + Objects.hashCode(this.details);
		hash = 79 * hash + Objects.hashCode(this.ordenation);
		return hash;
	}

	@Override
	@SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Filter other = (Filter) obj;
		if (!Objects.equals(this.details, other.details)) {
			return false;
		}
		return Objects.equals(this.ordenation, other.ordenation);
	}

	public List<Object> getDetails(SheetField sf) {
		return new ArrayList<>(details.get(sf));
	}
}
