/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors.dataprocessor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import sgvclin.data.Variant;

/**
 *
 * @author Nathan Oliveira
 */
public class VariantGroup {

	private static final String VARIANT_SEPARATOR = "/ ";

	public static List<VariantGroup> create(List<HashMap<Variant, Integer>> phonemes, int unused_fuckingTypeErasure) { // to not clash with the other method...
		HashMap<Variant, Integer> begin = new HashMap<>(phonemes.get(0));
		HashMap<Variant, Integer> end = new HashMap<>(phonemes.get(1));
		List<VariantGroup> list = new ArrayList<>(begin.size());
		while (!begin.isEmpty()) {
			List<Variant> lEq = new ArrayList<>(5);
			lEq.add(begin.keySet().iterator().next());
			String phoneme = lEq.get(0).getAnswer().substring(begin.get(lEq.get(0)), end.get(lEq.get(0)));
			begin.keySet().stream().forEach((var) -> {
				begin.keySet().stream().filter((v) -> !v.equals(var)).forEach((v) -> {
					if (phoneme.equals(v.getAnswer().substring(begin.get(v), end.get(v)))) {
						if (!lEq.contains(v)) {
							lEq.add(v);
						}
					}
				});
			});
			lEq.stream().forEach((var) -> {
				begin.remove(var);
				end.remove(var);
			});
			list.add(new VariantGroup(lEq, phoneme));
		}
		list.sort((VariantGroup o1, VariantGroup o2) -> o1.getRepresentation().compareTo(o2.getRepresentation()));
		return list;
	}

	public static List<VariantGroup> create(List<List<Variant>> equivalences) {
		List<VariantGroup> list = new ArrayList<>(equivalences.size());
		equivalences.stream().forEach((lEq) -> list.add(new VariantGroup(new ArrayList<>(lEq), getRepEq(lEq))));
		list.sort((VariantGroup o1, VariantGroup o2) -> o1.getRepresentation().compareTo(o2.getRepresentation()));
		return list;
	}

	private static String getRepEq(List<Variant> lEq) {
		lEq.sort((Variant o1, Variant o2) -> o1.getAnswer().compareTo(o2.getAnswer()));
		String str = lEq.stream().map((var) -> var.getAnswer() + VARIANT_SEPARATOR).reduce("", String::concat);
		return str.substring(0, str.length() - VARIANT_SEPARATOR.length());
	}

	private final List<Variant> variants;
	private final String representation;

	public VariantGroup(List<Variant> vars, String repr) {
		vars.sort((Variant o1, Variant o2) -> o1.getAnswer().compareTo(o2.getAnswer()));
		variants = vars;
		representation = repr;
	}

	public VariantGroup(VariantGroup vg) {
		variants = new ArrayList<>(vg.getVariants());
		representation = vg.getRepresentation();
	}

	public String getRepresentation() {
		return representation;
	}

	public List<Variant> getVariants() {
		return Collections.unmodifiableList(variants);
	}

	public boolean contains(Variant var) {
		return variants.contains(var);
	}

	@Override
	public String toString() {
		return representation;
	}
}
