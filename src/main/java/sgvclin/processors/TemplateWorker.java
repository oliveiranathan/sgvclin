/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors;

import boofcv.alg.filter.binary.BinaryImageOps;
import boofcv.alg.filter.binary.Contour;
import boofcv.alg.filter.binary.ThresholdImageOps;
import boofcv.alg.misc.ImageStatistics;
import boofcv.alg.shapes.ShapeFittingOps;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.struct.ConnectRule;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.GrayU8;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;
import javax.swing.SwingUtilities;
import sgvclin.UI.StatusBarImpl;
import sgvclin.UI.TemplateEditorPanel;
import sgvclin.data.Map;

/**
 * @author Nathan Oliveira
 */
public class TemplateWorker {

	public static Area getArea(boolean[][] template) {
		return splitIntoPolygonsAndConvertToArea(template, template.length, template[0].length);
	}

	private static Area splitIntoPolygonsAndConvertToArea(boolean[][] template, int xM, int yM) {
		BufferedImage temp = createTemplateImage(Color.BLACK, template, xM, yM);
		GrayF32 input = ConvertBufferedImage.convertFromSingle(temp, null, GrayF32.class);
		GrayU8 binary = new GrayU8(input.width, input.height);
		double mean = ImageStatistics.mean(input);
		ThresholdImageOps.threshold(input, binary, (float) mean, true);
		List<Contour> contours = BinaryImageOps.contour(binary, ConnectRule.FOUR, null);
		Area area = new Area();
		contours.stream().map((c) -> ShapeFittingOps.fitPolygon(c.external, true, 0.0005, 0.0001, 10000)).filter((vertexes) -> !(vertexes.size() < 3)).map((vertexes) -> {
			int npoints = vertexes.size();
			int[] xpoints = new int[npoints];
			int[] ypoints = new int[npoints];
			for (int i = 0; i < vertexes.size(); i++) {
				xpoints[i] = vertexes.get(i).x;
				ypoints[i] = vertexes.get(i).y;
			}
			return new Area(new Polygon(xpoints, ypoints, npoints));
		}).forEach((a) -> {
			area.add(a);
		});
		return area;
	}

	@SuppressWarnings(value = "AssignmentToForLoopParameter")
	private static BufferedImage createTemplateImage(Color color, boolean[][] template, int xM, int yM) {
		BufferedImage img = new BufferedImage(xM, yM, BufferedImage.TYPE_INT_ARGB_PRE);
		Graphics g = img.getGraphics();
		if (color == Color.BLACK) {
			g.setColor(new Color(255, 255, 255, 255));
		} else {
			g.setColor(new Color(255, 255, 255, 0));
		}
		g.fillRect(0, 0, xM, yM);
		g.setColor(color);
		for (int x = 0; x < xM; x++) {
			for (int ys = 0; ys < yM; ys++) {
				if (!template[x][ys]) {
					continue;
				}
				int ye = ys;
				for (; ye < yM; ye++) {
					if (!template[x][ye]) {
						ye--;
						break;
					}
				}
				if (ye == yM) {
					ye--;
				}
				g.drawLine(x, ys, x, ye);
				ys = ye;
			}
		}
		return img;
	}

	private static boolean[][] binarizeImage(final BufferedImage img) {
		int w = img.getWidth();
		int h = img.getHeight();
		boolean[][] res = new boolean[w][h];
		int limiar = Color.BLACK.getRGB();
		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				if (img.getRGB(x, y) == limiar) {
					res[x][y] = true;
				}
			}
		}
		return res;
	}

	private final Map map;
	private final TemplateEditorPanel tcp;
	private boolean[][] template;
	private boolean[][] old;
	private boolean[][] borders;
	private int xM = 0, yM = 0;
	private final Color FGColor = new Color(255, 0, 0, 150);
	private Mode mode = Mode.NONE;

	public TemplateWorker(Map map, TemplateEditorPanel tcp) {
		this.tcp = tcp;
		this.map = map;
		xM = this.map.getMapImage().getWidth();
		yM = this.map.getMapImage().getHeight();
		if (map.getTemplate() == null) {
			template = new boolean[xM][yM];
			map.setTemplate(splitIntoPolygonsAndConvertToArea(template, xM, yM));
		} else {
			BufferedImage img = new BufferedImage(xM, yM, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = img.createGraphics();
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, xM, yM);
			g.setColor(Color.BLACK);
			g.fill(map.getTemplate());
			g.dispose();
			template = binarizeImage(img);
			SwingUtilities.invokeLater(this::updateScreen);
		}
	}

	public void updateImg() {
		updateScreen();
	}

	public void calcBorders() {
		template = laplaceBorderDetection(map.getMapImage());
		clear(template);
		borders = new boolean[template.length][template[0].length];
		for (int i = 0; i < template.length; i++) {
			System.arraycopy(template[i], 0, borders[i], 0, template[i].length);
		}
		updateScreen();
	}

	@SuppressWarnings("AssignmentToForLoopParameter")
	private void clear(boolean[][] img) {
		SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().showProgress(img.length * 2));
		for (int size = 30; size <= 40; size += 10) {
			for (int x = 0; x < img.length - size; x++) {
				for (int y = 0; y < img[x].length - size; y++) {
					boolean disconnected = true;
					for (int i = x; i < size + x; i++) {
						if (img[i][y] || img[i][y + size]) {
							disconnected = false;
							break;
						}
					}
					if (!disconnected) {
						continue;
					}
					for (int j = y; j < size + y; j++) {
						if (img[x][j] || img[x + size][j]) {
							disconnected = false;
							y = j;
							break;
						}
					}
					if (disconnected) {
						for (int i = x; i < size + x; i++) {
							for (int j = y; j < size + y; j++) {
								img[i][j] = false;
							}
						}
					}
				}
				final int xf = x + (size < 40 ? 0 : img.length);
				SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().setProgress(xf));
			}
		}
		SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().showIndProgress());
	}

	private boolean[][] laplaceBorderDetection(final BufferedImage img) {
		int w = img.getWidth(), h = img.getHeight();
		boolean[][] res = new boolean[w][h];
		BufferedImage grayLvlImg = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_GRAY);
		grayLvlImg.getGraphics().drawImage(img, 0, 0, null);
		short[][] img_gray_short = new short[w][h];
		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				img_gray_short[x][y] = (short) new Color(grayLvlImg.getRGB(x, y)).getRed();
			}
		}
		short mask[][] = {{1, 1, 1}, {1, -8, 1}, {1, 1, 1}};
		int wM = mask.length;
		int wM_2 = wM / 2;
		int hM = mask[0].length;
		int hM_2 = hM / 2;
		int limit = 0;
		SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().showProgress(w));
		for (int x = wM_2; x < w - wM_2; x++) {
			for (int y = hM_2; y < h - hM_2; y++) {
				int sum = 0;
				for (int i = -wM_2; i <= wM_2; i++) {
					for (int j = -hM_2; j <= hM_2; j++) {
						sum += img_gray_short[x + i][y + j] * mask[i + wM_2][j + hM_2];
					}
				}
				sum /= 9;
				if (sum < limit) {
					res[x][y] = true;
				}
			}
			final int xf = x;
			SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().setProgress(xf));
		}
		SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().showIndProgress());
		return res;
	}

	public Mode getMode() {
		return mode;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	public void saveTemplate() throws SQLException, IOException {
		if (borders != null) {
			for (int x = 0; x < template.length; x++) {
				for (int y = 0; y < template[x].length; y++) {
					if (borders[x][y] && template[x][y]) {
						template[x][y] = false;
					}
				}
			}
		}
		map.setTemplate(splitIntoPolygonsAndConvertToArea(template, xM, yM));
		map.setInvalid(true);
		updateScreen();
		Map.updateMapTemplate(map);
	}

	public void invertTemplate() {
		old = null;
		SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().showProgress(xM));
		for (int x = 0; x < xM; x++) {
			for (int y = 0; y < yM; y++) {
				template[x][y] = !template[x][y];
				borders[x][y] = !borders[x][y];
			}
			final int xf = x;
			SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().setProgress(xf));
		}
		SwingUtilities.invokeLater(() -> StatusBarImpl.getInstance().showIndProgress());
		java.awt.EventQueue.invokeLater(this::updateScreen);
	}

	@SuppressWarnings("AssignmentToForLoopParameter")
	private void updateScreen() {
		tcp.setTemplate(createTemplateImage(FGColor, template, xM, yM));
	}

	public void useTool(Point[] points) {
		old = new boolean[xM][yM];
		for (int x = 0; x < template.length; x++) {
			System.arraycopy(template[x], 0, old[x], 0, template[x].length);
		}
		switch (mode) {
			case BUCKET:
				bucketTool(points);
				break;
			case ERASER:
				eraserTool(points);
				break;
			case LINE:
				lineTool(points, true);
				break;
			case PEN:
				penTool(points);
				break;
			case MOVE:
			case NONE:
			default:
				LoggerManager.getInstance().log(Level.INFO, "This should not happen. Some tool should be selected for the operation.", null);
				return;
		}
		java.awt.EventQueue.invokeLater(this::updateScreen);
	}

	private void penTool(Point[] points) {
		Point[] ps = new Point[2];
		for (int i = 0; i < points.length - 1; i++) {
			ps[0] = points[i];
			ps[1] = points[i + 1];
			lineTool(ps, true);
		}
	}

	private void bucketTool(Point[] points) {
		if (points.length != 1) {
			return;
		}
		Queue<Point> east = new ArrayDeque<>(500);
		Queue<Point> west = new ArrayDeque<>(500);
		Queue<Point> north = new ArrayDeque<>(500);
		Queue<Point> south = new ArrayDeque<>(500);
		List<Queue<Point>> list = new ArrayList<>(500);
		list.add(east);
		list.add(west);
		list.add(north);
		list.add(south);
		east.add(points[0]);
		while (!east.isEmpty() || !west.isEmpty() || !south.isEmpty() || !north.isEmpty()) {
			list.stream().forEach((q) -> {
				while (!q.isEmpty()) {
					Point p = q.poll();
					int x = p.x, y = p.y;
					if (template[x][y]) {
						continue;
					}
					template[x][y] = true;
					if (x + 1 < template.length && !template[x + 1][y]) {
						east.add(new Point(x + 1, y));
					}
					if (x - 1 >= 0 && !template[x - 1][y]) {
						west.add(new Point(x - 1, y));
					}
					if (y + 1 < template[0].length && !template[x][y + 1]) {
						south.add(new Point(x, y + 1));
					}
					if (y - 1 >= 0 && !template[x][y - 1]) {
						north.add(new Point(x, y - 1));
					}
				}
			});
		}
	}

	public void undo() {
		if (old == null) {
			return;
		}
		for (int x = 0; x < old.length; x++) {
			System.arraycopy(old[x], 0, template[x], 0, old[x].length);
		}
		java.awt.EventQueue.invokeLater(this::updateScreen);
	}

	private void lineTool(Point[] points, boolean set) {
		if (points.length != 2) {
			return;
		}
		int xA = points[0].x, yA = points[0].y, xB = points[1].x, yB = points[1].y;
		if (xA == xB) {
			if (yA > yB) {
				int tmp = yA;
				yA = yB;
				yB = tmp;
			}
			for (int y = yA; y <= yB; y++) {
				template[xA][y] = set;
			}
			return;
		}
		if (yA == yB) {
			if (xA > xB) {
				int tmp = xA;
				xA = xB;
				xB = tmp;
			}
			for (int x = xA; x <= xB; x++) {
				template[x][yA] = set;
			}
			return;
		}
		int dx = Math.abs(xB - xA), sx = xA < xB ? 1 : -1;
		int dy = Math.abs(yB - yA), sy = yA < yB ? 1 : -1;
		int err = (dx > dy ? dx : -dy) / 2, e2;
		for (;;) {
			template[xA][yA] = set;
			if (xA == xB && yA == yB) {
				break;
			}
			e2 = err;
			if (e2 > -dx) {
				err -= dy;
				xA += sx;
			}
			if (e2 < dy) {
				err += dx;
				yA += sy;
			}
		}
	}

	private void eraserTool(Point[] points) {
		Point[] ps = new Point[2];
		for (int i = 0; i < points.length - 1; i++) {
			ps[0] = points[i];
			ps[1] = points[i + 1];
			lineTool(ps, false);
		}
	}

	@SuppressWarnings("PublicInnerClass")
	public enum Mode {

		PEN,
		LINE,
		BUCKET,
		ERASER,
		MOVE,
		NONE
	}
}
