/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors;

import java.lang.reflect.InvocationTargetException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import sgvclin.UI.StatusBarImpl;
import sgvclin.data.AgeGroup;
import sgvclin.data.AgeRange;
import sgvclin.data.Datasheet;
import sgvclin.data.DatasheetVariant;
import sgvclin.data.GeographicPoint;
import sgvclin.data.Map;
import sgvclin.data.MapPoint;
import sgvclin.data.MapPrintSaver;
import sgvclin.data.MapSaver;
import sgvclin.data.MultivaluedData;
import sgvclin.data.Question;
import sgvclin.data.QuestionCategory;
import sgvclin.data.Questionnaire;
import sgvclin.data.Sheet;
import sgvclin.data.SheetField;
import sgvclin.data.Variant;

/**
 *
 * @author Nathan Oliveira
 */
public abstract class DBController {

	private static final List<Class<?>> classeS = Arrays.asList(DatasheetVariant.class, Datasheet.class, MapPrintSaver.class, MapSaver.class, AgeRange.class, AgeGroup.class,
			MapPoint.class, GeographicPoint.class, Map.class, MultivaluedData.class, SheetField.class, Sheet.class, Variant.class, Question.class, QuestionCategory.class,
			Questionnaire.class);

	private static DBController instance = null;

	public static DBController getInstance() {
		if (instance == null) {
			return null;
		}
		return instance;
	}

	protected static void setInstance(DBController instance) {
		DBController.instance = instance;
	}

	public static String getMD5(String text) {
		try {
			MessageDigest ms = MessageDigest.getInstance("MD5");
			byte[] digest = ms.digest(text.getBytes());
			StringBuilder sb = new StringBuilder(32);
			for (int i = 0; i < digest.length; i++) {
				sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			return "pqp666666666666666666666666666666666";// Will never throw anyway...
		}
	}

	private Connection connection = null;

	protected void setConnection(Connection connection) {
		this.connection = connection;
	}

	private void checkVersionAndUpdate() {
		int version;
		try {
			version = getVersion();
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
			return;
		}
		boolean cont = true;
		while (cont) {//Apply incremental updates to database...
			switch (version) {
				case 0:
					try {
						createQuestionListTable();
						createVersionTable();
						version++;
						setDatabaseVersion(version);
					} catch (SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);//If this fails, the base will be in an undefined state, aka, I'll have to correct it manually...
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR UPDATING THE DATABASE"));
						return;
					}
					break;
				case 1:
					try {
						createColorPatternStuff();
						version++;
						setDatabaseVersion(version);
					} catch (SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);//If this fails, the base will be in an undefined state, aka, I'll have to correct it manually...
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR UPDATING THE DATABASE"));
						return;
					}
					break;
				case 2:
					try {
						createQuestionsTextStuff();
						version++;
						setDatabaseVersion(version);
					} catch (SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);//If this fails, the base will be in an undefined state, aka, I'll have to correct it manually...
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR UPDATING THE DATABASE"));
						return;
					}
					break;
				//If changed here, update the version number at database creation.
				default:
					cont = false;
					break;
			}
		}
	}

	private int getVersion() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		if (!tables.stream().anyMatch((table) -> table.equalsIgnoreCase("DatabaseVersion"))) {//Special case from when I did not think the base would have to be changed :/
			return 0;
		}
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM `DatabaseVersion`")) {
			rs.next();
			return rs.getInt("version");
		}
	}

	private void createQuestionListTable() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `QuestionList` ("
				+ " `mapSaver` int(11) NOT NULL,"
				+ " `question` int(11) NOT NULL,"
				+ " `index` int(11) NOT NULL,"
				+ " PRIMARY KEY (`mapSaver`, `question`),"
				+ " CONSTRAINT `QuestionList_ibfk_1` FOREIGN KEY (`mapSaver`) REFERENCES `MapSaver` (`idMapSaver`) ON UPDATE CASCADE ON DELETE CASCADE,"
				+ " CONSTRAINT `QuestionList_ibfk_2` FOREIGN KEY (`question`) REFERENCES `Question` (`idQuestion`) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
	}

	private void createColorPatternStuff() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("ALTER TABLE `Color` ADD `bg` int(11) NOT NULL DEFAULT 0");
		DBController.getInstance().getStatement().executeUpdate("ALTER TABLE `Color` ADD `pattern` int(11) NOT NULL DEFAULT 0");
		DBController.getInstance().getStatement().executeUpdate("ALTER TABLE `Color` ADD `size` int(11) NOT NULL DEFAULT 0");
	}

	private void createQuestionsTextStuff() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("ALTER TABLE `MapPrintSaver` ADD `queX` int(11) DEFAULT 0");
		DBController.getInstance().getStatement().executeUpdate("ALTER TABLE `MapPrintSaver` ADD `queY` int(11) DEFAULT 0");
		DBController.getInstance().getStatement().executeUpdate("ALTER TABLE `MapPrintSaver` ADD `queW` int(11) DEFAULT 0");
		DBController.getInstance().getStatement().executeUpdate("ALTER TABLE `MapPrintSaver` ADD `queH` int(11) DEFAULT 0");
	}

	private void createVersionTable() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `DatabaseVersion` (`version` int(11) NOT NULL)");
	}

	private void setDatabaseVersion(int version) throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("DELETE FROM `DatabaseVersion`");
		DBController.getInstance().getStatement().executeUpdate("INSERT INTO `DatabaseVersion`(`version`) VALUES (" + version + ")");
	}

	public void closeConnection() {
		try {
			connection.close();
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
		instance = null;
	}

	public Connection getConnection() {
		return connection;
	}

	public Statement getStatement() throws SQLException {
		return getConnection().createStatement();
	}

	public ResultSet getResultSet(String table, int key) throws SQLException {
		return connection.createStatement().executeQuery("SELECT * FROM `" + table + "` WHERE id" + table + " = " + key);
	}

	public int getNewPrimaryKeyFor(String table) throws SQLException {
		return maxValueOfColumn(table, "id" + table) + 1;
	}

	public int maxValueOfColumn(String table, String column) throws SQLException {
		int k;
		try (ResultSet rs = connection.createStatement().executeQuery("SELECT MAX(`" + column + "`) AS max FROM `" + table + "`")) {
			rs.next();
			k = rs.getInt("max");
		}
		return k;
	}

	public void resetDatabase() throws SQLException {
		createSkeleton();
		connection.createStatement().executeUpdate("INSERT INTO `QuestionCategory`(`idQuestionCategory`, `description`, `description_hash`) "
				+ "VALUES(1001,'" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("LSQ")
				+ "', '" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("LSQ") + "'),"
				+ "(1002,'" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("PPQ")
				+ "', '" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("PPQ") + "'),"
				+ "(1003,'" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("MSQ")
				+ "', '" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("MSQ") + "'),"
				+ "(1004,'" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("OTHER")
				+ "', '" + java.util.ResourceBundle.getBundle("texts/Bundle").getString("OTHER") + "');");
		createVersionTable();
		setDatabaseVersion(3);
	}

	public void createSkeleton() throws SQLException {
		deleteDB();
		createDB();
	}

	private void deleteDB() throws SQLException { // This basically "implements" an interface. Did it this way to not have to change all the classes to add the interface :)
		classeS.stream().sequential().forEach((Class<?> c) -> {
			try {
				c.getMethod("deleteTables").invoke(null);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		});
	}

	public boolean hasSkeleton() throws SQLException {
		checkVersionAndUpdate();
		for (Class<?> c : classeS) {
			try {
				boolean b = (boolean) c.getMethod("verifyTables").invoke(null);
				if (!b) {
					LoggerManager.getInstance().log(Level.INFO, "!hasSkeleton();", null);
					return b;
				}
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		}
		return true;
	}

	private void createDB() throws SQLException {
		ArrayList<Class<?>> list = new ArrayList<>(classeS);
		Collections.reverse(list);
		list.stream().sequential().forEach((Class<?> c) -> {
			try {
				c.getMethod("createTables").invoke(null);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		});
	}
}
