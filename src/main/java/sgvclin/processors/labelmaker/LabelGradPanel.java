/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors.labelmaker;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import sgvclin.MainController;
import sgvclin.data.ColorPattern;

/**
 * @author Nathan Oliveira
 */
public class LabelGradPanel extends LabelPanel {

	private final int[] values;
	private String[] lll;

	public LabelGradPanel(String label, ColorPattern[] colors, int[] values, int width, LabelUser mapPanel) {
		super(new String[]{label}, colors, null, width, mapPanel, false);
		this.values = values;
	}

	@Override
	protected void paintLabel_() {
		BufferedImage newLegenda = new BufferedImage(getWidth(), 4000, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = newLegenda.createGraphics();
		g.setColor(Color.white);
		g.fillRect(0, 0, getWidth(), 4000);
		int sqLat = 19;
		int dx = 10;
		int dy = 10;
		int xS = dx + sqLat + dx;
		int y;
		g.setColor(Color.black);
		g.setFont(MainController.getInstance().getVariantFont(new Font("Arial", Font.BOLD, 20)));
		y = drawStr(g, java.util.ResourceBundle.getBundle("texts/Bundle").getString("LABEL"), xS, 25, getWidth()) + dx;
		g.setFont(MainController.getInstance().getVariantFont(new Font("Arial", Font.BOLD, 16)));
		y = drawStr(g, super.getLabels()[0], xS, y, getWidth() - dx - xS);
		int max = values[0];
		int[] percs = new int[getColors().length];
		for (int i = 0; i < percs.length; i++) {
			percs[i] = 100 * i / max;
		}
		lll = new String[getColors().length];
		for (int i = getColors().length - 1; i >= 0; i--) {
			g.setColor(new Color(getColors()[i].getForeground().getRGB() & 0x00ffffff | 0xc8000000));
			if (i != 0) {
				g.fillRect(dx, y - dy - 5, sqLat, sqLat);
			} else {
				g.setColor(Color.black);
				g.drawRect(dx, y - dy - 5, sqLat, sqLat);
			}
			g.setColor(Color.black);
			lll[i] = percs[i] + "% (" + i + ")";
			y = drawStr(g, lll[i], xS, y, getWidth() - dx - xS) + dy;
		}
		g.dispose();
		BufferedImage label = new BufferedImage(getWidth(), y, BufferedImage.TYPE_INT_RGB);
		label.getGraphics().drawImage(newLegenda, 0, 0, null);
		setImage(label);
	}

	@Override
	public String[] getLabels() {
		return Arrays.copyOf(lll, lll.length);
	}

}
