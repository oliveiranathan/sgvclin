/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors.labelmaker;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.colorchooser.ColorChooserComponentFactory;
import javax.swing.event.ChangeEvent;
import javax.swing.event.MouseInputAdapter;
import javax.swing.filechooser.FileNameExtensionFilter;
import sgvclin.MainController;
import sgvclin.UI.dataprocessor.ColorPicker;
import sgvclin.data.ColorPattern;
import sgvclin.processors.LoggerManager;

/**
 * @author Nathan Oliveira
 */
public class LabelPanel {

	public static BufferedImage createImage(List<String> text, int width) {
		BufferedImage newLegenda = new BufferedImage(width, 4000, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) newLegenda.getGraphics();
		g.setColor(new Color(255, 255, 255, 0));
		g.fillRect(0, 0, width, 4000);
		int dx = 10;
		int y = 25;
		g.setColor(Color.black);
		g.setFont(MainController.getInstance().getVariantFont(new Font("Arial", Font.BOLD, 20)));
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.78125f));
		for (String str : text) {
			y = drawStr(g, str, dx, y, width);
		}
		BufferedImage image = new BufferedImage(width, y, BufferedImage.TYPE_INT_ARGB);
		g = image.createGraphics();
		g.setColor(new Color(255, 255, 255, 0));
		g.fillRect(0, 0, width, y);
		g.drawImage(newLegenda, 0, 0, null);
		return image;
	}

	public static int drawStr(Graphics g, String str, int x, int y, int width) {
		FontMetrics fm = g.getFontMetrics();
		int lineHeight = fm.getHeight();
		int curX = x;
		int curY = y;
		String[] words = str.split(" ");
		for (String word : words) {
			int wordWidth = fm.stringWidth(word + " ");
			if (curX + wordWidth >= x + width) {
				curY += lineHeight;
				curX = x;
			}
			g.drawString(word, curX, curY);
			curX += wordWidth;
		}
		return curY + lineHeight;
	}

	private final String[] labels;
	private final ColorPattern[] colors;
	private final Rectangle[] rectangles;
	private final int width;
	private BufferedImage image;
	private JLabel label;
	private final JPopupMenu popUp;
	private int idxSelected = -1;
	private final LabelUser mapPanel;
	private String[] labelsPies = null;

	public LabelPanel(String[] labels, ColorPattern[] colors, String[] labelsPies, int width, LabelUser mapPanel, boolean displayPattern) {
		this.mapPanel = mapPanel;
		this.labels = labels;
		this.colors = colors;
		this.labelsPies = labelsPies;
		rectangles = new Rectangle[colors.length];
		this.width = width;
		popUp = new JPopupMenu();
		JMenuItem save = new JMenuItem(java.util.ResourceBundle.getBundle("texts/Bundle").getString("SAVE IMAGE..."));
		popUp.add(save);
		save.addActionListener((e) -> {
			JFileChooser fc = new JFileChooser();
			fc.setFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PNG IMAGE"), "PNG", "png"));
			if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				if (image == null) {
					paintLabel();
				}
				File f = fc.getSelectedFile();
				if (!f.getAbsolutePath().endsWith(".png")) {
					f = new File(f.getAbsolutePath() + ".png");
				}
				try {
					ImageIO.write(image, "PNG", f);
				} catch (IOException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				}
			}
		});
		if (mapPanel != null) {
			JMenuItem setColor = new JMenuItem(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CHANGE COLOR"));
			setColor.setToolTipText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CHANGES THE SELECTED COLOR"));
			popUp.add(setColor);
			setColor.addActionListener((e) -> {
				if (idxSelected != -1) {
					final JColorChooser jcc = new JColorChooser(colors[idxSelected].getForeground());
					ColorPicker cPicker = new ColorPicker(displayPattern, colors[idxSelected].getBackground(), colors[idxSelected].getPattern(), colors[idxSelected].getSize());
					jcc.setChooserPanels(new AbstractColorChooserPanel[]{cPicker, ColorChooserComponentFactory.getDefaultChooserPanels()[4]});
					jcc.setPreviewPanel(new JPanel());
					jcc.getSelectionModel().addChangeListener((ChangeEvent e1) -> {
						triggerUpdate(new ColorPattern(jcc.getColor(), cPicker.getBG(), cPicker.getPattern(), cPicker.getPattSize()));
					});
					final JDialog dialog = new JDialog();
					dialog.setLayout(new BorderLayout());
					dialog.add(jcc, BorderLayout.CENTER);
					JButton btn = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CLOSE"));
					dialog.add(btn, BorderLayout.SOUTH);
					dialog.getRootPane().setDefaultButton(btn);
					btn.addActionListener((e_) -> dialog.dispose());
					dialog.pack();
					dialog.setLocationRelativeTo(MainController.getInstance().getMainFrame());
					dialog.setVisible(true);
				}
			});
		}
	}

	public ColorPattern[] getColors() {
		return Arrays.copyOf(colors, colors.length);
	}

	public String[] getLabels() {
		return Arrays.copyOf(labels, labels.length);
	}

	private void triggerUpdate(ColorPattern pattern) {
		if (!pattern.equals(colors[idxSelected])) {
			colors[idxSelected] = new ColorPattern(pattern.getForeground(), pattern.getBackground(), pattern.getPattern(), pattern.getSize());
			paintLabel();
			if (mapPanel != null) {
				mapPanel.notifyColorChange(colors);
			}
		}
	}

	private void paintLabel() { // "Hides" the overridable notification on the constructor. It is on the lambdas, so...
		paintLabel_();
	}

	protected void paintLabel_() {
		BufferedImage newLegenda = new BufferedImage(width, 4000, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D) newLegenda.getGraphics();
		g.setColor(Color.white);
		g.fillRect(0, 0, width, 4000);
		int sqLat = 19;
		int dx = 10;
		int dy = 10;
		int xS = dx + sqLat + dx;
		int y;
		g.setColor(Color.black);
		g.setFont(MainController.getInstance().getVariantFont(new Font("Arial", Font.BOLD, 20)));
		y = drawStr(g, java.util.ResourceBundle.getBundle("texts/Bundle").getString("LABEL"), xS, 25, width) + dx;
		g.setFont(MainController.getInstance().getVariantFont(new Font("Arial", Font.BOLD, 16)));
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.78125f));
		for (int i = 0; i < labels.length; i++) {
			TexturePaint paint = new TexturePaint(colors[i].getTexture(), new Rectangle(sqLat / 2, sqLat / 2));
			g.setPaint(paint);
			rectangles[i] = new Rectangle(dx, y - dy - 5, sqLat, sqLat);
			g.fillRect(dx, y - dy - 5, sqLat, sqLat);
			g.setColor(Color.black);
			y = drawStr(g, labels[i], xS, y, width - dx - xS) + dy;
		}
		if (labelsPies != null) {
			y = drawLabelsPizzas(g, y, dx);
		}
		image = new BufferedImage(width, y, BufferedImage.TYPE_INT_RGB);
		image.getGraphics().drawImage(newLegenda, 0, 0, null);
	}

	@SuppressWarnings("AssignmentToMethodParameter")
	private int drawLabelsPizzas(Graphics g, int y, int dx) {
		boolean isEven = labelsPies.length % 2 == 0;
		for (int i = 0; i < labelsPies.length; i++) {
			int x;
			if (i % 2 == 0) {
				x = dx;
			} else {
				x = dx + 30;
			}
			if (!isEven && i + 1 == labelsPies.length) {
				x = dx + 15;
			}
			drawCircleWithText(g, x, y, "" + (i + 1));
			if (i % 2 == 1) {
				y += 25;
			}
		}
		if (!isEven) {
			y += 25;
		}
		y += 20;
		for (int i = 0; i < labelsPies.length; i++) {
			y = drawStr(g, (i + 1) + ": " + labelsPies[i], dx, y, width);
		}
		return y;
	}

	private void drawCircleWithText(Graphics g, int x, int y, String c) {
		g.setFont(new Font("Arial", Font.BOLD, 15));
		g.drawOval(x, y, 20, 20);
		g.drawString(c, x + 6, y + 15);
	}

	public JLabel getLabel() {
		if (image == null) {
			paintLabel();
		}
		label = new JLabel(new ImageIcon(image));
		Dimension d = new Dimension(image.getWidth(), image.getHeight());
		label.setMinimumSize(d);
		label.setMaximumSize(d);
		label.setPreferredSize(d);
		if (mapPanel != null) {
			label.addMouseListener(new MouseInputAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					if (e.isPopupTrigger()) {
						mostraPopUp(e);
					}
				}

				@Override
				public void mousePressed(MouseEvent e) {
					if (e.isPopupTrigger()) {
						mostraPopUp(e);
					}
				}

				private void mostraPopUp(MouseEvent e) {
					idxSelected = -1;
					for (int i = 0; i < rectangles.length; i++) {
						if (rectangles[i].contains(e.getPoint())) {
							idxSelected = i;
							break;
						}
					}
					popUp.show(label, e.getX(), e.getY());
				}
			});
		}
		return label;
	}

	public BufferedImage getImage() {
		if (image == null) {
			paintLabel();
		}
		return image;
	}

	protected int getWidth() {
		return width;
	}

	protected void setImage(BufferedImage image) {
		this.image = image;
	}
}
