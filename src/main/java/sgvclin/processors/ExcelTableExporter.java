/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import sgvclin.data.Datasheet;
import sgvclin.data.DatasheetVariant;
import sgvclin.data.GeographicPoint;
import sgvclin.data.Question;
import sgvclin.data.Sheet;
import sgvclin.data.SheetField;
import sgvclin.data.Variant;

/**
 * @author Nathan Oliveira
 */
public class ExcelTableExporter {

	public static void exportAnswers(Sheet f, Question q, File file) throws SQLException, Exception {
		List<Datasheet> datasheets = Datasheet.getListDatasheets(f);
		int max = 0;
		for (Datasheet fd : datasheets) {
			if (max < fd.getLocality().getCode()) {
				max = fd.getLocality().getCode();
			}
		}
		List<Variant> variants = Variant.getListOfVariants(q);
		int nVars = variants.size();
		String[] cols = new String[nVars + 4];
		cols[0] = java.util.ResourceBundle.getBundle("texts/Bundle").getString("POINT");
		cols[1] = java.util.ResourceBundle.getBundle("texts/Bundle").getString("REGION");
		cols[2] = java.util.ResourceBundle.getBundle("texts/Bundle").getString("STATE");
		cols[3] = java.util.ResourceBundle.getBundle("texts/Bundle").getString("LOCALITY");
		int i = 4;
		for (Variant v : variants) {
			cols[i] = v.getAnswer();
			++i;
		}
		ExcelTableExporter ete = new ExcelTableExporter(cols);
		int[] count;
		for (int point = 0; point <= max; point++) {
			count = new int[nVars];
			java.util.Arrays.fill(count, 0);
			for (Datasheet fd : datasheets) {
				if (fd.getLocality().getCode() == point) {
					DatasheetVariant dsv = DatasheetVariant.getDatasheetVariant(q, fd);
					if (dsv == null) {
						continue;
					}
					for (Variant v : dsv.getVariants()) {
						for (i = 0; i < nVars; i++) {
							if (variants.get(i).equals(v)) {
								count[i]++;
								break;
							}
						}
					}
				}
			}
			for (i = 0; i < count.length; i++) {
				if (count[i] != 0) {
					String[] rowData = new String[nVars + 4];
					GeographicPoint pg = GeographicPoint.getGeoPoint(point);
					rowData[0] = "" + pg.getCode();
					rowData[1] = calculaRegiao(pg.getState());
					rowData[2] = pg.getState();
					rowData[3] = pg.getName();
					for (i = 0; i < count.length; i++) {
						rowData[i + 4] = "" + count[i];
					}
					ete.addRow(rowData);
					break;
				}
			}
		}
		ete.sortRows();
		ete.saveTable(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ANSWERS"), file);
	}

	public static void exportDatasheets(Sheet f, Question q, File file) throws SQLException, Exception {
		List<Datasheet> datasheets = Datasheet.getListDatasheets(f);
		int max = 0;
		for (Datasheet fd : datasheets) {
			if (max < fd.getLocality().getCode()) {
				max = fd.getLocality().getCode();
			}
		}
		List<Variant> variants = Variant.getListOfVariants(q);
		int nVars = variants.size();
		List<SheetField> fields = SheetField.getListOfFields(f);
		String[] cols = new String[nVars + fields.size() + 4];
		cols[0] = java.util.ResourceBundle.getBundle("texts/Bundle").getString("POINT");
		cols[1] = java.util.ResourceBundle.getBundle("texts/Bundle").getString("REGION");
		cols[2] = java.util.ResourceBundle.getBundle("texts/Bundle").getString("STATE");
		cols[3] = java.util.ResourceBundle.getBundle("texts/Bundle").getString("LOCALITY");
		int i = 4;
		for (SheetField sf : fields) {
			cols[i] = sf.getName();
			i++;
		}
		for (Variant v : variants) {
			cols[i] = v.getAnswer();
			i++;
		}
		ExcelTableExporter ete = new ExcelTableExporter(cols);
		for (Datasheet ds : datasheets) {
			String[] rowData = new String[nVars + fields.size() + 4];
			GeographicPoint pg = ds.getLocality();
			rowData[0] = "" + pg.getCode();
			rowData[1] = calculaRegiao(pg.getState());
			rowData[2] = pg.getState();
			rowData[3] = pg.getName();
			i = 4;
			for (SheetField sf : fields) {
				rowData[i] = ds.getProperty(sf.getName()).toString();
				i++;
			}
			DatasheetVariant dsv = DatasheetVariant.getDatasheetVariant(q, ds);
			if (dsv == null) {
				continue;
			}
			for (i = fields.size() + 4; i < rowData.length; i++) {
				rowData[i] = "0";
			}
			for (Variant v : dsv.getVariants()) {
				for (i = 0; i < nVars; i++) {
					if (variants.get(i).equals(v)) {
						rowData[i + fields.size() + 4] = "" + 1;
					}
				}
			}
			ete.addRow(rowData);
		}
		ete.saveTable(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INFORMANT DATA"), file);
	}

	private static String calculaRegiao(String estado) {
		switch (estado) {
			case "AM":
			case "RR":
			case "AP":
			case "PA":
			case "TO":
			case "RO":
			case "AC":
				return "Norte";
			case "MA":
			case "PI":
			case "CE":
			case "RN":
			case "PE":
			case "PB":
			case "SE":
			case "AL":
			case "BA":
				return "Nordeste";
			case "MT":
			case "MS":
			case "GO":
			case "DF":
				return "Centro-Oeste";
			case "RJ":
			case "ES":
			case "SP":
			case "MG":
				return "Sudeste";
			case "PR":
			case "RS":
			case "SC":
				return "Sul";
			default:
				return "Error";
		}
	}

	private final String[] colsNames;
	private List<String[]> data;

	private ExcelTableExporter(String[] cols) {
		colsNames = new String[cols.length];
		System.arraycopy(cols, 0, colsNames, 0, colsNames.length);
		data = new ArrayList<>(10);
	}

	private void addRow(String[] row) {
		if (row.length != colsNames.length) {
			throw new IllegalArgumentException("The rows must have the same amount of columns.");
		}
		String[] rd = new String[colsNames.length];
		System.arraycopy(row, 0, rd, 0, colsNames.length);
		data.add(rd);
	}

	private void sortRows() {
		List<String[]> old = data;
		data = new ArrayList<>(old.size());
		while (!old.isEmpty()) {
			int min = Integer.MAX_VALUE;
			String[] row = null;
			for (String[] r : old) {
				int v = Integer.parseInt(r[0]);
				if (v < min) {
					min = v;
					row = r;
				}
			}
			old.remove(row);
			data.add(row);
		}
	}

	private boolean saveTable(String sheetName, File file) {
		try {
			WritableWorkbook workbook = Workbook.createWorkbook(file);
			WritableSheet sheet = workbook.createSheet(sheetName, 0);
			for (int col = 0; col < colsNames.length; col++) {
				sheet.addCell(new Label(col, 0, colsNames[col]));
			}
			for (int row = 0; row < data.size(); row++) {
				for (int col = 0; col < colsNames.length; col++) {
					sheet.addCell(new Label(col, row + 1, (data.get(row)[col]) != null ? data.get(row)[col] : ""));
				}
			}
			workbook.write();
			workbook.close();
			return true;
		} catch (IOException | WriteException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			return false;
		}
	}

}
