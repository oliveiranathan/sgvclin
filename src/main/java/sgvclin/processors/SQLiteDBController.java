/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors;

import java.io.File;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.sqlite.SQLiteConfig;

/**
 *
 * @author Nathan Oliveira
 */
public class SQLiteDBController extends DBController {

	private static String dbPath;

	public static void connect(File db) throws ClassNotFoundException, SQLException {
		setInstance(new SQLiteDBController());
		Class.forName("org.sqlite.JDBC");
		if (getInstance().getConnection() != null) {
			getInstance().closeConnection();
		}
		SQLiteConfig config = new SQLiteConfig();
		config.enforceForeignKeys(true);
		config.setEncoding(SQLiteConfig.Encoding.UTF8);
		getInstance().setConnection(DriverManager.getConnection("jdbc:sqlite:" + db.getAbsolutePath().replaceAll("\\\\", "/"), config.toProperties()));
		dbPath = db.getAbsolutePath();
		if (!getInstance().hasSkeleton()) {
			getInstance().resetDatabase();
		}
	}

	public static String getDbPath() {
		return dbPath;
	}
}
