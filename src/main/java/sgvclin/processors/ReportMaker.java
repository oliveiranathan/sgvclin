/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.List;
import javax.imageio.ImageIO;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.ReportTemplateBuilder;
import net.sf.dynamicreports.report.builder.column.ColumnBuilder;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.Components;
import net.sf.dynamicreports.report.builder.component.TextFieldBuilder;
import net.sf.dynamicreports.report.builder.group.GroupBuilder;
import net.sf.dynamicreports.report.builder.style.ReportStyleBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.builder.subtotal.SubtotalBuilder;
import net.sf.dynamicreports.report.builder.tableofcontents.TableOfContentsCustomizerBuilder;
import net.sf.dynamicreports.report.constant.*;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import sgvclin.data.Datasheet;
import sgvclin.data.DatasheetVariant;
import sgvclin.data.GeographicPoint;
import sgvclin.data.Map;
import sgvclin.data.MapPoint;
import sgvclin.data.Question;
import sgvclin.data.Sheet;
import sgvclin.data.SheetField;
import sgvclin.processors.dataprocessor.DataMaker;
import sgvclin.processors.dataprocessor.Filter;
import sgvclin.processors.dataprocessor.FilteredData;
import sgvclin.processors.dataprocessor.VariantGroup;

/**
 *
 * @author Nathan Oliveira
 */
public class ReportMaker {

	public static void createObservationReport(Sheet sheet, Question question, Map map, File file) throws IOException, SQLException, DRException {
		JasperReportBuilder report = DynamicReports.report();
		report.setColumnStyle(DynamicReports.stl.style().bold().setHorizontalTextAlignment(HorizontalTextAlignment.CENTER));
		Image img = ImageIO.read(ReportMaker.class.getResource("/logo.png"));
		report.title(DynamicReports.cmp.verticalList(DynamicReports.cmp.horizontalList(DynamicReports.cmp.image(img),
				DynamicReports.cmp.text(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OBSERVATIONS"))
						.setStyle(DynamicReports.stl.style().bold().setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT)
								.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE))),
				DynamicReports.cmp.horizontalList(
						DynamicReports.cmp.text(java.util.ResourceBundle.getBundle("texts/Bundle").getString("QUESTION NUMBER:"))
								.setFixedColumns(12),
						DynamicReports.cmp.text(question.getNumber()).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)),
				DynamicReports.cmp.horizontalList(
						DynamicReports.cmp.text(java.util.ResourceBundle.getBundle("texts/Bundle").getString("QUESTION:"))
								.setFixedColumns(5),
						DynamicReports.cmp.text(question.getQuestion()).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT))));
		report.setPageMargin(DynamicReports.margin().setLeft(50).setRight(10).setTop(10).setBottom(10));
		report.addPageFooter(Components.pageXslashY());
		report.summaryWithPageHeaderAndFooter();
		report.setTemplate(getReportTemplate());
		report.addPageFooter(DynamicReports.cmp.text(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DEVELOPED BY...")));
		String observations = "\n";
		List<GeographicPoint> pointsOnMap = MapPoint.getMapPointsOfMap(map);
		List<Datasheet> datasheets;
		datasheets = Datasheet.getListDatasheets(pointsOnMap, sheet);
		int lastPoint = -1;
		for (Datasheet ds : datasheets) {
			DatasheetVariant vfd = DatasheetVariant.getDatasheetVariant(question, ds);
			if (vfd == null || vfd.getObservation() == null || vfd.getObservation().isEmpty()) {
				continue;
			}
			if (vfd.getSheet().getLocality().getId() != lastPoint) {
				observations += "\n";
				lastPoint = vfd.getSheet().getLocality().getId();
			}
			observations += vfd.getSheet().getLocality().getCode() + "/" + vfd.getSheet().getInformant() + " - "
					+ vfd.getSheet().getLocality().getName() + "\n\t\t" + vfd.getObservation() + "\n";
		}
		report.addSummary(DynamicReports.cmp.text(observations).setStyle(DynamicReports.stl.style()
				.setHorizontalTextAlignment(HorizontalTextAlignment.JUSTIFIED)));
		export(file, report);
	}

	@SuppressWarnings("rawtypes")
	public static void createReportPerPoint(Sheet sheet, List<Filter> filters, Question question, List<VariantGroup> varsGroups, Map map, File file)
			throws SQLException, DRException, IOException {
		DataMaker dataMaker = new DataMaker(sheet, filters, Arrays.asList(question), varsGroups, map);
		SubtotalBuilder[] subtotals = null;
		TextColumnBuilder[] group = filters == null ? null : new TextColumnBuilder[1];
		List<ColumnBuilder> cbl = new ArrayList<>(varsGroups.size() * 2 + 1);
		if (filters != null) {
			group[0] = DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("GROUPING"), "filter", DynamicReports.type.stringType());
			cbl.add(group[0]);
		}
		cbl.add(DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("POINT"), "point", DynamicReports.type.integerType()));
		cbl.add(DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("LOCALITY"), "locality", DynamicReports.type.stringType()));
		String[] cols = new String[varsGroups.size() * 2 + 4];
		cols[0] = "filter";
		cols[1] = "point";
		cols[2] = "locality";
		cols[cols.length - 1] = "total";
		int i = 3;
		for (VariantGroup vg : varsGroups) {
			String var = vg.getRepresentation();
			cbl.add(DynamicReports.col.column(var, var, DynamicReports.type.integerType()));
			cbl.add(DynamicReports.col.column("%", var + "%", DynamicReports.type.percentageType()));
			cols[i] = var;
			i++;
			cols[i] = var + "%";
			i++;
		}
		cbl.add(DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("Total"), "total", DynamicReports.type.integerType()));
		ColumnBuilder[] columns = cbl.toArray(new ColumnBuilder[cbl.size()]);
		DRDataSource data = new DRDataSource(cols);
		int[] count = new int[varsGroups.size()];
		int NK = 0;
		int TP = 0;
		String NK_ = java.util.ResourceBundle.getBundle("texts/Bundle").getString("NK: ");
		String TP_ = java.util.ResourceBundle.getBundle("texts/Bundle").getString("TP: ");
		if (filters == null) {
			filters = new ArrayList<>(1);
			filters.add(null);
		}
		for (Filter filter : filters) {
			for (GeographicPoint gp : dataMaker.getPoints()) {
				Arrays.fill(count, 0);
				int total = 0;
				FilteredData fd = dataMaker.getFilteredData(gp);
				for (int inf : fd.getNK()) {
					NK++;
					NK_ += gp.getCode() + "/" + inf + ", ";
				}
				for (int inf : fd.getTP()) {
					TP++;
					TP_ += gp.getCode() + "/" + inf + ", ";
				}
				HashMap<VariantGroup, Integer> counts = fd.getVariantGroups(filter);
				if (counts == null) {
					continue;
				}
				total = varsGroups.stream().map((vg) -> {
					count[varsGroups.indexOf(vg)] += counts.getOrDefault(vg, 0);
					return vg;
				}).map((vg) -> counts.getOrDefault(vg, 0)).reduce(total, Integer::sum);
				Object[] strs = new Object[varsGroups.size() * 2 + 4];
				strs[0] = filter == null ? "filter" : filter.toString();
				strs[1] = gp.getCode();
				strs[2] = gp.getName();
				strs[strs.length - 1] = total;
				i = 3;
				for (int idxVar = 0; idxVar < varsGroups.size(); idxVar++) {
					strs[i] = count[idxVar];
					i++;
					strs[i] = (double) count[idxVar] / total;
					i++;
				}
				data.add(strs);
			}
		}
		String footer = "";
		if (NK != 0) {
			footer += NK_.substring(0, NK_.length() - 2);
		}
		if (TP != 0) {
			if (NK != 0) {
				footer += "; ";
			}
			footer += TP_.substring(0, TP_.length() - 2);
		}
		JasperReportBuilder report = buildReport(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PRODUCITIVITY PER POINT"), Arrays.asList(question),
				columns, group, subtotals, data, DynamicReports.cmp.text(footer));
		report.setPageFormat(PageType.A1, PageOrientation.LANDSCAPE);
		report.setColumnTitleStyle(DynamicReports.stl.style().setFontName("Doulos SIL"));
		export(file, report);
	}

	public static void createReportPerPoint(Sheet sheet, List<Filter> filters, List<Question> questions, List<VariantGroup> varsGroups, Map map, File file)
			throws SQLException, DRException, IOException {
		DataMaker dataMaker = new DataMaker(sheet, filters, questions, varsGroups, map);
		SubtotalBuilder[] subtotals = null;
		TextColumnBuilder[] group = filters == null ? null : new TextColumnBuilder[1];
		List<ColumnBuilder> cbl = new ArrayList<>(varsGroups.size() * 2 + 1);
		if (filters != null) {
			group[0] = DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("GROUPING"), "filter", DynamicReports.type.stringType());
			cbl.add(group[0]);
		}
		cbl.add(DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("POINT"), "point", DynamicReports.type.integerType()));
		cbl.add(DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("LOCALITY"), "locality", DynamicReports.type.stringType()));
		String[] cols = new String[varsGroups.size() * 2 + 4];
		cols[0] = "filter";
		cols[1] = "point";
		cols[2] = "locality";
		cols[cols.length - 1] = "total";
		int i = 3;
		for (VariantGroup vg : varsGroups) {
			String var = vg.getRepresentation();
			cbl.add(DynamicReports.col.column(var, var, DynamicReports.type.integerType()));
			cbl.add(DynamicReports.col.column("%", var + "%", DynamicReports.type.percentageType()));
			cols[i] = var;
			i++;
			cols[i] = var + "%";
			i++;
		}
		cbl.add(DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("Total"), "total", DynamicReports.type.integerType()));
		ColumnBuilder[] columns = cbl.toArray(new ColumnBuilder[cbl.size()]);
		DRDataSource data = new DRDataSource(cols);
		int[] count = new int[varsGroups.size()];
		int NK = 0;
		int TP = 0;
		String NK_ = java.util.ResourceBundle.getBundle("texts/Bundle").getString("NK: ");
		String TP_ = java.util.ResourceBundle.getBundle("texts/Bundle").getString("TP: ");
		if (filters == null) {
			filters = new ArrayList<>(1);
			filters.add(null);
		}
		for (Filter filter : filters) {
			for (GeographicPoint gp : dataMaker.getPoints()) {
				Arrays.fill(count, 0);
				int total = 0;
				FilteredData fd = dataMaker.getFilteredData(gp);
				for (int inf : fd.getNK()) {
					NK++;
					NK_ += gp.getCode() + "/" + inf + ", ";
				}
				for (int inf : fd.getTP()) {
					TP++;
					TP_ += gp.getCode() + "/" + inf + ", ";
				}
				HashMap<VariantGroup, Integer> counts = fd.getVariantGroups(filter);
				if (counts == null) {
					continue;
				}
				total = varsGroups.stream().map((vg) -> {
					count[varsGroups.indexOf(vg)] += counts.getOrDefault(vg, 0);
					return vg;
				}).map((vg) -> counts.getOrDefault(vg, 0)).reduce(total, Integer::sum);
				Object[] strs = new Object[varsGroups.size() * 2 + 4];
				strs[0] = filter == null ? "filter" : filter.toString();
				strs[1] = gp.getCode();
				strs[2] = gp.getName();
				strs[strs.length - 1] = total;
				i = 3;
				for (int idxVar = 0; idxVar < varsGroups.size(); idxVar++) {
					strs[i] = count[idxVar];
					i++;
					strs[i] = (double) count[idxVar] / total;
					i++;
				}
				data.add(strs);
			}
		}
		String footer = "";
		if (NK != 0) {
			footer += NK_.substring(0, NK_.length() - 2);
		}
		if (TP != 0) {
			if (NK != 0) {
				footer += "; ";
			}
			footer += TP_.substring(0, TP_.length() - 2);
		}
		JasperReportBuilder report = buildReport(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PRODUCITIVITY PER POINT"), questions,

				columns, group, subtotals, data, DynamicReports.cmp.text(footer));
		report.setPageFormat(PageType.A1, PageOrientation.LANDSCAPE);
		report.setColumnTitleStyle(DynamicReports.stl.style().setFontName("Doulos SIL"));
		export(file, report);
	}

	@SuppressWarnings("rawtypes")
	public static void createStateReport(Sheet sheet, List<Filter> filters, Question question, List<VariantGroup> varsGroups, Map map, File file)
			throws SQLException, IOException, DRException {
		DataMaker dataMaker = new DataMaker(sheet, filters, Arrays.asList(question), varsGroups, map);
		SubtotalBuilder[] subtotals = new SubtotalBuilder[1];
		List<TextColumnBuilder> groups = new ArrayList<>(filters.size() + 1);
		groups.add(DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("STATE"), "state", DynamicReports.type.stringType()));
		for (Filter f : filters) {
			if (f.isEmpty()) {
				continue;
			}
			f.getOrdenation().forEach((sf) -> groups.add(DynamicReports.col.column(sf.getName(), sf.getName(), DynamicReports.type.stringType())));
			break;
		}
		List<ColumnBuilder> columns = new ArrayList<>(groups.size() + 3);
		groups.stream().forEach((col) -> columns.add(col));
		columns.add(DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("VARIANTS"), "variants", DynamicReports.type.stringType())
				.setStyle(DynamicReports.stl.style().setFontName("Doulos SIL")));
		TextColumnBuilder<Integer> nroOcc = DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OCURRENCE COUNT"), "number",
				DynamicReports.type.integerType()).setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);
		columns.add(nroOcc);
		subtotals[0] = DynamicReports.sbt.sum(nroOcc);
		columns.add(DynamicReports.col.percentageColumn("%", nroOcc));
		String[] dataNames = new String[columns.size() - 1];
		for (int i = 0; i < columns.size() - 1; i++) {
			dataNames[i] = columns.get(i).getColumn().getName();
		}
		DRDataSource data = new DRDataSource(dataNames);
		List<String> stateNames = new ArrayList<>(5);
		dataMaker.getPoints().forEach((gp) -> {
			if (!stateNames.contains(gp.getState())) {
				stateNames.add(gp.getState());
			}
		});
		stateNames.sort((String o1, String o2) -> o1.compareTo(o2));
		int NK = 0;
		int TP = 0;
		String NK_ = java.util.ResourceBundle.getBundle("texts/Bundle").getString("NK: ");
		String TP_ = java.util.ResourceBundle.getBundle("texts/Bundle").getString("TP: ");
		for (String state : stateNames) {
			Object[] objs = new Object[columns.size() - 1];
			objs[0] = stateName(state);
			filters.stream().forEach((filter) -> {
				int i = 1;
				for (SheetField sf : filter.getOrdenation()) {
					objs[i] = filter.getRepresentation(sf);
					i++;
				}
				HashMap<VariantGroup, Integer> sums = new HashMap<>(5);
				dataMaker.getPoints().stream().forEach((gp) -> {
					if (gp.getState().equals(state)) {
						HashMap<VariantGroup, Integer> obtained = dataMaker.getFilteredData(gp).getVariantGroups(filter);
						if (obtained != null) {
							obtained.keySet().forEach((vg) -> sums.merge(vg, obtained.get(vg), Integer::sum));
						}
					}
				});
				while (!sums.isEmpty()) {
					int max = 0;
					VariantGroup varGrp = null;
					for (VariantGroup vg : sums.keySet()) {
						if (sums.get(vg) > max) {
							max = sums.get(vg);
							varGrp = vg;
						}
					}
					if (varGrp == null) {
						continue;
					}
					objs[objs.length - 2] = varGrp.getRepresentation();
					objs[objs.length - 1] = sums.remove(varGrp);
					data.add(objs);
				}
			});
			for (GeographicPoint gp : dataMaker.getPoints()) {
				if (!gp.getState().equals(state)) {
					continue;
				}
				FilteredData fd = dataMaker.getFilteredData(gp);
				for (int inf : fd.getNK()) {
					NK++;
					NK_ += gp.getCode() + "/" + inf + ", ";
				}
				for (int inf : fd.getTP()) {
					TP++;
					TP_ += gp.getCode() + "/" + inf + ", ";
				}
			}
		}
		Object[] objs = new Object[columns.size() - 1];
		objs[0] = " ";
		Arrays.fill(objs, null);
		objs[objs.length - 2] = java.util.ResourceBundle.getBundle("texts/Bundle").getString("NK");
		objs[objs.length - 1] = NK;
		data.add(objs);
		objs[objs.length - 2] = java.util.ResourceBundle.getBundle("texts/Bundle").getString("TP");
		objs[objs.length - 1] = TP;
		data.add(objs);
		String footer = "";
		if (NK != 0) {
			footer += NK_.substring(0, NK_.length() - 2);
		}
		if (TP != 0) {
			if (NK != 0) {
				footer += "; ";
			}
			footer += TP_.substring(0, TP_.length() - 2);
		}
		JasperReportBuilder report = buildReport(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PRODUCTIVITY PER STATE"), Arrays.asList(question),
				columns.toArray(new ColumnBuilder[columns.size()]), groups.toArray(new TextColumnBuilder[groups.size()]), subtotals, data, DynamicReports.cmp.text(footer));
		export(file, report);
	}

	@SuppressWarnings("rawtypes")
	public static void createReport(Sheet sheet, List<Filter> filters, List<Question> questions, List<VariantGroup> varsGroups, Map map, File file)
			throws SQLException, IOException, DRException {
		DataMaker dataMaker = new DataMaker(sheet, filters, questions, varsGroups, map);
		SubtotalBuilder[] subtotals = new SubtotalBuilder[1];
		List<TextColumnBuilder> groups = new ArrayList<>(filters.size() + 1);
		for (Filter f : filters) {
			if (f.isEmpty()) {
				continue;
			}
			f.getOrdenation().forEach((sf) -> groups.add(DynamicReports.col.column(sf.getName(), sf.getName(), DynamicReports.type.stringType())));
			break;
		}
		List<ColumnBuilder> columns = new ArrayList<>(groups.size() + 3);
		groups.stream().forEach((col) -> columns.add(col));
		columns.add(DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("VARIANTS"), "variants", DynamicReports.type.stringType())
				.setStyle(DynamicReports.stl.style().setFontName("Doulos SIL")));
		TextColumnBuilder<Integer> nroOcc = DynamicReports.col.column(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OCURRENCE COUNT"), "number",
				DynamicReports.type.integerType()).setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT);
		columns.add(nroOcc);
		subtotals[0] = DynamicReports.sbt.sum(nroOcc);
		columns.add(DynamicReports.col.percentageColumn("%", nroOcc));
		String[] dataNames = new String[columns.size() - 1];
		for (int i = 0; i < columns.size() - 1; i++) {
			dataNames[i] = columns.get(i).getColumn().getName();
		}
		DRDataSource data = new DRDataSource(dataNames);
		int NK = 0;
		int TP = 0;
		String NK_ = java.util.ResourceBundle.getBundle("texts/Bundle").getString("NK: ");
		String TP_ = java.util.ResourceBundle.getBundle("texts/Bundle").getString("TP: ");
		Object[] objs = new Object[columns.size() - 1];
		filters.stream().forEach((filter) -> {
			int i = 0;
			for (SheetField sf : filter.getOrdenation()) {
				objs[i] = filter.getRepresentation(sf);
				i++;
			}
			HashMap<VariantGroup, Integer> sums = new HashMap<>(5);
			dataMaker.getPoints().stream().forEach((gp) -> {
				HashMap<VariantGroup, Integer> obtained = dataMaker.getFilteredData(gp).getVariantGroups(filter);
				if (obtained != null) {
					obtained.keySet().forEach((vg) -> sums.merge(vg, obtained.get(vg), Integer::sum));
				}
			});
			while (!sums.isEmpty()) {
				int max = 0;
				VariantGroup varGrp = null;
				for (VariantGroup vg : sums.keySet()) {
					if (sums.get(vg) > max) {
						max = sums.get(vg);
						varGrp = vg;
					}
				}
				if (varGrp == null) {
					continue;
				}
				objs[objs.length - 2] = varGrp.getRepresentation();
				objs[objs.length - 1] = sums.remove(varGrp);
				data.add(objs);
			}
		});
		for (GeographicPoint gp : dataMaker.getPoints()) {
			FilteredData fd = dataMaker.getFilteredData(gp);
			for (int inf : fd.getNK()) {
				NK++;
				NK_ += gp.getCode() + "/" + inf + ", ";
			}
			for (int inf : fd.getTP()) {
				TP++;
				TP_ += gp.getCode() + "/" + inf + ", ";
			}
		}
		String footer = "";
		if (NK != 0) {
			footer += java.util.ResourceBundle.getBundle("texts/Bundle").getString("NK: ") + NK;
		}
		if (TP != 0) {
			if (NK != 0) {
				footer += ", ";
			}
			footer += java.util.ResourceBundle.getBundle("texts/Bundle").getString("TP: ") + TP;
		}
		if (NK != 0) {
			footer += ". " + NK_.substring(0, NK_.length() - 2);
		}
		if (TP != 0) {
			if (NK != 0) {
				footer += "; ";
			}
			footer += TP_.substring(0, TP_.length() - 2);
		}
		JasperReportBuilder report = buildReport(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OVERALL PRODUCTIVITY"), questions,
				columns.toArray(new ColumnBuilder[columns.size()]), groups.toArray(new TextColumnBuilder[groups.size()]), subtotals, data, DynamicReports.cmp.text(footer));
		export(file, report);
	}

	private static void export(File file, JasperReportBuilder report) throws DRException {
		File f = file;
		if (!f.getName().endsWith(".pdf") && !f.getName().endsWith(".docx") && !f.getName().endsWith(".xlsx")) {
			f = new File(f.getAbsoluteFile() + ".pdf");
		}
		if (f.getName().endsWith(".pdf")) {
			report.toPdf(DynamicReports.export.pdfExporter(f));
		} else if (f.getName().endsWith(".docx")) {
			report.toDocx(DynamicReports.export.docxExporter(f));
		} else {
			report.toXlsx(DynamicReports.export.xlsxExporter(f));
		}
	}

	@SuppressWarnings("rawtypes")
	private static JasperReportBuilder buildReport(String title, List<Question> questions, ColumnBuilder[] columns,
			TextColumnBuilder[] groups, SubtotalBuilder[] subtotals, DRDataSource data, TextFieldBuilder footer) throws IOException {
		JasperReportBuilder report = DynamicReports.report();
		report.setColumnStyle(DynamicReports.stl.style().bold().setHorizontalTextAlignment(HorizontalTextAlignment.CENTER));
		Image img = ImageIO.read(ReportMaker.class.getResource("/logo.png"));
		if (questions.size() == 1) {
			report.title(DynamicReports.cmp.verticalList(DynamicReports.cmp.horizontalList(DynamicReports.cmp.image(img),
					DynamicReports.cmp.text(title).setStyle(
							DynamicReports.stl.style().bold().setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT)
									.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE)))),
					DynamicReports.cmp.horizontalList(DynamicReports.cmp.text(java.util.ResourceBundle.getBundle("texts/Bundle").getString("QUESTION NUMBER:"))
							.setFixedColumns(12),
							DynamicReports.cmp.text(getQuestionsNumberRepresentation(questions)).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)),
					DynamicReports.cmp.horizontalList(DynamicReports.cmp.text(java.util.ResourceBundle.getBundle("texts/Bundle").getString("QUESTION")).setFixedColumns(5),
							DynamicReports.cmp.text(getQuestionsTextRepresentation(questions)).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)));
		} else {
			report.title(DynamicReports.cmp.verticalList(DynamicReports.cmp.horizontalList(DynamicReports.cmp.image(img),
					DynamicReports.cmp.text(title).setStyle(
							DynamicReports.stl.style().bold().setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT)
									.setVerticalTextAlignment(VerticalTextAlignment.MIDDLE)))),
					DynamicReports.cmp.horizontalList(DynamicReports.cmp.text(java.util.ResourceBundle.getBundle("texts/Bundle").getString("QUESTIONS"))
							.setFixedColumns(12),
							DynamicReports.cmp.text(getQuestionsRepresentation(questions)).setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)));
		}
		report.setPageMargin(DynamicReports.margin().setLeft(50).setRight(10).setTop(10).setBottom(10));
		report.addPageFooter(Components.pageXslashY());
		report.summaryWithPageHeaderAndFooter();
		report.setTemplate(getReportTemplate());
		report.columns(columns);
		GroupBuilder[] grp = null;
		if (groups != null && groups.length != 0) {
			grp = new GroupBuilder[groups.length];
			for (int i = 0; i < groups.length; i++) {
				grp[i] = DynamicReports.grp.group(groups[i]);
				report.groupBy(grp[i]);
			}
		}
		if (subtotals != null) {
			if (grp != null) {
				report.subtotalsAtGroupFooter(grp[grp.length - 1], subtotals);
			} else {
				report.subtotalsAtSummary(subtotals);
			}
		}
		report.setDataSource(data);
		report.addSummary(footer);
		report.addPageFooter(DynamicReports.cmp.text(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DEVELOPED BY...")));
		return report;
	}

	private static String getQuestionsRepresentation(List<Question> questions) {
		String rep = "";
		rep = questions.stream().map((q) -> q.getNumber() + " - " + q.getQuestion() + "\n").reduce(rep, String::concat);
		return rep.substring(0, rep.length() - 1);
	}

	private static String getQuestionsNumberRepresentation(List<Question> questions) {
		return "" + questions.get(0).getNumber();
	}

	private static String getQuestionsTextRepresentation(List<Question> questions) {
		return questions.get(0).getQuestion();
	}

	private static String stateName(String estado) {
		switch (estado) {
			case "AM":
				return "AM - Amazonas";
			case "RR":
				return "RR - Roraima";
			case "AP":
				return "AP - Amapá";
			case "PA":
				return "PA - Pará";
			case "TO":
				return "TO - Tocantins";
			case "RO":
				return "RO - Rondônia";
			case "AC":
				return "AC - Acre";
			case "MA":
				return "MA - Maranhão";
			case "PI":
				return "PI - Piauí";
			case "CE":
				return "CE - Ceará";
			case "RN":
				return "RN - Rio Grande do Norte";
			case "PE":
				return "PE - Pernambuco";
			case "PB":
				return "PB - Paraíba";
			case "SE":
				return "SE - Sergipe";
			case "AL":
				return "AL - Alagoas";
			case "BA":
				return "BA - Bahia";
			case "MT":
				return "MT - Mato Grosso";
			case "MS":
				return "MS - Mato Grosso do Sul";
			case "GO":
				return "GO - Goiás";
			case "DF":
				return "DF - Distrito Federal";
			case "RJ":
				return "RJ - Rio de Janeiro";
			case "ES":
				return "ES - Espírito Santo";
			case "SP":
				return "SP - São Paulo";
			case "MG":
				return "MG - Minas Gerais";
			case "PR":
				return "PR - Paraná";
			case "RS":
				return "RS - Rio Grande do Sul";
			case "SC":
				return "SC - Santa Catarina";
			default:
				return estado;
		}
	}

	private static ReportTemplateBuilder getReportTemplate() {
		return DynamicReports.template()
				.setLocale(Locale.ENGLISH)
				.setColumnStyle(getColumnStyle())
				.setColumnTitleStyle(getColumnTitleStyle())
				.setGroupStyle(getGroupStyle())
				.setGroupTitleStyle(getGroupStyle())
				.setSubtotalStyle(getSubtotalStyle())
				.highlightDetailEvenRows()
				.crosstabHighlightEvenRows()
				.setCrosstabGroupStyle(getCrosstabGroupStyle())
				.setCrosstabGroupTotalStyle(getCrosstabGroupTotalStyle())
				.setCrosstabGrandTotalStyle(getCrosstabGrandTotalStyle())
				.setCrosstabCellStyle(getCrosstabCellStyle())
				.setTableOfContentsCustomizer(getTableOfContentsCustomizer());
	}

	private static TableOfContentsCustomizerBuilder getTableOfContentsCustomizer() {
		return DynamicReports.tableOfContentsCustomizer()
				.setHeadingStyle(0, DynamicReports.stl.style(getRootStyle()).bold());
	}

	private static ReportStyleBuilder getCrosstabCellStyle() {
		return DynamicReports.stl.style(getColumnStyle())
				.setBorder(DynamicReports.stl.pen1Point());
	}

	private static ReportStyleBuilder getCrosstabGrandTotalStyle() {
		return DynamicReports.stl.style(getColumnTitleStyle())
				.setBackgroundColor(new Color(140, 140, 140));
	}

	private static ReportStyleBuilder getCrosstabGroupTotalStyle() {
		return DynamicReports.stl.style(getColumnTitleStyle())
				.setBackgroundColor(new Color(170, 170, 170));
	}

	private static ReportStyleBuilder getCrosstabGroupStyle() {
		return DynamicReports.stl.style(getColumnTitleStyle());
	}

	private static ReportStyleBuilder getSubtotalStyle() {
		return DynamicReports.stl.style(getBoldStyle())
				.setTopBorder(DynamicReports.stl.pen1Point());
	}

	private static ReportStyleBuilder getGroupStyle() {
		return DynamicReports.stl.style(getBoldStyle())
				.setHorizontalAlignment(HorizontalAlignment.LEFT);
	}

	private static ReportStyleBuilder getBoldStyle() {
		return DynamicReports.stl.style(getRootStyle()).bold();
	}

	private static ReportStyleBuilder getColumnTitleStyle() {
		return DynamicReports.stl.style(getColumnStyle())
				.setBorder(DynamicReports.stl.pen1Point())
				.setHorizontalAlignment(HorizontalAlignment.CENTER)
				.setBackgroundColor(Color.LIGHT_GRAY)
				.bold();
	}

	private static ReportStyleBuilder getColumnStyle() {
		return DynamicReports.stl.style(getRootStyle()).setVerticalAlignment(VerticalAlignment.MIDDLE);
	}

	private static StyleBuilder getRootStyle() {
		return DynamicReports.stl.style().setPadding(2);
	}

	private ReportMaker() {
	}
}
