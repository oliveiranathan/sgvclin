/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.processors;

import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipException;

/**
 *
 * @author Nathan Oliveira
 */
public class CBDUtils {

	public static byte[] getBytes(Area area) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
			PathIterator pathIterator = area.getPathIterator(null);
			while (!pathIterator.isDone()) {
				double point[] = new double[2];
				int segType = pathIterator.currentSegment(point);
				oos.writeInt(segType);
				oos.writeDouble(point[0]);
				oos.writeDouble(point[1]);
				pathIterator.next();
			}
		}
		return baos.toByteArray();
	}

	public static Area getArea(byte[] data) throws IOException, ClassNotFoundException {
		ByteArrayInputStream bais = new ByteArrayInputStream(data);
		Area area = new Area();
		Path2D.Double path = new Path2D.Double();
		try (ObjectInputStream ois = new ObjectInputStream(bais)) {
			while (ois.available() > 0) {
				int segType = ois.readInt();
				double x = ois.readDouble();
				double y = ois.readDouble();
				switch (segType) {
					case PathIterator.SEG_CLOSE:
						area.add(new Area(path));
						path.reset();
						break;
					case PathIterator.SEG_LINETO:
						path.lineTo(x, y);
						break;
					case PathIterator.SEG_MOVETO:
						path.moveTo(x, y);
						break;
				}
			}
		}
		return area;
	}

	public static boolean[][] decompress(byte[] data) throws IOException, ClassNotFoundException {
		ByteArrayInputStream bais = new ByteArrayInputStream(data);
		GZIPInputStream gzipis;
		try {
			gzipis = new GZIPInputStream(bais);
		} catch (ZipException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			return null;
		}
		boolean[][] ret;
		try (ObjectInputStream ois = new ObjectInputStream(gzipis)) {
			ret = (boolean[][]) ois.readObject();
		}
		return ret;
	}

	private CBDUtils() {
	}
}
