/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.lang.reflect.Field;
import java.util.logging.Level;
import javax.swing.UIManager;
import sgvclin.processors.LoggerManager;

/**
 * @author Nathan Oliveira
 */
public class Launcher {

	public static void main(String[] args) {
		start();
	}

	private static void start() {
		LoggerManager.getInstance();
		Thread.setDefaultUncaughtExceptionHandler((Thread t, Throwable e) -> LoggerManager.getInstance().log(Level.SEVERE, e.toString(), e));
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
			LoggerManager.getInstance().log(Level.INFO, null, ex);
		}
		try {// Changes the name of the application on Linux/OSX
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Field name = toolkit.getClass().getDeclaredField("awtAppClassName");
			name.setAccessible(true);
			name.set(toolkit, "SGVCLin");
		} catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException ex) { // Windows does not have the field. Die silently.
		}
		EventQueue.invokeLater(MainController::getInstance);
	}
}
