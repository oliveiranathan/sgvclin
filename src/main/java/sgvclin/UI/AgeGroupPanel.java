/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableCellRenderer;
import sgvclin.data.AgeGroup;
import sgvclin.data.AgeRange;
import sgvclin.processors.LoggerManager;

/**
 * @author Nathan Oliveira
 */
public class AgeGroupPanel extends javax.swing.JPanel {

	private static final long serialVersionUID = 1L;

	private final DefaultListModel<AgeGroup> ageGroupModel;
	private final AgeGroupTableModel ageRangeModel;

	public AgeGroupPanel() throws SQLException {
		ageGroupModel = new DefaultListModel<AgeGroup>() {
			private static final long serialVersionUID = 1L;

			private AgeGroup[] data = AgeGroup.getAgeGroups();

			@Override
			public int getSize() {
				if (data == null) {
					return 0;
				}
				return data.length;
			}

			@Override
			public AgeGroup getElementAt(int index) {
				if (data == null) {
					return null;
				}
				return data[index];
			}

			@Override
			public void addElement(AgeGroup element) {
				try {
					AgeGroup.createAgeGroup(element.getName());
					data = AgeGroup.getAgeGroups();
					fireContentsChanged(this, 0, data.length);
					ageGroupList.setSelectedValue(AgeGroup.getAgeGroup(element.getName()), true);
				} catch (SQLException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
					StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
				}
			}

			@Override
			public boolean removeElement(Object obj) {
				try {
					data = AgeGroup.getAgeGroups();
					fireContentsChanged(this, 0, data.length);
					ageGroupList.clearSelection();
				} catch (SQLException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				}
				return true;
			}
		};
		ageRangeModel = new AgeGroupTableModel();
		initComponents();
		setKeysOperations();
		ageRangeTable.setDefaultRenderer(String.class, createNewRenderer());
		ageRangeTable.setDefaultRenderer(Integer.class, createNewRenderer());
		ageRangeTable.getTableHeader().setFont(ageRangeTable.getTableHeader().getFont().deriveFont(Font.BOLD, 10f));
		((JLabel) ageRangeTable.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
	}

	private DefaultTableCellRenderer createNewRenderer() {
		return new DefaultTableCellRenderer() {
			private static final long serialVersionUID = 1L;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
				Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
				if (!table.isRowSelected(row)) {
					if (ageRangeModel.isWrong(row, col)) {
						c.setBackground(Color.RED);
					} else {
						c.setBackground(getBackground());
					}
				} else {
					c.setBackground(getBackground());
				}
				return c;
			}
		};
	}

	private void setKeysOperations() {
		ageRangeTable.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
		ageRangeTable.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK), "paste");
		ageRangeTable.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK), "copy");
		ageRangeTable.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK), "cut");
		ageRangeTable.getActionMap().put("delete", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (ageRangeTable.getSelectedColumn() == 0) {
					ageRangeTable.getModel().setValueAt(null, ageRangeTable.getSelectedRow(), ageRangeTable.getSelectedColumn());
				} else {
					ageRangeTable.getModel().setValueAt(-1, ageRangeTable.getSelectedRow(), ageRangeTable.getSelectedColumn());
				}
			}
		});
		ageRangeTable.getActionMap().put("paste", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				paste();
			}
		});
		ageRangeTable.getActionMap().put("copy", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				copy();
			}
		});
		ageRangeTable.getActionMap().put("cut", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				copy();
				int col = ageRangeTable.getSelectedColumn();
				ageRangeTable.getModel().setValueAt(null, ageRangeTable.getSelectedRow(), col);
			}
		});
	}

	private void paste() {
		int col = ageRangeTable.getSelectedColumn();
		try {
			String data = Toolkit.getDefaultToolkit().getSystemClipboard()
					.getContents(null).getTransferData(DataFlavor.stringFlavor).toString();
			ageRangeTable.getModel().setValueAt(data, ageRangeTable.getSelectedRow(), col);
		} catch (UnsupportedFlavorException | IOException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
	}

	private void copy() {
		int col = ageRangeTable.getSelectedColumn();
		int row = ageRangeTable.getSelectedRow();
		if (col != -1 && row != -1) {
			Object value = ageRangeTable.getValueAt(row, col);
			String data = "";
			if (value != null) {
				data = value.toString();
			}
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(data), null);
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
	 * content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings({"unchecked", "Convert2Lambda"})
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		ageGroupTF = new javax.swing.JTextField();
		addGroupBtn = new javax.swing.JButton();
		jScrollPane1 = new javax.swing.JScrollPane();
		ageGroupList = new javax.swing.JList<>();
		renameBtn = new javax.swing.JButton();
		delBtn = new javax.swing.JButton();
		jPanel2 = new javax.swing.JPanel();
		jScrollPane2 = new javax.swing.JScrollPane();
		ageRangeTable = new javax.swing.JTable();
		saveRangesBtn = new javax.swing.JButton();

		java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("texts/Bundle"); // NOI18N
		jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("RANGE GROUP"))); // NOI18N

		addGroupBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/add.png"))); // NOI18N
		addGroupBtn.setText(bundle.getString("ADD")); // NOI18N
		addGroupBtn.setToolTipText(bundle.getString("ADDS A NEW GROUP")); // NOI18N
		addGroupBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addGroupBtnActionPerformed(evt);
			}
		});

		ageGroupList.setModel(ageGroupModel);
		ageGroupList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		ageGroupList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
			public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
				ageGroupListValueChanged(evt);
			}
		});
		jScrollPane1.setViewportView(ageGroupList);

		renameBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/swap.png"))); // NOI18N
		renameBtn.setText(bundle.getString("RENAME")); // NOI18N
		renameBtn.setToolTipText(bundle.getString("RENAME THE GROUP")); // NOI18N
		renameBtn.setEnabled(false);
		renameBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				renameBtnActionPerformed(evt);
			}
		});

		delBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/remove.png"))); // NOI18N
		delBtn.setText(bundle.getString("DELETE")); // NOI18N
		delBtn.setToolTipText(bundle.getString("DELETE THE GROUP")); // NOI18N
		delBtn.setEnabled(false);
		delBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(ageGroupTF)
				.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(addGroupBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
								.addComponent(renameBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(delBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addContainerGap())
		);
		jPanel1Layout.setVerticalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addComponent(ageGroupTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(addGroupBtn)
						.addGap(18, 18, 18)
						.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(renameBtn)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(delBtn)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);

		jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("RANGES"))); // NOI18N

		ageRangeTable.setModel(ageRangeModel);
		ageRangeTable.setRowSelectionAllowed(false);
		ageRangeTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		ageRangeTable.getTableHeader().setReorderingAllowed(false);
		jScrollPane2.setViewportView(ageRangeTable);

		saveRangesBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/save.png"))); // NOI18N
		saveRangesBtn.setText(bundle.getString("SAVE RANGES")); // NOI18N
		saveRangesBtn.setToolTipText(bundle.getString("SAVES THE RANGES")); // NOI18N
		saveRangesBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				saveRangesBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout.setHorizontalGroup(
				jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
						.addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(0, 0, Short.MAX_VALUE))
				.addGroup(jPanel2Layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(saveRangesBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap())
		);
		jPanel2Layout.setVerticalGroup(
				jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
						.addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(saveRangesBtn)
						.addContainerGap())
		);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
		);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		);
	}// </editor-fold>//GEN-END:initComponents

	private void addGroupBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addGroupBtnActionPerformed
		if (ageGroupTF.getText().trim().isEmpty()) {
			return;
		}
		AgeGroup ag = new AgeGroup();
		ag.setName(ageGroupTF.getText().trim());
		ageGroupModel.addElement(ag);
	}//GEN-LAST:event_addGroupBtnActionPerformed

	private void saveRangesBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveRangesBtnActionPerformed
		if (ageRangeModel.validateData()) {
			try {
				AgeRange.deleteAllAgeRanges(ageRangeModel.getSelectedGroup());
				AgeRange.saveAllAgeRanges(ageRangeModel.getArrayAgeRange());
				StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("SAVED TO THE DATABASE"));
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR WRITING TO THE DATABASE"));
			}
		} else {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PLEASE, CHECK THE FIELDS IN RED"));
		}
	}//GEN-LAST:event_saveRangesBtnActionPerformed

	private void ageGroupListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_ageGroupListValueChanged
		if (evt.getValueIsAdjusting()) {
			return;
		}
		AgeGroup ag = ageGroupList.getSelectedValue();
		if (ag != null) {
			delBtn.setEnabled(true);
			renameBtn.setEnabled(true);
		} else {
			delBtn.setEnabled(false);
			renameBtn.setEnabled(false);
		}
		try {
			ageRangeModel.load(ag);
		} catch (NullPointerException | SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
	}//GEN-LAST:event_ageGroupListValueChanged

	private void renameBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_renameBtnActionPerformed
		if (ageGroupList.getSelectedValue() == null) {
			return;
		}
		String cat = ageGroupList.getSelectedValue().getName();
		String newName = JOptionPane.showInputDialog(this, java.util.ResourceBundle.getBundle("texts/Bundle").getString("NEW NAME"), cat);
		if (!newName.equals(cat)) {
			try {
				if (AgeGroup.getAgeGroup(newName) != null) {
					StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("AGE RANGE GROUP ALREADY EXISTS"));
				} else {
					ageGroupList.getSelectedValue().setName(newName);
					AgeGroup.saveAgeGroup(ageGroupList.getSelectedValue());
					ageGroupList.repaint();
				}
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		}
	}//GEN-LAST:event_renameBtnActionPerformed

	private void delBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delBtnActionPerformed
		if (ageGroupList.getSelectedValue() == null) {
			return;
		}
		AgeGroup group = ageGroupList.getSelectedValue();
		try {
			AgeGroup.deleteAgeGroup(group);
			ageGroupModel.removeElement(group);
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
	}//GEN-LAST:event_delBtnActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton addGroupBtn;
	private javax.swing.JList<AgeGroup> ageGroupList;
	private javax.swing.JTextField ageGroupTF;
	private javax.swing.JTable ageRangeTable;
	private javax.swing.JButton delBtn;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JButton renameBtn;
	private javax.swing.JButton saveRangesBtn;
	// End of variables declaration//GEN-END:variables
}
