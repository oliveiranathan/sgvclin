/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.Color;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.swing.UIManager;
import sgvclin.data.GeographicPoint;
import sgvclin.data.Map;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan Oliveira
 */
public class MapPointPanel extends javax.swing.JPanel {

	private static final long serialVersionUID = 1L;

	private final Map map;
	private GeographicPoint point = null;

	public MapPointPanel(Map map) throws SQLException {
		initComponents();
		this.map = map;
		point = GeographicPoint.getFirstGeoPoint(map);
		updateFields();
	}

	private void updateFields() {
		if (point == null) {
			localityTF.setText("");
			stateTF.setText("");
			numberTF.setText("");
			return;
		}
		localityTF.setText(point.getName());
		stateTF.setText(point.getState());
		numberTF.setText("" + point.getCode());
	}

	private boolean saveFields() {
		if (point == null) {
			return true;
		}
		if (localityTF.getText().trim().isEmpty()) {
			localityTF.setBackground(Color.red);
			return false;
		}
		localityTF.setBackground(UIManager.getColor("TextField.background"));
		if (stateTF.getText().trim().isEmpty()) {
			stateTF.setBackground(Color.red);
			return false;
		}
		stateTF.setBackground(UIManager.getColor("TextField.background"));
		if (numberTF.getText().trim().isEmpty()) {
			numberTF.setBackground(Color.red);
			return false;
		}
		try {
			Integer.parseInt(numberTF.getText().trim());
		} catch (NumberFormatException ex) {
			numberTF.setBackground(Color.red);
			return false;
		}
		numberTF.setBackground(UIManager.getColor("TextField.background"));
		point.setName(localityTF.getText().trim());
		point.setState(stateTF.getText().trim());
		point.setCode(Integer.parseInt(numberTF.getText().trim()));
		try {
			GeographicPoint.saveGeoPoint(point);
			StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("WRITTEN TO THE DATABASE"));
			return true;
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR WRITING TO THE DATABASE"));
			return false;
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
	 * content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings({"unchecked", "Convert2Lambda"})
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jLabel1 = new javax.swing.JLabel();
		localityTF = new javax.swing.JTextField();
		jLabel2 = new javax.swing.JLabel();
		stateTF = new javax.swing.JTextField();
		jLabel3 = new javax.swing.JLabel();
		numberTF = new javax.swing.JTextField();
		firstBtn = new javax.swing.JButton();
		prevBtn = new javax.swing.JButton();
		nextBtn = new javax.swing.JButton();
		lastBtn = new javax.swing.JButton();
		saveBtn = new javax.swing.JButton();

		java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("texts/Bundle"); // NOI18N
		jLabel1.setText(bundle.getString("LOCALITY")); // NOI18N

		jLabel2.setText(bundle.getString("STATE")); // NOI18N

		jLabel3.setText(bundle.getString("NUMBER")); // NOI18N

		firstBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/first.png"))); // NOI18N
		firstBtn.setText(bundle.getString("FIRST")); // NOI18N
		firstBtn.setToolTipText(bundle.getString("FIRST#POINTNW")); // NOI18N
		firstBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				firstBtnActionPerformed(evt);
			}
		});

		prevBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/previous.png"))); // NOI18N
		prevBtn.setText(bundle.getString("PREVIOUS")); // NOI18N
		prevBtn.setToolTipText(bundle.getString("PREV#POINTNW")); // NOI18N
		prevBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				prevBtnActionPerformed(evt);
			}
		});

		nextBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/next.png"))); // NOI18N
		nextBtn.setText(bundle.getString("NEXT")); // NOI18N
		nextBtn.setToolTipText(bundle.getString("NEXT#POINTNW")); // NOI18N
		nextBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				nextBtnActionPerformed(evt);
			}
		});

		lastBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/last.png"))); // NOI18N
		lastBtn.setText(bundle.getString("LAST")); // NOI18N
		lastBtn.setToolTipText(bundle.getString("LAST#POINTNW")); // NOI18N
		lastBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				lastBtnActionPerformed(evt);
			}
		});

		saveBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/save.png"))); // NOI18N
		saveBtn.setText(bundle.getString("SAVE")); // NOI18N
		saveBtn.setToolTipText(bundle.getString("SAVE#POINTNW")); // NOI18N
		saveBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				saveBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(layout.createSequentialGroup()
										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(jLabel1)
												.addComponent(jLabel2)
												.addComponent(jLabel3))
										.addGap(116, 116, 116)
										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(stateTF)
												.addComponent(numberTF)
												.addComponent(localityTF)))
								.addGroup(layout.createSequentialGroup()
										.addComponent(firstBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(prevBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(nextBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(lastBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(saveBtn)))
						.addContainerGap())
		);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel1)
								.addComponent(localityTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel2)
								.addComponent(stateTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel3)
								.addComponent(numberTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(firstBtn)
								.addComponent(prevBtn)
								.addComponent(nextBtn)
								.addComponent(lastBtn)
								.addComponent(saveBtn))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
	}// </editor-fold>//GEN-END:initComponents

	private void firstBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_firstBtnActionPerformed
		if (!saveFields()) {
			return;
		}
		try {
			point = GeographicPoint.getFirstGeoPoint(map);
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
		updateFields();
	}//GEN-LAST:event_firstBtnActionPerformed

	private void prevBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prevBtnActionPerformed
		if (!saveFields()) {
			return;
		}
		try {
			point = GeographicPoint.getPreviousGeoPoint(point, map);
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
		updateFields();
	}//GEN-LAST:event_prevBtnActionPerformed

	private void nextBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextBtnActionPerformed
		if (!saveFields()) {
			return;
		}
		try {
			point = GeographicPoint.getNextGeoPoint(point, map);
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
		updateFields();
	}//GEN-LAST:event_nextBtnActionPerformed

	private void lastBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lastBtnActionPerformed
		if (!saveFields()) {
			return;
		}
		try {
			point = GeographicPoint.getLastGeoPoint(map);
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
		updateFields();
	}//GEN-LAST:event_lastBtnActionPerformed

	private void saveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtnActionPerformed
		saveFields();
	}//GEN-LAST:event_saveBtnActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton firstBtn;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JButton lastBtn;
	private javax.swing.JTextField localityTF;
	private javax.swing.JButton nextBtn;
	private javax.swing.JTextField numberTF;
	private javax.swing.JButton prevBtn;
	private javax.swing.JButton saveBtn;
	private javax.swing.JTextField stateTF;
	// End of variables declaration//GEN-END:variables
}
