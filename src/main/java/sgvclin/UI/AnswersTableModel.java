/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.InvalidClassException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import sgvclin.data.Datasheet;
import sgvclin.data.DatasheetVariant;
import sgvclin.data.GeographicPoint;
import sgvclin.data.Map;
import sgvclin.data.MapPoint;
import sgvclin.data.Question;
import sgvclin.data.Sheet;
import sgvclin.data.Variant;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan Oliveira
 */
class AnswersTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;

	private final Sheet sheet;
	private Question question;
	private final List<Datasheet> datasheets;
	private JTable table;

	/*- Table definition:
     * 1st Col: Locality__Number           Integer     ~EDITABLE
     * 2nd Col: State__Point               String      ~EDITABLE
     * 3rd Col: Locality__Name		   String      ~EDITABLE
     * 4th Col: Informant                  Integer     ~EDITABLE
     * 5th Col: 1st variant                String      EDITABLE
     * .                                    .           .
     * .                                    .           .
     * .                                    .           .
     * _th Col: NK			   Boolean	   EDITABLE
     * _th Col: TP			   Boolean	   EDITABLE
     * _th Col: Observations		   String	   EDITABLE
	 */
	private final Integer[] localityNumber;
	private final String[] localityState;
	private final String[] localityName;
	private final Integer[] informantNumber;
	private final List<Variant[]> variants;
	private final Boolean[] NK;
	private final Boolean[] TP;
	private final String[] observation;
	private final int numberOfLines;
	private int numberOfVariants;

	AnswersTableModel(Sheet sheet, Map map) throws SQLException, InvalidClassException {
		this.sheet = sheet;
		question = null;
		datasheets = Datasheet.getListDatasheets(MapPoint.getMapPointsOfMap(map), sheet);
		numberOfLines = datasheets.size();
		localityNumber = new Integer[numberOfLines];
		localityState = new String[numberOfLines];
		localityName = new String[numberOfLines];
		informantNumber = new Integer[numberOfLines];
		variants = new ArrayList<>(5);
		NK = new Boolean[numberOfLines];
		TP = new Boolean[numberOfLines];
		observation = new String[numberOfLines];
		for (int i = 0; i < numberOfLines; i++) {
			GeographicPoint gp = datasheets.get(i).getLocality();
			localityNumber[i] = gp.getCode();
			localityState[i] = gp.getState();
			localityName[i] = gp.getName();
			informantNumber[i] = datasheets.get(i).getInformant();
		}
	}

	@Override
	public int getRowCount() {
		return numberOfLines;
	}

	@Override
	public int getColumnCount() {
		return 7 + numberOfVariants;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
			case 0:
				return localityNumber[rowIndex];
			case 1:
				return localityState[rowIndex];
			case 2:
				return localityName[rowIndex];
			case 3:
				return informantNumber[rowIndex];
		}
		switch (columnIndex - numberOfVariants) {
			case 4:
				return NK[rowIndex];
			case 5:
				return TP[rowIndex];
			case 6:
				return observation[rowIndex];
		}
		return variants.get(columnIndex - 4)[rowIndex];
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex > 3;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 1:
			case 2:
				return String.class;
			case 0:
			case 3:
				return Integer.class;
		}
		switch (columnIndex - numberOfVariants) {
			case 4:
			case 5:
				return Boolean.class;
			case 6:
				return String.class;
		}
		return Variant.class;
	}

	@Override
	public String getColumnName(int column) {
		switch (column) {
			case 0:
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("POINT");
			case 1:
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("STATE");
			case 2:
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("LOCALITY");
			case 3:
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("INFORMANT");
		}
		switch (column - numberOfVariants) {
			case 4:
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("NK");
			case 5:
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("TP");
			case 6:
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("OBSERVATIONS");
		}
		return (column - 3) + java.util.ResourceBundle.getBundle("texts/Bundle").getString("ANSWER");
	}

	void addColumn() throws SQLException {
		numberOfVariants++;
		variants.add(new Variant[numberOfLines]);
		fireTableStructureChanged();
		setVariantColumnsSelectors();
	}

	void setQuestion(Question question) throws SQLException {
		if (question == null) {
			this.question = null;
			numberOfVariants = 0;
			variants.clear();
			return;
		}
		if (!DatasheetVariant.hasDynamicVariantTables(sheet.getName())) {
			DatasheetVariant.createDynamicVariantTables(sheet.getName());
		}
		numberOfVariants = Variant.getListOfVariants(question).size();
		if (numberOfVariants < 5) {
			numberOfVariants = 5;
		}
		variants.clear();
		for (int i = 0; i < numberOfVariants; i++) {
			variants.add(new Variant[numberOfLines]);
		}
		fireTableStructureChanged();
		for (int i = 0; i < numberOfLines; i++) {
			DatasheetVariant dsv = DatasheetVariant.getDatasheetVariant(question, datasheets.get(i));
			if (dsv == null) {
				NK[i] = false;
				TP[i] = false;
				observation[i] = null;
				continue;
			}
			NK[i] = dsv.isNotKnown();
			TP[i] = dsv.isTechnicalProblem();
			observation[i] = dsv.getObservation();
			if (NK[i] || TP[i]) {
				continue;
			}
			for (int j = 0; j < dsv.getVariants().size(); j++) {
				variants.get(j)[i] = dsv.getVariants().get(j);
			}
		}
		fireTableDataChanged();
		for (int i = numberOfVariants - 1; i > 5; i--) {
			boolean ok = false;
			for (Variant str : variants.get(i)) {
				if (str != null) {
					ok = true;
				}
			}
			if (ok) {
				break;
			} else {
				variants.remove(i);
				numberOfVariants--;
			}
		}
		fireTableStructureChanged();
		this.question = question;
		setVariantColumnsSelectors();
		DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer() {
			private static final long serialVersionUID = 1L;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				Component tableCellRendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				((JLabel) tableCellRendererComponent).setHorizontalAlignment(JLabel.LEFT);
				return tableCellRendererComponent;
			}
		};
		table.getColumnModel().getColumn(0).setCellRenderer(dtcr);
		table.getColumnModel().getColumn(3).setCellRenderer(dtcr);
	}

	protected void setVariantColumnsSelectors() throws SQLException {
		if (question == null) {
			return;
		}
		JComboBox<Variant> inputCB = new JComboBox<>();
		for (Variant v : Variant.getListOfVariants(question)) {
			inputCB.addItem(v);
		}
		for (int i = 4; i < getColumnCount() - 3; i++) {
			table.getColumnModel().getColumn(i).setCellEditor(new DefaultCellEditor(inputCB));
		}
	}

	void setTable(JTable table) {
		this.table = table;
		setKeysOperations();
	}

	private void setKeysOperations() {
		table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
		table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK), "paste");
		table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK), "copy");
		table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK), "cut");
		table.getActionMap().put("delete", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				int col = table.getSelectedColumn();
				if (col < 4 || col > getColumnCount() - 3) {
					if (col == getColumnCount() - 1) {
						table.getModel().setValueAt(null, table.getSelectedRow(), col);
					}
					return;
				}
				int row = table.getSelectedRow();
				table.getModel().setValueAt(null, row, col);
			}
		});
		table.getActionMap().put("paste", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				paste();
			}
		});
		table.getActionMap().put("copy", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				copy();
			}
		});
		table.getActionMap().put("cut", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				copy();
				int col = table.getSelectedColumn();
				if (col == getColumnCount() - 1) {
					table.getModel().setValueAt(null, table.getSelectedRow(), col);
				}
			}
		});
	}

	private void paste() {
		int col = table.getSelectedColumn();
		if (col == getColumnCount() - 1) {
			try {
				String data = Toolkit.getDefaultToolkit().getSystemClipboard()
						.getContents(null).getTransferData(DataFlavor.stringFlavor).toString();
				table.getModel().setValueAt(data, table.getSelectedRow(), col);
			} catch (UnsupportedFlavorException | IOException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		}
	}

	private void copy() {
		int col = table.getSelectedColumn();
		int row = table.getSelectedRow();
		if (col != -1 && row != -1) {
			Object value = table.getValueAt(row, col);
			String data = "";
			if (value != null) {
				data = value.toString();
			}
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(data), null);
		}
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (getValueAt(rowIndex, columnIndex) != null && getValueAt(rowIndex, columnIndex).equals(aValue)) {
			return;
		}
		if (columnIndex == getColumnCount() - 1) { // Observation
			try {
				DatasheetVariant dsv = DatasheetVariant.getDatasheetVariant(question, datasheets.get(rowIndex));
				if (dsv != null) {
					dsv.setObservation((String) aValue);
				} else {
					dsv = new DatasheetVariant();
					dsv.setSheet(datasheets.get(rowIndex));
					dsv.setObservation((String) aValue);
					dsv.setQuestion(question);
				}
				DatasheetVariant.saveDatasheetVariant(dsv);
				observation[rowIndex] = (String) aValue;
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		} else if (columnIndex == getColumnCount() - 2) { // TP
			try {
				DatasheetVariant dsv = DatasheetVariant.getDatasheetVariant(question, datasheets.get(rowIndex));
				if (dsv != null) {
					dsv.setTechnicalProblem((boolean) aValue);
				} else {
					dsv = new DatasheetVariant();
					dsv.setSheet(datasheets.get(rowIndex));
					dsv.setTechnicalProblem((boolean) aValue);
					dsv.setQuestion(question);
				}
				DatasheetVariant.saveDatasheetVariant(dsv);
				TP[rowIndex] = (Boolean) aValue;
				for (int i = 0; i < variants.size(); i++) {
					if (variants.get(i)[rowIndex] != null) {
						table.getModel().setValueAt(null, rowIndex, i + 4);
					}
				}
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		} else if (columnIndex == getColumnCount() - 3) { // NK
			try {
				DatasheetVariant dsv = DatasheetVariant.getDatasheetVariant(question, datasheets.get(rowIndex));
				if (dsv != null) {
					dsv.setNotKnown((boolean) aValue);
				} else {
					dsv = new DatasheetVariant();
					dsv.setSheet(datasheets.get(rowIndex));
					dsv.setNotKnown((boolean) aValue);
					dsv.setQuestion(question);
				}
				DatasheetVariant.saveDatasheetVariant(dsv);
				NK[rowIndex] = (Boolean) aValue;
				for (int i = 0; i < variants.size(); i++) {
					if (variants.get(i)[rowIndex] != null) {
						table.getModel().setValueAt(null, rowIndex, i + 4);
					}
				}
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		} else { // Variant
			if (NK[rowIndex] || TP[rowIndex]) {
				if (aValue != null) {
					fireTableCellUpdated(rowIndex, columnIndex);
					return;
				}
			}
			int vecIdx = columnIndex - 4;
			try {
				DatasheetVariant dsv = DatasheetVariant.getDatasheetVariant(question, datasheets.get(rowIndex));
				if (dsv != null) {
					dsv.clearVariants();
				} else {
					dsv = new DatasheetVariant();
					dsv.setSheet(datasheets.get(rowIndex));
					dsv.setQuestion(question);
				}
				for (int i = 0; i < variants.size(); i++) {
					if (i == vecIdx) {
						if (aValue != null) {
							if (aValue instanceof String && !((String) aValue).trim().isEmpty()) {
								dsv.addVariant(Variant.getVariant(question, (String) aValue));
							} else if (aValue instanceof Variant) {
								dsv.addVariant((Variant) aValue);
							}
						}
					} else if (variants.get(i)[rowIndex] != null) {
						dsv.addVariant(variants.get(i)[rowIndex]);
					}
				}
				DatasheetVariant.saveDatasheetVariant(dsv);
				if (aValue instanceof String) {
					variants.get(vecIdx)[rowIndex] = Variant.getVariant(question, (String) aValue);
				} else if (aValue instanceof Variant) {
					variants.get(vecIdx)[rowIndex] = (Variant) aValue;
				} else {
					variants.get(vecIdx)[rowIndex] = null;
				}
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		}
		fireTableCellUpdated(rowIndex, columnIndex);
	}
}
