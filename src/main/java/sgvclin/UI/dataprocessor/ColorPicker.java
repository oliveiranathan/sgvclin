/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI.dataprocessor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.text.ParseException;
import java.util.logging.Level;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFormattedTextField;
import javax.swing.colorchooser.DefaultColorSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.MaskFormatter;
import sgvclin.data.ColorPattern;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan
 */
public class ColorPicker extends javax.swing.colorchooser.AbstractColorChooserPanel {

	private static final long serialVersionUID = 1L;
	private final JFormattedTextField.AbstractFormatterFactory hexFormatter;
	private final boolean displayPatterns;
	private String lastCode = "";
	private boolean colorChanging = false;
	private Color background = Color.BLACK;
	private ColorPattern.Pattern pattern;
	private int size;

	/**
	 * Creates new form ColorPicker
	 *
	 * @param patterns
	 * @param bg
	 * @param pat
	 */
	public ColorPicker(boolean patterns, Color bg, ColorPattern.Pattern pat, int size) {
		hexFormatter = new JFormattedTextField.AbstractFormatterFactory() {
			@Override
			public JFormattedTextField.AbstractFormatter getFormatter(JFormattedTextField tf) {
				try {
					return new MaskFormatter("'#HHHHHH");
				} catch (ParseException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
					return null;
				}
			}
		};
		displayPatterns = patterns;
		background = bg;
		pattern = pat;
		this.size = size;
	}

	public Color getBG() {
		return background;
	}

	public ColorPattern.Pattern getPattern() {
		return pattern;
	}

	public int getPattSize() {
		return patternSizeSlider.getValue();
	}

	private void processColorButton(ActionEvent evt) {
		updateColorLater(((Component) evt.getSource()).getBackground());
	}

	private void processSliders() {
		if (colorChanging) {
			return;
		}
		updateColorLater(new Color(colorSliderRed.getValue(), colorSliderGreen.getValue(), colorSliderBlue.getValue()));
	}

	private void processCode() {
		if (colorChanging) {
			return;
		}
		String text = colorCodeField.getText();
		if (text.equals(lastCode)) {
			return;
		}
		lastCode = text;
		if (text.trim().length() != 7) {
			return;
		}
		int r = Integer.parseInt(text.substring(1, 3), 16);
		int g = Integer.parseInt(text.substring(3, 5), 16);
		int b = Integer.parseInt(text.substring(5, 7), 16);
		updateColorLater(new Color(r, g, b));
	}

	private void updateColorLater(Color c) {
		getColorSelectionModel().setSelectedColor(c);
	}

	private void updateColor(Color c) {
		colorChanging = true;
		int r = c.getRed();
		int g = c.getGreen();
		int b = c.getBlue();
		colorSliderRed.setBackground(new Color(r, 0, 0));
		colorSliderRed.setValue(r);
		colorSliderGreen.setBackground(new Color(0, g, 0));
		colorSliderGreen.setValue(g);
		colorSliderBlue.setBackground(new Color(0, 0, b));
		colorSliderBlue.setValue(b);
		String code = "#"
				+ (r < 16 ? "0" : "") + Integer.toHexString(r).toUpperCase()
				+ (g < 16 ? "0" : "") + Integer.toHexString(g).toUpperCase()
				+ (b < 16 ? "0" : "") + Integer.toHexString(b).toUpperCase();
		if (!code.equals(lastCode)) {
			colorCodeField.setText(code);
		}
		paintTextureButtons();
		colorChanging = false;
	}

	private void paintTextureButtons() {
		Color foreground = getColorFromModel();
		BufferedImage[] textures = ColorPattern.paintTextures(ColorPattern.loadTextures(), foreground, background);
		patternClearBtn.setIcon(new ImageIcon(textures[0].getScaledInstance(ColorPattern.DEFAULT_SIZE, ColorPattern.DEFAULT_SIZE, Image.SCALE_SMOOTH)));
		patternSlashBtn.setIcon(new ImageIcon(textures[1].getScaledInstance(ColorPattern.DEFAULT_SIZE, ColorPattern.DEFAULT_SIZE, Image.SCALE_SMOOTH)));
		patternBackslashBtn.setIcon(new ImageIcon(textures[2].getScaledInstance(ColorPattern.DEFAULT_SIZE, ColorPattern.DEFAULT_SIZE, Image.SCALE_SMOOTH)));
		patternDotBtn.setIcon(new ImageIcon(textures[3].getScaledInstance(ColorPattern.DEFAULT_SIZE, ColorPattern.DEFAULT_SIZE, Image.SCALE_SMOOTH)));
		patternVertBtn.setIcon(new ImageIcon(textures[4].getScaledInstance(ColorPattern.DEFAULT_SIZE, ColorPattern.DEFAULT_SIZE, Image.SCALE_SMOOTH)));
		patternHorizBtn.setIcon(new ImageIcon(textures[5].getScaledInstance(ColorPattern.DEFAULT_SIZE, ColorPattern.DEFAULT_SIZE, Image.SCALE_SMOOTH)));
		patternTimesBtn.setIcon(new ImageIcon(textures[6].getScaledInstance(ColorPattern.DEFAULT_SIZE, ColorPattern.DEFAULT_SIZE, Image.SCALE_SMOOTH)));
		patternPlusBtn.setIcon(new ImageIcon(textures[7].getScaledInstance(ColorPattern.DEFAULT_SIZE, ColorPattern.DEFAULT_SIZE, Image.SCALE_SMOOTH)));
	}

	private void updateTextures() {
		paintTextureButtons();
	}

	@Override
	public void updateChooser() {
		updateColor(getColorFromModel());
	}

	@Override
	protected void buildChooser() {
		initComponents();
		colorCodeField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				processCode();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				processCode();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				processCode();
			}
		});
		patternPanel.setVisible(displayPatterns);
		if (displayPatterns) {
			paintTextureButtons();
			patternSizeSlider.setValue(size);
		}
	}

	@Override
	public String getDisplayName() {
		return java.util.ResourceBundle.getBundle("texts/Bundle").getString("COLOR");
	}

	@Override
	public Icon getSmallDisplayIcon() {
		return null;//nop
	}

	@Override
	public Icon getLargeDisplayIcon() {
		return null;//nop
	}

	private void fireUpdate() {
		for (ChangeListener l : ((DefaultColorSelectionModel) getColorSelectionModel()).getChangeListeners()) {
			l.stateChanged(new ChangeEvent(getColorSelectionModel()));
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
	 * content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        colorButton1 = new javax.swing.JButton();
        colorButton2 = new javax.swing.JButton();
        colorButton3 = new javax.swing.JButton();
        colorButton4 = new javax.swing.JButton();
        colorButton5 = new javax.swing.JButton();
        colorButton6 = new javax.swing.JButton();
        colorButton7 = new javax.swing.JButton();
        colorButton8 = new javax.swing.JButton();
        colorButton9 = new javax.swing.JButton();
        colorButton10 = new javax.swing.JButton();
        colorButton11 = new javax.swing.JButton();
        colorButton12 = new javax.swing.JButton();
        colorButton13 = new javax.swing.JButton();
        colorButton14 = new javax.swing.JButton();
        colorButton15 = new javax.swing.JButton();
        colorButton16 = new javax.swing.JButton();
        colorButton17 = new javax.swing.JButton();
        colorButton18 = new javax.swing.JButton();
        colorButton19 = new javax.swing.JButton();
        colorButton20 = new javax.swing.JButton();
        colorButton21 = new javax.swing.JButton();
        colorButton22 = new javax.swing.JButton();
        colorButton23 = new javax.swing.JButton();
        colorButton24 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        colorSliderRed = new javax.swing.JSlider();
        colorSliderGreen = new javax.swing.JSlider();
        colorSliderBlue = new javax.swing.JSlider();
        jLabel1 = new javax.swing.JLabel();
        colorCodeField = new javax.swing.JFormattedTextField();
        patternPanel = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        patternClearBtn = new javax.swing.JButton();
        patternSlashBtn = new javax.swing.JButton();
        patternBackslashBtn = new javax.swing.JButton();
        patternDotBtn = new javax.swing.JButton();
        patternVertBtn = new javax.swing.JButton();
        patternHorizBtn = new javax.swing.JButton();
        patternTimesBtn = new javax.swing.JButton();
        patternPlusBtn = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        patternSizeSlider = new javax.swing.JSlider();
        setAsBG = new javax.swing.JButton();

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("texts/Bundle"); // NOI18N
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("COLOR"))); // NOI18N

        colorButton1.setBackground(java.awt.Color.red);
        colorButton1.setContentAreaFilled(false);
        colorButton1.setOpaque(true);
        colorButton1.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton1ActionPerformed(evt);
            }
        });

        colorButton2.setBackground(java.awt.Color.blue);
        colorButton2.setContentAreaFilled(false);
        colorButton2.setOpaque(true);
        colorButton2.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton2ActionPerformed(evt);
            }
        });

        colorButton3.setBackground(new java.awt.Color(0, 192, 0));
        colorButton3.setContentAreaFilled(false);
        colorButton3.setOpaque(true);
        colorButton3.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton3ActionPerformed(evt);
            }
        });

        colorButton4.setBackground(java.awt.Color.yellow);
        colorButton4.setContentAreaFilled(false);
        colorButton4.setOpaque(true);
        colorButton4.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton4ActionPerformed(evt);
            }
        });

        colorButton5.setBackground(java.awt.Color.black);
        colorButton5.setContentAreaFilled(false);
        colorButton5.setOpaque(true);
        colorButton5.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton5ActionPerformed(evt);
            }
        });

        colorButton6.setBackground(java.awt.Color.magenta);
        colorButton6.setContentAreaFilled(false);
        colorButton6.setOpaque(true);
        colorButton6.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton6ActionPerformed(evt);
            }
        });

        colorButton7.setBackground(java.awt.Color.cyan);
        colorButton7.setContentAreaFilled(false);
        colorButton7.setOpaque(true);
        colorButton7.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton7ActionPerformed(evt);
            }
        });

        colorButton8.setBackground(new java.awt.Color(178, 140, 0));
        colorButton8.setContentAreaFilled(false);
        colorButton8.setOpaque(true);
        colorButton8.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton8ActionPerformed(evt);
            }
        });

        colorButton9.setBackground(new java.awt.Color(156, 93, 82));
        colorButton9.setContentAreaFilled(false);
        colorButton9.setOpaque(true);
        colorButton9.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton9ActionPerformed(evt);
            }
        });

        colorButton10.setBackground(new java.awt.Color(64, 255, 64));
        colorButton10.setContentAreaFilled(false);
        colorButton10.setOpaque(true);
        colorButton10.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton10ActionPerformed(evt);
            }
        });

        colorButton11.setBackground(new java.awt.Color(255, 179, 0));
        colorButton11.setContentAreaFilled(false);
        colorButton11.setOpaque(true);
        colorButton11.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton11ActionPerformed(evt);
            }
        });

        colorButton12.setBackground(new java.awt.Color(128, 62, 117));
        colorButton12.setContentAreaFilled(false);
        colorButton12.setOpaque(true);
        colorButton12.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton12ActionPerformed(evt);
            }
        });

        colorButton13.setBackground(new java.awt.Color(255, 104, 0));
        colorButton13.setContentAreaFilled(false);
        colorButton13.setOpaque(true);
        colorButton13.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton13ActionPerformed(evt);
            }
        });

        colorButton14.setBackground(new java.awt.Color(166, 189, 215));
        colorButton14.setContentAreaFilled(false);
        colorButton14.setOpaque(true);
        colorButton14.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton14ActionPerformed(evt);
            }
        });

        colorButton15.setBackground(new java.awt.Color(193, 0, 32));
        colorButton15.setContentAreaFilled(false);
        colorButton15.setOpaque(true);
        colorButton15.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton15ActionPerformed(evt);
            }
        });

        colorButton16.setBackground(new java.awt.Color(206, 162, 98));
        colorButton16.setContentAreaFilled(false);
        colorButton16.setOpaque(true);
        colorButton16.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton16ActionPerformed(evt);
            }
        });

        colorButton17.setBackground(new java.awt.Color(129, 112, 102));
        colorButton17.setContentAreaFilled(false);
        colorButton17.setOpaque(true);
        colorButton17.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton17ActionPerformed(evt);
            }
        });

        colorButton18.setBackground(new java.awt.Color(0, 125, 52));
        colorButton18.setContentAreaFilled(false);
        colorButton18.setOpaque(true);
        colorButton18.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton18ActionPerformed(evt);
            }
        });

        colorButton19.setBackground(new java.awt.Color(246, 118, 142));
        colorButton19.setContentAreaFilled(false);
        colorButton19.setOpaque(true);
        colorButton19.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton19ActionPerformed(evt);
            }
        });

        colorButton20.setBackground(new java.awt.Color(0, 83, 138));
        colorButton20.setContentAreaFilled(false);
        colorButton20.setOpaque(true);
        colorButton20.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton20ActionPerformed(evt);
            }
        });

        colorButton21.setBackground(new java.awt.Color(83, 55, 122));
        colorButton21.setContentAreaFilled(false);
        colorButton21.setOpaque(true);
        colorButton21.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton21ActionPerformed(evt);
            }
        });

        colorButton22.setBackground(new java.awt.Color(127, 24, 13));
        colorButton22.setContentAreaFilled(false);
        colorButton22.setOpaque(true);
        colorButton22.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton22ActionPerformed(evt);
            }
        });

        colorButton23.setBackground(new java.awt.Color(147, 170, 0));
        colorButton23.setContentAreaFilled(false);
        colorButton23.setOpaque(true);
        colorButton23.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton23ActionPerformed(evt);
            }
        });

        colorButton24.setBackground(new java.awt.Color(89, 51, 21));
        colorButton24.setContentAreaFilled(false);
        colorButton24.setOpaque(true);
        colorButton24.setPreferredSize(new java.awt.Dimension(25, 25));
        colorButton24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButton24ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(colorButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(colorButton9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(colorButton17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorButton24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(colorButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(colorButton9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(colorButton17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(colorButton24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        colorSliderRed.setBackground(java.awt.Color.red);
        colorSliderRed.setMaximum(255);
        colorSliderRed.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                colorSliderRedStateChanged(evt);
            }
        });

        colorSliderGreen.setBackground(java.awt.Color.green);
        colorSliderGreen.setMaximum(255);
        colorSliderGreen.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                colorSliderGreenStateChanged(evt);
            }
        });

        colorSliderBlue.setBackground(java.awt.Color.blue);
        colorSliderBlue.setMaximum(255);
        colorSliderBlue.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                colorSliderBlueStateChanged(evt);
            }
        });

        jLabel1.setText(bundle.getString("COLOR CODE")); // NOI18N

        colorCodeField.setFormatterFactory(hexFormatter);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(colorSliderRed, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(colorSliderGreen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(colorSliderBlue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorCodeField)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(colorSliderRed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorSliderGreen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(colorSliderBlue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(colorCodeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        patternPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("PATTERN"))); // NOI18N

        patternClearBtn.setPreferredSize(new java.awt.Dimension(45, 45));
        patternClearBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patternClearBtnActionPerformed(evt);
            }
        });

        patternSlashBtn.setPreferredSize(new java.awt.Dimension(45, 45));
        patternSlashBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patternSlashBtnActionPerformed(evt);
            }
        });

        patternBackslashBtn.setPreferredSize(new java.awt.Dimension(45, 45));
        patternBackslashBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patternBackslashBtnActionPerformed(evt);
            }
        });

        patternDotBtn.setPreferredSize(new java.awt.Dimension(45, 45));
        patternDotBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patternDotBtnActionPerformed(evt);
            }
        });

        patternVertBtn.setPreferredSize(new java.awt.Dimension(45, 45));
        patternVertBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patternVertBtnActionPerformed(evt);
            }
        });

        patternHorizBtn.setPreferredSize(new java.awt.Dimension(45, 45));
        patternHorizBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patternHorizBtnActionPerformed(evt);
            }
        });

        patternTimesBtn.setPreferredSize(new java.awt.Dimension(45, 45));
        patternTimesBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patternTimesBtnActionPerformed(evt);
            }
        });

        patternPlusBtn.setPreferredSize(new java.awt.Dimension(45, 45));
        patternPlusBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patternPlusBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(patternClearBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(patternSlashBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(patternBackslashBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(patternDotBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(patternVertBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(patternHorizBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(patternTimesBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(patternPlusBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(patternClearBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(patternSlashBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(patternBackslashBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(patternDotBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(patternVertBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(patternHorizBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(patternTimesBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(patternPlusBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jLabel2.setText(bundle.getString("SIZE")); // NOI18N

        patternSizeSlider.setMaximum(500);
        patternSizeSlider.setMinimum(10);
        patternSizeSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                patternSizeSliderStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(patternSizeSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(patternSizeSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setAsBG.setText(bundle.getString("SET CURR AS BG")); // NOI18N
        setAsBG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setAsBGActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout patternPanelLayout = new javax.swing.GroupLayout(patternPanel);
        patternPanel.setLayout(patternPanelLayout);
        patternPanelLayout.setHorizontalGroup(
            patternPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(patternPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(patternPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(setAsBG, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        patternPanelLayout.setVerticalGroup(
            patternPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(patternPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(setAsBG))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(patternPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(patternPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void colorButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton1ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton1ActionPerformed

    private void colorButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton2ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton2ActionPerformed

    private void colorButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton3ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton3ActionPerformed

    private void colorButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton4ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton4ActionPerformed

    private void colorButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton5ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton5ActionPerformed

    private void colorButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton6ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton6ActionPerformed

    private void colorButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton7ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton7ActionPerformed

    private void colorButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton8ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton8ActionPerformed

    private void colorButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton9ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton9ActionPerformed

    private void colorButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton10ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton10ActionPerformed

    private void colorButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton11ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton11ActionPerformed

    private void colorButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton12ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton12ActionPerformed

    private void colorButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton13ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton13ActionPerformed

    private void colorButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton14ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton14ActionPerformed

    private void colorButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton15ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton15ActionPerformed

    private void colorButton16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton16ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton16ActionPerformed

    private void colorButton17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton17ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton17ActionPerformed

    private void colorButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton18ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton18ActionPerformed

    private void colorButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton19ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton19ActionPerformed

    private void colorButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton20ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton20ActionPerformed

    private void colorButton21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton21ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton21ActionPerformed

    private void colorButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton22ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton22ActionPerformed

    private void colorButton23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton23ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton23ActionPerformed

    private void colorButton24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButton24ActionPerformed
		processColorButton(evt);
    }//GEN-LAST:event_colorButton24ActionPerformed

    private void colorSliderRedStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_colorSliderRedStateChanged
		processSliders();
    }//GEN-LAST:event_colorSliderRedStateChanged

    private void colorSliderGreenStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_colorSliderGreenStateChanged
		processSliders();
    }//GEN-LAST:event_colorSliderGreenStateChanged

    private void colorSliderBlueStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_colorSliderBlueStateChanged
		processSliders();
    }//GEN-LAST:event_colorSliderBlueStateChanged

    private void setAsBGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setAsBGActionPerformed
		background = getColorFromModel();
		updateTextures();
		fireUpdate();
    }//GEN-LAST:event_setAsBGActionPerformed

    private void patternClearBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patternClearBtnActionPerformed
		pattern = ColorPattern.Pattern.plain_color;
		fireUpdate();
    }//GEN-LAST:event_patternClearBtnActionPerformed

    private void patternSlashBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patternSlashBtnActionPerformed
		pattern = ColorPattern.Pattern.slash;
		fireUpdate();
    }//GEN-LAST:event_patternSlashBtnActionPerformed

    private void patternBackslashBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patternBackslashBtnActionPerformed
		pattern = ColorPattern.Pattern.backslash;
		fireUpdate();
    }//GEN-LAST:event_patternBackslashBtnActionPerformed

    private void patternDotBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patternDotBtnActionPerformed
		pattern = ColorPattern.Pattern.dot;
		fireUpdate();
    }//GEN-LAST:event_patternDotBtnActionPerformed

    private void patternVertBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patternVertBtnActionPerformed
		pattern = ColorPattern.Pattern.vertical;
		fireUpdate();
    }//GEN-LAST:event_patternVertBtnActionPerformed

    private void patternHorizBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patternHorizBtnActionPerformed
		pattern = ColorPattern.Pattern.horizontal;
		fireUpdate();
    }//GEN-LAST:event_patternHorizBtnActionPerformed

    private void patternTimesBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patternTimesBtnActionPerformed
		pattern = ColorPattern.Pattern.times;
		fireUpdate();
    }//GEN-LAST:event_patternTimesBtnActionPerformed

    private void patternPlusBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patternPlusBtnActionPerformed
		pattern = ColorPattern.Pattern.plus;
		fireUpdate();
    }//GEN-LAST:event_patternPlusBtnActionPerformed

    private void patternSizeSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_patternSizeSliderStateChanged
		fireUpdate();
    }//GEN-LAST:event_patternSizeSliderStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton colorButton1;
    private javax.swing.JButton colorButton10;
    private javax.swing.JButton colorButton11;
    private javax.swing.JButton colorButton12;
    private javax.swing.JButton colorButton13;
    private javax.swing.JButton colorButton14;
    private javax.swing.JButton colorButton15;
    private javax.swing.JButton colorButton16;
    private javax.swing.JButton colorButton17;
    private javax.swing.JButton colorButton18;
    private javax.swing.JButton colorButton19;
    private javax.swing.JButton colorButton2;
    private javax.swing.JButton colorButton20;
    private javax.swing.JButton colorButton21;
    private javax.swing.JButton colorButton22;
    private javax.swing.JButton colorButton23;
    private javax.swing.JButton colorButton24;
    private javax.swing.JButton colorButton3;
    private javax.swing.JButton colorButton4;
    private javax.swing.JButton colorButton5;
    private javax.swing.JButton colorButton6;
    private javax.swing.JButton colorButton7;
    private javax.swing.JButton colorButton8;
    private javax.swing.JButton colorButton9;
    private javax.swing.JFormattedTextField colorCodeField;
    private javax.swing.JSlider colorSliderBlue;
    private javax.swing.JSlider colorSliderGreen;
    private javax.swing.JSlider colorSliderRed;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JButton patternBackslashBtn;
    private javax.swing.JButton patternClearBtn;
    private javax.swing.JButton patternDotBtn;
    private javax.swing.JButton patternHorizBtn;
    private javax.swing.JPanel patternPanel;
    private javax.swing.JButton patternPlusBtn;
    private javax.swing.JSlider patternSizeSlider;
    private javax.swing.JButton patternSlashBtn;
    private javax.swing.JButton patternTimesBtn;
    private javax.swing.JButton patternVertBtn;
    private javax.swing.JButton setAsBG;
    // End of variables declaration//GEN-END:variables
}
