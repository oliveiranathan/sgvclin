/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI.dataprocessor;

import java.util.List;
import sgvclin.data.Map;
import sgvclin.data.Question;
import sgvclin.data.Questionnaire;
import sgvclin.data.Sheet;
import sgvclin.processors.dataprocessor.Filter;
import sgvclin.processors.dataprocessor.VariantGroup;

/**
 *
 * @author Nathan Oliveira
 */
public interface SelectorInterface {

	void invoke(Sheet sheet, List<Filter> filters, List<Question> questions, List<VariantGroup> varsGroups, Map map, int numberOfVariantsToDisplay, Questionnaire questionnaire);
}
