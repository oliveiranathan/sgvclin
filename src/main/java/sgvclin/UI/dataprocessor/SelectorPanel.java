/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI.dataprocessor;

import java.awt.Component;
import java.awt.Container;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JInternalFrame;
import sgvclin.UI.StatusBarImpl;
import sgvclin.data.Map;
import sgvclin.data.Question;
import sgvclin.data.Questionnaire;
import sgvclin.data.Sheet;
import sgvclin.data.Variant;
import sgvclin.processors.LoggerManager;
import sgvclin.processors.dataprocessor.DataException;
import sgvclin.processors.dataprocessor.Filter;
import sgvclin.processors.dataprocessor.VariantGroup;

/**
 *
 * @author Nathan Oliveira
 */
public class SelectorPanel extends javax.swing.JPanel {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("rawtypes")
	private final List<SelectorPanelIface> elements;
	private final SelectorInterface invoker;
	private final boolean variantGrouping;
	private final boolean questionGrouping;
	private final boolean question;
	private final String btnLabel;

	public SelectorPanel(String name, boolean sheet, boolean question, boolean map, boolean groupings,
			boolean variantsGrouping, boolean quantity, boolean questionnaire, boolean questionGrouping, SelectorInterface invoker) throws SQLException, DataException {
		btnLabel = java.util.ResourceBundle.getBundle("texts/Bundle").getString("GENERATE ") + name;
		variantGrouping = variantsGrouping;
		this.questionGrouping = questionGrouping;
		this.question = question;
		this.invoker = invoker;
		elements = new ArrayList<>(5);
		initComponents();
		jScrollPane1.getVerticalScrollBar().setUnitIncrement(16);
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		createPanels(sheet, question, map, groupings, quantity, name.equals(java.util.ResourceBundle.getBundle("texts/Bundle").getString("MAP#BTNLABEL")), questionnaire);
		updatePanel();
	}

	private void createPanels(boolean sheet, boolean question, boolean map, boolean groupings, boolean quantity, boolean limitTo2, boolean questionnaire) throws SQLException, DataException {
		if (map) {
			try {
				if (Map.getMaps().isEmpty()) {
					throw new DataException(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE A MAP FIRST"));
				}
				elements.add(new SelectorElementPanel<>(java.util.ResourceBundle.getBundle("texts/Bundle").getString("MAP"),
						"", "", false, Map.getMaps(), this, false));
			} catch (ClassNotFoundException | IOException | NullPointerException | SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				throw new DataException(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
			}
		}
		if (sheet) {
			if (Sheet.getListOfSheets().isEmpty()) {
				throw new DataException(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE A SHEET FIRST"));
			}
			elements.add(new SelectorElementPanel<>(java.util.ResourceBundle.getBundle("texts/Bundle").getString("SHEET"),
					java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECT VARIABLES"),
					java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECT VARIABLES#TOOLTIP"),
					groupings, Sheet.getListOfSheets(), this, limitTo2));
		}
		if (question || questionnaire) {
			if (Questionnaire.getQuestionnaires().isEmpty()) {
				throw new DataException(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE A QUESTIONNAIRE FIRST"));
			}
			elements.add(new SelectorElementPanel<>(java.util.ResourceBundle.getBundle("texts/Bundle").getString("QUESTIONNAIRE"),
					"", "", false, Questionnaire.getQuestionnaires(), this, false));
		}
		if (quantity) {
			List<NumberRenderer> list = new ArrayList<>(11);
			for (int i = 0; i <= 10; i++) {
				list.add(new NumberRenderer(i));
			}
			elements.add(new SelectorElementPanel<>(java.util.ResourceBundle.getBundle("texts/Bundle").getString("QUANTITY OF VARIANTS TO DISPLAY"),
					"", "", false, list, this, false));
		}
		new ArrayList<>(elements).stream().forEach((element) -> notifySelectionChange(element)); // Creates copy because the collection may change...
	}

	private void updatePanel() {
		panel.removeAll();
		elements.stream().forEach((element) -> panel.add((Component) element)); // Maybe the interface should have this guarantee, but I do not know how to do that with the NetBeans GUI designer...
		panel.add(Box.createVerticalGlue());
		panel.revalidate();
	}

	@SuppressWarnings("rawtypes")
	void notifySelectionChange(SelectorPanelIface sep) {
		if (sep.getSelectedElement() == null) {
			return;
		}
		if (sep.getElementClass().equals(Map.class)) {
			elements.forEach((element) -> element.notifyFilterChanges());
		} else if (sep.getElementClass().equals(Sheet.class)) {
			elements.forEach((element) -> element.notifyFilterChanges());
		} else if (sep.getElementClass().equals(Question.class)) {
			elements.forEach((element) -> element.notifyQuestionChanges());
		} else if (sep.getElementClass().equals(Questionnaire.class) && question) {
			SelectorPanelIface<Question> pp = null;
			try {
				if (questionGrouping) {
					pp = new ListSelector<>(this, Question.getListOfQuestions((Questionnaire) sep.getSelectedElement()));
				} else {
					pp = new SelectorElementPanel<>(java.util.ResourceBundle.getBundle("texts/Bundle").getString("QUESTION"),
							java.util.ResourceBundle.getBundle("texts/Bundle").getString("DEFINE EQUIVALENCES"),
							java.util.ResourceBundle.getBundle("texts/Bundle").getString("DEFINE EQUIVALENCES#TOOLTIP"),
							variantGrouping, Question.getListOfQuestions((Questionnaire) sep.getSelectedElement()), this, false);
				}
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
			if (pp != null) {
				int index = elements.indexOf(sep);
				if (elements.size() > index + 1 && elements.get(index + 1).getElementClass().equals(Question.class)) {
					elements.set(index + 1, pp);
				} else {
					elements.add(index + 1, pp);
				}
				elements.forEach((element) -> element.notifyQuestionChanges());
				updatePanel();
			}
		}
	}

	@SuppressWarnings("rawtypes")
	Map getSelectedMap() {
		for (SelectorPanelIface p : elements) {
			if (p.getElementClass().equals(Map.class)) {
				return (Map) p.getSelectedElement();
			}
		}
		return null;
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
	 * content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings({"unchecked", "Convert2Lambda"})
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		panel = new javax.swing.JPanel();
		acceptBtn = new javax.swing.JButton();
		cancelBtn = new javax.swing.JButton();

		javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
		panel.setLayout(panelLayout);
		panelLayout.setHorizontalGroup(
				panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 685, Short.MAX_VALUE)
		);
		panelLayout.setVerticalGroup(
				panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 596, Short.MAX_VALUE)
		);

		jScrollPane1.setViewportView(panel);

		acceptBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/yes.png"))); // NOI18N
		java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("texts/Bundle"); // NOI18N
		acceptBtn.setText(bundle.getString("ACCEPT#GO")); // NOI18N
		acceptBtn.setToolTipText(btnLabel);
		acceptBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				acceptBtnActionPerformed(evt);
			}
		});

		cancelBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/no.png"))); // NOI18N
		cancelBtn.setText(bundle.getString("CANCEL")); // NOI18N
		cancelBtn.setToolTipText(bundle.getString("CANCEL ACTION")); // NOI18N
		cancelBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cancelBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jScrollPane1)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(acceptBtn)
						.addGap(18, 18, 18)
						.addComponent(cancelBtn)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 556, Short.MAX_VALUE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(acceptBtn)
								.addComponent(cancelBtn))
						.addContainerGap())
		);
	}// </editor-fold>//GEN-END:initComponents

	private void acceptBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_acceptBtnActionPerformed
		if (invoker == null) {
			return;
		}
		Sheet sheet = null;
		List<Filter> filters = null;
		Questionnaire questionnaire = null;
		List<Question> questions = null;
		List<List<Variant>> equivalents = null;
		List<HashMap<Variant, Integer>> phonemes = null;
		Map map = null;
		int quantity = 0;
		for (@SuppressWarnings("rawtypes") SelectorPanelIface sep : elements) {
			if (sep instanceof ListSelector) { //It is the only place where it is used nowadays, so leave like this...
				if (sep.getElementClass().equals(Question.class)) {
					@SuppressWarnings("unchecked")//I've just fucking checked :/
					ListSelector<Question> ls = (ListSelector<Question>) sep;
					questions = ls.getSelectedElements();
					phonemes = ls.getPhonemes();
					equivalents = ls.getEquivalents();
				}
			} else if (sep.getElementClass().equals(Sheet.class)) {
				@SuppressWarnings("unchecked")
				SelectorElementPanel<Sheet> s = (SelectorElementPanel<Sheet>) sep;
				sheet = s.getSelectedElement();
				filters = s.getFilters();
				if (filters == null) {
					filters = new ArrayList<>(1);
					filters.add(new Filter());
				}
			} else if (sep.getElementClass().equals(Questionnaire.class)) {
				@SuppressWarnings("unchecked")
				SelectorElementPanel<Questionnaire> s = (SelectorElementPanel<Questionnaire>) sep;
				questionnaire = s.getSelectedElement();
			} else if (sep.getElementClass().equals(Question.class)) {
				@SuppressWarnings("unchecked")
				SelectorElementPanel<Question> s = (SelectorElementPanel<Question>) sep;
				questions = Arrays.asList(s.getSelectedElement());
				equivalents = s.getEquivalents();
				phonemes = s.getPhonemes();
			} else if (sep.getElementClass().equals(Map.class)) {
				@SuppressWarnings("unchecked")
				SelectorElementPanel<Map> s = (SelectorElementPanel<Map>) sep;
				map = s.getSelectedElement();
			} else if (sep.getElementClass().equals((NumberRenderer.class))) {
				@SuppressWarnings("unchecked")
				SelectorElementPanel<NumberRenderer> s = (SelectorElementPanel<NumberRenderer>) sep;
				quantity = s.getSelectedElement().getNumber();
			}
		}
		if (questions == null) {
			invoker.invoke(sheet, filters, questions, null, map, quantity, questionnaire);
		} else if (questionGrouping) {
			if (questions.stream().allMatch((q) -> q.getCategory().isPhonetic())) {
				List<HashMap<Variant, Integer>> ph = phonemes == null ? new ArrayList<>(2) : phonemes;
				if (phonemes == null) {
					HashMap<Variant, Integer> begin = new HashMap<>(10);
					HashMap<Variant, Integer> end = new HashMap<>(10);
					questions.stream().forEach((q) -> {
						try {
							Variant.getListOfVariants(q).stream()
									.forEach((var) -> {
										begin.put(var, 0);
										end.put(var, var.getAnswer().length());
									});
						} catch (SQLException ex) {
							LoggerManager.getInstance().log(Level.SEVERE, null, ex);
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
						}
					});
					ph.add(begin);
					ph.add(end);
				}
				invoker.invoke(sheet, filters, questions, VariantGroup.create(ph, 0), map, quantity, questionnaire);
			} else {
				try {
					List<List<Variant>> eqs = equivalents == null ? new ArrayList<>(10) : equivalents;
					if (equivalents == null) {
						for (Question question : questions) {
							Variant.getListOfVariants(question).stream().map((var) -> {
								List<Variant> l = new ArrayList<>(1);
								l.add(var);
								return l;
							}).forEach((l) -> eqs.add(l));
						}
					}
					invoker.invoke(sheet, filters, questions, VariantGroup.create(eqs), map, quantity, questionnaire);
				} catch (SQLException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
					StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
				}
			}
		} else if (questions.get(0).getCategory().isPhonetic()) {
			try {
				List<HashMap<Variant, Integer>> ph = phonemes == null ? new ArrayList<>(2) : phonemes;
				if (phonemes == null) {
					HashMap<Variant, Integer> begin = new HashMap<>(10);
					HashMap<Variant, Integer> end = new HashMap<>(10);
					Variant.getListOfVariants(questions.get(0)).stream().forEach((var) -> {
						begin.put(var, 0);
						end.put(var, var.getAnswer().length());
					});
					ph.add(begin);
					ph.add(end);
				}
				invoker.invoke(sheet, filters, questions, VariantGroup.create(ph, 0), map, quantity, questionnaire);
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
			}
		} else {
			try {
				List<List<Variant>> eqs = equivalents == null ? new ArrayList<>(10) : equivalents;
				if (equivalents == null) {
					Variant.getListOfVariants(questions.get(0)).stream().map((var) -> {
						List<Variant> l = new ArrayList<>(1);
						l.add(var);
						return l;
					}).forEach((l) -> eqs.add(l));
				}
				invoker.invoke(sheet, filters, questions, VariantGroup.create(eqs), map, quantity, questionnaire);
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
			}
		}
		cancelBtnActionPerformed(evt); // Disposes the window too...
	}//GEN-LAST:event_acceptBtnActionPerformed

	private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
		Container c = this;
		while (!(c instanceof JInternalFrame)) {
			c = c.getParent();
		}
		((JInternalFrame) c).dispose();
	}//GEN-LAST:event_cancelBtnActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton acceptBtn;
	private javax.swing.JButton cancelBtn;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JPanel panel;
	// End of variables declaration//GEN-END:variables

	private class NumberRenderer {

		private final int number;

		NumberRenderer(int number) {
			this.number = number;
		}

		public int getNumber() {
			return number;
		}

		@Override
		public String toString() {
			if (number == 0) {
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("ALL");
			} else {
				return Integer.toString(number);
			}
		}
	}
}
