/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI.dataprocessor;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.text.DefaultCaret;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import sgvclin.data.Question;
import sgvclin.data.Variant;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan Oliveira
 */
public class PhonemeSelectorPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private final List<HashMap<Variant, Integer>> indices = new ArrayList<>(2);
	private final HashMap<Variant, List<Variant>> equivalentVars;
	private JDialog myDialog;

	public PhonemeSelectorPanel(Question question) throws UnsupportedOperationException, SQLException {
		equivalentVars = null;
		if (!question.getCategory().isPhonetic()) {
			throw new UnsupportedOperationException("Cannot do this to a non phonetic question...");
		}
		List<Variant> vars = Variant.getListOfVariants(question);
		initialize(vars);
	}

	public PhonemeSelectorPanel(List<Question> questions) throws UnsupportedOperationException, SQLException {
		equivalentVars = new HashMap<>(5);
		List<Variant> vars = new ArrayList<>(questions.size());
		for (Question question : questions) {
			for (Variant variant : Variant.getListOfVariants(question)) {
				if (vars.stream().anyMatch((var) -> variant.equalsByAnswer(var))) {
					equivalentVars.get(
							vars.stream()
									.filter((var) -> variant.equalsByAnswer(var))
									.findFirst().get()
					).add(variant);
				} else {
					vars.add(variant);
					List<Variant> list = new ArrayList<>(1);
					list.add(variant);
					equivalentVars.put(variant, list);
				}
			}
		}
		initialize(vars);
	}

	private void initialize(List<Variant> vars) {
		indices.add(new HashMap<>(vars.size()));
		indices.add(new HashMap<>(vars.size()));
		setLayout(new GridLayout(0, 1));
		if (vars.isEmpty()) {
			throw new UnsupportedOperationException("Cannot select the phonemes, as there are no variants...");
		}
		final JTextField[] textFs = new JTextField[vars.size()];
		for (int i = 0; i < vars.size(); i++) {
			textFs[i] = new JTextField(vars.get(i).getAnswer(), 20);
			textFs[i].setCaret(new HighlightCaret());
			add(textFs[i]);
		}
		textFs[0].addCaretListener((CaretEvent e) -> {
			int dot = e.getDot();
			int mark = e.getMark();
			if (dot < mark) {
				int aux = dot;
				dot = mark;
				mark = aux;
			}
			for (int i = 1; i < textFs.length; i++) {
				textFs[i].setSelectionStart(mark);
				textFs[i].setSelectionEnd(dot);
				textFs[i].requestFocusInWindow();
			}
			textFs[0].requestFocusInWindow();
		});
		final JButton btn = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ACCEPT"));
		try {
			btn.setIcon(new ImageIcon(ImageIO.read(getClass().getResource("/icons/yes.png"))));
		} catch (IOException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
		btn.setToolTipText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ACCEPTS THE SELECTION AND CONTINUES"));
		add(btn);
		btn.addActionListener(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < textFs.length; i++) {
					int dot = textFs[i].getSelectionStart();
					int mark = textFs[i].getSelectionEnd();
					if (dot > mark) {
						int aux = dot;
						dot = mark;
						mark = aux;
					}
					if (dot == 0 && mark == 0) {
						mark = textFs[i].getText().length();
					}
					indices.get(0).put(vars.get(i), dot);
					indices.get(1).put(vars.get(i), mark);
				}
				if (myDialog != null) {
					myDialog.setVisible(false);
					myDialog = null;
				}
			}
		});
	}

	void setDialog(JDialog dialog) {
		myDialog = dialog;
	}

	List<HashMap<Variant, Integer>> getPhonemes() {
		if (equivalentVars == null) {
			return Collections.unmodifiableList(indices);
		} else {
			List<HashMap<Variant, Integer>> idxs = new ArrayList<>(2);
			idxs.add(new HashMap<>(equivalentVars.size()));
			idxs.add(new HashMap<>(equivalentVars.size()));
			equivalentVars.keySet().forEach((keyVariant)
					-> equivalentVars.get(keyVariant).stream().forEach((variant) -> {
						idxs.get(0).put(variant, indices.get(0).get(keyVariant));
						idxs.get(1).put(variant, indices.get(1).get(keyVariant));
					}));
			return idxs;
		}
	}

	@SuppressWarnings("CloneableImplementsClone")
	private class HighlightCaret extends DefaultCaret {

		private static final long serialVersionUID = 1L;

		private final Highlighter.HighlightPainter unfocusedPainter = new DefaultHighlighter.DefaultHighlightPainter(Color.GREEN);
		private final Highlighter.HighlightPainter focusedPainter = new DefaultHighlighter.DefaultHighlightPainter(Color.yellow);
		private boolean isFocused;

		@Override
		protected Highlighter.HighlightPainter getSelectionPainter() {
			return isFocused ? focusedPainter : unfocusedPainter;
		}

		@Override
		public void setSelectionVisible(boolean vis) {
			if (vis != isFocused) {
				isFocused = vis;
				super.setSelectionVisible(false);
				super.setSelectionVisible(true);
			}
		}
	}
}
