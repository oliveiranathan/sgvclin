/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import sgvclin.processors.LoggerManager;

/**
 * This is defined to recreate the OptionPane class functionality that is needed by the software, but with icons.
 *
 * @author Nathan Oliveira
 */
public class OptionPane {

	public static final int YES_NO_OPTION = JOptionPane.YES_NO_OPTION;
	public static final int YES_OPTION = JOptionPane.YES_OPTION;
	public static final int QUESTION_MESSAGE = JOptionPane.QUESTION_MESSAGE;
	public static final int PLAIN_MESSAGE = JOptionPane.PLAIN_MESSAGE;
	public static final int CANCEL_OPTION = JOptionPane.CANCEL_OPTION;
	public static final int NO_OPTION = JOptionPane.NO_OPTION;

	public static int showConfirmDialog(Component parentComponent, String message, String title, int optionType) {
		final JOptionPane jop = new JOptionPane();
		jop.setMessage(message);
		jop.setMessageType(OptionPane.PLAIN_MESSAGE);
		JButton[] buttons = new JButton[2];
		buttons[0] = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("YES"));
		buttons[1] = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO"));
		try {
			buttons[0].setIcon(new ImageIcon(ImageIO.read(OptionPane.class.getResource("/icons/yes.png"))));
			buttons[1].setIcon(new ImageIcon(ImageIO.read(OptionPane.class.getResource("/icons/no.png"))));
		} catch (IOException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
		jop.setOptions(buttons);
		final JDialog dialog = jop.createDialog(parentComponent, title);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		buttons[0].addActionListener(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				jop.setValue(JOptionPane.YES_OPTION);
				dialog.dispose();
			}
		});
		buttons[1].addActionListener(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				jop.setValue(JOptionPane.NO_OPTION);
				dialog.dispose();
			}
		});
		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				jop.setValue(JOptionPane.CANCEL_OPTION);
			}
		});
		jop.getRootPane().setDefaultButton(buttons[0]);
		dialog.setVisible(true);
		return (Integer) jop.getValue();
	}

	public static String showInputDialog(Component parentComponent, String message, String title, int messageType) {
		final JOptionPane jop = new JOptionPane();
		jop.setMessage(message);
		jop.setMessageType(messageType);
		JTextField tf = new JTextField(25);
		JButton btn1 = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OK"));
		JButton btn2 = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CANCEL"));
		try {
			btn1.setIcon(new ImageIcon(ImageIO.read(OptionPane.class.getResource("/icons/yes.png"))));
			btn2.setIcon(new ImageIcon(ImageIO.read(OptionPane.class.getResource("/icons/no.png"))));
		} catch (IOException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
		btn1.setPreferredSize(btn2.getPreferredSize());
		jop.setOptions(new Object[]{tf, btn1, btn2});
		final JDialog dialog = jop.createDialog(parentComponent, title);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		btn1.addActionListener(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				jop.setValue(JOptionPane.YES_OPTION);
				dialog.dispose();
			}
		});
		btn2.addActionListener(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				jop.setValue(JOptionPane.CANCEL_OPTION);
			}
		});
		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				jop.setValue(JOptionPane.CANCEL_OPTION);
			}
		});
		dialog.getRootPane().setDefaultButton(btn1);
		dialog.setVisible(true);
		if (jop.getValue().equals(JOptionPane.CANCEL_OPTION)) {
			return null;
		}
		return tf.getText();
	}

	public static <T> T showInputDialog(Component parentComponent, String message, String title, int messageType, Icon icon,
			T[] selectionValues, T initialSelectionValue) {
		final JOptionPane jop = new JOptionPane();
		jop.setMessage(message);
		jop.setMessageType(messageType);
		jop.setIcon(icon);
		Object[] objects = new Object[3];
		JComboBox<T> cb = new JComboBox<>(selectionValues);
		cb.setEditable(false);
		cb.setSelectedItem(initialSelectionValue);
		JButton btn1 = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OK"));
		JButton btn2 = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CANCEL"));
		try {
			btn1.setIcon(new ImageIcon(ImageIO.read(OptionPane.class.getResource("/icons/yes.png"))));
			btn2.setIcon(new ImageIcon(ImageIO.read(OptionPane.class.getResource("/icons/no.png"))));
		} catch (IOException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
		btn1.setPreferredSize(btn2.getPreferredSize());
		objects[0] = cb;
		objects[1] = btn1;
		objects[2] = btn2;
		jop.setOptions(objects);
		final JDialog dialog = jop.createDialog(parentComponent, title);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		btn1.addActionListener(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				jop.setValue(JOptionPane.YES_OPTION);
				dialog.dispose();
			}
		});
		btn2.addActionListener(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				jop.setValue(JOptionPane.CANCEL_OPTION);
			}
		});
		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				jop.setValue(JOptionPane.CANCEL_OPTION);
			}
		});
		dialog.getRootPane().setDefaultButton(btn1);
		dialog.setVisible(true);
		if (jop.getValue().equals(JOptionPane.CANCEL_OPTION)) {
			return null;
		}
		return cb.getItemAt(cb.getSelectedIndex());
	}

	private OptionPane() {
	}
}
