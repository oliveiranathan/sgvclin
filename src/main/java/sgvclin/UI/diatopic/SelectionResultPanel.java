/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI.diatopic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.io.InvalidClassException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import sgvclin.MainController;
import sgvclin.UI.StatusBarImpl;
import sgvclin.data.ColorPattern;
import sgvclin.data.Map;
import sgvclin.data.MapSaver;
import sgvclin.data.Question;
import sgvclin.data.Sheet;
import sgvclin.processors.LoggerManager;
import sgvclin.processors.dataprocessor.Filter;
import sgvclin.processors.dataprocessor.PieDataMaker;
import sgvclin.processors.dataprocessor.VariantGroup;
import sgvclin.processors.labelmaker.LabelUser;

/**
 *
 * @author Nathan Oliveira
 */
public class SelectionResultPanel extends ResultPanel implements LabelUser, SelectionTableUser {

	private static final long serialVersionUID = 1L;

	public SelectionResultPanel(Map map, Sheet sheet, List<Filter> filters, List<Question> questions, List<VariantGroup> varsGroups, int maxVarsDisplayed)
			throws SQLException, InvalidClassException {
		this(map, sheet, filters, questions, varsGroups, maxVarsDisplayed, null, null, 0, null, null, PieDataMaker.GraphType.PIE);
	}

	protected SelectionResultPanel(Map map, Sheet sheet, List<Filter> filters, List<Question> questions, List<VariantGroup> varsGroups, int maxVarsDisplayed,
			String title, HashMap<Point2D.Double, Point2D.Double> deltas, int pieRadius, boolean[] selected, Color[] colors, PieDataMaker.GraphType type) throws SQLException, InvalidClassException {
		super(map, sheet, filters, questions, varsGroups, maxVarsDisplayed, title, deltas, pieRadius, colors, type);
		updateInterface(selected);
		updateDataMaker();
		updateData();
	}

	private void updateDataMaker() throws SQLException, InvalidClassException {
		getDataMaker().setSelection(new boolean[getDataMaker().getVariantsGroupsRepresentation().length]);
	}

	private void updateInterface(boolean[] selected) { // This probably depends on the base class UI creation not being changed, BUT, is easier and more or less elegant...
		SwingUtilities.invokeLater(() -> {
			final SelectionTableModel tableModel = new SelectionTableModel(getDataMaker().getVariantsGroupsRepresentation(), this);
			final JTable table = new JTable(tableModel);
			table.setFont(MainController.getInstance().getVariantFont(table.getFont()));
			table.getTableHeader().setReorderingAllowed(false);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			table.setSelectionBackground(Color.white);
			table.setSelectionForeground(Color.black);
			table.getColumnModel().getColumn(0).setPreferredWidth(25);
			table.getColumnModel().getColumn(1).setPreferredWidth(270);
			table.setMinimumSize(new Dimension(295, table.getHeight()));
			table.setMaximumSize(new Dimension(295, 5000));
			table.setAlignmentX(JTable.LEFT_ALIGNMENT);
			table.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (table.getSelectedColumn() == 1) {
						tableModel.setValueAt(!(boolean) tableModel.getValueAt(table.getSelectedRow(), 0), table.getSelectedRow(), 0);
					} else {
						resetFocus();
					}
				}
			});
			getBackPanel().add(table, 0);
			revalidate();
			repaint();
			tableModel.setSelected(selected);
		});
	}

	@Override
	public void setVars(boolean[] sel) {
		if (sel == null) {
			return;
		}
		try {
			getDataMaker().setSelection(sel);
			updateData();
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
	}

	@Override
	public void resetFocus() {
		super.resetFocus();
	}

	private void updateData() {
		getHistogramDisplayArea().removeAll();
		getHistogramDisplayArea().add(getDataMaker().getHistogramChartPanel());
		getHistogramDisplayArea().revalidate();
		getHistogramDisplayArea().repaint();
		getLabelsDisplayArea().removeAll();
		getLabelsDisplayArea().add(getDataMaker().getLabel().getLabel());
		getLabelsDisplayArea().revalidate();
		getLabelsDisplayArea().repaint();
		getMapPanel().setPlots(getDataMaker().getPlots(), getDataMaker().getSize(), this);
	}

	@Override
	public void notifyColorChange(ColorPattern[] patterns) {
		getDataMaker().updateColors(ColorPattern.toColors(patterns));
	}

	@Override
	public MapSaver getMapSaver() {
		MapSaver ms = super.getMapSaver();
		switch (getDataMaker().getGraphType()) {
			case PIE:
				ms.setType(MapSaver.Type.PIE_SELECTION);
				break;
			case HISTOGRAM:
				ms.setType(MapSaver.Type.HISTOGRAM_SELECTION);
				break;
		}
		return ms;
	}
}
