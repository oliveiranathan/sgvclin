/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI.diatopic;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InvalidClassException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ScrollPaneConstants;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.Plot;
import sgvclin.UI.StatusBarImpl;
import sgvclin.data.ColorPattern;
import sgvclin.data.Map;
import sgvclin.data.MapSaver;
import sgvclin.data.Question;
import sgvclin.data.Sheet;
import sgvclin.processors.LoggerManager;
import sgvclin.processors.MapInterface;
import sgvclin.processors.MapMaker;
import sgvclin.processors.dataprocessor.Filter;
import sgvclin.processors.dataprocessor.PieDataMaker;
import sgvclin.processors.dataprocessor.VariantGroup;
import sgvclin.processors.labelmaker.LabelPanel;

/**
 *
 * @author Nathan Oliveira
 */
public class ResultPanel extends JPanel implements PiePanelUser, MapInterface {

	private static final long serialVersionUID = 1L;

	public static ResultPanel load(MapSaver ms) throws SQLException, InvalidClassException {
		switch (ms.getType()) {
			case PIE:
				return new ResultPanel(ms.getMap(), ms.getSheet(), ms.getFilters(), ms.getQuestions(), ms.getVariantGroups(),
						ms.getQttsVariants(), ms.getTitle(), ms.getDeltas(), ms.getRadius(), ColorPattern.toColors(ms.getColors()), PieDataMaker.GraphType.PIE);
			case PIE_SELECTION:
				return new SelectionResultPanel(ms.getMap(), ms.getSheet(), ms.getFilters(), ms.getQuestions(), ms.getVariantGroups(),
						ms.getQttsVariants(), ms.getTitle(), ms.getDeltas(), ms.getRadius(), ms.getSelected(), ColorPattern.toColors(ms.getColors()), PieDataMaker.GraphType.PIE);
			case HISTOGRAM:
				return new ResultPanel(ms.getMap(), ms.getSheet(), ms.getFilters(), ms.getQuestions(), ms.getVariantGroups(),
						ms.getQttsVariants(), ms.getTitle(), ms.getDeltas(), ms.getRadius(), ColorPattern.toColors(ms.getColors()), PieDataMaker.GraphType.HISTOGRAM);
			case HISTOGRAM_SELECTION:
				return new SelectionResultPanel(ms.getMap(), ms.getSheet(), ms.getFilters(), ms.getQuestions(), ms.getVariantGroups(),
						ms.getQttsVariants(), ms.getTitle(), ms.getDeltas(), ms.getRadius(), ms.getSelected(), ColorPattern.toColors(ms.getColors()), PieDataMaker.GraphType.HISTOGRAM);
			default:
				throw new AssertionError(ms.getType().name());
		}
	}

	private PlotPanel mapPanel;
	private JTextField titleTF;
	private JPanel plotDisplayArea;

	private final PieDataMaker dataMaker;

	private String lastTitle = null;
	private JPanel backPanel = null;
	private JPanel labelsDisplayArea = null;
	private JPanel histogramDisplayArea = null;

	public ResultPanel(Map map, Sheet sheet, List<Filter> filters, List<Question> questions, List<VariantGroup> varsGroups, int maxVarsDisplayed)
			throws SQLException, InvalidClassException {
		this(map, sheet, filters, questions, varsGroups, maxVarsDisplayed, null, null, 0, null, PieDataMaker.GraphType.PIE);
	}

	protected ResultPanel(Map map, Sheet sheet, List<Filter> filters, List<Question> questions, List<VariantGroup> varsGroups, int maxVarsDisplayed,
			String title, HashMap<Point2D.Double, Point2D.Double> deltas, int pieRadius, Color[] colors, PieDataMaker.GraphType type) throws SQLException, InvalidClassException {
		dataMaker = new PieDataMaker(sheet, filters, questions, varsGroups, map, maxVarsDisplayed, pieRadius, this, colors, type);
		createInterface(map, title, deltas);
	}

	private void createInterface(Map map, String title, HashMap<Point2D.Double, Point2D.Double> deltas) {
		mapPanel = new PlotPanel(map.getMapImage(), deltas);
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS));
		rightPanel.setPreferredSize(new Dimension(320, 600));
		rightPanel.setMinimumSize(new Dimension(320, 50));
		rightPanel.setMaximumSize(new Dimension(320, 32000));
		JPanel titlePanel = new JPanel(new FlowLayout());
		titlePanel.setPreferredSize(new Dimension(300, 32));
		titlePanel.setMaximumSize(titlePanel.getPreferredSize());
		titleTF = new JTextField(java.util.ResourceBundle.getBundle("texts/Bundle").getString("TYPE IN THE TITLE..."));
		titleTF.setPreferredSize(new Dimension(250, 32));
		titleTF.setMinimumSize(titleTF.getPreferredSize());
		titleTF.setMaximumSize(titleTF.getPreferredSize());
		titleTF.setSize(titleTF.getPreferredSize());
		titlePanel.add(titleTF);
		titleTF.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				titleTF.selectAll();
			}
		});
		titleTF.addActionListener((e) -> {
			if (lastTitle != null && !lastTitle.equals(titleTF.getText())) {
				MapMaker.getMapMaker().removeMap(lastTitle);
			}
			if (!titleTF.getText().equals(java.util.ResourceBundle.getBundle("texts/Bundle").getString("TYPE IN THE TITLE..."))) {
				lastTitle = titleTF.getText();
				MapMaker.getMapMaker().addMap(lastTitle, this);
			}
			mapPanel.requestFocus();
		});
		if (title != null) {
			titleTF.setText(title);
			lastTitle = title;
			javax.swing.SwingUtilities.invokeLater(() -> {
				titleTF.dispatchEvent(new ActionEvent(titleTF, 0, null));
				MapMaker.getMapMaker().addMap(lastTitle, this);
			});
		}
		labelsDisplayArea = new JPanel();
		labelsDisplayArea.setBorder(BorderFactory.createEtchedBorder());
		labelsDisplayArea.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		JLabel lbl = dataMaker.getLabel().getLabel();
		lbl.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		labelsDisplayArea.add(lbl);
		labelsDisplayArea.setMaximumSize(new Dimension(310, 5000));
		plotDisplayArea = new JPanel();
		plotDisplayArea.setLayout(new BoxLayout(plotDisplayArea, BoxLayout.PAGE_AXIS));
		plotDisplayArea.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		plotDisplayArea.setBorder(BorderFactory.createEtchedBorder());
		histogramDisplayArea = new JPanel();
		histogramDisplayArea.setBorder(BorderFactory.createEtchedBorder());
		histogramDisplayArea.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		histogramDisplayArea.add(dataMaker.getHistogramChartPanel());
		histogramDisplayArea.setPreferredSize(dataMaker.getHistogramChartPanel().getPreferredSize());
		histogramDisplayArea.setMaximumSize(new Dimension(310, 5000));
		backPanel = new JPanel();
		backPanel.setLayout(new BoxLayout(backPanel, BoxLayout.PAGE_AXIS));
		rightPanel.add(titlePanel);
		JComboBox<PieDataMaker.GraphType> typeSelectionCB = new JComboBox<>(PieDataMaker.GraphType.values());
		typeSelectionCB.setPreferredSize(new Dimension(250, 32));
		typeSelectionCB.setMaximumSize(new Dimension(250, 32));
		typeSelectionCB.setSize(new Dimension(250, 32));
		typeSelectionCB.setEditable(false);
		ImageIcon pieIcon_;
		ImageIcon histIcon_;
		try {
			pieIcon_ = new ImageIcon(ImageIO.read(getClass().getResource("/icons/pie.png")));
			histIcon_ = new ImageIcon(ImageIO.read(getClass().getResource("/icons/histogram.png")));
		} catch (IOException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			pieIcon_ = null;
			histIcon_ = null;
		}
		final ImageIcon pieIcon = pieIcon_;
		final ImageIcon histIcon = histIcon_;
		typeSelectionCB.setRenderer(new ListCellRenderer<PieDataMaker.GraphType>() {
			private final DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

			@Override
			public Component getListCellRendererComponent(JList<? extends PieDataMaker.GraphType> list, PieDataMaker.GraphType value, int index, boolean isSelected, boolean cellHasFocus) {
				JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				switch (value) {
					case PIE:
						renderer.setText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PIE"));
						renderer.setIcon(pieIcon);
						break;
					case HISTOGRAM:
						renderer.setText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("HISTOGRAM"));
						renderer.setIcon(histIcon);
						break;
					default:
						throw new AssertionError(value.name());
				}
				return renderer;
			}
		});
		typeSelectionCB.setSelectedItem(dataMaker.getGraphType());
		typeSelectionCB.addActionListener((e) -> {
			try {
				dataMaker.setType((PieDataMaker.GraphType) typeSelectionCB.getSelectedItem());
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
			}
			displayPlots(null);
			mapPanel.requestFocus();
		});
		rightPanel.add(typeSelectionCB);
		backPanel.add(labelsDisplayArea);
		backPanel.add(plotDisplayArea);
		backPanel.add(histogramDisplayArea);
		JScrollPane sp = new JScrollPane(backPanel);
		sp.getVerticalScrollBar().setUnitIncrement(16);
		sp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		rightPanel.add(sp);
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		add(mapPanel);
		add(rightPanel);
		updatePlotsOnMap();
	}

	public void updatePlotsOnMap() {
		mapPanel.setPlots(dataMaker.getPlots(), dataMaker.getSize(), this);
	}

	public String getTitle() {
		return lastTitle;
	}

	@Override
	public void displayPlots(Point2D.Double point) {
		plotDisplayArea.removeAll();
		if (point != null) {
			try {
				List<Plot> plots = dataMaker.getPlots().get(point);
				String[] texts = dataMaker.getLabelsTexts(point);
				String[] filters = dataMaker.getFiltersLabels();
				if (filters.length != plots.size()) {
					throw new UnsupportedOperationException();
				}
				JLabel label = new JLabel(texts[0]);
				label.setAlignmentX(JLabel.LEFT_ALIGNMENT);
				plotDisplayArea.add(label);
				for (int i = 0; i < plots.size(); i++) {
					if (plots.get(i) == null) {
						continue;
					}
					switch (dataMaker.getGraphType()) {
						case PIE:
							if (((PiePlot) plots.get(i)).getDataset().getItemCount() == 0) {
								continue;
							}
							break;
						case HISTOGRAM:
							if (((CategoryPlot) plots.get(i)).getDataset().getColumnCount() == 0
									&& ((CategoryPlot) plots.get(i)).getDataset().getRowCount() == 0) {
								continue;
							}
							break;
						default:
							throw new UnsupportedOperationException("FORGOT TO ADD IMPLEMENTATION HERE :)");
					}
					ChartPanel panel = new ChartPanel(dataMaker.createChart(plots.get(i), filters.length == 1 ? "" : filters[i]), false, true, true, false, true);
					panel.getChart().getTitle().setFont(new Font("Arial", Font.BOLD, 14));
					panel.setPreferredSize(new Dimension(300, 200));
					panel.setMaximumSize(panel.getPreferredSize());
					panel.setAlignmentX(ChartPanel.LEFT_ALIGNMENT);
					plotDisplayArea.add(panel);
				}
				JLabel obss = new JLabel(texts[1]);
				obss.setAlignmentX(JLabel.LEFT_ALIGNMENT);
				obss.setMaximumSize(new Dimension(300, 5000));
				plotDisplayArea.add(obss);
				plotDisplayArea.setMaximumSize(new Dimension(310, 5000));
			} catch (ClassNotFoundException | IOException | SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
			} catch (UnsupportedOperationException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		}
		plotDisplayArea.revalidate();
		plotDisplayArea.repaint();
	}

	@Override
	public BufferedImage getMap() {
		BufferedImage map = new BufferedImage(mapPanel.getW(), mapPanel.getH(), BufferedImage.TYPE_INT_ARGB);
		mapPanel.saveMap(map.getGraphics());
		return map;
	}

	@Override
	public BufferedImage getLabel() {
		return dataMaker.getLabel().getImage();
	}

	@Override
	public BufferedImage getHistogram() {
		return dataMaker.getHistogramChartPanel().getChart().createBufferedImage(dataMaker.getHistogramChartPanel().getWidth(), dataMaker.getHistogramChartPanel().getHeight());
	}

	@Override
	public MapSaver getMapSaver() {
		MapSaver ms = dataMaker.getMapSaver();
		ms.setTitle(lastTitle);
		ms.setDeltas(mapPanel.getDeltas());
		ms.setRadius(mapPanel.getPlotSize());
		switch (dataMaker.getGraphType()) {
			case PIE:
				ms.setType(MapSaver.Type.PIE);
				break;
			case HISTOGRAM:
				ms.setType(MapSaver.Type.HISTOGRAM);
				break;
		}
		return ms;
	}

	protected PieDataMaker getDataMaker() {
		return dataMaker;
	}

	protected JPanel getLabelsDisplayArea() {
		return labelsDisplayArea;
	}

	protected JPanel getBackPanel() {
		return backPanel;
	}

	public JPanel getHistogramDisplayArea() {
		return histogramDisplayArea;
	}

	public PlotPanel getMapPanel() {
		return mapPanel;
	}

	protected void resetFocus() {
		mapPanel.requestFocusInWindow();
	}

	public void setLabels(JLabel label) {
		labelsDisplayArea.removeAll();
		if (label != null) {
			labelsDisplayArea.add(label);
		}
		labelsDisplayArea.revalidate();
		labelsDisplayArea.repaint();
	}

	public void setHistogram(ChartPanel histogram) {
		histogramDisplayArea.removeAll();
		if (histogram != null) {
			histogramDisplayArea.add(histogram);
		}
		histogramDisplayArea.revalidate();
		histogramDisplayArea.repaint();
	}

	@Override
	public BufferedImage getQuestions() {
		return LabelPanel.createImage(dataMaker.getQuestionsRepresentations(), 295);
	}
}
