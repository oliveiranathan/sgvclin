/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI.diatopic;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.logging.Level;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import sgvclin.UI.StatusBarImpl;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan Oliveira
 */
public class SelectionTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;

	private final String[] varsStrs;
	private boolean[] checkBoxes;
	private final SelectionTableUser panel;
	private final boolean isHighlander;

	SelectionTableModel(String[] varsStrs, SelectionTableUser panel) {
		this.varsStrs = varsStrs;
		this.panel = panel;
		checkBoxes = new boolean[varsStrs.length];
		isHighlander = false;
		fireTableStructureChanged();
		fireTableDataChanged();
	}

	public SelectionTableModel(String[] varsStrs, SelectionTableUser panel, boolean isHighlander) {
		this.varsStrs = varsStrs;
		this.panel = panel;
		checkBoxes = new boolean[varsStrs.length];
		this.isHighlander = isHighlander;
		fireTableStructureChanged();
		fireTableDataChanged();
	}

	@Override
	public int getRowCount() {
		return varsStrs.length;
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return (columnIndex == 0) ? checkBoxes[rowIndex] : varsStrs[rowIndex];
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex == 0;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return (columnIndex == 0) ? Boolean.class : String.class;
	}

	@Override
	public String getColumnName(int column) {
		return (column == 0)
				? java.util.ResourceBundle.getBundle("texts/Bundle").getString("DISPLAY")
				: java.util.ResourceBundle.getBundle("texts/Bundle").getString("VARIANT");
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		boolean value = (boolean) aValue;
		if (checkBoxes[rowIndex] != value) {
			if (isHighlander) {
				checkBoxes = new boolean[varsStrs.length];
			}
			checkBoxes[rowIndex] = value;
			fireTableDataChanged();
			StatusBarImpl.getInstance().showIndProgress();
			try {
				if (!SwingUtilities.isEventDispatchThread()) {
					SwingUtilities.invokeAndWait(() -> panel.setVars(checkBoxes));
				} else {
					panel.setVars(checkBoxes);
				}
			} catch (InterruptedException | InvocationTargetException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
			StatusBarImpl.getInstance().hideProgress();
		}
		panel.resetFocus();
	}

	public void setSelected(boolean[] chb) {
		if (chb != null) {
			checkBoxes = Arrays.copyOf(chb, chb.length);
		} else {
			checkBoxes = new boolean[varsStrs.length];
		}
		fireTableDataChanged();
		panel.setVars(checkBoxes);
	}
}
