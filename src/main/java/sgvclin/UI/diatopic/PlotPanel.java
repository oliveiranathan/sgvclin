/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI.diatopic;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.coobird.thumbnailator.Thumbnailator;
import net.sf.dynamicreports.report.exception.DRException;
import org.jfree.chart.plot.Plot;
import sgvclin.UI.DataSelectionUtils;
import sgvclin.UI.StatusBarImpl;
import sgvclin.data.MapSaver;
import sgvclin.processors.LoggerManager;
import sgvclin.processors.MapMaker;

/**
 *
 * @author Nathan Oliveira
 */
public class PlotPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private BufferedImage mapOriginal = null;
	private BufferedImage mapCurrent = null;
	private BufferedImage uiDefault;
	private BufferedImage uiPies;
	private float zoomLevel = 1.f;
	private int xIni = 0, yIni = 0;
	private int height = 0, width = 0;
	private boolean showMenu = false;
	private int defaultPlotSize = 0;
	private HashMap<Point2D.Double, List<Plot>> plots = null;
	private HashMap<Point2D.Double, List<BufferedImage>> scaledPies = null;
	private HashMap<Point2D.Double, Point2D.Double> deltas = null;
	private HashMap<Rectangle, Point2D.Double> areasToPoints = null;
	private PiePanelUser user = null;
	private Point2D.Double lockedPoint = null;
	private boolean started = false;
	private JPopupMenu popUp = null;

	public PlotPanel(BufferedImage mapa, HashMap<Point2D.Double, Point2D.Double> _deltas) {
		try {
			uiDefault = ImageIO.read(getClass().getResource("/ui.png"));
			uiPies = ImageIO.read(getClass().getResource("/ui_pie.png"));
		} catch (IOException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			uiDefault = null;
			uiPies = null;
		}
		mapOriginal = mapa;
		if (_deltas != null) {
			deltas = new HashMap<>(_deltas.size());
			_deltas.keySet().stream().forEach((p2d) -> {
				deltas.put(p2d, new Point2D.Double(_deltas.get(p2d).x, _deltas.get(p2d).y));
			});
		}
		mapCurrent = mapOriginal;
		popUp = new JPopupMenu();
		JMenuItem save = new JMenuItem(java.util.ResourceBundle.getBundle("texts/Bundle").getString("SAVE IMAGE..."));
		popUp.add(save);
		save.addActionListener((e) -> {
			JFileChooser fc = new JFileChooser();
			fc.setFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PDF FILE"), "PDF", "pdf"));
			fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PNG IMAGE"), "png"));
			if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				BufferedImage image = new BufferedImage(mapOriginal.getWidth(),
						mapOriginal.getHeight(), BufferedImage.TYPE_INT_RGB);
				saveMap(image.getGraphics());
				File f = fc.getSelectedFile();
				if (!f.getAbsolutePath().endsWith(".pdf") && !f.getAbsolutePath().endsWith(".png")) {
					f = new File(f.getAbsolutePath() + ".png");
				}
				try {
					MapMaker.saveImage(image, f);
					if (Desktop.isDesktopSupported()) {
						try {
							Desktop.getDesktop().open(f);
						} catch (IOException ex) {
							LoggerManager.getInstance().log(Level.SEVERE, null, ex);
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
						}
					} else {
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
					}
				} catch (DRException | IOException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				}
			}
		});
		JMenuItem loadPositions = new JMenuItem(java.util.ResourceBundle.getBundle("texts/Bundle").getString("LOAD PIES POSITIONS"));
		popUp.add(loadPositions);
		loadPositions.addActionListener((e) -> {
			MapSaver ms = DataSelectionUtils.selectMapSaver(true);
			if (ms == null) {
				return;
			}
			if (ms.getDeltas() == null) {
				return;
			}
			defaultPlotSize = ms.getRadius();
			HashMap<Point2D.Double, Point2D.Double> newDs = ms.getDeltas();
			deltas = new HashMap<>(newDs.size());
			newDs.keySet().stream().forEach((key) -> {
				Point2D.Double value = newDs.get(key);
				deltas.put(key, new Point2D.Double(value.x, value.y));
			});
			scalePies();
			repaint();
		});
		addComponentListener(new ComponentListener() {
			@Override
			public void componentResized(ComponentEvent e) {
				height = getSize().height;
				width = getSize().width;
				repaint();
			}

			@Override
			public void componentMoved(ComponentEvent e) {
				repaint();
			}

			@Override
			public void componentShown(ComponentEvent e) {
				repaint();
			}

			@Override
			public void componentHidden(ComponentEvent e) {
				repaint();
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				showMenu = e.getX() < 90 && e.getY() < 133;
				if (lockedPoint == null) {
					int x = e.getX(), y = e.getY();
					displayPlot(null);
					if (areasToPoints != null) {
						for (Map.Entry<Rectangle, Point2D.Double> rect : areasToPoints.entrySet()) {
							if (rect.getKey().contains(x, y)) {
								displayPlot(rect.getValue());
								break;
							}
						}
					}
				}
				repaint();
			}
		});
		addMouseWheelListener(new MouseAdapter() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.getWheelRotation() > 0) {
					zoomOut();
				} else if (e.getWheelRotation() < 0) {
					zoomIn();
				}
				repaint();
			}
		});
		addMouseListener(new MouseAdapter() {
			private int xini, yini;

			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showPopUp(e);
				}
				if (SwingUtilities.isMiddleMouseButton(e)) {
					xini = e.getX();
					yini = e.getY();
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showPopUp(e);
				}
				if (SwingUtilities.isMiddleMouseButton(e)) {
					xIni += xini - e.getX();
					yIni += yini - e.getY();
					if (xIni < 0) {
						xIni = 0;
					}
					if (yIni < 0) {
						yIni = 0;
					}
					repaint();
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int x = e.getX(), y = e.getY();
				if (lockedPoint == null) {
					displayPlot(null);
				}
				if (y > 77 && y < 111) {
					if (x > 9 && x < 42) {
						if (lockedPoint == null) {
							zoomOut();
						} else {
							int inc = Math.round(1 / zoomLevel);
							defaultPlotSize -= inc;
							scalePies();
							repaint();
						}
						return;
					} else if (x > 43 && x < 77) {
						if (lockedPoint == null) {
							zoomIn();
						} else {
							int inc = Math.round(1 / zoomLevel);
							defaultPlotSize += inc;
							scalePies();
							repaint();
						}
						return;
					}
				} else if (y > 15 && y < 33 && x > 33 && x < 53) {
					if (lockedPoint != null) {
						Point2D.Double p = deltas.get(lockedPoint);
						p.y -= Math.round(1 / zoomLevel);
						deltas.put(lockedPoint, p);
						repaint();
					} else {
						yIni -= 10;
						if (yIni < 0) {
							yIni = 0;
						}
					}
					repaint();
					return;
				} else if (y > 51 && y < 71 && x > 33 && x < 53) {
					if (lockedPoint != null) {
						Point2D.Double p = deltas.get(lockedPoint);
						p.y += Math.round(1 / zoomLevel);
						deltas.put(lockedPoint, p);
						repaint();
					} else {
						yIni += 10;
						if (mapCurrent.getHeight() - yIni < height) {
							yIni = mapCurrent.getHeight() - height;
						}
					}
					repaint();
					return;
				} else if (y > 33 && y < 51) {
					if (x > 13 && x < 33) {
						if (lockedPoint != null) {
							Point2D.Double p = deltas.get(lockedPoint);
							p.x -= Math.round(1 / zoomLevel);
							deltas.put(lockedPoint, p);
							repaint();
						} else {
							xIni -= 10;
							if (xIni < 0) {
								xIni = 0;
							}
						}
						repaint();
						return;
					} else if (x > 53 && x < 73) {
						if (lockedPoint != null) {
							Point2D.Double p = deltas.get(lockedPoint);
							p.x += Math.round(1 / zoomLevel);
							deltas.put(lockedPoint, p);
							repaint();
						} else {
							xIni += 10;
							if (mapCurrent.getWidth() - xIni < width) {
								xIni = mapCurrent.getWidth() - width;
							}
						}
						repaint();
						return;
					}
				}
				if (SwingUtilities.isLeftMouseButton(e)) {
					lockedPoint = null;
					StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO PIE SELECTED"));
					if (areasToPoints != null) {
						for (Map.Entry<Rectangle, Point2D.Double> rect : areasToPoints.entrySet()) {
							if (rect.getKey().contains(x, y)) {
								displayPlot(rect.getValue());
								lockedPoint = rect.getValue();
								StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PIE SELECTED"));
								break;
							}
						}
					}
				}
			}
		});
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyChar() == '+' || e.getKeyChar() == '=') {
					if (lockedPoint != null) {
						int inc = Math.round(1 / zoomLevel);
						defaultPlotSize += inc;
						scalePies();
						repaint();
					} else {
						zoomIn();
					}
				} else if (e.getKeyChar() == '-') {
					if (lockedPoint != null) {
						int inc = Math.round(1 / zoomLevel);
						defaultPlotSize -= inc;
						scalePies();
						repaint();
					} else {
						zoomOut();
					}
				} else {
					switch (e.getExtendedKeyCode()) {
						case KeyEvent.VK_UP:
							if (lockedPoint != null) {
								Point2D.Double p = deltas.get(lockedPoint);
								p.y -= Math.round(1 / zoomLevel);
								deltas.put(lockedPoint, p);
								repaint();
							} else {
								yIni -= 10;
								if (yIni < 0) {
									yIni = 0;
								}
							}
							break;
						case KeyEvent.VK_DOWN:
							if (lockedPoint != null) {
								Point2D.Double p = deltas.get(lockedPoint);
								p.y += Math.round(1 / zoomLevel);
								deltas.put(lockedPoint, p);
								repaint();
							} else {
								yIni += 10;
								if (mapCurrent.getHeight() - yIni < height) {
									yIni = mapCurrent.getHeight() - height;
								}
							}
							break;
						case KeyEvent.VK_LEFT:
							if (lockedPoint != null) {
								Point2D.Double p = deltas.get(lockedPoint);
								p.x -= Math.round(1 / zoomLevel);
								deltas.put(lockedPoint, p);
								repaint();
							} else {
								xIni -= 10;
								if (xIni < 0) {
									xIni = 0;
								}
							}
							break;
						case KeyEvent.VK_RIGHT:
							if (lockedPoint != null) {
								Point2D.Double p = deltas.get(lockedPoint);
								p.x += Math.round(1 / zoomLevel);
								deltas.put(lockedPoint, p);
								repaint();
							} else {
								xIni += 10;
								if (mapCurrent.getWidth() - xIni < width) {
									xIni = mapCurrent.getWidth() - width;
								}
							}
							break;
					}
					repaint();
				}
			}
		});
		setFocusable(true);
		requestFocus();
	}

	private void showPopUp(MouseEvent e) {
		popUp.show(e.getComponent(), e.getX(), e.getY());
	}

	public void zoomOut() {
		zoomLevel -= 0.1f;
		if (zoomLevel <= 0.0f) {
			zoomLevel = 0.1f;
		}
		float perX = (float) xIni / mapCurrent.getWidth();
		float perY = (float) yIni / mapCurrent.getHeight();
		mapCurrent = Thumbnailator.createThumbnail(mapOriginal,
				Math.round(zoomLevel * mapOriginal.getWidth()),
				Math.round(zoomLevel * mapOriginal.getHeight()));
		xIni = Math.round(perX * mapCurrent.getWidth());
		yIni = Math.round(perY * mapCurrent.getHeight());
		if ((mapCurrent.getWidth() - xIni) < width) {
			if (mapCurrent.getWidth() >= width) {
				xIni = mapCurrent.getWidth() - width;
			} else {
				xIni = 0;
			}
		}
		if ((mapCurrent.getHeight() - yIni) < height) {
			if (mapCurrent.getHeight() >= height) {
				yIni = mapCurrent.getHeight() - height;
			} else {
				yIni = 0;
			}
		}
		scalePies();
		repaint();
	}

	public void zoomIn() {
		zoomLevel += 0.1f;
		if (zoomLevel > 1.0f) {
			zoomLevel = 1.0f;
		}
		float perX = (float) xIni / mapCurrent.getWidth();
		float perY = (float) yIni / mapCurrent.getHeight();
		mapCurrent = Thumbnailator.createThumbnail(mapOriginal,
				Math.round(zoomLevel * mapOriginal.getWidth()),
				Math.round(zoomLevel * mapOriginal.getHeight()));
		xIni = Math.round(perX * mapCurrent.getWidth());
		yIni = Math.round(perY * mapCurrent.getHeight());
		scalePies();
		repaint();
	}

	private void scalePies() {
		int radius = Math.round(defaultPlotSize * zoomLevel);
		if (plots != null) {
			scaledPies = new HashMap<>(plots.size());
			plots.entrySet().stream().forEach((set) -> {
				List<BufferedImage> listImgs = new ArrayList<>(set.getValue().size());
				set.getValue().stream().forEach((plot) -> {
					if (plot != null) {
						BufferedImage img = new BufferedImage(radius, radius, BufferedImage.TYPE_INT_ARGB);
						plot.draw(img.createGraphics(), new Rectangle(0, 0, radius, radius), null, null, null);
						listImgs.add(img);
					} else {
						listImgs.add(null);
					}
				});
				scaledPies.put(set.getKey(), listImgs);
			});
		}
	}

	private void showMenu(Graphics g) {
		if (lockedPoint == null) {
			g.drawImage(uiDefault, 5, 5, null);
		} else {
			g.drawImage(uiPies, 5, 5, null);
		}
	}

	public int getH() {
		return mapOriginal.getHeight();
	}

	public int getW() {
		return mapOriginal.getWidth();
	}

	public HashMap<Point2D.Double, Point2D.Double> getDeltas() {
		return new HashMap<>(deltas);
	}

	protected void setPlots(HashMap<Point2D.Double, List<Plot>> plots, int plotSize, PiePanelUser user) {
		if (plots == null) {
			this.plots = null;
			deltas = null;
			this.user = null;
			areasToPoints = null;
			return;
		}
		if (defaultPlotSize == 0) {
			defaultPlotSize = plotSize;
		}
		this.plots = new HashMap<>(plots.size());
		plots.keySet().stream().forEach((key) -> this.plots.put(key, new ArrayList<>(plots.get(key))));
		scalePies();
		if (deltas == null) {
			deltas = new HashMap<>(plots.size());
			plots.keySet().stream().forEach((p) -> {
				deltas.put(p, new Point2D.Double(0, 0));
			});
		}
		this.user = user;
		repaint();
	}

	protected void displayPlot(Point2D.Double point) {
		if (user != null) {
			user.displayPlots(point);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (!started) {
			started = true;
			float x = (float) getWidth() / mapOriginal.getWidth();
			float y = (float) getHeight() / mapOriginal.getHeight();
			zoomLevel = ((x < y) ? x : y) - 0.1f;
			zoomIn();
		}
		g.drawImage(mapCurrent, 0, 0, width, height, xIni, yIni, xIni + width, yIni + height, null);
		if (defaultPlotSize > 0 && plots != null && plots.size() > 0) {
			if (areasToPoints == null) {
				areasToPoints = new HashMap<>(scaledPies.size());
			} else {
				areasToPoints.clear();
			}
			int dist = Math.round(zoomLevel * getGap());
			int sizePies = Math.round(defaultPlotSize * zoomLevel);
			int deltaOval = Math.round(sizePies * 0.1f);
			int sizePiesO = Math.round(sizePies * 0.9f);
			scaledPies.entrySet().stream().forEach((set) -> {
				int baseX = (int) Math.round((set.getKey().x + deltas.get(set.getKey()).x)
						* zoomLevel - xIni);
				int baseY = (int) Math.round((set.getKey().y + deltas.get(set.getKey()).y)
						* zoomLevel - yIni);
				for (int i = 0; i < set.getValue().size(); i++) {
					Rectangle rect = new Rectangle();
					rect.x = baseX + (i % 2 == 0 ? 0 : sizePies + dist);
					if (set.getValue().size() % 2 == 1 && set.getValue().size() - 1 == i && set.getValue().size() != 1) {
						rect.x += (sizePies + dist) / 2;
					}
					rect.y = baseY + i / 2 * (sizePies + dist);
					rect.height = sizePies;
					rect.width = sizePies;
					areasToPoints.put(rect, set.getKey());
					if (set.getValue().get(i) != null) {
						g.drawImage(set.getValue().get(i), rect.x, rect.y, null);
					} else {
						g.drawOval(rect.x + deltaOval, rect.y + deltaOval, sizePiesO - deltaOval, sizePiesO - deltaOval);
					}
				}
				if (set.getValue().size() > 1) {
					g.setColor(Color.BLACK);
					int numberOfLines = (set.getValue().size() + 1) / 2;
					boolean isOdd = set.getValue().size() % 2 == 1;
					int vertX = baseX + sizePies + dist;
					int vertYS = baseY + dist;
					int vertYE = baseY + numberOfLines * (sizePies + dist);
					if (isOdd) {
						vertYE -= sizePies + dist;
					}
					g.drawLine(vertX, vertYS, vertX, vertYE);
					for (int i = 1; i < numberOfLines; i++) {
						int horY = baseY + (sizePies + dist) * i;
						int horXS = baseX + dist;
						int horXE = baseX + (sizePies + dist) * 2;
						g.drawLine(horXS, horY, horXE, horY);
					}
				}
			});
		}
		if (showMenu || lockedPoint != null) {
			showMenu(g);
		}
	}

	protected final void saveMap(Graphics g) {
		float zoom = zoomLevel;
		zoomLevel = 1f;
		scalePies();
		g.drawImage(mapOriginal, 0, 0, null);
		if (defaultPlotSize > 0 && plots != null && plots.size() > 0) {
			int dist = getGap();
			int sizePies = defaultPlotSize;
			int deltaOval = Math.round(sizePies * 0.1f);
			int sizePiesO = Math.round(sizePies * 0.9f);
			scaledPies.entrySet().stream().forEach((set) -> {
				int baseX = (int) Math.round(set.getKey().x + deltas.get(set.getKey()).x);
				int baseY = (int) Math.round(set.getKey().y + deltas.get(set.getKey()).y);
				for (int i = 0; i < set.getValue().size(); i++) {
					int x = baseX + (i % 2 == 0 ? 0 : sizePies + dist);
					if (set.getValue().size() % 2 == 1 && set.getValue().size() - 1 == i && set.getValue().size() != 1) {
						x += (sizePies + dist) / 2;
					}
					int y = baseY + i / 2 * (sizePies + dist);
					if (set.getValue().get(i) != null) {
						g.drawImage(set.getValue().get(i), x, y, null);
					} else {
						g.drawOval(x + deltaOval, y + deltaOval, sizePiesO - deltaOval, sizePiesO - deltaOval);
					}
				}
				if (set.getValue().size() > 1) {
					g.setColor(Color.BLACK);
					int numberOfLines = (set.getValue().size() + 1) / 2;
					boolean isOdd = set.getValue().size() % 2 == 1;
					int vertX = baseX + sizePies + dist;
					int vertYS = baseY + dist;
					int vertYE = baseY + numberOfLines * (sizePies + dist);
					if (isOdd) {
						vertYE -= sizePies + dist;
					}
					g.drawLine(vertX, vertYS, vertX, vertYE);
					for (int i = 1; i < numberOfLines; i++) {
						int horY = baseY + (sizePies + dist) * i;
						int horXS = baseX + dist;
						int horXE = baseX + (sizePies + dist) * 2;
						g.drawLine(horXS, horY, horXE, horY);
					}
				}
			});
		}
		zoomLevel = zoom;
		scalePies();
	}

	public int getPlotSize() {
		return defaultPlotSize;
	}

	public int getGap() {
		return -1;
	}
}
