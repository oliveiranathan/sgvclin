/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import net.coobird.thumbnailator.Thumbnailator;
import sgvclin.processors.TemplateWorker;

/**
 * @author Nathan Oliveira
 */
public class TemplateEditorPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private final BufferedImage originalMap;
	private BufferedImage currentMap;
	private BufferedImage template, currentTemplate;
	private boolean initializated = false;
	private double zoomLevel;
	private int width, height, xIni, yIni;
	private final TemplatePanel uiPanel;
	private TemplateWorker worker;
	private final Cursor pen;
	private final Cursor eraser;
	private final Cursor line;
	private final Cursor bucket;
	private final Cursor move;
	private final List<Point> drawPoints = new ArrayList<>(50);

	public TemplateEditorPanel(BufferedImage mapa, TemplatePanel uiPanel) {
		pen = Toolkit.getDefaultToolkit().createCustomCursor(Toolkit.getDefaultToolkit().createImage(getClass().getResource("/icons/pen32.png")), new Point(2, 28), "pen");
		eraser = Toolkit.getDefaultToolkit().createCustomCursor(Toolkit.getDefaultToolkit().createImage(getClass().getResource("/icons/eraser32.png")), new Point(7, 28), "eraser");
		line = Toolkit.getDefaultToolkit().createCustomCursor(Toolkit.getDefaultToolkit().createImage(getClass().getResource("/icons/line32.png")), new Point(1, 29), "line");
		bucket = Toolkit.getDefaultToolkit().createCustomCursor(Toolkit.getDefaultToolkit().createImage(getClass().getResource("/icons/bucket32.png")), new Point(30, 29), "bucket");
		move = Toolkit.getDefaultToolkit().createCustomCursor(Toolkit.getDefaultToolkit().createImage(getClass().getResource("/icons/move32.png")), new Point(16, 15), "move");
		this.originalMap = mapa;
		this.uiPanel = uiPanel;
		xIni = 0;
		yIni = 0;
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				width = getSize().width;
				height = getSize().height;
				repaint();
			}
		});
		MouseAdapter ma = new MouseAdapter() {
			private int xini, yini;
			private List<Point> points;
			private boolean add = false;

			@Override
			public void mouseMoved(MouseEvent e) {
				switch (worker.getMode()) {
					case PEN:
						setCursor(pen);
						break;
					case LINE:
						setCursor(line);
						break;
					case BUCKET:
						setCursor(bucket);
						break;
					case ERASER:
						setCursor(eraser);
						break;
					case MOVE:
						setCursor(move);
						break;
					case NONE:
						setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
						break;
					default:
						break;
				}
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				if (add) {
					points.add(calcPoint(e));
				}
				if (points != null && !points.isEmpty()) {
					drawPoints.clear();
					switch (worker.getMode()) {
						case LINE: {
							Point pE = calcPoint(e);
							drawPoints.addAll(points);
							drawPoints.add(pE);
							break;
						}
						case PEN:
						case ERASER:
							drawPoints.addAll(points);
							break;
					}
					repaint();
				}
				if (worker.getMode() == TemplateWorker.Mode.MOVE) {
					xIni += xini - e.getX();
					yIni += yini - e.getY();
					xini = e.getX();
					yini = e.getY();
					if (xIni < 0) {
						xIni = 0;
					}
					if (yIni < 0) {
						yIni = 0;
					}
					repaint();
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				if (worker.getMode() == TemplateWorker.Mode.MOVE) {
					xini = e.getX();
					yini = e.getY();
				}
				if (SwingUtilities.isLeftMouseButton(e)) {
					switch (worker.getMode()) {
						case LINE:
							points = new ArrayList<>(2);
							points.add(calcPoint(e));
							break;
						case PEN:
						case ERASER:
							points = new ArrayList<>(50);
							points.add(calcPoint(e));
							add = true;
							break;
						case BUCKET:
						case NONE:
						default:
							break;
					}
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				new Thread() {
					@Override
					public void run() {
						StatusBarImpl.getInstance().showIndProgress();
						if (worker.getMode() == TemplateWorker.Mode.MOVE) {
							xIni += xini - e.getX();
							yIni += yini - e.getY();
							if (xIni < 0) {
								xIni = 0;
							}
							if (yIni < 0) {
								yIni = 0;
							}
							repaint();
						}
						if (SwingUtilities.isLeftMouseButton(e)) {
							switch (worker.getMode()) {
								case BUCKET:
									points = new ArrayList<>(1);
									points.add(calcPoint(e));
									worker.useTool(points.toArray(new Point[points.size()]));
									break;
								case LINE:
									points.add(calcPoint(e));
									worker.useTool(points.toArray(new Point[points.size()]));
									break;
								case ERASER:
								case PEN:
									add = false;
									points.add(calcPoint(e));
									worker.useTool(points.toArray(new Point[points.size()]));
									break;
								case NONE:
								default:
									break;
							}
						}
						StatusBarImpl.getInstance().hideProgress();
					}
				}.start();
				drawPoints.clear();
			}
		};
		addMouseListener(ma);
		addMouseMotionListener(ma);
	}

	private Point calcPoint(MouseEvent e) {
		int x = xIni + e.getX();
		int y = yIni + e.getY();
		double x_ = x / zoomLevel;
		double y_ = y / zoomLevel;
		x = (int) Math.round(x_);
		y = (int) Math.round(y_);
		return new Point(x, y);
	}

	protected void setWorker(TemplateWorker worker) {
		this.worker = worker;
	}

	private void adjustZoomImgs() {
		currentMap = Thumbnailator.createThumbnail(originalMap,
				(int) Math.round(zoomLevel * originalMap.getWidth()),
				(int) Math.round(zoomLevel * originalMap.getHeight()));
		if (template != null) {
			currentTemplate = Thumbnailator.createThumbnail(template,
					(int) Math.round(zoomLevel * template.getWidth()),
					(int) Math.round(zoomLevel * template.getHeight()));
		}
	}

	public void setTemplate(BufferedImage template) {
		this.template = template;
		adjustZoomImgs();
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (!initializated) {
			initializated = true;
			double x = (double) getWidth() / originalMap.getWidth();
			double y = (double) getHeight() / originalMap.getHeight();
			setZoomLevel((x < y) ? x : y);
			uiPanel.setZoomSlider(zoomLevel);
		}
		g.drawImage(currentMap, 0, 0, width, height, xIni, yIni, xIni + width, yIni + height, null);
		if (currentTemplate != null) {
			g.drawImage(currentTemplate, 0, 0, width, height, xIni, yIni, xIni + width, yIni + height, null);
		}
		if (!drawPoints.isEmpty()) {
			switch (worker.getMode()) {
				case LINE:
					g.setColor(Color.BLUE);
					g.drawLine((int) (drawPoints.get(0).x * zoomLevel), (int) (drawPoints.get(0).y * zoomLevel),
							(int) (drawPoints.get(1).x * zoomLevel), (int) (drawPoints.get(1).y * zoomLevel));
					break;
				case PEN:
					g.setColor(Color.BLUE);
					for (int i = 0; i < drawPoints.size() - 1; i++) {
						g.drawLine((int) (drawPoints.get(i).x * zoomLevel), (int) (drawPoints.get(i).y * zoomLevel),
								(int) (drawPoints.get(i + 1).x * zoomLevel), (int) (drawPoints.get(i + 1).y * zoomLevel));
					}
					break;
				case ERASER:
					g.setColor(Color.PINK);
					for (int i = 0; i < drawPoints.size() - 1; i++) {
						g.drawLine((int) (drawPoints.get(i).x * zoomLevel), (int) (drawPoints.get(i).y * zoomLevel),
								(int) (drawPoints.get(i + 1).x * zoomLevel), (int) (drawPoints.get(i + 1).y * zoomLevel));
					}
					break;
			}
		}
	}

	public void setZoomLevel(double zoom) {
		zoomLevel = zoom;
		adjustZoomLevel();
		adjustZoomImgs();
		if (currentMap.getWidth() - xIni <= width) {
			xIni = 0;
		}
		if (currentMap.getHeight() - yIni <= height) {
			yIni = 0;
		}
		repaint();
	}

	private void adjustZoomLevel() {
		double[] values = {1.0 / 4.0, 1.0 / 2.0, 3.0 / 4.0, 1.0, 2.0, 4.0, 6.0, 8.0};
		if (zoomLevel >= 8.0) {
			zoomLevel = 8.0;
			return;
		}
		if (zoomLevel <= 1.0 / 4.0 + 0.001) {
			zoomLevel = 1.0 / 4.0;
			return;
		}
		for (int i = 0; i < values.length - 1; i++) {
			if (zoomLevel >= values[i] && zoomLevel <= values[i + 1]) {
				zoomLevel = values[i + 1];
				return;
			}
		}
	}
}
