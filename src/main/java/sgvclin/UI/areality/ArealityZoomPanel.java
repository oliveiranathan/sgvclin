/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI.areality;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.coobird.thumbnailator.Thumbnailator;
import net.sf.dynamicreports.report.exception.DRException;
import sgvclin.UI.StatusBarImpl;
import sgvclin.processors.LoggerManager;
import sgvclin.processors.MapMaker;

/**
 * @author Nathan Oliveira
 */
public class ArealityZoomPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private BufferedImage originalMap;
	private BufferedImage originalOverlay;
	private BufferedImage currentMap = null;
	private BufferedImage currentOverlay = null;
	private float zoomLevel;
	private int xIni, yIni;
	private int height, width;
	private boolean showMenu = false;
	private Polygon tUp, tDw, tLf, tRg;
	private boolean started = false;
	private JPopupMenu popUp;
	private final double maxDistance;
	private Point2D.Double pontoTravado = null;
	private final ArealityPanel arealityPanel;

	public ArealityZoomPanel(BufferedImage mapa, final List<Point2D.Double> points, ArealityPanel arealityPanel) {
		originalMap = mapa;
		currentMap = originalMap;
		zoomLevel = 1.f;
		xIni = 0;
		yIni = 0;
		height = 0;
		width = 0;
		this.arealityPanel = arealityPanel;
		double max = 0;
		for (Point2D.Double p2d : points) {
			double min = Double.MAX_VALUE;
			for (Point2D.Double p2d_ : points) {
				if (p2d.equals(p2d_)) {
					continue;
				}
				double m = p2d.distance(p2d_);
				if (min > m) {
					min = m;
				}
			}
			if (max < min) {
				max = min;
			}
		}
		maxDistance = max / 2;
		popUp = new JPopupMenu();
		JMenuItem save = new JMenuItem(java.util.ResourceBundle.getBundle("texts/Bundle").getString("SAVE IMAGE..."));
		popUp.add(save);
		save.addActionListener((evt) -> {
			JFileChooser fc = new JFileChooser();
			fc.setFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PDF FILE"), "PDF", "pdf"));
			fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PNG IMAGE"), "png"));
			if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				BufferedImage image = new BufferedImage(originalMap.getWidth(),
						originalMap.getHeight(), BufferedImage.TYPE_INT_RGB);
				saveMap(image.getGraphics());
				File f = fc.getSelectedFile();
				if (!f.getAbsolutePath().endsWith(".pdf") && !f.getAbsolutePath().endsWith(".png")) {
					f = new File(f.getAbsolutePath() + ".png");
				}
				try {
					MapMaker.saveImage(image, f);
					if (Desktop.isDesktopSupported()) {
						try {
							Desktop.getDesktop().open(f);
						} catch (IOException ex) {
							LoggerManager.getInstance().log(Level.SEVERE, null, ex);
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
						}
					} else {
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
					}
				} catch (DRException | IOException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				}
			}
		});
		createPolygons();
		addComponentListener(new ComponentListener() {
			@Override
			public void componentResized(ComponentEvent e) {
				height = getSize().height;
				width = getSize().width;
				repaint();
			}

			@Override
			public void componentMoved(ComponentEvent e) {
				repaint();
			}

			@Override
			public void componentShown(ComponentEvent e) {
				repaint();
			}

			@Override
			public void componentHidden(ComponentEvent e) {
				repaint();
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				showMenu = e.getX() < 90 && e.getY() < 135;
				repaint();
				if (pontoTravado != null || points == null) {
					return;
				}
				double minDistance = Double.MAX_VALUE;
				Point2D.Double pSel = null, pAtual = new Point2D.Double((e.getX() + xIni) / zoomLevel, (e.getY() + yIni) / zoomLevel);
				for (Point2D.Double p2d : points) {
					double dist = p2d.distance(pAtual);
					if (minDistance > dist) {
						minDistance = dist;
						pSel = p2d;
					}
				}
				if (minDistance < maxDistance) {
					displayData(pSel);
				}
			}
		});
		addMouseWheelListener(new MouseAdapter() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.getWheelRotation() > 0) {
					zoomOut();
				} else if (e.getWheelRotation() < 0) {
					zoomIn();
				}
				repaint();
			}
		});
		addMouseListener(new MouseAdapter() {
			private int xini, yini;

			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showPopUp(e);
				}
				if (SwingUtilities.isMiddleMouseButton(e)) {
					xini = e.getX();
					yini = e.getY();
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showPopUp(e);
				}
				if (SwingUtilities.isMiddleMouseButton(e)) {
					xIni += xini - e.getX();
					yIni += yini - e.getY();
					if (xIni < 0) {
						xIni = 0;
					}
					if (yIni < 0) {
						yIni = 0;
					}
					repaint();
				}
				requestFocusInWindow();
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int x = e.getX(), y = e.getY();
				if (SwingUtilities.isLeftMouseButton(e)) {
					pontoTravado = null;
					displayData(null);
					StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO AREA SELECTED"));
					double minDistance = Double.MAX_VALUE;
					Point2D.Double pSel = null, pAtual = new Point2D.Double((e.getX() + xIni) / zoomLevel, (e.getY() + yIni) / zoomLevel);
					for (Point2D.Double p2d : points) {
						double dist = p2d.distance(pAtual);
						if (minDistance > dist) {
							minDistance = dist;
							pSel = p2d;
						}
					}
					if (minDistance < maxDistance) {
						pontoTravado = pSel;
						StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("AREA SELECTED"));
						displayData(pSel);
					}
				}
				if (x > 10 && x < 40 && y > 90 && y < 120) {
					zoomIn();
				} else if (x > 50 && x < 80 && y > 90 && y < 120) {
					zoomOut();
				} else {
					if (tUp.contains(x, y)) {
						yIni -= 10;
						if (yIni < 0) {
							yIni = 0;
						}
					} else if (tDw.contains(x, y)) {
						yIni += 10;
						if (currentMap.getHeight() - yIni < height) {
							yIni = currentMap.getHeight() - height;
						}
					} else if (tLf.contains(x, y)) {
						xIni -= 10;
						if (xIni < 0) {
							xIni = 0;
						}
					} else if (tRg.contains(x, y)) {
						xIni += 10;
						if (currentMap.getWidth() - xIni < width) {
							xIni = currentMap.getWidth() - width;
						}
					} else {
						return;
					}
					repaint();
				}
			}
		});
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyChar() == '+' || e.getKeyChar() == '=') {
					zoomIn();
				} else if (e.getKeyChar() == '-') {
					zoomOut();
				} else {
					switch (e.getExtendedKeyCode()) {
						case KeyEvent.VK_UP:
							yIni -= 10;
							if (yIni < 0) {
								yIni = 0;
							}
							break;
						case KeyEvent.VK_DOWN:
							yIni += 10;
							if (currentMap.getHeight() - yIni < height) {
								yIni = currentMap.getHeight() - height;
							}
							break;
						case KeyEvent.VK_LEFT:
							xIni -= 10;
							if (xIni < 0) {
								xIni = 0;
							}
							break;
						case KeyEvent.VK_RIGHT:
							xIni += 10;
							if (currentMap.getWidth() - xIni < width) {
								xIni = currentMap.getWidth() - width;
							}
							break;
					}
					repaint();
				}
			}
		});
		setFocusable(true);
		requestFocus();
	}

	private void displayData(Point2D.Double point) {
		arealityPanel.updatePoint(point);
	}

	private void showPopUp(MouseEvent e) {
		popUp.show(e.getComponent(), e.getX(), e.getY());
	}

	private void zoomOut() {
		zoomLevel -= 0.1f;
		if (zoomLevel <= 0.0f) {
			zoomLevel = 0.1f;
		}
		float perX = (float) xIni / currentMap.getWidth();
		float perY = (float) yIni / currentMap.getHeight();
		currentMap = Thumbnailator.createThumbnail(originalMap,
				Math.round(zoomLevel * originalMap.getWidth()),
				Math.round(zoomLevel * originalMap.getHeight()));
		if (originalOverlay != null) {
			currentOverlay = Thumbnailator.createThumbnail(originalOverlay,
					Math.round(zoomLevel * originalMap.getWidth()),
					Math.round(zoomLevel * originalMap.getHeight()));
		}
		xIni = Math.round(perX * currentMap.getWidth());
		yIni = Math.round(perY * currentMap.getHeight());
		if ((currentMap.getWidth() - xIni) < width) {
			if (currentMap.getWidth() >= width) {
				xIni = currentMap.getWidth() - width;
			} else {
				xIni = 0;
			}
		}
		if ((currentMap.getHeight() - yIni) < height) {
			if (currentMap.getHeight() >= height) {
				yIni = currentMap.getHeight() - height;
			} else {
				yIni = 0;
			}
		}
		repaint();
	}

	private void zoomIn() {
		zoomLevel += 0.1f;
		if (zoomLevel > 1.0f) {
			zoomLevel = 1.0f;
		}
		float perX = (float) xIni / currentMap.getWidth();
		float perY = (float) yIni / currentMap.getHeight();
		currentMap = Thumbnailator.createThumbnail(originalMap,
				Math.round(zoomLevel * originalMap.getWidth()),
				Math.round(zoomLevel * originalMap.getHeight()));
		if (originalOverlay != null) {
			currentOverlay = Thumbnailator.createThumbnail(originalOverlay,
					Math.round(zoomLevel * originalMap.getWidth()),
					Math.round(zoomLevel * originalMap.getHeight()));
		}
		xIni = Math.round(perX * currentMap.getWidth());
		yIni = Math.round(perY * currentMap.getHeight());
		repaint();
	}

	protected final void saveMap(Graphics g) {
		g.drawImage(originalMap, 0, 0, null);
		if (originalOverlay != null) {
			g.drawImage(originalOverlay, 0, 0, null);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (!started) {
			started = true;
			float x = (float) getWidth() / originalMap.getWidth();
			float y = (float) getHeight() / originalMap.getHeight();
			zoomLevel = ((x < y) ? x : y) - 0.1f;
			zoomIn();
		}
		g.drawImage(currentMap, 0, 0, width, height, xIni, yIni, xIni + width, yIni + height, null);
		if (currentOverlay != null) {
			g.drawImage(currentOverlay, 0, 0, width, height, xIni, yIni, xIni + width, yIni + height, null);
		}
		if (showMenu) {
			showMenu(g);
		}
	}

	private void showMenu(Graphics g) {
		g.setColor(Color.gray);
		g.fillRoundRect(5, 5, 80, 125, 10, 15);
		g.setColor(Color.black);
		g.fillPolygon(tUp);
		g.fillPolygon(tDw);
		g.fillPolygon(tLf);
		g.fillPolygon(tRg);
		g.fillOval(35, 35, 20, 20);
		drawMagGlass(g, true);
		drawMagGlass(g, false);
	}

	private void drawMagGlass(Graphics g, boolean plus) {
		int xini = (plus) ? 10 : 50;
		int yini = 90;
		g.fillOval(xini, yini, 20, 20);
		g.setColor(Color.gray);
		g.fillOval(xini + 3, yini + 3, 14, 14);
		g.setColor(Color.black);
		g.fillRect(xini + 4, yini + 9, 12, 2);
		if (plus) {
			g.fillRect(xini + 9, yini + 4, 2, 12);
		}
		Polygon cabo = new Polygon();
		cabo.addPoint(xini + 19, yini + 15);
		cabo.addPoint(xini + 15, yini + 19);
		cabo.addPoint(xini + 28, yini + 32);
		cabo.addPoint(xini + 32, yini + 28);
		g.fillPolygon(cabo);
	}

	private void createPolygons() {
		tUp = new Polygon();
		tUp.addPoint(45, 10);
		tUp.addPoint(30, 25);
		tUp.addPoint(60, 25);
		tDw = new Polygon();
		tDw.addPoint(30, 65);
		tDw.addPoint(60, 65);
		tDw.addPoint(45, 80);
		tLf = new Polygon();
		tLf.addPoint(10, 45);
		tLf.addPoint(25, 30);
		tLf.addPoint(25, 60);
		tRg = new Polygon();
		tRg.addPoint(65, 30);
		tRg.addPoint(65, 60);
		tRg.addPoint(80, 45);
	}

	protected void setOverlay(BufferedImage overlay) {
		originalOverlay = overlay;
		currentOverlay = Thumbnailator.createThumbnail(originalOverlay,
				Math.round(zoomLevel * originalMap.getWidth()),
				Math.round(zoomLevel * originalMap.getHeight()));
		repaint();
	}
}
