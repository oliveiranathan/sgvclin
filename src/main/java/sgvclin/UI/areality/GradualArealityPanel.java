/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI.areality;

import java.io.InvalidClassException;
import java.sql.SQLException;
import java.util.List;
import sgvclin.data.ColorPattern;
import sgvclin.data.Map;
import sgvclin.data.MapSaver;
import sgvclin.data.Question;
import sgvclin.data.Sheet;
import sgvclin.processors.dataprocessor.Filter;
import sgvclin.processors.dataprocessor.VariantGroup;

/**
 *
 * @author Nathan Oliveira
 */
public class GradualArealityPanel extends ArealityPanel {

	private static final long serialVersionUID = 1L;

	public GradualArealityPanel(Map map, Sheet sheet, List<Filter> filters, List<Question> question, List<VariantGroup> varsGroups) throws SQLException, InvalidClassException {
		this(map, sheet, filters, question, varsGroups, null, null, null);
	}

	protected GradualArealityPanel(Map map, Sheet sheet, List<Filter> filters, List<Question> question, List<VariantGroup> varsGroups, String title, ColorPattern[] colors, boolean[] selected)
			throws SQLException, InvalidClassException {
		super(map, sheet, filters, question, varsGroups, true, title, colors, selected);
	}

	@Override
	public MapSaver getMapSaver() {
		MapSaver ms = super.getMapSaver();
		ms.setType(MapSaver.Type.AREALITY_GRADUAL);
		return ms;
	}
}
