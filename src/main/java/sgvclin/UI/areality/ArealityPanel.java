/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI.areality;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.InvalidClassException;
import java.sql.SQLException;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import sgvclin.MainController;
import sgvclin.UI.StatusBarImpl;
import sgvclin.UI.diatopic.SelectionTableModel;
import sgvclin.UI.diatopic.SelectionTableUser;
import sgvclin.data.ColorPattern;
import sgvclin.data.GeographicPoint;
import sgvclin.data.Map;
import sgvclin.data.MapSaver;
import sgvclin.data.Question;
import sgvclin.data.Sheet;
import sgvclin.processors.MapInterface;
import sgvclin.processors.MapMaker;
import sgvclin.processors.dataprocessor.ArealityDataMaker;
import sgvclin.processors.dataprocessor.Filter;
import sgvclin.processors.dataprocessor.VariantGroup;
import sgvclin.processors.labelmaker.LabelPanel;

/**
 *
 * @author Nathan Oliveira
 */
public class ArealityPanel extends JPanel implements SelectionTableUser, MapInterface {

	private static final long serialVersionUID = 1L;

	public static ArealityPanel load(MapSaver ms) throws SQLException, InvalidClassException {
		switch (ms.getType()) {
			case AREALITY:
				return new ArealityPanel(ms.getMap(), ms.getSheet(), ms.getFilters(), ms.getQuestions(), ms.getVariantGroups(), false, ms.getTitle(), ms.getColors(), ms.getSelected());
			case AREALITY_GRADUAL:
				return new GradualArealityPanel(ms.getMap(), ms.getSheet(), ms.getFilters(), ms.getQuestions(), ms.getVariantGroups(), ms.getTitle(), ms.getColors(), ms.getSelected());
			default:
				throw new AssertionError(ms.getType().name());
		}
	}

	private final ArealityDataMaker dataMaker;
	private ArealityZoomPanel mapPanel;
	private JPanel pointArea;
	private JPanel labelArea;
	private JTextField titleTF;

	private String lastTitle;

	public ArealityPanel(Map map, Sheet sheet, List<Filter> filters, List<Question> question, List<VariantGroup> varsGroups) throws SQLException, InvalidClassException {
		this(map, sheet, filters, question, varsGroups, false, null, null, null);
	}

	@SuppressWarnings("LeakingThisInConstructor")
	protected ArealityPanel(Map map, Sheet sheet, List<Filter> filters, List<Question> question, List<VariantGroup> varsGroups, boolean isHighlander, String title, ColorPattern[] colors, boolean[] selected)
			throws SQLException, InvalidClassException {
		dataMaker = new ArealityDataMaker(sheet, question, varsGroups, map, isHighlander, colors, this);
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		JPanel lateral = new JPanel();
		lateral.setBorder(BorderFactory.createEtchedBorder());
		lateral.setLayout(new BoxLayout(lateral, BoxLayout.PAGE_AXIS));
		lateral.setMaximumSize(new Dimension(320, 5000));
		lateral.setMinimumSize(new Dimension(320, 5));
		lateral.setPreferredSize(new Dimension(320, 600));
		final SelectionTableModel stm = new SelectionTableModel(dataMaker.getVariantsGroupsRepresentation(), this, isHighlander);
		JTable table = new JTable(stm);
		table.setFont(MainController.getInstance().getVariantFont(table.getFont()));
		table.setMaximumSize(table.getPreferredSize());
		table.setMinimumSize(table.getPreferredSize());
		table.getTableHeader().setReorderingAllowed(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setSelectionBackground(Color.white);
		table.setSelectionForeground(Color.black);
		table.getColumnModel().getColumn(0).setPreferredWidth(25);
		table.getColumnModel().getColumn(1).setPreferredWidth(270);
		table.setMinimumSize(new Dimension(295, table.getHeight()));
		table.setMaximumSize(new Dimension(295, 5000));
		table.setAlignmentX(JTable.LEFT_ALIGNMENT);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (table.getSelectedColumn() == 1) {
					stm.setValueAt(!(boolean) stm.getValueAt(table.getSelectedRow(), 0), table.getSelectedRow(), 0);
				}
			}
		});
		pointArea = new JPanel();
		pointArea.setLayout(new BoxLayout(pointArea, BoxLayout.PAGE_AXIS));
		pointArea.setMaximumSize(new Dimension(300, 5000));
		pointArea.setMinimumSize(new Dimension(300, 50));
		pointArea.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		labelArea = new JPanel();
		labelArea.setLayout(new BoxLayout(labelArea, BoxLayout.LINE_AXIS));
		labelArea.setMaximumSize(new Dimension(300, 5000));
		labelArea.setMinimumSize(new Dimension(300, 50));
		labelArea.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		JPanel back = new JPanel();
		back.setMinimumSize(new Dimension(300, 50));
		back.setLayout(new BoxLayout(back, BoxLayout.PAGE_AXIS));
		back.add(table);
		back.add(pointArea);
		back.add(labelArea);
		JScrollPane sp = new JScrollPane(back);
		sp.getVerticalScrollBar().setUnitIncrement(16);
		titleTF = new JTextField(java.util.ResourceBundle.getBundle("texts/Bundle").getString("TYPE IN THE TITLE..."));
		titleTF.setPreferredSize(new Dimension(250, 32));
		titleTF.setMinimumSize(titleTF.getPreferredSize());
		titleTF.setMaximumSize(titleTF.getPreferredSize());
		titleTF.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				titleTF.selectAll();
			}
		});
		titleTF.addActionListener((evt) -> {
			if (lastTitle != null && !lastTitle.equals(titleTF.getText())) {
				MapMaker.getMapMaker().removeMap(lastTitle);
			}
			if (!titleTF.getText().equals(java.util.ResourceBundle.getBundle("texts/Bundle").getString("TYPE IN THE TITLE..."))) {
				lastTitle = titleTF.getText();
				MapMaker.getMapMaker().addMap(lastTitle, this);
			}
			mapPanel.requestFocus();
		});
		if (title != null) {
			titleTF.setText(title);
			lastTitle = title;
			javax.swing.SwingUtilities.invokeLater(() -> {
				titleTF.dispatchEvent(new ActionEvent(titleTF, 0, null));
				MapMaker.getMapMaker().addMap(lastTitle, this);
			});
		}
		JPanel titlePanel = new JPanel(new FlowLayout());
		titlePanel.add(titleTF);
		lateral.add(titlePanel);
		lateral.add(sp);
		mapPanel = new ArealityZoomPanel(dataMaker.getMap().getMapImage(), dataMaker.getListPoint2D(), this);
		mapPanel.setMinimumSize(new Dimension(480, 320));
		mapPanel.setPreferredSize(new Dimension(480, 320));
		this.add(mapPanel);
		this.add(lateral);
		StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DONE"));
		if (selected != null) {
			SwingUtilities.invokeLater(() -> stm.setSelected(selected));
		}
	}

	public String getTitle() {
		return lastTitle;
	}

	void updatePoint(Point2D.Double point) {
		pointArea.removeAll();
		if (point != null) {
			if (dataMaker.getPointsToPoints2D().containsValue(point)) {
				int ponto = -1;
				for (GeographicPoint g : dataMaker.getPointsToPoints2D().keySet()) {
					if (dataMaker.getPointsToPoints2D().get(g).equals(point)) {
						ponto = g.getId();
						break;
					}
				}
				String[] strs = dataMaker.getLabelsTexts(ponto);
				pointArea.add(new JLabel(strs[0] + "<hr>"));
				if (strs[1] != null && !strs[1].isEmpty()) {
					pointArea.add(new JLabel("<html>" + strs[1] + "<hr>"));
				}
				if (strs[2] != null && !strs[2].isEmpty()) {
					pointArea.add(new JLabel("<html>" + strs[2]));
				}
			}
		}
		pointArea.revalidate();
		pointArea.repaint();
	}

	@Override
	public void setVars(boolean[] selectedB) {
		dataMaker.setVars(selectedB);
	}

	@Override
	public void resetFocus() {
		mapPanel.requestFocus();
	}

	@Override
	public BufferedImage getMap() {
		BufferedImage img = new BufferedImage(dataMaker.getMap().getMapImage().getWidth(), dataMaker.getMap().getMapImage().getHeight(), BufferedImage.TYPE_INT_ARGB);
		mapPanel.saveMap(img.getGraphics());
		return img;
	}

	@Override
	public BufferedImage getLabel() {
		return dataMaker.getLabelMaker().getImage();
	}

	@Override
	public BufferedImage getHistogram() {
		return null;
	}

	public void setLabels(JLabel label) {
		labelArea.removeAll();
		if (label != null) {
			labelArea.add(label);
		}
		labelArea.revalidate();
	}

	public void setOverlay(BufferedImage overlay) {
		mapPanel.setOverlay(overlay);
	}

	@Override
	public MapSaver getMapSaver() {
		MapSaver ms = dataMaker.createMapSaver();
		ms.setTitle(lastTitle);
		ms.setType(MapSaver.Type.AREALITY);
		return ms;
	}

	@Override
	public BufferedImage getQuestions() {
		return LabelPanel.createImage(dataMaker.getQuestionsRepresentations(), 295);
	}
}
