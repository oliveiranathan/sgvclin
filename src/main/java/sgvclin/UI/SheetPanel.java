/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.Color;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.UIManager;
import sgvclin.data.Datasheet;
import sgvclin.data.Sheet;
import sgvclin.data.SheetField;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan Oliveira
 */
public class SheetPanel extends javax.swing.JPanel {

	private static final long serialVersionUID = 1L;

	private final List<SheetFieldPanel> panels;
	private final DefaultListModel<Sheet> sheetModel;

	public SheetPanel() throws SQLException {
		panels = new ArrayList<>(5);
		sheetModel = new DefaultListModel<Sheet>() {
			private static final long serialVersionUID = 1L;

			private List<Sheet> data = Sheet.getListOfSheets();

			@Override
			public int getSize() {
				if (data == null) {
					return 0;
				}
				return data.size();
			}

			@Override
			public Sheet getElementAt(int index) {
				if (data == null) {
					return null;
				}
				return data.get(index);
			}

			@Override
			public void addElement(Sheet element) {
				try {
					data = Sheet.getListOfSheets();
					fireContentsChanged(this, 0, data.size() - 1);
					if (element == null) {
						sheetsList.clearSelection();
					} else {
						sheetsList.setSelectedValue(Sheet.getSheet(element.getName()), true);
					}
				} catch (SQLException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				}
			}
		};
		initComponents();
		loadSheet(null);
		jScrollPane1.getVerticalScrollBar().setUnitIncrement(16);
	}

	private void createBasicPanels() {
		sheetNameTF.setText("");
		SheetFieldPanel p = new SheetFieldPanel(this);
		p.setFieldName(java.util.ResourceBundle.getBundle("texts/Bundle").getString("POINT NUMBER"));
		p.setFieldType(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INTEGER NUMBER"));
		p.freezeField(true);
		panels.add(p);
		p = new SheetFieldPanel(this);
		p.setFieldName(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INFORMANT NUMBER"));
		p.setFieldType(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INTEGER NUMBER"));
		p.freezeField(true);
		panels.add(p);
	}

	private void loadSheet(Sheet sheet) {
		panels.clear();
		createBasicPanels();
		if (sheet != null) {
			sheetNameTF.setText(sheet.getName());
			try {
				List<SheetField> fields = SheetField.getListOfFields(sheet);
				fields.stream().forEach((field) -> {
					SheetFieldPanel sfp = new SheetFieldPanel(this);
					sfp.setFieldName(field.getName());
					sfp.setGroupable(field.isGroupable());
					switch (field.getType()) {
						case TEXT:
							sfp.setFieldType(java.util.ResourceBundle.getBundle("texts/Bundle").getString("TEXT"));
							break;
						case DATE:
							sfp.setFieldType(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DATE"));
							break;
						case INTEGER:
							sfp.setFieldType(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INTEGER NUMBER"));
							break;
						case DOUBLE:
							sfp.setFieldType(java.util.ResourceBundle.getBundle("texts/Bundle").getString("REAL NUMBER"));
							break;
						case MULTIVALUED_UNIQUE:
						case MULTIVALUED_MULTIPLE:
							sfp.setFieldType(java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECTION"));
							sfp.setSelectionPanel(field);
							break;
						default:
							throw new AssertionError(field.getType().name());
					}
					panels.add(sfp);
				});
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		}
		updatePanels();
	}

	private void updatePanels() {
		fieldsPanel.removeAll();
		fieldsPanel.setLayout(new BoxLayout(fieldsPanel, BoxLayout.PAGE_AXIS));
		panels.stream().forEach((panel) -> fieldsPanel.add(panel));
		fieldsPanel.add(Box.createVerticalGlue());
		fieldsPanel.revalidate();
		jScrollPane1.repaint();
	}

	void removeField(SheetFieldPanel child) {
		panels.remove(child);
		updatePanels();
	}

	private void deleteSheet() {
		try {
			Sheet sheet = sheetsList.getSelectedValue();
			if (sheet != null) {
				Datasheet.deleteSheetTables(sheet);
				Sheet.deleteSheet(sheet);
			}
			sheetModel.addElement(null);
			loadSheet(null);
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
	 * content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings({"unchecked", "Convert2Lambda"})
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jScrollPane2 = new javax.swing.JScrollPane();
		sheetsList = new javax.swing.JList<>();
		newSheetBtn = new javax.swing.JButton();
		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		sheetNameTF = new javax.swing.JTextField();
		addFieldBtn = new javax.swing.JButton();
		saveSheetBtn = new javax.swing.JButton();
		jScrollPane1 = new javax.swing.JScrollPane();
		fieldsPanel = new javax.swing.JPanel();
		deleteSheetBtn = new javax.swing.JButton();

		sheetsList.setModel(sheetModel);
		sheetsList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		sheetsList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
			public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
				sheetsListValueChanged(evt);
			}
		});
		jScrollPane2.setViewportView(sheetsList);

		newSheetBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/add.png"))); // NOI18N
		java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("texts/Bundle"); // NOI18N
		newSheetBtn.setText(bundle.getString("NEW SHEET")); // NOI18N
		newSheetBtn.setToolTipText(bundle.getString("INSERT NEW SHEET")); // NOI18N
		newSheetBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				newSheetBtnActionPerformed(evt);
			}
		});

		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		jLabel1.setText(bundle.getString("SHEET NAME")); // NOI18N

		addFieldBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/add.png"))); // NOI18N
		addFieldBtn.setText(bundle.getString("ADD FIELD")); // NOI18N
		addFieldBtn.setToolTipText(bundle.getString("ADDS A FIELD TO THE MODEL")); // NOI18N
		addFieldBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addFieldBtnActionPerformed(evt);
			}
		});

		saveSheetBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/add_sheet.png"))); // NOI18N
		saveSheetBtn.setText(bundle.getString("CREATE SHEET")); // NOI18N
		saveSheetBtn.setToolTipText(bundle.getString("CREATES THE SHEET MODEL")); // NOI18N
		saveSheetBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				saveSheetBtnActionPerformed(evt);
			}
		});

		jScrollPane1.setPreferredSize(new java.awt.Dimension(649, 735));

		javax.swing.GroupLayout fieldsPanelLayout = new javax.swing.GroupLayout(fieldsPanel);
		fieldsPanel.setLayout(fieldsPanelLayout);
		fieldsPanelLayout.setHorizontalGroup(
				fieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 775, Short.MAX_VALUE)
		);
		fieldsPanelLayout.setVerticalGroup(
				fieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 732, Short.MAX_VALUE)
		);

		jScrollPane1.setViewportView(fieldsPanel);

		deleteSheetBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/remove.png"))); // NOI18N
		deleteSheetBtn.setText(bundle.getString("DELETE SHEET")); // NOI18N
		deleteSheetBtn.setToolTipText(bundle.getString("DELETES THE SHEET")); // NOI18N
		deleteSheetBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				deleteSheetBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addComponent(jLabel1)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(sheetNameTF)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(addFieldBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(saveSheetBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(deleteSheetBtn)))
						.addContainerGap())
		);
		jPanel1Layout.setVerticalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel1)
								.addComponent(sheetNameTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(addFieldBtn)
								.addComponent(saveSheetBtn)
								.addComponent(deleteSheetBtn))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
						.addContainerGap())
		);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
								.addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
								.addComponent(newSheetBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE))
						.addGap(18, 18, 18)
						.addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap())
		);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(layout.createSequentialGroup()
										.addComponent(newSheetBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 737, Short.MAX_VALUE))
								.addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addContainerGap())
		);
	}// </editor-fold>//GEN-END:initComponents

	private void saveSheetBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveSheetBtnActionPerformed
		if (sheetNameTF.getText().trim().isEmpty()) {
			sheetNameTF.setBackground(Color.red);
			return;
		}
		sheetNameTF.setBackground(UIManager.getColor("TextField.background"));
		if (panels.stream().anyMatch((panel) -> !panel.checkValidity())) {
			return;
		}
		List<String> names = new ArrayList<>(panels.size());
		for (SheetFieldPanel panel : panels) {
			if (names.contains(panel.getFieldName())) {
				panel.setNameError();
				return;
			}
			names.add(panel.getFieldName());
		}
		Sheet selectedSheet = sheetsList.getSelectedValue();
		if (OptionPane.showConfirmDialog(this, java.util.ResourceBundle.getBundle("texts/Bundle").getString(selectedSheet == null ? "CREATE SHEET?" : "UPDATE SHEET?"),
				"", OptionPane.YES_NO_OPTION) != OptionPane.YES_OPTION) {
			return;
		}
		if (selectedSheet == null) {
			Sheet sheet;
			try {
				sheet = Sheet.saveSheet(sheetNameTF.getText().trim());
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR WRITING TO THE DATABASE"));
				return;
			}
			panels.stream().forEach((fieldPanel) -> {
				fieldPanel.saveSheetField(sheet);
			});
			try {
				Datasheet.createSheetOnDB(sheet);
				StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("SAVED TO THE DATABASE"));
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR WRITING TO THE DATABASE"));
			}
			sheetModel.addElement(sheet);
		} else {
			try {
				String newName = null;
				if (!selectedSheet.getName().equals(sheetNameTF.getText().trim())) {
					newName = sheetNameTF.getText().trim();
				}
				List<SheetField> oldFields = SheetField.getListOfFields(selectedSheet);
				List<SheetFieldPanel> renamed = new ArrayList<>(5);
				panels.stream().filter((sfp) -> sfp.getOldName() != null && !sfp.getOldName().equals(sfp.getFieldName())).forEach((sfp) -> {
					renamed.add(sfp);
				});
				List<SheetField> removed = new ArrayList<>(5);
				for (SheetField sf : oldFields) {
					boolean found = false;
					for (SheetFieldPanel panel : panels) {
						if (panel.getOldName() != null && panel.getOldName().equals(sf.getName())) {
							found = true;
							break;
						}
					}
					if (!found) {
						removed.add(sf);
					}
				}
				List<SheetFieldPanel> added = new ArrayList<>(5);
				panels.stream().filter((sfp) -> sfp.getOldName() == null).forEach((sfp) -> added.add(sfp));
				Datasheet.updateSheet(selectedSheet, newName, oldFields, renamed, removed, added, panels);
				sheetModel.addElement(selectedSheet);
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR WRITING TO THE DATABASE"));
			}
		}
	}//GEN-LAST:event_saveSheetBtnActionPerformed

	private void addFieldBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addFieldBtnActionPerformed
		panels.add(new SheetFieldPanel(this));
		updatePanels();
	}//GEN-LAST:event_addFieldBtnActionPerformed

	private void newSheetBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newSheetBtnActionPerformed
		loadSheet(null);
	}//GEN-LAST:event_newSheetBtnActionPerformed

	private void sheetsListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_sheetsListValueChanged
		if (!evt.getValueIsAdjusting()) {
			loadSheet(sheetsList.getSelectedValue());
		}
	}//GEN-LAST:event_sheetsListValueChanged

	private void deleteSheetBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteSheetBtnActionPerformed
		if (sheetsList.getSelectedIndex() == -1) {
			loadSheet(null);
		}
		if (OptionPane.showConfirmDialog(this, java.util.ResourceBundle.getBundle("texts/Bundle").getString("DELETE SHEET") + "?",
				"", OptionPane.YES_NO_OPTION) != OptionPane.YES_OPTION) {
			return;
		}
		deleteSheet();
	}//GEN-LAST:event_deleteSheetBtnActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton addFieldBtn;
	private javax.swing.JButton deleteSheetBtn;
	private javax.swing.JPanel fieldsPanel;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JButton newSheetBtn;
	private javax.swing.JButton saveSheetBtn;
	private javax.swing.JTextField sheetNameTF;
	private javax.swing.JList<Sheet> sheetsList;
	// End of variables declaration//GEN-END:variables
}
