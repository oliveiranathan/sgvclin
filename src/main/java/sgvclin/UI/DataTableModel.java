/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.InvalidClassException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EventObject;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import org.jdesktop.swingx.table.DatePickerCellEditor;
import org.jdesktop.swingx.table.NumberEditorExt;
import org.jdesktop.swingx.text.NumberFormatExt;
import sgvclin.data.Data;
import sgvclin.data.Datasheet;
import sgvclin.data.GeographicPoint;
import sgvclin.data.Map;
import sgvclin.data.MapPoint;
import sgvclin.data.MultivaluedData;
import sgvclin.data.Sheet;
import sgvclin.data.SheetField;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan Oliveira
 */
class DataTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;

	private JTable table;
	private final List<SheetField> fields;
	private final List<Object> data;
	private List<Datasheet> datasheets;
	private final Map map;
	private final Sheet sheet;
	private int lastRowUsed = -1;

	DataTableModel(Sheet sheet, Map map) throws SQLException, InvalidClassException {
		this.map = map;
		this.sheet = sheet;
		fields = SheetField.getListOfFields(sheet);
		data = new ArrayList<>(fields.size() + 2);
		data.add(createEmptyList(GeographicPoint.class));
		data.add(createEmptyList(Integer.class));
		fields.stream().forEach((field) -> {
			switch (field.getType()) {
				case DATE:
					data.add(createEmptyList(Date.class));
					break;
				case DOUBLE:
					data.add(createEmptyList(Double.class));
					break;
				case INTEGER:
					data.add(createEmptyList(Integer.class));
					break;
				case TEXT:
					data.add(createEmptyList(String.class));
					break;
				case MULTIVALUED_MULTIPLE:
					data.add(createEmptyList(List.class));
					break;
				case MULTIVALUED_UNIQUE:
					data.add(createEmptyList(boolean[].class));
					break;
				default:
					data.add(createEmptyList(String.class));
					break;
			}
		});
		loadData(sheet, map);
	}

	private void setCellEditors() throws SQLException {
		{
			JComboBox<GeographicPoint> cb = new JComboBox<>();
			MapPoint.getMapPointsOfMap(map).stream().forEach((gp) -> cb.addItem(gp));
			TableCellEditor cellEditor = new DefaultCellEditor(cb);
			table.getColumnModel().getColumn(0).setCellEditor(cellEditor);
		}
		{
			TableCellEditor cellEditor = new NumberEditorExt(NumberFormatExt.getIntegerInstance());
			table.getColumnModel().getColumn(1).setCellEditor(cellEditor);
		}
		for (int i = 0; i < fields.size(); i++) {
			TableCellEditor cellEditor;
			switch (fields.get(i).getType()) {
				case TEXT: {
					cellEditor = new DefaultCellEditor(new JTextField());
					break;
				}
				case DATE: {
					cellEditor = new DatePickerCellEditor(DateFormat.getDateInstance());
					break;
				}
				case INTEGER: {
					cellEditor = new NumberEditorExt(NumberFormatExt.getIntegerInstance());
					break;
				}
				case DOUBLE: {
					cellEditor = new NumberEditorExt(NumberFormatExt.getNumberInstance());
					break;
				}
				case MULTIVALUED_UNIQUE: {
					JComboBox<MultivaluedData> cb = new JComboBox<>();
					MultivaluedData.getListOfMultivaluedData(fields.get(i)).stream().forEach((mvd) -> cb.addItem(mvd));
					cellEditor = new DefaultCellEditor(cb);
					break;
				}
				case MULTIVALUED_MULTIPLE: {
					cellEditor = new CheckBoxesEditor(MultivaluedData.getListOfMultivaluedData(fields.get(i)));
					table.getColumnModel().getColumn(i + 2).setCellRenderer(new CheckBoxesRenderer(MultivaluedData.getListOfMultivaluedData(fields.get(i))));
					break;
				}
				default: {
					throw new AssertionError(fields.get(i).getType().name());
				}
			}
			if (cellEditor != null) {
				table.getColumnModel().getColumn(i + 2).setCellEditor(cellEditor);
			}
		}
	}

	private void loadData(Sheet sheet, Map map) throws SQLException, InvalidClassException {
		datasheets = Datasheet.getListDatasheets(MapPoint.getMapPointsOfMap(map), sheet);
		if (datasheets.size() > 50) {
			int size = datasheets.size();
			size -= 50;
			while (size > 0) {
				addRows();
				size -= 50;
			}
		}
		lastRowUsed = datasheets.size() - 1;
		for (int i = 0; i < datasheets.size(); i++) {
			{
				@SuppressWarnings("unchecked")
				List<GeographicPoint> list = (List<GeographicPoint>) data.get(0);
				list.set(i, datasheets.get(i).getLocality());
			}
			{
				@SuppressWarnings("unchecked")
				List<Integer> list = (List<Integer>) data.get(1);
				list.set(i, datasheets.get(i).getInformant());
			}
			for (int j = 0; j < fields.size(); j++) {
				switch (fields.get(j).getType()) {
					case DATE: {
						@SuppressWarnings("unchecked")
						List<Date> list = (List<Date>) data.get(j + 2);
						list.set(i, datasheets.get(i).getProperty(fields.get(j).getName()).getDate());
						break;
					}
					case DOUBLE: {
						@SuppressWarnings("unchecked")
						List<Double> list = (List<Double>) data.get(j + 2);
						list.set(i, datasheets.get(i).getProperty(fields.get(j).getName()).getDouble());
						break;
					}
					case INTEGER: {
						@SuppressWarnings("unchecked")
						List<Integer> list = (List<Integer>) data.get(j + 2);
						list.set(i, datasheets.get(i).getProperty(fields.get(j).getName()).getInt());
						break;
					}
					case TEXT: {
						@SuppressWarnings("unchecked")
						List<String> list = (List<String>) data.get(j + 2);
						list.set(i, datasheets.get(i).getProperty(fields.get(j).getName()).getText());
						break;
					}
					case MULTIVALUED_UNIQUE: {
						@SuppressWarnings("unchecked")
						List<MultivaluedData> list = (List<MultivaluedData>) data.get(j + 2);
						list.set(i, datasheets.get(i).getProperty(fields.get(j).getName()).getSelected());
						break;
					}
					case MULTIVALUED_MULTIPLE: {
						@SuppressWarnings("unchecked")
						List<boolean[]> list = (List<boolean[]>) data.get(j + 2);
						List<MultivaluedData> selectedValues = datasheets.get(i).getProperty(fields.get(j).getName()).getSelectedValues();
						List<MultivaluedData> listOfMultivaluedData = MultivaluedData.getListOfMultivaluedData(fields.get(j));
						boolean[] bs = new boolean[listOfMultivaluedData.size()];
						for (int k = 0; k < bs.length; k++) {
							bs[k] = selectedValues.contains(listOfMultivaluedData.get(k));
						}
						list.set(i, bs);
						break;
					}
				}
			}
		}
	}

	private <T> List<T> createEmptyList(Class<T> type) {
		List<T> list = new ArrayList<>(50);
		for (int i = 0; i < 50; i++) {
			list.add(null);
		}
		return list;
	}

	@Override
	public int getRowCount() {
		@SuppressWarnings("unchecked")
		List<GeographicPoint> list = (List<GeographicPoint>) data.get(0);
		return list.size();
	}

	@Override
	public int getColumnCount() {
		return fields.size() + 2;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		@SuppressWarnings("rawtypes")
		List l = (List) data.get(columnIndex);
		return l.get(rowIndex);
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (aValue != null && !aValue.getClass().equals(getColumnClass(columnIndex)) && !aValue.getClass().equals(String.class)) {
			LoggerManager.getInstance().log(Level.INFO, "setValueAt()@DataTableModel received wrong class of object. Expected was: "
					+ getColumnClass(columnIndex).getName() + "; received: " + aValue.getClass().getName() + ".", null);
			return;
		}
		int row = rowIndex;
		if (rowIndex > lastRowUsed) {
			lastRowUsed++;
			row = lastRowUsed;
		}
		switch (columnIndex) {
			case 0: {
				@SuppressWarnings("unchecked")
				List<GeographicPoint> list = (List<GeographicPoint>) data.get(columnIndex);
				GeographicPoint gp;
				if (aValue == null) {
					gp = null;
				} else if (aValue instanceof GeographicPoint) {
					gp = (GeographicPoint) aValue;
				} else {
					try {
						gp = GeographicPoint.getGeoPointByCode(Integer.parseInt(((String) aValue).split(" - ")[0].trim()));
					} catch (NumberFormatException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						return;
					}
				}
				if (list.get(row) == null || !list.get(row).equals(gp)) {
					list.set(row, gp);
				}
				break;
			}
			case 1: {
				@SuppressWarnings("unchecked")
				List<Integer> list = (List<Integer>) data.get(columnIndex);
				Integer i;
				if (aValue == null) {
					i = null;
				} else if (aValue instanceof Integer) {
					i = (Integer) aValue;
				} else {
					try {
						i = Integer.parseInt((String) aValue);
					} catch (NumberFormatException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						return;
					}
				}
				if (list.get(row) == null || !list.get(row).equals(i)) {
					list.set(row, i);
				}
				break;
			}
			default:
				switch (fields.get(columnIndex - 2).getType()) {
					case TEXT: {
						@SuppressWarnings("unchecked")
						List<String> list = (List<String>) data.get(columnIndex);
						if (list.get(row) == null || !list.get(row).equals(aValue)) {
							list.set(row, (String) aValue);
						}
						break;
					}
					case DATE: {
						@SuppressWarnings("unchecked")
						List<Date> list = (List<Date>) data.get(columnIndex);
						Date d;
						if (aValue == null) {
							d = null;
						} else if (aValue instanceof Date) {
							d = (Date) aValue;
						} else {
							try {
								d = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.US).parse((String) aValue);
							} catch (ParseException ex) {
								LoggerManager.getInstance().log(Level.SEVERE, null, ex);
								return;
							}
						}
						if (list.get(row) == null || !list.get(row).equals(d)) {
							list.set(row, d);
						}
						break;
					}
					case INTEGER: {
						@SuppressWarnings("unchecked")
						List<Integer> list = (List<Integer>) data.get(columnIndex);
						Integer i;
						if (aValue == null) {
							i = null;
						} else if (aValue instanceof Integer) {
							i = (Integer) aValue;
						} else {
							try {
								i = Integer.parseInt((String) aValue);
							} catch (NumberFormatException ex) {
								LoggerManager.getInstance().log(Level.SEVERE, null, ex);
								return;
							}
						}
						if (list.get(row) == null || !list.get(row).equals(i)) {
							list.set(row, i);
						}
						break;
					}
					case DOUBLE: {
						@SuppressWarnings("unchecked")
						List<Double> list = (List<Double>) data.get(columnIndex);
						Double d;
						if (aValue == null) {
							d = null;
						} else if (aValue instanceof Double) {
							d = (Double) aValue;
						} else {
							try {
								d = Double.parseDouble((String) aValue);
							} catch (NumberFormatException ex) {
								LoggerManager.getInstance().log(Level.SEVERE, null, ex);
								return;
							}
						}
						if (list.get(row) == null || !list.get(row).equals(d)) {
							list.set(row, d);
						}
						break;
					}
					case MULTIVALUED_UNIQUE: {
						@SuppressWarnings("unchecked")
						List<MultivaluedData> list = (List<MultivaluedData>) data.get(columnIndex);
						MultivaluedData md;
						if (aValue == null) {
							md = null;
						} else if (aValue instanceof MultivaluedData) {
							md = (MultivaluedData) aValue;
						} else {
							try {
								md = null;
								for (MultivaluedData m : MultivaluedData.getListOfMultivaluedData(fields.get(columnIndex - 2))) {
									if (m.toString().equals(aValue)) {
										md = m;
										break;
									}
								}
							} catch (SQLException ex) {
								LoggerManager.getInstance().log(Level.SEVERE, null, ex);
								return;
							}
						}
						if (list.get(row) == null || !list.get(row).equals(md)) {
							list.set(row, md);
						}
						break;
					}
					case MULTIVALUED_MULTIPLE: {
						@SuppressWarnings("unchecked")
						List<boolean[]> list = (List<boolean[]>) data.get(columnIndex);
						boolean[] bs;
						if (aValue == null) {
							bs = null;
						} else if (aValue instanceof boolean[]) {
							bs = (boolean[]) aValue;
						} else {
							try {
								String[] values = ((String) aValue).split(",");
								if (values.length != MultivaluedData.getListOfMultivaluedData(fields.get(columnIndex - 2)).size()) {
									return;
								}
								bs = new boolean[values.length];
								for (int i = 0; i < bs.length; i++) {
									bs[i] = values[i].equals("1");
								}
							} catch (SQLException ex) {
								LoggerManager.getInstance().log(Level.SEVERE, null, ex);
								return;
							}
						}
						if (list.get(row) == null || !Arrays.equals(list.get(row), bs)) {
							list.set(row, bs);
						}
						break;
					}
					default: {
						throw new AssertionError(fields.get(columnIndex - 2).getType().name());
					}
				}
		}
		fireTableCellUpdated(row, columnIndex);
		saveRow(row);
	}

	private void saveRow(int row) {
		try {
			Datasheet ds;
			if (row < datasheets.size()) {
				ds = datasheets.get(row);
			} else {
				try {
					ds = Datasheet.getNewDatasheet(sheet);
				} catch (SQLException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
					return;
				}
			}
			ds.setLocality((GeographicPoint) getValueAt(row, 0));
			ds.setInformant((Integer) getValueAt(row, 1));
			ds.setName(sheet.getName());
			for (int i = 0; i < fields.size(); i++) {
				Object value = null;
				Object tableValue = getValueAt(row, i + 2);
				Data oldValue = ds.getProperty(fields.get(i).getName());
				try {
					switch (fields.get(i).getType()) {
						case TEXT:
							if (oldValue == null || !oldValue.getText().equals(tableValue)) {
								value = tableValue;
							}
							break;
						case DATE:
							java.sql.Date d = (tableValue == null ? null : new java.sql.Date(((Date) tableValue).getTime()));
							if (oldValue == null || !oldValue.getDate().equals(d)) {
								value = d;
							}
							break;
						case INTEGER:
							if (oldValue == null || oldValue.getInt() != (Integer) tableValue) {
								value = tableValue;
							}
							break;
						case DOUBLE:
							if (oldValue == null || oldValue.getDouble() != (Double) tableValue) {
								value = tableValue;
							}
							break;
						case MULTIVALUED_UNIQUE:
							if (oldValue == null || !oldValue.getSelected().equals(tableValue)) {
								value = tableValue;
							}
							break;
						case MULTIVALUED_MULTIPLE:
							List<MultivaluedData> mds = MultivaluedData.getListOfMultivaluedData(fields.get(i));
							boolean[] bs = (tableValue == null ? null : (boolean[]) tableValue);
							if (bs != null && bs.length == mds.size()) {
								List<MultivaluedData> res = new ArrayList<>(mds.size());
								for (int j = 0; j < bs.length; j++) {
									if (bs[j]) {
										res.add(mds.get(j));
									}
								}
								value = res;
							}
							break;
						default:
							value = null;
							throw new AssertionError(fields.get(i).getType().name());
					}
				} catch (AssertionError | InvalidClassException | SQLException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				}
				if (value != null) {
					try {
						ds.addProperty(fields.get(i).getName(), fields.get(i).getType(), value);
					} catch (InvalidClassException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						return;
					}
				}
			}
			try {
				Datasheet.saveDatasheet(ds);
				StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("SAVED TO THE DATABASE"));
				while (datasheets.size() <= row) {
					datasheets.add(null);
				}
				datasheets.set(row, ds);
			} catch (NullPointerException ex) {
				// triggers in the cases where there are some of the data missing. Just do nothing then...
			} catch (Exception ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR WRITING TO THE DATABASE"));
			}
		} catch (NullPointerException ex) {
			// triggers in the cases where there are some of the data missing. Just do nothing then...
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 0:
				return GeographicPoint.class;
			case 1:
				return Integer.class;
			default:
				switch (fields.get(columnIndex - 2).getType()) {
					case DATE:
						return Date.class;
					case DOUBLE:
						return Double.class;
					case INTEGER:
						return Integer.class;
					case TEXT:
						return String.class;
					case MULTIVALUED_MULTIPLE:
						return boolean[].class;
					case MULTIVALUED_UNIQUE:
						return MultivaluedData.class;
					default:
						return String.class;
				}
		}
	}

	@Override
	public String getColumnName(int column) {
		switch (column) {
			case 0:
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("POINT NUMBER");
			case 1:
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("INFORMANT NUMBER");
			default:
				return fields.get(column - 2).getName();
		}
	}

	@SuppressWarnings("unchecked")
	public void addRows() {
		data.stream().map((o) -> (List) o).forEach((l) -> {
			for (int i = 0; i < 50; i++) {
				l.add(null);
			}
		});
	}

	void setTable(JTable table) {
		this.table = table;
		setKeysOperations();
		try {
			setCellEditors();
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
	}

	private void setKeysOperations() {
		table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
		table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK), "paste");
		table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK), "copy");
		table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK), "cut");
		table.getActionMap().put("delete", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				int col = table.getSelectedColumn();
				if (col < 4 || col > getColumnCount() - 3) {
					if (col == getColumnCount() - 1) {
						table.getModel().setValueAt(null, table.getSelectedRow(), col);
					}
					return;
				}
				int row = table.getSelectedRow();
				table.getModel().setValueAt(null, row, col);
			}
		});
		table.getActionMap().put("paste", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				paste();
			}
		});
		table.getActionMap().put("copy", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				copy();
			}
		});
		table.getActionMap().put("cut", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				copy();
				int col = table.getSelectedColumn();
				if (col == getColumnCount() - 1) {
					table.getModel().setValueAt(null, table.getSelectedRow(), col);
				}
			}
		});
	}

	private void paste() {
		int col = table.getSelectedColumn();
		try {
			String d = Toolkit.getDefaultToolkit().getSystemClipboard()
					.getContents(null).getTransferData(DataFlavor.stringFlavor).toString();
			table.getModel().setValueAt(d, table.getSelectedRow(), col);
		} catch (UnsupportedFlavorException | IOException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
	}

	private void copy() {
		int col = table.getSelectedColumn();
		int row = table.getSelectedRow();
		if (col != -1 && row != -1) {
			Object value = table.getValueAt(row, col);
			String d = "";
			if (value != null) {
				if (value instanceof boolean[]) {
					for (boolean b : (boolean[]) value) {
						d += (b ? "1" : "0") + ",";
					}
					d = d.substring(0, d.length() - 1);
				} else {
					d = value.toString();
				}
			}
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(d), null);
		}
	}

	/*
     * Based on aterai's public domain code available @ http://java-swing-tips.blogspot.com/2011/03/checkboxes-in-jtable-cell.html
	 */
	private class CheckBoxesPanel extends JPanel {

		private static final long serialVersionUID = 1L;

		private final List<MultivaluedData> mvds;
		private final JCheckBox[] chbs;

		CheckBoxesPanel(List<MultivaluedData> mvds) {
			super();
			setOpaque(false);
			setBackground(new Color(0, 0, 0, 0));
			setLayout(new GridLayout(0, 4));
			this.mvds = new ArrayList<>(mvds);
			chbs = new JCheckBox[this.mvds.size()];
			createCheckBoxes();
		}

		private void createCheckBoxes() {
			for (int i = 0; i < chbs.length; i++) {
				chbs[i] = new JCheckBox(mvds.get(i).toString());
				chbs[i].setOpaque(false);
				chbs[i].setFocusable(false);
				chbs[i].setRolloverEnabled(false);
				chbs[i].setBackground(new Color(0, 0, 0, 0));
				add(chbs[i]);
			}
		}

		@SuppressWarnings("AssignmentToMethodParameter")
		protected void updateCheckBoxes(boolean[] selection) {
			if (selection == null) {
				selection = new boolean[chbs.length];
				Arrays.fill(selection, false);
			}
			if (selection.length != chbs.length) {
				LoggerManager.getInstance().log(Level.INFO, "selection.length != chbs.length @ CheckBoxPanel@DataTableModel", null);
				return;
			}
			for (int i = 0; i < selection.length; i++) {
				chbs[i].setSelected(selection[i]);
			}
		}

		protected JCheckBox[] getCHBS() {
			return Arrays.copyOf(chbs, chbs.length);
		}

		@Override
		public Dimension getPreferredSize() {
			int max = 0;
			for (JCheckBox chb : chbs) {
				max = Math.max(max, chb.getPreferredSize().width);
			}
			return new Dimension(max * (chbs.length < 4 ? chbs.length : 4), chbs[0].getPreferredSize().height * (chbs.length / 4 + 1));
		}
	}

	/*
     * Based on aterai's public domain code available @ http://java-swing-tips.blogspot.com/2011/03/checkboxes-in-jtable-cell.html
	 */
	private class CheckBoxesRenderer extends CheckBoxesPanel implements TableCellRenderer {

		private static final long serialVersionUID = 1L;

		CheckBoxesRenderer(List<MultivaluedData> mvds) {
			super(mvds);
			setName("Table.cellRenderer");
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			if (value instanceof boolean[] || value == null) {
				updateCheckBoxes((boolean[]) value);
			} else {
				LoggerManager.getInstance().log(Level.INFO, "getTableCellRendererComponent()@CheckBoxesRenderer@DataTableModel received strange value.", null);
			}
			return this;
		}
	}

	/*
     * Based on aterai's public domain code available @ http://java-swing-tips.blogspot.com/2011/03/checkboxes-in-jtable-cell.html
	 */
	private class CheckBoxesEditor extends CheckBoxesPanel implements TableCellEditor {

		private static final long serialVersionUID = 1L;

		protected transient ChangeEvent changeEvent = null;

		CheckBoxesEditor(List<MultivaluedData> mvds) {
			super(mvds);
			for (JCheckBox chb : getCHBS()) {
				chb.addActionListener((evt) -> {
					if (table.getCellEditor() != null) {
						table.getCellEditor().stopCellEditing();
					}
				});
			}
		}

		@Override
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			if (value instanceof boolean[] || value == null) {
				updateCheckBoxes((boolean[]) value);
			} else {
				LoggerManager.getInstance().log(Level.INFO, "getTableCellEditorComponent()@CheckBoxesEditor@DataTableModel received strange value.", null);
			}
			return this;
		}

		@Override
		public Object getCellEditorValue() {
			JCheckBox[] chbs = getCHBS();
			boolean[] bs = new boolean[chbs.length];
			for (int i = 0; i < chbs.length; i++) {
				bs[i] = chbs[i].isSelected();
			}
			return bs;
		}

		@Override
		public boolean isCellEditable(EventObject anEvent) {
			return true;
		}

		@Override
		public boolean shouldSelectCell(EventObject anEvent) {
			return true;
		}

		@Override
		public boolean stopCellEditing() {
			Object[] listeners = listenerList.getListenerList();
			for (int i = listeners.length - 2; i >= 0; i -= 2) {
				if (listeners[i] == CellEditorListener.class) {
					if (changeEvent == null) {
						changeEvent = new ChangeEvent(this);
					}
					((CellEditorListener) listeners[i + 1]).editingStopped(changeEvent);
				}
			}
			return true;
		}

		@Override
		public void cancelCellEditing() {
			Object[] listeners = listenerList.getListenerList();
			for (int i = listeners.length - 2; i >= 0; i -= 2) {
				if (listeners[i] == CellEditorListener.class) {
					if (changeEvent == null) {
						changeEvent = new ChangeEvent(this);
					}
					((CellEditorListener) listeners[i + 1]).editingCanceled(changeEvent);
				}
			}
		}

		@Override
		public void addCellEditorListener(CellEditorListener l) {
			listenerList.add(CellEditorListener.class, l);
		}

		@Override
		public void removeCellEditorListener(CellEditorListener l) {
			listenerList.remove(CellEditorListener.class, l);
		}

		public CellEditorListener[] getCellEditorListeners() {
			return listenerList.getListeners(CellEditorListener.class);
		}
	}
}
