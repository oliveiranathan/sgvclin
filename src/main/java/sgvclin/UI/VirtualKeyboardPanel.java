/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import sgvclin.MainController;

/**
 * @author Nathan Oliveira
 */
public class VirtualKeyboardPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private final JTextField inputArea;
	private final Font font;

	public VirtualKeyboardPanel(JTextField inputArea) {
		this.inputArea = inputArea;
		font = MainController.getInstance().getVariantFont(new Font("Arial", Font.BOLD, 18));
		this.add(createStrangeVK());
	}// as this is a really simple, but with many buttons, panel, I'll not refactor it to use the Netbeans form editor...

	private JPanel createStrangeVK() {
		JPanel panel = new JPanel(new GridLayout(8, 6));
		char[] list1 = {'[', ']', '\u02a7', 'x', 'l'};// line 1
		char[] list2 = {'\u030d', '\u0329', '\u02a4', '\u0263', '\u028e', '\u02b2',// line 2
			'p', 'f', 'a', 'h', '\u026b', '\u02b7',// line 3
			'b', 'v', '\u025b', '\u0266', 'm', '\u02b0',// line 4
			't', 's', 'e', '\u027e', 'n', 'j',// line 5
			'd', 'z', 'i', 'r', '\u0272', 'w',// line 6
			'k', '\u0283', 'o', '\u027d', '\u02dc', '\u028a',// line 7
			'\u0261', '\u01b7', '\u0254', 'u', '\u0259', '\u026a'};// line 8
		JButton btn;
		for (final char c : list1) {
			btn = new JButton("" + c);
			btn.setFont(font);
			panel.add(btn);
			btn.addActionListener((e) -> doClick(c));
		}
		btn = new JButton("DEL");
		btn.setFont(font);
		panel.add(btn);
		btn.addActionListener((e) -> doClick('\0'));
		for (final char c : list2) {
			btn = new JButton("" + c);
			btn.setFont(font);
			panel.add(btn);
			btn.addActionListener((e) -> doClick(c));
		}
		return panel;
	}

	private void doClick(char c) {
		int caret = inputArea.getCaretPosition();
		String text = inputArea.getText();
		if (caret < 0) {
			return;
		}
		switch (c) {
			case '\0': // DELETE
				if (inputArea.getText().length() != 0 && caret > 0 && caret <= text.length()) {
					inputArea.setText(text.substring(0, caret - 1) + text.substring(caret));
					caret -= 2;
				}
				break;
			case '\u030d':
			case '\u0329':
				inputArea.setText(text.substring(0, caret) + " " + c + text.substring(caret));
				caret++;
				break;
			case '\u02dc': // ~
				char old;
				if (caret == 0) {
					old = '§';
				} else {
					old = text.charAt(caret - 1);
				}
				String newC;
				switch (old) {
					case 'a':
						newC = "ã";
						break;
					case 'e':
						newC = "\u1ebd";
						break;
					case 'i':
						newC = "\u0129";
						break;
					case 'o':
						newC = "õ";
						break;
					case 'u':
						newC = "\u0169";
						break;
					case '§':
					case ' ':
						newC = "~";
						break;
					default:
						newC = old + "\u0303";
				}
				if (caret == 0) {
					inputArea.setText(newC + text);
				} else {
					inputArea.setText(text.substring(0, caret - 1) + newC + text.substring(caret));
				}
				break;
			default:
				inputArea.setText(text.substring(0, caret) + c + text.substring(caret));
		}
		inputArea.setCaretPosition(caret + 1);
		inputArea.requestFocusInWindow();
	}
}
