/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

/**
 * @author Nathan Oliveira
 */
public class StatusBarImpl extends JPanel implements StatusBar {

	private static final long serialVersionUID = 1L;

	private static StatusBar instance = null;

	public static StatusBar getInstance() {
		if (instance == null) {
			instance = new StatusBarImpl();
		}
		return instance;
	}

	private final JLabel text;
	private final JPanel connected;
	private final JProgressBar progBar;

	private StatusBarImpl() {
		text = new JLabel();
		progBar = new JProgressBar(SwingConstants.HORIZONTAL);
		connected = new JPanel();
		connected.setBackground(Color.red);
		createUI(false);
	}

	private void createUI(boolean showProgress) {
		removeAll();
		progBar.setMinimumSize(new Dimension(150, 22));
		progBar.setMaximumSize(new Dimension(150, 22));
		progBar.setPreferredSize(new Dimension(150, 22));
		connected.setPreferredSize(new Dimension(22, 22));
		connected.setMinimumSize(connected.getPreferredSize());
		connected.setMaximumSize(connected.getPreferredSize());
		connected.setToolTipText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INDICATES THE CONNECTION WITH THE DATABASE"));
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		add(text);
		add(Box.createHorizontalGlue());
		if (showProgress) {
			add(progBar);
		}
		add(connected);
		setPreferredSize(new Dimension(10, 23));
		setMinimumSize(getPreferredSize());
		setMaximumSize(new Dimension(32000, 23));
		revalidate();
		repaint();
	}

	@Override
	public void showProgress(int maxValue) {
		progBar.setIndeterminate(false);
		progBar.setMinimum(0);
		progBar.setMaximum(maxValue);
		progBar.setValue(0);
		createUI(true);
	}

	@Override
	public void setProgress(int value) {
		progBar.setValue(value);
	}

	@Override
	public void hideProgress() {
		createUI(false);
	}

	@Override
	public void showIndProgress() {
		progBar.setIndeterminate(true);
		createUI(true);
	}

	@Override
	public StatusBar setOKStatus(String status) {
		text.setText("<HTML>" + status);
		text.setForeground(fg_ok);
		return this;
	}

	@Override
	public StatusBar setERRStatus(String status) {
		text.setText("<HTML><b>" + status + "</b>");
		text.setForeground(fg_error);
		return this;
	}

	@Override
	public StatusBar setConnected() {
		connected.setBackground(Color.green);
		return this;
	}

	@Override
	public StatusBar setDisconnected() {
		connected.setBackground(Color.red);
		return this;
	}
}
