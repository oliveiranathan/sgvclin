/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
import sgvclin.data.AgeGroup;
import sgvclin.data.AgeRange;

/**
 *
 * @author Nathan Oliveira
 */
class AgeGroupTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	private AgeGroup selectedGroup = null;
	private AgeRange[] ranges = null;
	private boolean[][] wrong = null;

	protected void clear() {
		ranges = new AgeRange[10];
		wrong = new boolean[10][3];
		for (int i = 0; i < 10; i++) {
			ranges[i] = new AgeRange();
			ranges[i].setAgeGroups(selectedGroup);
			ranges[i].setBegin(-1);
			ranges[i].setEnd(-1);
		}
		fireTableStructureChanged();
		fireTableDataChanged();
	}

	protected void load(AgeGroup selectedGroup) throws NullPointerException, SQLException {
		this.selectedGroup = selectedGroup;
		if (selectedGroup == null) {
			ranges = null;
		} else {
			AgeRange[] r = AgeRange.getListOfAgeRanges(selectedGroup);
			ranges = new AgeRange[10];
			wrong = new boolean[10][3];
			int i = 0;
			for (; i < r.length; i++) {
				ranges[i] = r[i];
			}
			for (; i < 10; i++) {
				ranges[i] = new AgeRange();
				ranges[i].setAgeGroups(selectedGroup);
				ranges[i].setBegin(-1);
				ranges[i].setEnd(-1);
			}
		}
		fireTableStructureChanged();
		fireTableDataChanged();
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public int getRowCount() {
		if (ranges == null) {
			return 0;
		} else {
			return ranges.length;
		}
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return true;
	}

	@Override
	public Object getValueAt(int row, int col) {
		if (ranges != null) {
			switch (col) {
				case 0:
					return ranges[row].getName();
				case 1:
					return ranges[row].getBegin() != -1 ? ranges[row].getBegin() : null;
				case 2:
					return ranges[row].getEnd() != -1 ? ranges[row].getEnd() : null;
			}
		}
		return null;
	}

	@Override
	public Class<?> getColumnClass(int col) {
		switch (col) {
			case 0:
				return String.class;
			case 1:
			case 2:
				return Integer.class;
		}
		return String.class;
	}

	@Override
	public String getColumnName(int col) {
		switch (col) {
			case 0:
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("RANGE NAME");
			case 1:
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("MINIMUM");
			case 2:
				return java.util.ResourceBundle.getBundle("texts/Bundle").getString("MAXIMUM");
		}
		return "";
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		if (ranges != null) {
			switch (col) {
				case 0:
					ranges[row].setName((String) value);
					break;
				case 1:
					ranges[row].setBegin((int) value);
					break;
				case 2:
					ranges[row].setEnd((int) value);
					break;
			}
		}
		validateData();
	}

	boolean validateData() {
		if (ranges == null) {
			return false;
		}
		wrong = new boolean[10][3];
		boolean lacksNumbers = false;
		for (int i = 0; i < ranges.length; i++) {
			if (ranges[i].getName() == null || ranges[i].getName().trim().isEmpty()) {
				continue;
			}
			for (int j = i + 1; j < ranges.length; j++) {
				if (ranges[j].getName() == null || ranges[j].getName().trim().isEmpty()) {
					continue;
				}
				if (ranges[i].getName().equals(ranges[j].getName())) {
					wrong[i][0] = true;
					wrong[j][0] = true;
				}
			}
			if (ranges[i].getBegin() == -1) {
				lacksNumbers = true;
				wrong[i][1] = true;
			}
			if (ranges[i].getEnd() == -1) {
				lacksNumbers = true;
				wrong[i][2] = true;
			}
		}
		if (!lacksNumbers) {
			for (int i = 0; i < ranges.length; i++) {
				if (ranges[i].getName() == null || ranges[i].getName().trim().isEmpty()) {
					continue;
				}
				if (ranges[i].getBegin() > ranges[i].getEnd()) {
					wrong[i][1] = true;
					wrong[i][2] = true;
				}
				for (int j = i + 1; j < ranges.length; j++) {
					if (ranges[j].getName() == null || ranges[j].getName().trim().isEmpty()) {
						continue;
					}
					if (isRangeInsideRange(ranges[i].getBegin(), ranges[i].getEnd(), ranges[j].getBegin(), ranges[j].getEnd())) {
						wrong[i][1] = true;
						wrong[i][2] = true;
						wrong[j][1] = true;
						wrong[j][2] = true;
					}
				}
			}
		}
		fireTableStructureChanged();
		for (boolean[] row : wrong) {
			for (boolean el : row) {
				if (el) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean isRangeInsideRange(int b1, int e1, int b2, int e2) {
		if (b1 > e1 || b2 > e2 || b1 == b2 || e1 == e2 || b1 == e2 || b2 == e1) {
			return true;
		}
		final int FIRST = 0;
		final int SECOND = 1;
		int smallerB = b1 < b2 ? FIRST : SECOND;
		return smallerB == FIRST && e1 >= b2 || smallerB == SECOND && e2 >= b1;
	}

	@SuppressWarnings("empty-statement")
	AgeRange[] getArrayAgeRange() {
		int i = 0;
		for (; i < ranges.length && ranges[i].getName() != null && !ranges[i].getName().trim().isEmpty() && ranges[i].getBegin() != -1 && ranges[i].getEnd() != -1; i++);
		AgeRange[] toSave = new AgeRange[i];
		for (i--; i >= 0; i--) {
			toSave[i] = ranges[i];
		}
		return toSave;
	}

	boolean isWrong(int row, int col) {
		if (wrong == null) {
			return false;
		}
		return wrong[row][col];
	}

	public AgeGroup getSelectedGroup() {
		return selectedGroup;
	}
}
