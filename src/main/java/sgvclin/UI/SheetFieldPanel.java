/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.UIManager;
import sgvclin.data.MultivaluedData;
import sgvclin.data.Sheet;
import sgvclin.data.SheetField;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan Oliveira
 */
public class SheetFieldPanel extends javax.swing.JPanel {

	private static final long serialVersionUID = 1L;

	private final DefaultComboBoxModel<String> typeModel;
	private SheetFieldSelectionPanel selectionPanel;
	private final SheetPanel parent;
	private String oldName = null;

	SheetFieldPanel(SheetPanel parent) {
		this.parent = parent;
		typeModel = new DefaultComboBoxModel<String>() {
			private static final long serialVersionUID = 1L;

			private final String[] data = {
				java.util.ResourceBundle.getBundle("texts/Bundle").getString("TEXT"),
				java.util.ResourceBundle.getBundle("texts/Bundle").getString("DATE"),
				java.util.ResourceBundle.getBundle("texts/Bundle").getString("INTEGER NUMBER"),
				java.util.ResourceBundle.getBundle("texts/Bundle").getString("REAL NUMBER"),
				java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECTION")};

			@Override
			public int getSize() {
				return data.length;
			}

			@Override
			public String getElementAt(int index) {
				return data[index];
			}
		};
		initComponents();
		selectionConfigPanel.removeAll();
		selectionConfigPanel.setLayout(new BoxLayout(selectionConfigPanel, BoxLayout.LINE_AXIS));
		selectionConfigPanel.revalidate();
	}

	public SheetField saveSheetField(Sheet sheet) {
		if (nameTF.getText().equals(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INFORMANT NUMBER"))
				|| nameTF.getText().equals(java.util.ResourceBundle.getBundle("texts/Bundle").getString("POINT NUMBER"))) {
			return null; // does not save it as it is here just to guide the user...
		}
		SheetField sf = new SheetField();
		sf.setName(nameTF.getText().trim());
		sf.setSheet(sheet);
		String type = (String) typeComboBox.getSelectedItem();
		if (java.util.ResourceBundle.getBundle("texts/Bundle").getString("TEXT").equals(type)) {
			sf.setType(SheetField.Type.TEXT);
		} else if (java.util.ResourceBundle.getBundle("texts/Bundle").getString("DATE").equals(type)) {
			sf.setType(SheetField.Type.DATE);
		} else if (java.util.ResourceBundle.getBundle("texts/Bundle").getString("INTEGER NUMBER").equals(type)) {
			sf.setType(SheetField.Type.INTEGER);
		} else if (java.util.ResourceBundle.getBundle("texts/Bundle").getString("REAL NUMBER").equals(type)) {
			sf.setType(SheetField.Type.DOUBLE);
		} else if (java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECTION").equals(type)
				&& selectionPanel.isUnique()) {
			sf.setType(SheetField.Type.MULTIVALUED_UNIQUE);
		} else if (java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECTION").equals(type)
				&& selectionPanel.isMultiple()) {
			sf.setType(SheetField.Type.MULTIVALUED_MULTIPLE);
		}
		sf.setGroupable(groupableCheckBox.isSelected());
		try {
			SheetField.saveSheetField(sf);
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR WRITING TO THE DATABASE"));
			return null;
		}
		if (selectionPanel != null) {
			for (String value : selectionPanel.getListOptions()) {
				MultivaluedData md = new MultivaluedData();
				md.setSheetField(sf);
				md.setText(value);
				try {
					MultivaluedData.saveMultivaluatedData(md);
				} catch (SQLException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
					StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR WRITING TO THE DATABASE"));
				}
			}
		}
		return sf;
	}

	protected void setNameError() {
		nameTF.setBackground(Color.red);
	}

	public String getFieldName() {
		nameTF.setBackground(UIManager.getColor("TextField.background"));
		return nameTF.getText().trim();
	}

	protected boolean checkValidity() {
		if (nameTF.getText().trim().isEmpty()) {
			nameTF.setBackground(Color.red);
			return false;
		}
		nameTF.setBackground(UIManager.getColor("TextField.background"));
		if (typeComboBox.getSelectedItem() == null) {
			typeComboBox.setBackground(Color.red);
			return false;
		}
		typeComboBox.setBackground(UIManager.getColor("ComboBox.background"));
		if (typeComboBox.getSelectedItem().equals(java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECTION"))) {
			if (selectionPanel == null) {
				return false;
			}
			return selectionPanel.checkValidity();
		}
		return true;
	}

	protected void setFieldName(String name) {
		oldName = name;
		nameTF.setText(name);
		typeComboBox.setEnabled(false);
	}

	public String getOldName() {
		return oldName;
	}

	protected void setFieldType(String type) {
		typeComboBox.setSelectedItem(type);
	}

	protected void freezeField(boolean freeze) {
		removeFieldBtn.setVisible(false);
		groupableCheckBox.setVisible(false);
		nameTF.setEnabled(!freeze);
		typeComboBox.setEnabled(!freeze);
	}

	void setSelectionPanel(SheetField field) {
		selectionPanel.setData(field);
	}

	public boolean isGroupable() {
		return groupableCheckBox.isSelected();
	}

	void setGroupable(boolean groupable) {
		groupableCheckBox.setSelected(groupable);
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
	 * content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings({"unchecked", "Convert2Lambda"})
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jLabel1 = new javax.swing.JLabel();
		nameTF = new javax.swing.JTextField();
		jLabel2 = new javax.swing.JLabel();
		typeComboBox = new javax.swing.JComboBox<>();
		groupableCheckBox = new javax.swing.JCheckBox();
		removeFieldBtn = new javax.swing.JButton();
		selectionConfigPanel = new javax.swing.JPanel();

		setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

		java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("texts/Bundle"); // NOI18N
		jLabel1.setText(bundle.getString("FIELD NAME")); // NOI18N

		jLabel2.setText(bundle.getString("FIELD TYPE")); // NOI18N

		typeComboBox.setModel(typeModel);
		typeComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				typeComboBoxItemStateChanged(evt);
			}
		});

		groupableCheckBox.setSelected(true);
		groupableCheckBox.setText(bundle.getString("GROUPABLE")); // NOI18N

		removeFieldBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/remove.png"))); // NOI18N
		removeFieldBtn.setText(bundle.getString("REMOVE FIELD")); // NOI18N
		removeFieldBtn.setToolTipText(bundle.getString("REMOVE FIELD#TOOLTIP")); // NOI18N
		removeFieldBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				removeFieldBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout selectionConfigPanelLayout = new javax.swing.GroupLayout(selectionConfigPanel);
		selectionConfigPanel.setLayout(selectionConfigPanelLayout);
		selectionConfigPanelLayout.setHorizontalGroup(
				selectionConfigPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 0, Short.MAX_VALUE)
		);
		selectionConfigPanelLayout.setVerticalGroup(
				selectionConfigPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 162, Short.MAX_VALUE)
		);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(jLabel1)
								.addComponent(jLabel2))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
								.addGroup(layout.createSequentialGroup()
										.addComponent(typeComboBox, 0, 300, Short.MAX_VALUE)
										.addGap(18, 18, 18)
										.addComponent(groupableCheckBox))
								.addComponent(nameTF))
						.addGap(18, 18, 18)
						.addComponent(removeFieldBtn)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addComponent(selectionConfigPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel1)
								.addComponent(nameTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(removeFieldBtn))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel2)
								.addComponent(typeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(groupableCheckBox))
						.addGap(18, 18, 18)
						.addComponent(selectionConfigPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
		);
	}// </editor-fold>//GEN-END:initComponents

	private void removeFieldBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeFieldBtnActionPerformed
		parent.removeField(this);
	}//GEN-LAST:event_removeFieldBtnActionPerformed

	private void typeComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_typeComboBoxItemStateChanged
		if (evt.getStateChange() == ItemEvent.DESELECTED) {
			return;
		}
		String type = (String) typeComboBox.getSelectedItem();
		if (type.equals(java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECTION"))) {
			selectionConfigPanel.removeAll();
			selectionPanel = new SheetFieldSelectionPanel();
			selectionConfigPanel.add(selectionPanel);
			selectionConfigPanel.revalidate();
		} else {
			selectionConfigPanel.removeAll();
			selectionConfigPanel.revalidate();
			selectionPanel = null;
		}
	}//GEN-LAST:event_typeComboBoxItemStateChanged

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JCheckBox groupableCheckBox;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JTextField nameTF;
	private javax.swing.JButton removeFieldBtn;
	private javax.swing.JPanel selectionConfigPanel;
	private javax.swing.JComboBox<String> typeComboBox;
	// End of variables declaration//GEN-END:variables
}
