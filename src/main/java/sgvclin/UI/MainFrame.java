/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import com.lowagie.text.Font;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.io.InvalidClassException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.sf.dynamicreports.report.exception.DRException;
import sgvclin.MainController;
import sgvclin.UI.areality.ArealityPanel;
import sgvclin.UI.areality.GradualArealityPanel;
import sgvclin.UI.dataprocessor.SelectorInterface;
import sgvclin.UI.dataprocessor.SelectorPanel;
import sgvclin.UI.diatopic.ResultPanel;
import sgvclin.UI.diatopic.SelectionResultPanel;
import sgvclin.data.Map;
import sgvclin.data.MapSaver;
import sgvclin.data.Sheet;
import sgvclin.processors.DBController;
import sgvclin.processors.ExcelTableExporter;
import sgvclin.processors.LoggerManager;
import sgvclin.processors.MapInterface;
import sgvclin.processors.MapMaker;
import sgvclin.processors.ReportMaker;
import sgvclin.processors.SQLiteDBController;
import sgvclin.processors.VoronoiTesselator;
import sgvclin.processors.dataprocessor.DataException;
import sgvclin.processors.dataprocessor.Filter;

/**
 *
 * @author Nathan Oliveira
 */
public class MainFrame extends javax.swing.JFrame {

	private static final long serialVersionUID = 1L;

	private BufferedImage background;
	private final List<JInternalFrame> internalFrames = new ArrayList<>(5);
	private boolean connected = false;
	private boolean quitDialogIsOn = true; // this is used to remove the fucking quit dialog in the testing environments...
	private JInternalFrame vkFrame = null; // this is used to prevent more than one virtual keyboard internal frame.

	public MainFrame() {
		Image icon = null;
		try {
			background = ImageIO.read(getClass().getResource("/background.jpg"));
		} catch (IOException ex) {
			background = null;
			LoggerManager.getInstance().log(Level.INFO, null, ex);
		}
		try {
			icon = ImageIO.read(getClass().getResource("/icon.png"));
		} catch (IOException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
		setIconImage(icon);
		initComponents();
		StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("WAITING FOR DATABASE CONNECTION"));
		StatusBarImpl.getInstance().hideProgress();
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (!quitDialogIsOn || OptionPane.showConfirmDialog(MainController.getInstance().getMainFrame(),
						java.util.ResourceBundle.getBundle("texts/Bundle").getString("QUIT?"),
						"", OptionPane.YES_NO_OPTION) == OptionPane.YES_OPTION) {
					saveProperties();
					MainController.getInstance().finishExecution();
				}
			}
		});
		desktopPane.setBackground(UIManager.getColor("Panel.background"));
		loadProperties();
	}

	private void saveProperties() {
		Preferences prefs = Preferences.userRoot().node("sgvclin");
		boolean isMaximized = (getExtendedState() & JFrame.MAXIMIZED_BOTH) == JFrame.MAXIMIZED_BOTH;
		prefs.putBoolean("maximized", isMaximized);
		prefs.putInt("position.x", getBounds().x);
		prefs.putInt("position.y", getBounds().y);
		prefs.putInt("position.w", getBounds().width);
		prefs.putInt("position.h", getBounds().height);
		if (DBController.getInstance() instanceof SQLiteDBController) {
			prefs.put("lastDB", SQLiteDBController.getDbPath());
		}
		try {
			prefs.flush();
		} catch (BackingStoreException ex) {
			LoggerManager.getInstance().log(Level.WARNING, null, ex);
		}
		prefs.putBoolean("quitDialog", quitDialogIsOn);
	}

	private void loadProperties() {
		Preferences prefs = Preferences.userRoot().node("sgvclin");
		Rectangle bounds = new Rectangle();
		bounds.x = prefs.getInt("position.x", 50);
		bounds.y = prefs.getInt("position.y", 50);
		bounds.width = prefs.getInt("position.w", 1050);
		bounds.height = prefs.getInt("position.h", 750);
		setBounds(bounds);
		if (prefs.getBoolean("maximized", false)) {
			setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
		}
		if (prefs.get("lastDB", null) != null) {
			File f = new File(prefs.get("lastDB", null));
			if (f.exists() && f.isFile() && f.canRead() && f.canWrite()) {
				try {
					SQLiteDBController.connect(f);
					connectionOpened();
					StatusBarImpl.getInstance().setOKStatus(f.getAbsolutePath() + java.util.ResourceBundle.getBundle("texts/Bundle").getString("LOADED"));
				} catch (ClassNotFoundException | SQLException ex) {
				}
			}
		}
		quitDialogIsOn = prefs.getBoolean("quitDialog", true);
	}

	public void enablePrinting() {
		filePrintMI.setEnabled(true);
		fileSaveMapMI.setEnabled(true);
	}

	public void disablePrinting() {
		filePrintMI.setEnabled(false);
		fileSaveMapMI.setEnabled(false);
	}

	private JInternalFrame addIFrame(JPanel data, String title, boolean resizable, boolean closable, boolean maximizable, boolean iconifiable) {
		return addIFrame(data, title, resizable, closable, maximizable, iconifiable, false);
	}

	public JInternalFrame addIFrame(final JPanel data, String title, boolean resizable, boolean closable, boolean maximizable, boolean iconifiable, boolean maximized) {
		final JInternalFrame frm = new JInternalFrame(title, resizable, closable, maximizable, iconifiable);
		frm.setContentPane(data);
		desktopPane.add(frm);
		frm.pack();
		internalFrames.add(frm);
		frm.setVisible(true);
		try {
			frm.setMaximum(maximized);
		} catch (PropertyVetoException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
		frm.addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
				StatusBarImpl.getInstance().setOKStatus("");
				internalFrames.remove(frm);
				if (data instanceof ResultPanel) {
					MapMaker.getMapMaker().removeMap(((ResultPanel) data).getTitle());
				}
				if (data instanceof ArealityPanel) {
					MapMaker.getMapMaker().removeMap(((ArealityPanel) data).getTitle());
				}
				if (data instanceof MapPanel) {
					final Map map = ((MapPanel) data).getMap();
					if (map != null) {
						map.setWorker(new Thread() {
							@Override
							public void run() {
								VoronoiTesselator vt = new VoronoiTesselator(map);
								try {
									vt.generate();
								} catch (IllegalArgumentException ex) {
									StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE THE MAP TEMPLATE FIRST"));
									LoggerManager.getInstance().log(Level.SEVERE, null, ex);
								} catch (IllegalStateException ex) {
									LoggerManager.getInstance().log(Level.SEVERE, null, ex);
								} catch (IOException | SQLException ex) {
									LoggerManager.getInstance().log(Level.SEVERE, null, ex);
									StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
								}
							}
						});
						map.getWorker().start();
					}
				}
				super.internalFrameClosing(e);
			}
		});
		StatusBarImpl.getInstance().setOKStatus("");
		return frm;
	}

	protected void connectionFailure() {
		connected = false;
		createMenu.setEnabled(false);
		registerMenu.setEnabled(false);
		queryMenu.setEnabled(false);
		StatusBarImpl.getInstance().setDisconnected().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR IN THE DATABASE CONNECTION"));
	}

	protected void connectionOpened() {
		connected = true;
		createMenu.setEnabled(true);
		registerMenu.setEnabled(true);
		queryMenu.setEnabled(true);
		internalFrames.forEach((internalFrame) -> internalFrame.dispose());
		StatusBarImpl.getInstance().setConnected().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CONNECTED TO THE DATABASE"));
	}

	protected JDesktopPane getDesktop() {
		return desktopPane;
	}

	protected void showVirtualKeyboard(JTextField inputArea) {
		if (vkFrame != null) {
			vkFrame.dispose();
		}
		VirtualKeyboardPanel vk = new VirtualKeyboardPanel(inputArea);
		vkFrame = addIFrame(vk, java.util.ResourceBundle.getBundle("texts/Bundle").getString("VIRTUAL KEYBOARD"), false, true, false, false);
		vkFrame.setLocation(900, 0);
	}

	protected void openDSInput(Sheet sheet) {
		try {
			if (!connected) {
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OPEN A CONNECTION FIRST"));
				return;
			}
			if (sheet == null) {
				return;
			}
			Map map = DataSelectionUtils.selectMap();
			if (map == null) {
				return;
			}
			addIFrame(new DataPanel(sheet, map),
					java.util.ResourceBundle.getBundle("texts/Bundle").getString("INSERT DATA#ALT"),
					true, true, true, true, true);
			StatusBarImpl.getInstance().setOKStatus("");
		} catch (InvalidClassException | SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
	}

	private void openConsult(String title, String name, boolean sheet, boolean question, boolean map, boolean groupings, boolean variantsGrouping, boolean quantity, boolean questionnaire, boolean questionGrouping, SelectorInterface invoker) {
		try {
			addIFrame(new SelectorPanel(name, sheet, question, map, groupings, variantsGrouping, quantity, questionnaire, questionGrouping, invoker),
					title, true, true, true, true, true);
		} catch (DataException | SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}

	}

	private void openConsult(String name, boolean sheet, boolean question, boolean map, boolean groupings, boolean variantsGrouping, boolean quantity, boolean questionnaire, boolean questionGrouping, SelectorInterface invoker) {
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("QUERY"), name, sheet, question, map, groupings, variantsGrouping, quantity, questionnaire, questionGrouping, invoker);
	}

	private boolean checkMapValidity(Map map) {
		try {
			if (map.isInvalid()) {
				if (map.getWorker() == null) {
					try {
						VoronoiTesselator cc = new VoronoiTesselator(map);
						cc.generate();
					} catch (IllegalArgumentException ex) {
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE THE MAP TEMPLATE FIRST"));
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
					} catch (IllegalStateException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
					} catch (IOException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
					}
				} else {
					StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("WAITING FOR THE CREATION OF THE REGIONS"));
					map.getWorker().join();
				}
				if (map.isInvalid()) {
					StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("THE REGIONS ARE NOT CALCULATED"));
					return false;
				}
			}
			return true;
		} catch (InterruptedException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
			return false;
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
	 * content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings({"unchecked", "Convert2Lambda"})
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = (JPanel) StatusBarImpl.getInstance();
        desktopPane = new javax.swing.JDesktopPane() {
            private static final long serialVersionUID = 1L;

            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2d = (Graphics2D) g;
                double dx = (double) desktopPane.getWidth() / background.getWidth(null);
                double dy = (double) desktopPane.getHeight() / background.getHeight(null);
                if (dx < dy) {
                    dx = dy;
                }
                AffineTransform trans = AffineTransform.getScaleInstance(dx, dx);
                g2d.drawImage(background, trans, null);
            }
        };
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        fileConnectMI = new javax.swing.JMenuItem();
        connSep = new javax.swing.JPopupMenu.Separator();
        fileSaveMapMI = new javax.swing.JMenuItem();
        filePrintMI = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        fileQuitMI = new javax.swing.JMenuItem();
        createMenu = new javax.swing.JMenu();
        createQuestionCatsMI = new javax.swing.JMenuItem();
        createQuestionsAndVarsMI = new javax.swing.JMenuItem();
        createMapMI = new javax.swing.JMenuItem();
        createGeoPointsMI = new javax.swing.JMenuItem();
        createSheetMI = new javax.swing.JMenuItem();
        createAgeGroupMI = new javax.swing.JMenuItem();
        registerMenu = new javax.swing.JMenu();
        registerDSMI = new javax.swing.JMenuItem();
        registerAnswersMI = new javax.swing.JMenuItem();
        queryMenu = new javax.swing.JMenu();
        jMenu6 = new javax.swing.JMenu();
        queryAnsToXLSMI = new javax.swing.JMenuItem();
        queryDatasheetsToXLSMI = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        repOverallMI = new javax.swing.JMenuItem();
        repOverQuestionGroupsMI = new javax.swing.JMenuItem();
        repOverPointMI = new javax.swing.JMenuItem();
        repOverPointQuestionGroupsMI = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        repStateMI = new javax.swing.JMenuItem();
        jSeparator7 = new javax.swing.JPopupMenu.Separator();
        repObservationMI = new javax.swing.JMenuItem();
        jMenu8 = new javax.swing.JMenu();
        mapSavedMI = new javax.swing.JMenuItem();
        jSeparator8 = new javax.swing.JPopupMenu.Separator();
        mapDiatopicMI = new javax.swing.JMenuItem();
        mapDiatopicSelMI = new javax.swing.JMenuItem();
        mapDiatopicQuestionGroupsMI = new javax.swing.JMenuItem();
        mapDiatopicQuestionGroupsSelMI = new javax.swing.JMenuItem();
        jSeparator9 = new javax.swing.JPopupMenu.Separator();
        mapArealityMI = new javax.swing.JMenuItem();
        mapArealityQuestionGroupsMI = new javax.swing.JMenuItem();
        mapGradArealityMI = new javax.swing.JMenuItem();
        mapGradArealityQuestionGroupsMI = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        helpContentsMI = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        helpAboutMI = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("SGVCLin"); // NOI18N
        setMinimumSize(new java.awt.Dimension(800, 600));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1000, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 22, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout desktopPaneLayout = new javax.swing.GroupLayout(desktopPane);
        desktopPane.setLayout(desktopPaneLayout);
        desktopPaneLayout.setHorizontalGroup(
            desktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        desktopPaneLayout.setVerticalGroup(
            desktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 669, Short.MAX_VALUE)
        );

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/file.png"))); // NOI18N
        jMenu3.setMnemonic(KeyEvent.VK_F);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("texts/Bundle"); // NOI18N
        jMenu3.setText(bundle.getString("FILE")); // NOI18N

        fileConnectMI.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        fileConnectMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/connect.png"))); // NOI18N
        fileConnectMI.setMnemonic(KeyEvent.VK_O);
        fileConnectMI.setText(bundle.getString("CONNECT")); // NOI18N
        fileConnectMI.setToolTipText(bundle.getString("CONNECTS TO A DATABASE")); // NOI18N
        fileConnectMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileConnectMIActionPerformed(evt);
            }
        });
        jMenu3.add(fileConnectMI);
        jMenu3.add(connSep);

        fileSaveMapMI.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        fileSaveMapMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/map_save.png"))); // NOI18N
        fileSaveMapMI.setMnemonic(KeyEvent.VK_S);
        fileSaveMapMI.setText(bundle.getString("SAVE MAP")); // NOI18N
        fileSaveMapMI.setToolTipText(bundle.getString("SAVE A MAP TO THE DATABASE")); // NOI18N
        fileSaveMapMI.setEnabled(false);
        fileSaveMapMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileSaveMapMIActionPerformed(evt);
            }
        });
        jMenu3.add(fileSaveMapMI);

        filePrintMI.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        filePrintMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/print.png"))); // NOI18N
        filePrintMI.setMnemonic(KeyEvent.VK_P);
        filePrintMI.setText(bundle.getString("PRINT")); // NOI18N
        filePrintMI.setToolTipText(bundle.getString("PRINTS A MAP TO A FILE")); // NOI18N
        filePrintMI.setEnabled(false);
        filePrintMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filePrintMIActionPerformed(evt);
            }
        });
        jMenu3.add(filePrintMI);
        jMenu3.add(jSeparator4);

        fileQuitMI.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        fileQuitMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/quit.png"))); // NOI18N
        fileQuitMI.setMnemonic(KeyEvent.VK_Q);
        fileQuitMI.setText(bundle.getString("QUIT")); // NOI18N
        fileQuitMI.setToolTipText(bundle.getString("QUITS SGVCLIN")); // NOI18N
        fileQuitMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileQuitMIActionPerformed(evt);
            }
        });
        jMenu3.add(fileQuitMI);

        jMenuBar1.add(jMenu3);

        createMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/create.png"))); // NOI18N
        createMenu.setMnemonic(KeyEvent.VK_C);
        createMenu.setText(bundle.getString("CREATE")); // NOI18N
        createMenu.setEnabled(false);

        createQuestionCatsMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/categories.png"))); // NOI18N
        createQuestionCatsMI.setMnemonic(KeyEvent.VK_C);
        createQuestionCatsMI.setText(bundle.getString("QUESTION CATEGORIES")); // NOI18N
        createQuestionCatsMI.setToolTipText(bundle.getString("CREATES QUESTIONS CATEGORIES")); // NOI18N
        createQuestionCatsMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createQuestionCatsMIActionPerformed(evt);
            }
        });
        createMenu.add(createQuestionCatsMI);

        createQuestionsAndVarsMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/questions.png"))); // NOI18N
        createQuestionsAndVarsMI.setMnemonic(KeyEvent.VK_V);
        createQuestionsAndVarsMI.setText(bundle.getString("QUESTIONS AND VARIANTS")); // NOI18N
        createQuestionsAndVarsMI.setToolTipText(bundle.getString("CREATES QUESTIONS AND ITS VARIANTS")); // NOI18N
        createQuestionsAndVarsMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createQuestionsAndVarsMIActionPerformed(evt);
            }
        });
        createMenu.add(createQuestionsAndVarsMI);

        createMapMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/map.png"))); // NOI18N
        createMapMI.setMnemonic(KeyEvent.VK_M);
        createMapMI.setText(bundle.getString("MAPS")); // NOI18N
        createMapMI.setToolTipText(bundle.getString("CREATES MAPS AND ITS DETAILS")); // NOI18N
        createMapMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createMapMIActionPerformed(evt);
            }
        });
        createMenu.add(createMapMI);

        createGeoPointsMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/points.png"))); // NOI18N
        createGeoPointsMI.setMnemonic(KeyEvent.VK_P);
        createGeoPointsMI.setText(bundle.getString("POINTS NETWORKS")); // NOI18N
        createGeoPointsMI.setToolTipText(bundle.getString("CREATES GEOGRAPHIC LOCATIONS")); // NOI18N
        createGeoPointsMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createGeoPointsMIActionPerformed(evt);
            }
        });
        createMenu.add(createGeoPointsMI);

        createSheetMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/sheets.png"))); // NOI18N
        createSheetMI.setMnemonic(KeyEvent.VK_S);
        createSheetMI.setText(bundle.getString("SHEETS")); // NOI18N
        createSheetMI.setToolTipText(bundle.getString("CREATES THE INFORMANT SHEET MODEL")); // NOI18N
        createSheetMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createSheetMIActionPerformed(evt);
            }
        });
        createMenu.add(createSheetMI);

        createAgeGroupMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/age_group.png"))); // NOI18N
        createAgeGroupMI.setMnemonic(KeyEvent.VK_A);
        createAgeGroupMI.setText(bundle.getString("RANGES")); // NOI18N
        createAgeGroupMI.setToolTipText(bundle.getString("CREATES NUMERIC RANGES")); // NOI18N
        createAgeGroupMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createAgeGroupMIActionPerformed(evt);
            }
        });
        createMenu.add(createAgeGroupMI);

        jMenuBar1.add(createMenu);

        registerMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/data.png"))); // NOI18N
        registerMenu.setMnemonic(KeyEvent.VK_R);
        registerMenu.setText(bundle.getString("REGISTER")); // NOI18N
        registerMenu.setEnabled(false);

        registerDSMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/datasheets.png"))); // NOI18N
        registerDSMI.setMnemonic(KeyEvent.VK_I);
        registerDSMI.setText(bundle.getString("INFORMANT DATASHEET")); // NOI18N
        registerDSMI.setToolTipText(bundle.getString("ENTERS THE INFORMANT DATASHEETS")); // NOI18N
        registerDSMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerDSMIActionPerformed(evt);
            }
        });
        registerMenu.add(registerDSMI);

        registerAnswersMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/answers.png"))); // NOI18N
        registerAnswersMI.setMnemonic(KeyEvent.VK_A);
        registerAnswersMI.setText(bundle.getString("ANSWERS")); // NOI18N
        registerAnswersMI.setToolTipText(bundle.getString("ENTERS THE INFORMANT ANSWERS")); // NOI18N
        registerAnswersMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerAnswersMIActionPerformed(evt);
            }
        });
        registerMenu.add(registerAnswersMI);

        jMenuBar1.add(registerMenu);

        queryMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/query.png"))); // NOI18N
        queryMenu.setMnemonic(KeyEvent.VK_Q);
        queryMenu.setText(bundle.getString("QUERY")); // NOI18N
        queryMenu.setEnabled(false);

        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/export_xls.png"))); // NOI18N
        jMenu6.setMnemonic(KeyEvent.VK_E);
        jMenu6.setText(bundle.getString("EXPORT")); // NOI18N

        queryAnsToXLSMI.setText(bundle.getString("ANSWERS TO XLS")); // NOI18N
        queryAnsToXLSMI.setToolTipText(bundle.getString("EXPORTS ANSWERS, GROUPED BY LOCALITY")); // NOI18N
        queryAnsToXLSMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                queryAnsToXLSMIActionPerformed(evt);
            }
        });
        jMenu6.add(queryAnsToXLSMI);

        queryDatasheetsToXLSMI.setText(bundle.getString("INFORMANT DATASHEETS TO XLS")); // NOI18N
        queryDatasheetsToXLSMI.setToolTipText(bundle.getString("EXPORT INFORMANT DATA AND ANSWERS")); // NOI18N
        queryDatasheetsToXLSMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                queryDatasheetsToXLSMIActionPerformed(evt);
            }
        });
        jMenu6.add(queryDatasheetsToXLSMI);

        queryMenu.add(jMenu6);

        jMenu7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/reports.png"))); // NOI18N
        jMenu7.setMnemonic(KeyEvent.VK_R);
        jMenu7.setText(bundle.getString("REPORTS")); // NOI18N

        repOverallMI.setText(bundle.getString("OVERALL PRODUCTIVITY")); // NOI18N
        repOverallMI.setToolTipText(bundle.getString("CREATE OVERALL PRODUCTIVITY REPORT")); // NOI18N
        repOverallMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repOverallMIActionPerformed(evt);
            }
        });
        jMenu7.add(repOverallMI);

        repOverQuestionGroupsMI.setText(bundle.getString("OVERALL W QUESTION GROUPS")); // NOI18N
        repOverQuestionGroupsMI.setToolTipText(bundle.getString("CREATES REPORT, WITH QUESTION GROUPINGS")); // NOI18N
        repOverQuestionGroupsMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repOverQuestionGroupsMIActionPerformed(evt);
            }
        });
        jMenu7.add(repOverQuestionGroupsMI);

        repOverPointMI.setText(bundle.getString("OVERALL PRODUCTIVITY PER POINT")); // NOI18N
        repOverPointMI.setToolTipText(bundle.getString("CREATES OVERALL PRODUCTIVITY REPORT, GROUPED BY POINT")); // NOI18N
        repOverPointMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repOverPointMIActionPerformed(evt);
            }
        });
        jMenu7.add(repOverPointMI);

        repOverPointQuestionGroupsMI.setText(bundle.getString("OVERALL PER POINT WITH QUESTION GROUPS")); // NOI18N
        repOverPointQuestionGroupsMI.setToolTipText(bundle.getString("CREATES OVERALL PER POINT WITH QUESTION GROUPS")); // NOI18N
        repOverPointQuestionGroupsMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repOverPointQuestionGroupsMIActionPerformed(evt);
            }
        });
        jMenu7.add(repOverPointQuestionGroupsMI);
        jMenu7.add(jSeparator5);

        repStateMI.setText(bundle.getString("PER STATE")); // NOI18N
        repStateMI.setToolTipText(bundle.getString("CREATES REPORT, GROUPED BY STATE")); // NOI18N
        repStateMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repStateMIActionPerformed(evt);
            }
        });
        jMenu7.add(repStateMI);
        jMenu7.add(jSeparator7);

        repObservationMI.setText(bundle.getString("OBSERVATIONS")); // NOI18N
        repObservationMI.setToolTipText(bundle.getString("CREATE OBSERVATIONS REPORT")); // NOI18N
        repObservationMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repObservationMIActionPerformed(evt);
            }
        });
        jMenu7.add(repObservationMI);

        queryMenu.add(jMenu7);

        jMenu8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/map_edit.png"))); // NOI18N
        jMenu8.setMnemonic(KeyEvent.VK_M);
        jMenu8.setText(bundle.getString("MAPS_")); // NOI18N

        mapSavedMI.setText(bundle.getString("SAVED MAPS")); // NOI18N
        mapSavedMI.setToolTipText(bundle.getString("LOADS A SAVED MAP")); // NOI18N
        mapSavedMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mapSavedMIActionPerformed(evt);
            }
        });
        jMenu8.add(mapSavedMI);
        jMenu8.add(jSeparator8);

        mapDiatopicMI.setText(bundle.getString("DIATOPIC")); // NOI18N
        mapDiatopicMI.setToolTipText(bundle.getString("CREATES DIATOPIC MAP")); // NOI18N
        mapDiatopicMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mapDiatopicMIActionPerformed(evt);
            }
        });
        jMenu8.add(mapDiatopicMI);

        mapDiatopicSelMI.setText(bundle.getString("DIATOPIC WITH SELECTION")); // NOI18N
        mapDiatopicSelMI.setToolTipText(bundle.getString("CREATES DIATOPIC MAP, WITH VARIANT SELECTION")); // NOI18N
        mapDiatopicSelMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mapDiatopicSelMIActionPerformed(evt);
            }
        });
        jMenu8.add(mapDiatopicSelMI);

        mapDiatopicQuestionGroupsMI.setText(bundle.getString("DIATOPIC W QUESTION GROUPS")); // NOI18N
        mapDiatopicQuestionGroupsMI.setToolTipText(bundle.getString("CREATES DIATOPIC MAP, WITH QUESTION GROUPINGS")); // NOI18N
        mapDiatopicQuestionGroupsMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mapDiatopicQuestionGroupsMIActionPerformed(evt);
            }
        });
        jMenu8.add(mapDiatopicQuestionGroupsMI);

        mapDiatopicQuestionGroupsSelMI.setText(bundle.getString("DIATOPIC QUESTION GROUPS SELECTION")); // NOI18N
        mapDiatopicQuestionGroupsSelMI.setToolTipText(bundle.getString("CREATE DIATOPIC QUESTION GROUPINGS SELECTION")); // NOI18N
        mapDiatopicQuestionGroupsSelMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mapDiatopicQuestionGroupsSelMIActionPerformed(evt);
            }
        });
        jMenu8.add(mapDiatopicQuestionGroupsSelMI);
        jMenu8.add(jSeparator9);

        mapArealityMI.setText(bundle.getString("AREALITY")); // NOI18N
        mapArealityMI.setToolTipText(bundle.getString("CREATES AREALITY MAP")); // NOI18N
        mapArealityMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mapArealityMIActionPerformed(evt);
            }
        });
        jMenu8.add(mapArealityMI);

        mapArealityQuestionGroupsMI.setText(bundle.getString("AREALITY WITH QUESTION GROUPS")); // NOI18N
        mapArealityQuestionGroupsMI.setToolTipText(bundle.getString("CREATE AREALITY WITH QUESTION GROUPS")); // NOI18N
        mapArealityQuestionGroupsMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mapArealityQuestionGroupsMIActionPerformed(evt);
            }
        });
        jMenu8.add(mapArealityQuestionGroupsMI);

        mapGradArealityMI.setText(bundle.getString("GRADUAL AREALITY")); // NOI18N
        mapGradArealityMI.setToolTipText(bundle.getString("CREATES GRADUAL AREALITY MAP")); // NOI18N
        mapGradArealityMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mapGradArealityMIActionPerformed(evt);
            }
        });
        jMenu8.add(mapGradArealityMI);

        mapGradArealityQuestionGroupsMI.setText(bundle.getString("GRADUAL AREALITY WITH QUESTION GROUPS")); // NOI18N
        mapGradArealityQuestionGroupsMI.setToolTipText(bundle.getString("CREATE GRADUAL AREALITY WITH QUESTION GROUPS")); // NOI18N
        mapGradArealityQuestionGroupsMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mapGradArealityQuestionGroupsMIActionPerformed(evt);
            }
        });
        jMenu8.add(mapGradArealityQuestionGroupsMI);

        queryMenu.add(jMenu8);

        jMenuBar1.add(queryMenu);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/help.png"))); // NOI18N
        jMenu5.setMnemonic(KeyEvent.VK_H);
        jMenu5.setText(bundle.getString("HELP")); // NOI18N

        helpContentsMI.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        helpContentsMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/content.png"))); // NOI18N
        helpContentsMI.setMnemonic(KeyEvent.VK_C);
        helpContentsMI.setText(bundle.getString("CONTENTS")); // NOI18N
        helpContentsMI.setToolTipText(bundle.getString("HELP CONTENTS")); // NOI18N
        helpContentsMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpContentsMIActionPerformed(evt);
            }
        });
        jMenu5.add(helpContentsMI);
        jMenu5.add(jSeparator1);

        helpAboutMI.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/about.png"))); // NOI18N
        helpAboutMI.setMnemonic(KeyEvent.VK_A);
        helpAboutMI.setText(bundle.getString("ABOUT")); // NOI18N
        helpAboutMI.setToolTipText(bundle.getString("ABOUT#TOOLTIP")); // NOI18N
        helpAboutMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpAboutMIActionPerformed(evt);
            }
        });
        jMenu5.add(helpAboutMI);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(desktopPane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(desktopPane)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void fileConnectMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileConnectMIActionPerformed
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("SGVCLIN DATABASE FILE"), "sdb"));
		if (fc.showOpenDialog(MainController.getInstance().getMainFrame()) == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			if (!file.getAbsolutePath().endsWith(".sdb")) {
				file = new File(file.getAbsolutePath() + ".sdb");
			}
			try {
				StatusBarImpl.getInstance().showIndProgress();
				SQLiteDBController.connect(file);
			} catch (SQLException | ClassNotFoundException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR CONNECTING TO THE DATABASE"));
				MainController.getInstance().getMainFrame().connectionFailure();
				StatusBarImpl.getInstance().hideProgress();
				return;
			}
			MainController.getInstance().getMainFrame().connectionOpened();
			StatusBarImpl.getInstance().hideProgress();
		}
	}//GEN-LAST:event_fileConnectMIActionPerformed

	private void fileSaveMapMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileSaveMapMIActionPerformed
		String[] openMaps = MapMaker.getMapMaker().getMapsTitles();
		if (openMaps.length == 0) {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE A MAP FIRST"));
			return;
		}
		String map;
		if (openMaps.length == 1) {
			map = openMaps[0];
		} else {
			try {
				map = OptionPane.showInputDialog(this, java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECT THE MAP TO SAVE"), "",
						OptionPane.QUESTION_MESSAGE, null, openMaps, openMaps[0]);
			} catch (Exception ex) {
				return;
			}
		}
		if (map == null) {
			return;
		}
		try {
			if (MapSaver.getMapSaver(map) != null) {
				if (OptionPane.showConfirmDialog(this, java.util.ResourceBundle.getBundle("texts/Bundle").getString("OVERWRITE MAP?"),
						"", OptionPane.YES_NO_OPTION) != OptionPane.YES_OPTION) {
					return;
				}
			}
			MapMaker.getMapMaker().saveMap(map);
			StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("MAP SAVED"));
		} catch (IOException | SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR WRITING TO THE DATABASE"));
		}
	}//GEN-LAST:event_fileSaveMapMIActionPerformed

	private void filePrintMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filePrintMIActionPerformed
		String[] openMaps = MapMaker.getMapMaker().getMapsTitles();
		if (openMaps.length == 0) {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE OR LOAD A MAP FIRST"));
			return;
		}
		String map;
		if (openMaps.length == 1) {
			map = openMaps[0];
		} else {
			try {
				map = OptionPane.showInputDialog(this, java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECT THE MAP TO PRINT"), "",
						OptionPane.QUESTION_MESSAGE, null, openMaps, openMaps[0]);
			} catch (Exception ex) {
				return;
			}
		}
		if (map == null) {
			return;
		}
		MapSaver ms = null;
		try {
			ms = MapSaver.getMapSaver(map);
		} catch (SQLException ex) {
		}
		MapInterface mi = MapMaker.getMapMaker().getMap(map);
		PrintPanel pp = new PrintPanel(ms, mi.getMap(), MapMaker.createTitle(map), MapMaker.createCopyright(), mi.getLabel(), mi.getHistogram(), mi.getQuestions(), map);
		addIFrame(pp, map, true, true, true, true, true);
	}//GEN-LAST:event_filePrintMIActionPerformed

	private void fileQuitMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileQuitMIActionPerformed
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}//GEN-LAST:event_fileQuitMIActionPerformed

	private void createQuestionCatsMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createQuestionCatsMIActionPerformed
		if (connected) {
			try {
				addIFrame(new QuestionCategoryPanel(),
						java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE QUESTION SUBCATEGORIES"),
						true, true, false, true);
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
			}
		} else {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OPEN A CONNECTION FIRST"));
		}
	}//GEN-LAST:event_createQuestionCatsMIActionPerformed

	private void createQuestionsAndVarsMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createQuestionsAndVarsMIActionPerformed
		if (connected) {
			try {
				addIFrame(new QuestionnairePanel(),
						java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE QUESTIONS AND VARIANTS"),
						false, true, false, true);
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
			}
		} else {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OPEN A CONNECTION FIRST"));
		}
	}//GEN-LAST:event_createQuestionsAndVarsMIActionPerformed

	private void createMapMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createMapMIActionPerformed
		if (connected) {
			addIFrame(new MapPanel(),
					java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE MAP"),
					true, true, true, true, true);
		} else {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OPEN A CONNECTION FIRST"));
		}
	}//GEN-LAST:event_createMapMIActionPerformed

	private void createGeoPointsMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createGeoPointsMIActionPerformed
		if (connected) {
			Map m = DataSelectionUtils.selectMap();
			if (m == null) {
				return;
			}
			try {
				addIFrame(new MapPointPanel(m),
						java.util.ResourceBundle.getBundle("texts/Bundle").getString("EDIT POINT GRID"),
						false, true, false, true);
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(ex.getMessage());
			}
		} else {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OPEN A CONNECTION FIRST"));
		}
	}//GEN-LAST:event_createGeoPointsMIActionPerformed

	private void createSheetMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createSheetMIActionPerformed
		if (connected) {
			try {
				addIFrame(new SheetPanel(),
						java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE SHEET#TITLE"),
						true, true, true, true, true);
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		} else {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OPEN A CONNECTION FIRST"));
		}
	}//GEN-LAST:event_createSheetMIActionPerformed

	private void createAgeGroupMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createAgeGroupMIActionPerformed
		if (connected) {
			try {
				addIFrame(new AgeGroupPanel(), java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE RANGES"), false, true, false, true, false);
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
			}
		} else {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OPEN A CONNECTION FIRST"));
		}
	}//GEN-LAST:event_createAgeGroupMIActionPerformed

	private void registerDSMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerDSMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INSERT DATA#ALT"),
				true, false, true, false, false, false, false, false, (sheet, filters, question, varsGroups, map, numberOfVariantsToDisplay, questionnaire) -> {
					try {
						addIFrame(new DataPanel(sheet, map),
								java.util.ResourceBundle.getBundle("texts/Bundle").getString("INSERT DATA#ALT"),
								true, true, true, true, true);
						StatusBarImpl.getInstance().setOKStatus("");
					} catch (InvalidClassException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
					}
				});
	}//GEN-LAST:event_registerDSMIActionPerformed

	private void registerAnswersMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerAnswersMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("REGISTER IN..."),
				java.util.ResourceBundle.getBundle("texts/Bundle").getString("INSERT DATA"),
				true, false, true, false, false, false, true, false, (sheet, filters, question, varsGroups, map, numberOfVariantsToDisplay, questionnaire) -> {
					try {
						addIFrame(new AnswersPanel(sheet, questionnaire, map),
								java.util.ResourceBundle.getBundle("texts/Bundle").getString("INSERT DATA"),
								true, true, true, true, true);
						StatusBarImpl.getInstance().setOKStatus("");
					} catch (InvalidClassException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
					}
				});
	}//GEN-LAST:event_registerAnswersMIActionPerformed

	private void helpContentsMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpContentsMIActionPerformed
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().open(new File(java.util.ResourceBundle.getBundle("texts/Bundle").getString("HELP PATH")));
			} catch (IOException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
			}
		} else {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
		}
	}//GEN-LAST:event_helpContentsMIActionPerformed

	private void helpAboutMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpAboutMIActionPerformed
		final JDialog diag = new JDialog(this, java.util.ResourceBundle.getBundle("texts/Bundle").getString("ABOUT"), true);
		BufferedImage img;
		JLabel base;
		try {
			img = ImageIO.read(getClass().getResource("/about_background.png"));
		} catch (IOException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			return;
		}
		base = new JLabel(new ImageIcon(img));
		base.setText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ABOUT#TEXT"));
		base.setIconTextGap(-img.getWidth());
		base.setFont(base.getFont().deriveFont(Font.BOLD, 19f));
		JButton btn = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CLOSE"));
		diag.setLayout(new BorderLayout());
		diag.add(base, BorderLayout.CENTER);
		diag.add(btn, BorderLayout.SOUTH);
		diag.getRootPane().setDefaultButton(btn);
		btn.addActionListener((e) -> diag.dispose());
		diag.pack();
		diag.setLocationRelativeTo(this);
		diag.setVisible(true);
	}//GEN-LAST:event_helpAboutMIActionPerformed

	private void queryAnsToXLSMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_queryAnsToXLSMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("EXCEL DOCUMENT"),
				true, true, false, false, false, false, false, false, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					File file;
					JFileChooser fc = new JFileChooser();
					fc.setFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("EXCEL DOCUMENT"), "xls"));
					if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
						file = fc.getSelectedFile();
						if (!file.getAbsolutePath().endsWith(".xls")) {
							file = new File(file.getAbsolutePath() + ".xls");
						}
					} else {
						return;
					}
					try {
						ExcelTableExporter.exportAnswers(sheet, questions.get(0), file);
						StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("FILE SAVED"));
						if (Desktop.isDesktopSupported()) {
							try {
								Desktop.getDesktop().open(file);
							} catch (IOException ex) {
								LoggerManager.getInstance().log(Level.SEVERE, null, ex);
								StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
							}
						} else {
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
						}
					} catch (Exception ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR SAVING FILE"));
					}
				});
	}//GEN-LAST:event_queryAnsToXLSMIActionPerformed

	private void queryDatasheetsToXLSMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_queryDatasheetsToXLSMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("EXCEL DOCUMENT"),
				true, true, false, false, false, false, false, false, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					File file;
					JFileChooser fc = new JFileChooser();
					fc.setFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("EXCEL DOCUMENT"), "xls"));
					if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
						file = fc.getSelectedFile();
						if (!file.getAbsolutePath().endsWith(".xls")) {
							file = new File(file.getAbsolutePath() + ".xls");
						}
					} else {
						return;
					}
					try {
						ExcelTableExporter.exportDatasheets(sheet, questions.get(0), file);
						StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("FILE SAVED"));

						if (Desktop.isDesktopSupported()) {
							try {
								Desktop.getDesktop().open(file);
							} catch (IOException ex) {
								LoggerManager.getInstance().log(Level.SEVERE, null, ex);
								StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
							}
						} else {
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
						}
					} catch (Exception ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR SAVING FILE"));
					}
				});
	}//GEN-LAST:event_queryDatasheetsToXLSMIActionPerformed

	private void repOverallMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repOverallMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("REPORT"),
				true, true, true, true, true, false, false, false, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					File file;
					JFileChooser fc = new JFileChooser();
					fc.setFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PDF FILE"), "pdf"));
					fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("WORD DOCUMENT"), "docx"));
					fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("EXCEL DOCUMENT"), "xlsx"));
					if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
						file = fc.getSelectedFile();
						if (!file.getAbsolutePath().endsWith(".pdf") && !file.getAbsolutePath().endsWith(".docx") && !file.getAbsolutePath().endsWith(".xlsx")) {
							file = new File(file.getAbsolutePath() + "." + ((FileNameExtensionFilter) fc.getFileFilter()).getExtensions()[0]);
						}
					} else {
						return;
					}
					try {
						ReportMaker.createReport(sheet, filters, questions, varsGroups, map, file);
					} catch (DRException | IOException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR SAVING FILE"));
						return;
					}
					if (Desktop.isDesktopSupported()) {
						try {
							Desktop.getDesktop().open(file);
						} catch (IOException ex) {
							LoggerManager.getInstance().log(Level.SEVERE, null, ex);
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
						}
					} else {
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
					}
					StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("FILE SAVED"));
				});
	}//GEN-LAST:event_repOverallMIActionPerformed

	private void repStateMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repStateMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("REPORT"),
				true, true, true, true, true, false, false, false, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					File file;
					JFileChooser fc = new JFileChooser();
					fc.setFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PDF FILE"), "pdf"));
					fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("WORD DOCUMENT"), "docx"));
					fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("EXCEL DOCUMENT"), "xlsx"));
					if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
						file = fc.getSelectedFile();
						if (!file.getAbsolutePath().endsWith(".pdf") && !file.getAbsolutePath().endsWith(".docx") && !file.getAbsolutePath().endsWith(".xlsx")) {
							file = new File(file.getAbsolutePath() + "." + ((FileNameExtensionFilter) fc.getFileFilter()).getExtensions()[0]);
						}
					} else {
						return;
					}
					try {
						ReportMaker.createStateReport(sheet, filters, questions.get(0), varsGroups, map, file);
					} catch (DRException | IOException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR SAVING FILE"));
						return;
					}
					if (Desktop.isDesktopSupported()) {
						try {
							Desktop.getDesktop().open(file);
						} catch (IOException ex) {
							LoggerManager.getInstance().log(Level.SEVERE, null, ex);
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
						}
					} else {
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
					}
					StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("FILE SAVED"));
				});
	}//GEN-LAST:event_repStateMIActionPerformed

	private void repObservationMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repObservationMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("REPORT"),
				true, true, true, false, false, false, false, false, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					File file;
					JFileChooser fc = new JFileChooser();
					fc.setFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PDF FILE"), "pdf"));
					fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("WORD DOCUMENT"), "docx"));
					fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("EXCEL DOCUMENT"), "xlsx"));
					if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
						file = fc.getSelectedFile();
						if (!file.getAbsolutePath().endsWith(".pdf") && !file.getAbsolutePath().endsWith(".docx") && !file.getAbsolutePath().endsWith(".xlsx")) {
							file = new File(file.getAbsolutePath() + "." + ((FileNameExtensionFilter) fc.getFileFilter()).getExtensions()[0]);
						}
					} else {
						return;
					}
					try {
						ReportMaker.createObservationReport(sheet, questions.get(0), map, file);
					} catch (DRException | IOException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR SAVING FILE"));
						return;
					}
					if (Desktop.isDesktopSupported()) {
						try {
							Desktop.getDesktop().open(file);
						} catch (IOException ex) {
							LoggerManager.getInstance().log(Level.SEVERE, null, ex);
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
						}
					} else {
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
					}
					StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("FILE SAVED"));
				});
	}//GEN-LAST:event_repObservationMIActionPerformed

	private void mapSavedMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mapSavedMIActionPerformed
		if (!connected) {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OPEN A CONNECTION FIRST"));
			return;
		}
		MapSaver ms = DataSelectionUtils.selectMapSaver(false);
		if (ms == null) {
			return;
		}
		try {
			JPanel thePanel = null;
			switch (ms.getType()) {
				case AREALITY:
				case AREALITY_GRADUAL:
					thePanel = ArealityPanel.load(ms);
					break;
				case PIE:
				case PIE_SELECTION:
				case HISTOGRAM:
				case HISTOGRAM_SELECTION:
					thePanel = ResultPanel.load(ms);
					break;
				default:
					throw new AssertionError(ms.getType().name());
			}
			MapMaker.getMapMaker().addMap(ms.getTitle(), (MapInterface) thePanel);
			addIFrame(thePanel, ms.getTitle(), true, true, true, true, true);
		} catch (NullPointerException | SQLException | InvalidClassException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OPEN A CONNECTION FIRST"));
		}
	}//GEN-LAST:event_mapSavedMIActionPerformed

	private void mapDiatopicMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mapDiatopicMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("MAP#BTNLABEL"),
				true, true, true, true, true, true, false, false, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					try {
						addIFrame(new ResultPanel(map, sheet, filters, questions, varsGroups, quantity),
								java.util.ResourceBundle.getBundle("texts/Bundle").getString("DIATOPIC MAP"),
								true, true, true, true, true);
					} catch (IllegalStateException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO POINTS IN MAP"));
					} catch (InvalidClassException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
					}
				});
	}//GEN-LAST:event_mapDiatopicMIActionPerformed

	private void mapDiatopicSelMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mapDiatopicSelMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("MAP#BTNLABEL"),
				true, true, true, true, true, false, false, false, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					try {
						addIFrame(new SelectionResultPanel(map, sheet, filters, questions, varsGroups, quantity),
								java.util.ResourceBundle.getBundle("texts/Bundle").getString("DIATOPIC MAP"),
								true, true, true, true, true);
					} catch (IllegalStateException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO POINTS IN MAP"));
					} catch (InvalidClassException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
					}
				});
	}//GEN-LAST:event_mapDiatopicSelMIActionPerformed

	private void mapArealityMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mapArealityMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("MAP#BTNLABEL"),
				true, true, true, true, true, false, false, false, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					try {
						if (map.getTemplate() == null) {
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE THE MAP TEMPLATE FIRST"));
							return;
						}
						if (!checkMapValidity(map)) {
							return;
						}
						String baseTitle = java.util.ResourceBundle.getBundle("texts/Bundle").getString("AREALITY MAP");
						for (Filter filter : filters) {
							String title = baseTitle;
							if (filters.size() > 1) {
								title += " - " + filter;
							}
							addIFrame(new ArealityPanel(map, sheet, Arrays.asList(filter), questions, varsGroups),
									title, true, true, true, true, false);
						}
					} catch (IllegalStateException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO POINTS IN MAP"));
					} catch (InvalidClassException | NullPointerException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
					}
				});
	}//GEN-LAST:event_mapArealityMIActionPerformed

	private void mapGradArealityMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mapGradArealityMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("MAP#BTNLABEL"),
				true, true, true, true, true, false, false, false, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					try {
						if (map.getTemplate() == null) {
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE THE MAP TEMPLATE FIRST"));
							return;
						}
						if (!checkMapValidity(map)) {
							return;
						}
						String baseTitle = java.util.ResourceBundle.getBundle("texts/Bundle").getString("AREALITY MAP");
						for (Filter filter : filters) {
							String title = baseTitle;
							if (filters.size() > 1) {
								title += " - " + filter;
							}
							addIFrame(new GradualArealityPanel(map, sheet, Arrays.asList(filter), questions, varsGroups),
									title, true, true, true, true, false);
						}
					} catch (IllegalStateException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO POINTS IN MAP"));
					} catch (InvalidClassException | NullPointerException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
					}
				});
	}//GEN-LAST:event_mapGradArealityMIActionPerformed

	private void repOverPointMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repOverPointMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("REPORT"),
				true, true, true, true, true, false, false, false, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					File file;
					JFileChooser fc = new JFileChooser();
					fc.setFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PDF FILE"), "pdf"));
					fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("WORD DOCUMENT"), "docx"));
					fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("EXCEL DOCUMENT"), "xlsx"));
					if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
						file = fc.getSelectedFile();
						if (!file.getAbsolutePath().endsWith(".pdf") && !file.getAbsolutePath().endsWith(".docx") && !file.getAbsolutePath().endsWith(".xlsx")) {
							file = new File(file.getAbsolutePath() + "." + ((FileNameExtensionFilter) fc.getFileFilter()).getExtensions()[0]);
						}
					} else {
						return;
					}
					try {
						ReportMaker.createReportPerPoint(sheet, filters, questions.get(0), varsGroups, map, file);
					} catch (IllegalStateException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO POINTS IN MAP"));
					} catch (DRException | IOException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR SAVING FILE"));
						return;
					}
					if (Desktop.isDesktopSupported()) {
						try {
							Desktop.getDesktop().open(file);
						} catch (IOException ex) {
							LoggerManager.getInstance().log(Level.SEVERE, null, ex);
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
						}
					} else {
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
					}
					StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("FILE SAVED"));
				});
	}//GEN-LAST:event_repOverPointMIActionPerformed

    private void mapDiatopicQuestionGroupsMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mapDiatopicQuestionGroupsMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("MAP#BTNLABEL"),
				true, true, true, true, true, true, false, true, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					try {
						addIFrame(new ResultPanel(map, sheet, filters, questions, varsGroups, quantity),
								java.util.ResourceBundle.getBundle("texts/Bundle").getString("DIATOPIC MAP"),
								true, true, true, true, true);
					} catch (IllegalStateException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO POINTS IN MAP"));
					} catch (InvalidClassException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
					}
				});
    }//GEN-LAST:event_mapDiatopicQuestionGroupsMIActionPerformed

    private void repOverQuestionGroupsMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repOverQuestionGroupsMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("REPORT"),
				true, true, true, true, true, false, false, true, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					File file;
					JFileChooser fc = new JFileChooser();
					fc.setFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PDF FILE"), "pdf"));
					fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("WORD DOCUMENT"), "docx"));
					fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("EXCEL DOCUMENT"), "xlsx"));
					if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
						file = fc.getSelectedFile();
						if (!file.getAbsolutePath().endsWith(".pdf") && !file.getAbsolutePath().endsWith(".docx") && !file.getAbsolutePath().endsWith(".xlsx")) {
							file = new File(file.getAbsolutePath() + "." + ((FileNameExtensionFilter) fc.getFileFilter()).getExtensions()[0]);
						}
					} else {
						return;
					}
					try {
						ReportMaker.createReport(sheet, filters, questions, varsGroups, map, file);
					} catch (IllegalStateException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO POINTS IN MAP"));
					} catch (DRException | IOException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR SAVING FILE"));
						return;
					}
					if (Desktop.isDesktopSupported()) {
						try {
							Desktop.getDesktop().open(file);
						} catch (IOException ex) {
							LoggerManager.getInstance().log(Level.SEVERE, null, ex);
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
						}
					} else {
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
					}
					StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("FILE SAVED"));
				});
    }//GEN-LAST:event_repOverQuestionGroupsMIActionPerformed

    private void repOverPointQuestionGroupsMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repOverPointQuestionGroupsMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("REPORT"),
				true, true, true, true, true, false, false, true, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					File file;
					JFileChooser fc = new JFileChooser();
					fc.setFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PDF FILE"), "pdf"));
					fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("WORD DOCUMENT"), "docx"));
					fc.addChoosableFileFilter(new FileNameExtensionFilter(java.util.ResourceBundle.getBundle("texts/Bundle").getString("EXCEL DOCUMENT"), "xlsx"));
					if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
						file = fc.getSelectedFile();
						if (!file.getAbsolutePath().endsWith(".pdf") && !file.getAbsolutePath().endsWith(".docx") && !file.getAbsolutePath().endsWith(".xlsx")) {
							file = new File(file.getAbsolutePath() + "." + ((FileNameExtensionFilter) fc.getFileFilter()).getExtensions()[0]);
						}
					} else {
						return;
					}
					try {
						ReportMaker.createReportPerPoint(sheet, filters, questions, varsGroups, map, file);
					} catch (IllegalStateException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO POINTS IN MAP"));
					} catch (DRException | IOException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR SAVING FILE"));
						return;
					}
					if (Desktop.isDesktopSupported()) {
						try {
							Desktop.getDesktop().open(file);
						} catch (IOException ex) {
							LoggerManager.getInstance().log(Level.SEVERE, null, ex);
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
						}
					} else {
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR OPENING FILE"));
					}
					StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("FILE SAVED"));
				});
    }//GEN-LAST:event_repOverPointQuestionGroupsMIActionPerformed

    private void mapDiatopicQuestionGroupsSelMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mapDiatopicQuestionGroupsSelMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("MAP#BTNLABEL"),
				true, true, true, true, true, false, false, true, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					try {
						addIFrame(new SelectionResultPanel(map, sheet, filters, questions, varsGroups, quantity),
								java.util.ResourceBundle.getBundle("texts/Bundle").getString("DIATOPIC MAP"),
								true, true, true, true, true);
					} catch (IllegalStateException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO POINTS IN MAP"));
					} catch (InvalidClassException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
					}
				});
    }//GEN-LAST:event_mapDiatopicQuestionGroupsSelMIActionPerformed

    private void mapArealityQuestionGroupsMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mapArealityQuestionGroupsMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("MAP#BTNLABEL"),
				true, true, true, true, true, false, false, true, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					try {
						if (map.getTemplate() == null) {
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE THE MAP TEMPLATE FIRST"));
							return;
						}
						if (!checkMapValidity(map)) {
							return;
						}
						String baseTitle = java.util.ResourceBundle.getBundle("texts/Bundle").getString("AREALITY MAP");
						for (Filter filter : filters) {
							String title = baseTitle;
							if (filters.size() > 1) {
								title += " - " + filter;
							}
							addIFrame(new ArealityPanel(map, sheet, Arrays.asList(filter), questions, varsGroups),
									title, true, true, true, true, false);
						}
					} catch (IllegalStateException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO POINTS IN MAP"));
					} catch (InvalidClassException | NullPointerException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
					}
				});
    }//GEN-LAST:event_mapArealityQuestionGroupsMIActionPerformed

    private void mapGradArealityQuestionGroupsMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mapGradArealityQuestionGroupsMIActionPerformed
		openConsult(java.util.ResourceBundle.getBundle("texts/Bundle").getString("MAP#BTNLABEL"),
				true, true, true, true, true, false, false, true, (sheet, filters, questions, varsGroups, map, quantity, questionnaire) -> {
					try {
						if (map.getTemplate() == null) {
							StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE THE MAP TEMPLATE FIRST"));
							return;
						}
						if (!checkMapValidity(map)) {
							return;
						}
						String baseTitle = java.util.ResourceBundle.getBundle("texts/Bundle").getString("AREALITY MAP");
						for (Filter filter : filters) {
							String title = baseTitle;
							if (filters.size() > 1) {
								title += " - " + filter;
							}
							addIFrame(new GradualArealityPanel(map, sheet, Arrays.asList(filter), questions, varsGroups),
									title, true, true, true, true, false);
						}
					} catch (IllegalStateException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("NO POINTS IN MAP"));
					} catch (InvalidClassException | NullPointerException | SQLException ex) {
						LoggerManager.getInstance().log(Level.SEVERE, null, ex);
						StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
					}
				});
    }//GEN-LAST:event_mapGradArealityQuestionGroupsMIActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPopupMenu.Separator connSep;
    private javax.swing.JMenuItem createAgeGroupMI;
    private javax.swing.JMenuItem createGeoPointsMI;
    private javax.swing.JMenuItem createMapMI;
    private javax.swing.JMenu createMenu;
    private javax.swing.JMenuItem createQuestionCatsMI;
    private javax.swing.JMenuItem createQuestionsAndVarsMI;
    private javax.swing.JMenuItem createSheetMI;
    private javax.swing.JDesktopPane desktopPane;
    private javax.swing.JMenuItem fileConnectMI;
    private javax.swing.JMenuItem filePrintMI;
    private javax.swing.JMenuItem fileQuitMI;
    private javax.swing.JMenuItem fileSaveMapMI;
    private javax.swing.JMenuItem helpAboutMI;
    private javax.swing.JMenuItem helpContentsMI;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator7;
    private javax.swing.JPopupMenu.Separator jSeparator8;
    private javax.swing.JPopupMenu.Separator jSeparator9;
    private javax.swing.JMenuItem mapArealityMI;
    private javax.swing.JMenuItem mapArealityQuestionGroupsMI;
    private javax.swing.JMenuItem mapDiatopicMI;
    private javax.swing.JMenuItem mapDiatopicQuestionGroupsMI;
    private javax.swing.JMenuItem mapDiatopicQuestionGroupsSelMI;
    private javax.swing.JMenuItem mapDiatopicSelMI;
    private javax.swing.JMenuItem mapGradArealityMI;
    private javax.swing.JMenuItem mapGradArealityQuestionGroupsMI;
    private javax.swing.JMenuItem mapSavedMI;
    private javax.swing.JMenuItem queryAnsToXLSMI;
    private javax.swing.JMenuItem queryDatasheetsToXLSMI;
    private javax.swing.JMenu queryMenu;
    private javax.swing.JMenuItem registerAnswersMI;
    private javax.swing.JMenuItem registerDSMI;
    private javax.swing.JMenu registerMenu;
    private javax.swing.JMenuItem repObservationMI;
    private javax.swing.JMenuItem repOverPointMI;
    private javax.swing.JMenuItem repOverPointQuestionGroupsMI;
    private javax.swing.JMenuItem repOverQuestionGroupsMI;
    private javax.swing.JMenuItem repOverallMI;
    private javax.swing.JMenuItem repStateMI;
    // End of variables declaration//GEN-END:variables
}
