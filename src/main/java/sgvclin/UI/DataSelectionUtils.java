/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import sgvclin.MainController;
import sgvclin.data.Map;
import sgvclin.data.MapPrintSaver;
import sgvclin.data.MapSaver;
import sgvclin.data.Question;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan Oliveira
 */
public class DataSelectionUtils {

	public static String enterNewVariantForm(Question q) {
		JLabel varLabel = new JLabel(java.util.ResourceBundle.getBundle("texts/Bundle").getString("VARIANT"));
		JTextField varTF = new JTextField(20);
		JButton addBtn = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ADD"));
		addBtn.setToolTipText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ADDS A NEW VARIANT"));
		addBtn.setIcon(new javax.swing.ImageIcon(DataSelectionUtils.class.getResource("/icons/add.png")));
		JButton canBtn = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CANCEL"));
		canBtn.setIcon(new javax.swing.ImageIcon(DataSelectionUtils.class.getResource("/icons/no.png")));
		JPanel panel = new JPanel(new BorderLayout());
		JPanel mid = new JPanel(new FlowLayout());
		JPanel bottom = new JPanel(new FlowLayout());
		mid.add(varLabel);
		mid.add(varTF);
		panel.add(mid, BorderLayout.CENTER);
		if (q.getCategory().isPhonetic()) {
			VirtualKeyboardPanel vkp = new VirtualKeyboardPanel(varTF);
			panel.add(vkp, BorderLayout.EAST);
		}
		Boolean[] ok = {false};
		bottom.add(addBtn);
		bottom.add(canBtn);
		panel.add(bottom, BorderLayout.SOUTH);
		JDialog dialog = new JDialog(MainController.getInstance().getMainFrame(), true);
		dialog.add(panel);
		addBtn.addActionListener((e) -> {
			ok[0] = true;
			dialog.setVisible(false);
		});
		canBtn.addActionListener((e) -> dialog.setVisible(false));
		dialog.pack();
		dialog.setVisible(true);
		return ok[0] ? varTF.getText() : null;
	}

	public static Map selectMap() {
		Map[] maps;
		try {
			maps = Map.getMaps().toArray(new Map[Map.getMaps().size()]);
		} catch (ClassNotFoundException | IOException | NullPointerException | SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
			return null;
		}
		if (maps.length == 0) {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE A MAP FIRST"));
			return null;
		}
		return OptionPane.showInputDialog(MainController.getInstance().getMainFrame(),
				java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECT THE MAP"), "", OptionPane.QUESTION_MESSAGE, null, maps, maps[0]);
	}

	public static MapSaver selectMapSaver(boolean diatopicOnly) {
		MapSaver[] maps;
		try {
			if (diatopicOnly) {
				maps = MapSaver.getDiatopicMapSavers();
			} else {
				maps = MapSaver.getMapSavers();
			}
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			return null;
		}
		if (maps.length == 0) {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("SAVE A MAP FIRST"));
			return null;
		}
		final JDialog dialog = new JDialog(MainController.getInstance().getMainFrame(), java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECT THE MAP:"));
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		JPanel backPanel = new JPanel(new BorderLayout());
		JList<MapSaver> list = new JList<>(new DefaultListModel<>());
		for (MapSaver map : maps) {
			((DefaultListModel<MapSaver>) list.getModel()).addElement(map);
		}
		backPanel.add(new JScrollPane(list), BorderLayout.CENTER);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setVisibleRowCount(10);
		JPanel btnPanel = new JPanel(new FlowLayout());
		JButton btnOK = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("LOAD"));
		JButton btnDel = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DELETE"));
		JButton btnCancel = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CANCEL"));
		try {
			btnOK.setIcon(new ImageIcon(ImageIO.read(OptionPane.class.getResource("/icons/yes.png"))));
			btnDel.setIcon(new ImageIcon(ImageIO.read(OptionPane.class.getResource("/icons/remove.png"))));
			btnCancel.setIcon(new ImageIcon(ImageIO.read(OptionPane.class.getResource("/icons/no.png"))));
		} catch (IOException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
		btnOK.setToolTipText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("LOADS THE SELECTED MAP"));
		btnDel.setToolTipText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DELETES SELECTED MAP"));
		btnCancel.setToolTipText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CANCELS THE OPERATION"));
		btnPanel.add(btnOK);
		btnPanel.add(btnDel);
		btnPanel.add(btnCancel);
		backPanel.add(btnPanel, BorderLayout.SOUTH);
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
					btnOK.doClick();
				}
			}

		});
		btnOK.addActionListener((evt) -> dialog.dispose());
		btnDel.addActionListener((evt) -> {
			if (OptionPane.showConfirmDialog(MainController.getInstance().getMainFrame(),
					java.util.ResourceBundle.getBundle("texts/Bundle").getString("DELETE MAP?"),
					"", OptionPane.YES_NO_OPTION) == OptionPane.YES_OPTION) {
				try {
					MapSaver.deleteMapSaver(list.getSelectedValue());
					((DefaultListModel) list.getModel()).removeElement(list.getSelectedValue());
				} catch (SQLException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
					StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR WRITING TO THE DATABASE"));
				}
			}
		});
		btnCancel.addActionListener((evt) -> {
			list.clearSelection();
			dialog.dispose();
		});
		dialog.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				list.clearSelection();
				dialog.dispose();
			}
		});
		dialog.setModal(true);
		dialog.getRootPane().setContentPane(backPanel);
		dialog.getRootPane().setDefaultButton(btnOK);
		dialog.pack();
		dialog.setLocationRelativeTo(MainController.getInstance().getMainFrame());
		dialog.setVisible(true);
		return list.getSelectedValue();
	}

	public static MapPrintSaver selectMapPrintSaver(int mapKey) {
		MapPrintSaver[] maps;
		try {
			maps = MapPrintSaver.getMapPrintSavers(mapKey);
		} catch (NullPointerException | SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			return null;
		}
		if (maps.length == 0) {
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CREATE OR LOAD A MAP FIRST"));
			return null;
		}
		final JDialog dialog = new JDialog(MainController.getInstance().getMainFrame(), java.util.ResourceBundle.getBundle("texts/Bundle").getString("SELECT THE MAP:"));
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		JPanel backPanel = new JPanel(new BorderLayout());
		JList<MapPrintSaver> list = new JList<>(new DefaultListModel<>());
		for (MapPrintSaver map : maps) {
			((DefaultListModel<MapPrintSaver>) list.getModel()).addElement(map);
		}
		backPanel.add(new JScrollPane(list), BorderLayout.CENTER);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setVisibleRowCount(10);
		JPanel btnPanel = new JPanel(new FlowLayout());
		JButton btnOK = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("OPEN"));
		JButton btnDel = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DELETE"));
		JButton btnCancel = new JButton(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CANCEL"));
		try {
			btnOK.setIcon(new ImageIcon(ImageIO.read(OptionPane.class.getResource("/icons/yes.png"))));
			btnDel.setIcon(new ImageIcon(ImageIO.read(OptionPane.class.getResource("/icons/remove.png"))));
			btnCancel.setIcon(new ImageIcon(ImageIO.read(OptionPane.class.getResource("/icons/no.png"))));
		} catch (IOException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
		btnOK.setToolTipText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("LOADS THE SELECTED MAP"));
		btnDel.setToolTipText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DELETES SELECTED MAP"));
		btnCancel.setToolTipText(java.util.ResourceBundle.getBundle("texts/Bundle").getString("CANCELS THE OPERATION"));
		btnPanel.add(btnOK);
		btnPanel.add(btnDel);
		btnPanel.add(btnCancel);
		backPanel.add(btnPanel, BorderLayout.SOUTH);
		btnOK.addActionListener((evt) -> dialog.dispose());
		btnDel.addActionListener((evt) -> {
			if (OptionPane.showConfirmDialog(MainController.getInstance().getMainFrame(),
					java.util.ResourceBundle.getBundle("texts/Bundle").getString("DELETE MAP?"),
					"", OptionPane.YES_NO_OPTION) == OptionPane.YES_OPTION) {
				try {
					MapPrintSaver.deleteMapPrintSaver(list.getSelectedValue());
					((DefaultListModel) list.getModel()).removeElement(list.getSelectedValue());
				} catch (SQLException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
					StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
				}
			}
		});
		btnCancel.addActionListener((evt) -> {
			list.clearSelection();
			dialog.dispose();
		});
		dialog.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				list.clearSelection();
				dialog.dispose();
			}
		});
		dialog.setModal(true);
		dialog.getRootPane().setContentPane(backPanel);
		dialog.getRootPane().setDefaultButton(btnOK);
		dialog.pack();
		dialog.setLocationRelativeTo(MainController.getInstance().getMainFrame());
		dialog.setVisible(true);
		return list.getSelectedValue();
	}

	private DataSelectionUtils() {
	}
}
