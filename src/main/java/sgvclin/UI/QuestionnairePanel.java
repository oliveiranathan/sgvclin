/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.Color;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import sgvclin.MainController;
import sgvclin.data.Question;
import sgvclin.data.QuestionCategory;
import sgvclin.data.Questionnaire;
import sgvclin.data.Variant;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan Oliveira
 */
public class QuestionnairePanel extends javax.swing.JPanel {

	private static final long serialVersionUID = 1L;

	private final DefaultComboBoxModel<QuestionCategory> categoryModel;
	private final DefaultListModel<Questionnaire> questionnaireModel;
	private final DefaultListModel<Variant> variantModel;
	private Question question = null;
	private boolean isNew = true;

	public QuestionnairePanel() throws SQLException {
		categoryModel = new DefaultComboBoxModel<>(QuestionCategory.getQuestionCategories());
		questionnaireModel = new DefaultListModel<Questionnaire>() {
			private static final long serialVersionUID = 1L;

			private List<Questionnaire> data = Questionnaire.getQuestionnaires();

			@Override
			public int getSize() {
				if (data == null) {
					return 0;
				}
				return data.size();
			}

			@Override
			public Questionnaire getElementAt(int index) {
				if (data == null) {
					return null;
				}
				return data.get(index);
			}

			@Override
			public void addElement(Questionnaire element) {
				try {
					Questionnaire.saveQuestionnaire(element.getName());
					data = Questionnaire.getQuestionnaires();
					fireContentsChanged(this, 0, data.size() - 1);
					questionnaireList.setSelectedValue(Questionnaire.getQuestionnaire(element.getName()), true);
				} catch (SQLException | NullPointerException ex) { // If UNIQUE, already have the category, so do not notify the user...
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				}
			}

			@Override
			public boolean removeElement(Object obj) {
				try {
					data = Questionnaire.getQuestionnaires();
					fireContentsChanged(this, 0, data.size());
					questionnaireList.clearSelection();
				} catch (SQLException ex) {
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				}
				return true;
			}
		};
		variantModel = new DefaultListModel<Variant>() {
			private static final long serialVersionUID = 1L;

			private List<Variant> data = null;

			@Override
			public int getSize() {
				if (data == null) {
					return 0;
				}
				return data.size();
			}

			@Override
			public Variant getElementAt(int index) {
				if (data == null) {
					return null;
				}
				return data.get(index);
			}

			@Override
			public void addElement(Variant element) { // if it receives null, it only updates the list. Sorry about it :)
				if (question == null) {
					return;
				}
				try {
					if (element != null && element.getId() == 0) {
						Variant.saveVariant(question, element.getAnswer());
					}
					data = Variant.getListOfVariants(question);
					fireContentsChanged(this, 0, data.size());
				} catch (SQLException ex) { // If UNIQUE, already have the category, so do not notify the user...
					LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				}
			}

			@Override
			public void clear() {
				if (data == null) {
					return;
				}
				int oldSize = data.size();
				data = null;
				fireContentsChanged(this, 0, oldSize - 1);
			}
		};
		initComponents();
		setVKBtnStatus();
	}

	private void setVKBtnStatus() {
		if (categoryCombBox.getSelectedItem() == null) {
			return;
		}
		if (categoryCombBox.getSelectedItem().toString().startsWith(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PPQ"))) {
			vkBtn.setEnabled(true);
		} else {
			vkBtn.setEnabled(false);
		}
	}

	private void updateQuestionData() throws SQLException {
		questionNumberTF.setBackground(UIManager.getColor("TextField.background"));
		questionTitleTF.setBackground(UIManager.getColor("TextField.background"));
		Questionnaire selected = questionnaireList.getSelectedValue();
		if (selected == null) {
			delBtn.setEnabled(false);
			renameBtn.setEnabled(false);
			questionNumberTF.setText("");
			questionTitleTF.setText("");
			question = null;
			variantModel.clear();
			variantTF.setText("");
			return;
		}
		delBtn.setEnabled(true);
		renameBtn.setEnabled(true);
		if (question == null || !question.getQuestionnaire().equals(selected)) {
			question = Question.getFirstQuestion(selected);
			isNew = question.getQuestion() == null || question.getQuestion().isEmpty();
		}
		questionNumberTF.setText(isNew ? "" : "" + question.getNumber());
		questionTitleTF.setText(isNew ? "" : question.getQuestion());
		categoryCombBox.setSelectedItem(question.getCategory());
		if (!isNew && !Variant.getListOfVariants(question).isEmpty()) {
			variantModel.addElement(Variant.getListOfVariants(question).get(0));
		} else {
			variantModel.clear();
		}
		variantTF.setText("");
	}

	private boolean saveQuestion() {
		int number;
		try {
			number = Integer.parseInt(questionNumberTF.getText());
			questionNumberTF.setBackground(UIManager.getColor("TextField.background"));
		} catch (NumberFormatException ex) {
			questionNumberTF.setBackground(Color.red);
			return false;
		}
		question.setNumber(number);
		if (questionTitleTF.getText().trim().isEmpty()) {
			questionTitleTF.setBackground(Color.red);
			return false;
		}
		questionTitleTF.setBackground(UIManager.getColor("TextField.background"));
		question.setQuestion(questionTitleTF.getText());
		if (categoryCombBox.getSelectedItem() == null) {
			categoryCombBox.setBackground(Color.red);
			return false;
		}
		categoryCombBox.setBackground(UIManager.getColor("ComboBox.background"));
		question.setCategory((QuestionCategory) categoryCombBox.getSelectedItem());
		try {
			Question.saveQuestion(question);
			isNew = false;
			StatusBarImpl.getInstance().setOKStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("WRITTEN TO THE DATABASE"));
			return true;
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR WRITING TO THE DATABASE"));
			return false;
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
	 * content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings({"unchecked", "Convert2Lambda"})
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		questionnaireTF = new javax.swing.JTextField();
		addQuestionnaireTF = new javax.swing.JButton();
		jScrollPane1 = new javax.swing.JScrollPane();
		questionnaireList = new javax.swing.JList<>();
		delBtn = new javax.swing.JButton();
		renameBtn = new javax.swing.JButton();
		jPanel2 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		questionNumberTF = new javax.swing.JTextField();
		jLabel2 = new javax.swing.JLabel();
		questionTitleTF = new javax.swing.JTextField();
		jLabel3 = new javax.swing.JLabel();
		categoryCombBox = new javax.swing.JComboBox<>();
		jPanel3 = new javax.swing.JPanel();
		jScrollPane2 = new javax.swing.JScrollPane();
		variantsList = new javax.swing.JList<>();
		jLabel4 = new javax.swing.JLabel();
		variantTF = new javax.swing.JTextField();
		addVarBtn = new javax.swing.JButton();
		delVarBtn = new javax.swing.JButton();
		swapVarBtn = new javax.swing.JButton();
		vkBtn = new javax.swing.JButton();
		firstBtn = new javax.swing.JButton();
		previousBtn = new javax.swing.JButton();
		nextBtn = new javax.swing.JButton();
		lastBtn = new javax.swing.JButton();
		newBtn = new javax.swing.JButton();
		saveBtn = new javax.swing.JButton();
		deleteBtn = new javax.swing.JButton();

		java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("texts/Bundle"); // NOI18N
		jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("QUESTION GROUPS"))); // NOI18N

		addQuestionnaireTF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/add.png"))); // NOI18N
		addQuestionnaireTF.setText(bundle.getString("ADD")); // NOI18N
		addQuestionnaireTF.setToolTipText(bundle.getString("ADDS A NEW GROUP")); // NOI18N
		addQuestionnaireTF.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addQuestionnaireTFActionPerformed(evt);
			}
		});

		questionnaireList.setModel(questionnaireModel);
		questionnaireList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		questionnaireList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
			public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
				questionnaireListValueChanged(evt);
			}
		});
		jScrollPane1.setViewportView(questionnaireList);

		delBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/remove.png"))); // NOI18N
		delBtn.setText(bundle.getString("DELETE")); // NOI18N
		delBtn.setEnabled(false);
		delBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delBtnActionPerformed(evt);
			}
		});

		renameBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/swap.png"))); // NOI18N
		renameBtn.setText(bundle.getString("RENAME")); // NOI18N
		renameBtn.setEnabled(false);
		renameBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				renameBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(questionnaireTF)
				.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(addQuestionnaireTF, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
								.addComponent(delBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(renameBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addContainerGap())
		);
		jPanel1Layout.setVerticalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addComponent(questionnaireTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(addQuestionnaireTF)
						.addGap(18, 18, 18)
						.addComponent(jScrollPane1)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(renameBtn)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(delBtn)
						.addContainerGap())
		);

		jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("QUESTIONS"))); // NOI18N

		jLabel1.setText(bundle.getString("NUMBER")); // NOI18N

		jLabel2.setText(bundle.getString("TITLE")); // NOI18N

		jLabel3.setText(bundle.getString("CATEGORY")); // NOI18N

		categoryCombBox.setModel(categoryModel);
		categoryCombBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				categoryCombBoxItemStateChanged(evt);
			}
		});

		jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("VARIANTS#BORDERTITLE"))); // NOI18N

		jScrollPane2.setMinimumSize(new java.awt.Dimension(259, 131));

		variantsList.setModel(variantModel);
		variantsList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		jScrollPane2.setViewportView(variantsList);

		jLabel4.setText(bundle.getString("VARIANT")); // NOI18N

		addVarBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/add.png"))); // NOI18N
		addVarBtn.setText(bundle.getString("ADD")); // NOI18N
		addVarBtn.setToolTipText(bundle.getString("ADDS A NEW VARIANT")); // NOI18N
		addVarBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addVarBtnActionPerformed(evt);
			}
		});

		delVarBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/no.png"))); // NOI18N
		delVarBtn.setText(bundle.getString("REMOVE")); // NOI18N
		delVarBtn.setToolTipText(bundle.getString("REMOVES SELECTED VARIANT")); // NOI18N
		delVarBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				delVarBtnActionPerformed(evt);
			}
		});

		swapVarBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/swap.png"))); // NOI18N
		swapVarBtn.setText(bundle.getString("SWAP")); // NOI18N
		swapVarBtn.setToolTipText(bundle.getString("SWAPS SELECTED VARIANT")); // NOI18N
		swapVarBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				swapVarBtnActionPerformed(evt);
			}
		});

		vkBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/keyboard.png"))); // NOI18N
		vkBtn.setText(bundle.getString("VIRTUAL KEYBOARD")); // NOI18N
		vkBtn.setToolTipText(bundle.getString("OPENS A VIRTUAL KEYBOARD WITH IPA SIMBOLS")); // NOI18N
		vkBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				vkBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
		jPanel3.setLayout(jPanel3Layout);
		jPanel3Layout.setHorizontalGroup(
				jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel3Layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(jPanel3Layout.createSequentialGroup()
										.addComponent(jLabel4)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(variantTF))
								.addGroup(jPanel3Layout.createSequentialGroup()
										.addComponent(addVarBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(delVarBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(swapVarBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(vkBtn)
										.addGap(0, 0, Short.MAX_VALUE)))
						.addContainerGap())
		);
		jPanel3Layout.setVerticalGroup(
				jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel3Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(jPanel3Layout.createSequentialGroup()
										.addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel4)
												.addComponent(variantTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(addVarBtn)
												.addComponent(delVarBtn)
												.addComponent(swapVarBtn)
												.addComponent(vkBtn)))
								.addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);

		firstBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/first.png"))); // NOI18N
		firstBtn.setText(bundle.getString("FIRST")); // NOI18N
		firstBtn.setToolTipText(bundle.getString("FIRST#TOOLTIP")); // NOI18N
		firstBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				firstBtnActionPerformed(evt);
			}
		});

		previousBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/previous.png"))); // NOI18N
		previousBtn.setText(bundle.getString("PREVIOUS")); // NOI18N
		previousBtn.setToolTipText(bundle.getString("PREVIOUS#TOOLTIP")); // NOI18N
		previousBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				previousBtnActionPerformed(evt);
			}
		});

		nextBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/next.png"))); // NOI18N
		nextBtn.setText(bundle.getString("NEXT")); // NOI18N
		nextBtn.setToolTipText(bundle.getString("NEXT#TOOLTIP")); // NOI18N
		nextBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				nextBtnActionPerformed(evt);
			}
		});

		lastBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/last.png"))); // NOI18N
		lastBtn.setText(bundle.getString("LAST")); // NOI18N
		lastBtn.setToolTipText(bundle.getString("LAST#TOOLTIP")); // NOI18N
		lastBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				lastBtnActionPerformed(evt);
			}
		});

		newBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/add_item.png"))); // NOI18N
		newBtn.setText(bundle.getString("NEW")); // NOI18N
		newBtn.setToolTipText(bundle.getString("NEW#TOOLTIP")); // NOI18N
		newBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				newBtnActionPerformed(evt);
			}
		});

		saveBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/save.png"))); // NOI18N
		saveBtn.setText(bundle.getString("SAVE")); // NOI18N
		saveBtn.setToolTipText(bundle.getString("SAVE#TOOLTIP")); // NOI18N
		saveBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				saveBtnActionPerformed(evt);
			}
		});

		deleteBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/no.png"))); // NOI18N
		deleteBtn.setText(bundle.getString("DELETE")); // NOI18N
		deleteBtn.setToolTipText(bundle.getString("DELETE#TOOLTIP")); // NOI18N
		deleteBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				deleteBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout.setHorizontalGroup(
				jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addGroup(jPanel2Layout.createSequentialGroup()
										.addComponent(jLabel1)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(questionNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(jLabel2)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(questionTitleTF))
								.addGroup(jPanel2Layout.createSequentialGroup()
										.addComponent(jLabel3)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(categoryCombBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGroup(jPanel2Layout.createSequentialGroup()
										.addComponent(firstBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(previousBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(nextBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(lastBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(newBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(saveBtn)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(deleteBtn)
										.addGap(0, 0, Short.MAX_VALUE)))
						.addContainerGap())
		);
		jPanel2Layout.setVerticalGroup(
				jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel1)
								.addComponent(questionNumberTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(jLabel2)
								.addComponent(questionTitleTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel3)
								.addComponent(categoryCombBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(firstBtn)
								.addComponent(previousBtn)
								.addComponent(nextBtn)
								.addComponent(lastBtn)
								.addComponent(newBtn)
								.addComponent(saveBtn)
								.addComponent(deleteBtn))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		);
	}// </editor-fold>//GEN-END:initComponents

	private void addQuestionnaireTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addQuestionnaireTFActionPerformed
		if (questionnaireTF.getText().trim().isEmpty()) {
			return;
		}
		Questionnaire q = new Questionnaire();
		q.setName(questionnaireTF.getText().trim());
		questionnaireModel.addElement(q);
		questionnaireTF.setText("");
	}//GEN-LAST:event_addQuestionnaireTFActionPerformed

	private void addVarBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addVarBtnActionPerformed
		if (variantTF.getText().trim().isEmpty()) {
			return;
		}
		if (isNew) {
			if (!saveQuestion()) {
				return;
			}
		}
		Variant v = new Variant();
		v.setAnswer(variantTF.getText().trim());
		variantModel.addElement(v);
		variantTF.setText("");
	}//GEN-LAST:event_addVarBtnActionPerformed

	private void questionnaireListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_questionnaireListValueChanged
		try {
			saveQuestion();
			updateQuestionData();
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
	}//GEN-LAST:event_questionnaireListValueChanged

	private void delVarBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delVarBtnActionPerformed
		Variant v = variantsList.getSelectedValue();
		if (v == null) {
			return;
		}
		try {
			Variant.deleteVariant(v);
			variantModel.addElement(null);
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
	}//GEN-LAST:event_delVarBtnActionPerformed

	private void swapVarBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_swapVarBtnActionPerformed
		Variant v = variantsList.getSelectedValue();
		if (v == null || variantTF.getText().trim().isEmpty()) {
			return;
		}
		String swapped = v.getAnswer();
		try {
			Variant.swapVariant(v, variantTF.getText().trim());
			variantModel.addElement(null);
			variantTF.setText(swapped);
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
	}//GEN-LAST:event_swapVarBtnActionPerformed

	private void vkBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vkBtnActionPerformed
		MainController.getInstance().getMainFrame().showVirtualKeyboard(variantTF);
	}//GEN-LAST:event_vkBtnActionPerformed

	private void firstBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_firstBtnActionPerformed
		if (!questionTitleTF.getText().trim().isEmpty()) {
			if (!saveQuestion()) {
				return;
			}
		}
		try {
			question = Question.getFirstQuestion(questionnaireList.getSelectedValue());
			isNew = question.getQuestion() == null || question.getQuestion().isEmpty();
			updateQuestionData();
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
	}//GEN-LAST:event_firstBtnActionPerformed

	private void previousBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_previousBtnActionPerformed
		if (!questionTitleTF.getText().trim().isEmpty()) {
			if (!saveQuestion()) {
				return;
			}
		}
		try {
			question = Question.getPreviousQuestion(questionnaireList.getSelectedValue(), question);
			isNew = question.getQuestion() == null || question.getQuestion().isEmpty();
			updateQuestionData();
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
	}//GEN-LAST:event_previousBtnActionPerformed

	private void nextBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextBtnActionPerformed
		if (!questionTitleTF.getText().trim().isEmpty()) {
			if (!saveQuestion()) {
				return;
			}
		}
		try {
			question = Question.getNextQuestion(questionnaireList.getSelectedValue(), question);
			isNew = question.getQuestion() == null || question.getQuestion().isEmpty();
			updateQuestionData();
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
	}//GEN-LAST:event_nextBtnActionPerformed

	private void lastBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lastBtnActionPerformed
		if (!questionTitleTF.getText().trim().isEmpty()) {
			if (!saveQuestion()) {
				return;
			}
		}
		try {
			question = Question.getLastQuestion(questionnaireList.getSelectedValue());
			isNew = question.getQuestion() == null || question.getQuestion().isEmpty();
			updateQuestionData();
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
	}//GEN-LAST:event_lastBtnActionPerformed

	private void newBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newBtnActionPerformed
		if (!questionTitleTF.getText().trim().isEmpty()) {
			if (!saveQuestion()) {
				return;
			}
		}
		try {
			question = Question.getNewQuestion(questionnaireList.getSelectedValue());
			isNew = question.getQuestion() == null || question.getQuestion().isEmpty();
			updateQuestionData();
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
	}//GEN-LAST:event_newBtnActionPerformed

	private void saveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtnActionPerformed
		saveQuestion();
	}//GEN-LAST:event_saveBtnActionPerformed

	private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBtnActionPerformed
		try {
			question = Question.deleteQuestion(questionnaireList.getSelectedValue(), question);
			updateQuestionData();
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("ERROR READING FROM THE DATABASE"));
		}
	}//GEN-LAST:event_deleteBtnActionPerformed

	private void categoryCombBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_categoryCombBoxItemStateChanged
		setVKBtnStatus();
	}//GEN-LAST:event_categoryCombBoxItemStateChanged

	private void delBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delBtnActionPerformed
		if (questionnaireList.getSelectedValue() == null) {
			return;
		}
		Questionnaire cat = questionnaireList.getSelectedValue();
		try {
			int count = Question.getListOfQuestions(cat).size();
			if (count != 0) {
				if (JOptionPane.showConfirmDialog(this,
						java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("texts/Bundle")
								.getString("REMOVING THIS QUESTIONNAIRE WILL CAUSE THE EXCLUSION OF {0} QUESTIONS.\\NARE YOU SURE YOU WANT TO CONTINUE?"),
								count), "", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
					return;
				}
			}
			Questionnaire.removeQuestionnaire(cat);
			questionnaireModel.removeElement(cat);
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
	}//GEN-LAST:event_delBtnActionPerformed

	private void renameBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_renameBtnActionPerformed
		if (questionnaireList.getSelectedValue() == null) {
			return;
		}
		String cat = questionnaireList.getSelectedValue().getName();
		String newName = JOptionPane.showInputDialog(this, java.util.ResourceBundle.getBundle("texts/Bundle").getString("NEW NAME"), cat);
		if (!newName.equals(cat)) {
			try {
				if (Questionnaire.getQuestionnaire(newName) != null) {
					StatusBarImpl.getInstance().setERRStatus(java.util.ResourceBundle.getBundle("texts/Bundle").getString("QUESTIONNAIRE ALREADY EXISTS"));
				} else {
					questionnaireList.getSelectedValue().setName(newName);
					Questionnaire.updateQuestionnaire(questionnaireList.getSelectedValue());
					questionnaireList.repaint();
				}
			} catch (SQLException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		}
	}//GEN-LAST:event_renameBtnActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton addQuestionnaireTF;
	private javax.swing.JButton addVarBtn;
	private javax.swing.JComboBox<QuestionCategory> categoryCombBox;
	private javax.swing.JButton delBtn;
	private javax.swing.JButton delVarBtn;
	private javax.swing.JButton deleteBtn;
	private javax.swing.JButton firstBtn;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JPanel jPanel3;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JButton lastBtn;
	private javax.swing.JButton newBtn;
	private javax.swing.JButton nextBtn;
	private javax.swing.JButton previousBtn;
	private javax.swing.JTextField questionNumberTF;
	private javax.swing.JTextField questionTitleTF;
	private javax.swing.JList<Questionnaire> questionnaireList;
	private javax.swing.JTextField questionnaireTF;
	private javax.swing.JButton renameBtn;
	private javax.swing.JButton saveBtn;
	private javax.swing.JButton swapVarBtn;
	private javax.swing.JTextField variantTF;
	private javax.swing.JList<Variant> variantsList;
	private javax.swing.JButton vkBtn;
	// End of variables declaration//GEN-END:variables
}
