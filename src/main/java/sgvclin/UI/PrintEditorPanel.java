/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.UI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import net.coobird.thumbnailator.Thumbnailator;
import sgvclin.data.MapPrintSaver;

/**
 *
 * @author Nathan Oliveira
 */
class PrintEditorPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private final BufferedImage labels, histogram, questions;
	private final BufferedImage[] images;
	private final ImgPos mapPos, copyrightPos, labelsPos, histogramPos, titlePos, questionsPos;
	private final ImgPos[] positions;
	private ImgPos editing = null;
	private final Rectangle[] handles;
	private int W = 0, H = 0;
	private float scale = 1.f;

	PrintEditorPanel(MapPrintSaver mps, BufferedImage map, BufferedImage title, BufferedImage copyright, BufferedImage labels, BufferedImage histogram, BufferedImage questions) {
		if (map == null || copyright == null || title == null) {
			throw new IllegalArgumentException("Nor the map, nor the title, nor the copyright images can be null");
		}
		this.labels = labels;
		this.histogram = histogram;
		this.questions = questions;
		if (mps == null) {
			titlePos = new ImgPos(0, 0, title.getWidth(), title.getHeight());
			mapPos = new ImgPos(0, titlePos.h + 20, map.getWidth(), map.getHeight());
			int ch = map.getHeight() / 20;
			int cw = copyright.getWidth() * ch / copyright.getHeight();
			copyrightPos = new ImgPos(mapPos.w - cw, mapPos.y + mapPos.h - ch, cw, ch);
			if (labels != null) {
				labelsPos = new ImgPos(mapPos.w + 20, titlePos.h + 20, labels.getWidth(), labels.getHeight());
			} else {
				labelsPos = null;
			}
			if (histogram != null) {
				histogramPos = new ImgPos(mapPos.w + 20, titlePos.h + 20 + (labels != null ? labelsPos.h + 20 : 0), histogram.getWidth(), histogram.getHeight());
			} else {
				histogramPos = null;
			}
			if (questions != null) {
				questionsPos = new ImgPos(0, mapPos.h + 20, questions.getWidth(), questions.getHeight());
			} else {
				questionsPos = null;
			}
		} else {
			if (mps.getTitleW() * title.getHeight() == mps.getTitleH() * title.getWidth()) {
				titlePos = new ImgPos(mps.getTitleX(), mps.getTitleY(), mps.getTitleW(), mps.getTitleH());
			} else {
				titlePos = new ImgPos(mps.getTitleX(), mps.getTitleY(), mps.getTitleW(), mps.getTitleW() * title.getHeight() / title.getWidth());
			}
			mapPos = new ImgPos(mps.getMapX(), mps.getMapY(), mps.getMapW(), mps.getMapH());
			copyrightPos = new ImgPos(mps.getCrX(), mps.getCrY(), mps.getCrW(), mps.getCrH());
			if (labels != null) {
				if (mps.getLegW() * labels.getHeight() == mps.getLegH() * labels.getWidth()) {
					labelsPos = new ImgPos(mps.getLegX(), mps.getLegY(), mps.getLegW(), mps.getLegH());
				} else {
					labelsPos = new ImgPos(mps.getLegX(), mps.getLegY(), mps.getLegW(), mps.getLegW() * labels.getHeight() / labels.getWidth());
				}
			} else {
				labelsPos = null;
			}
			if (histogram != null) {
				histogramPos = new ImgPos(mps.getHisX(), mps.getHisY(), mps.getHisW(), mps.getHisH());
			} else {
				histogramPos = null;
			}
			if (questions != null) {
				questionsPos = new ImgPos(mps.getQueX(), mps.getQueY(), mps.getQueW(), mps.getQueH());
			} else {
				questionsPos = null;
			}
		}
		images = new BufferedImage[6];
		positions = new ImgPos[6];
		images[0] = map;
		positions[0] = mapPos;
		images[1] = title;
		positions[1] = titlePos;
		images[2] = copyright;
		positions[2] = copyrightPos;
		images[3] = labels;
		positions[3] = labelsPos;
		images[4] = histogram;
		positions[4] = histogramPos;
		images[5] = questions;
		positions[5] = questionsPos;
		handles = new Rectangle[6];
		for (int i = 0; i < handles.length; i++) {
			handles[i] = null;
		}
		MouseAdapter ma = new MouseAdapter() {
			private ImgPos position = null;
			private Rectangle rectangle = null;
			private int x, y;
			private int xIni, yIni;

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.getWheelRotation() > 0) {
					scale -= .05f;
					if (scale < .1f) {
						scale = .1f;
					}
				} else if (e.getWheelRotation() < 0) {
					scale += .05f;
					if (scale > 1.f) {
						scale = 1.f;
					}
				}
				repaint();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				requestFocus();
				int xEvt = (int) (e.getX() / scale);
				int yEvt = (int) (e.getY() / scale);
				for (Rectangle rect : handles) {
					if (rect != null && rect.contains(e.getX(), e.getY())) {
						rectangle = rect;
					}
				}
				for (ImgPos pos : positions) {
					if (pos != null && pos.getRect().contains(xEvt, yEvt)) {
						position = pos;
						editing = pos;
					}
				}
				if (rectangle != null && position == null) {
					rectangle = null;
					return;
				}
				if (position == null) {
					return;
				}
				x = position.x;
				y = position.y;
				if (rectangle != null) {
					x = position.w;
					y = position.h;
				}
				xIni = (int) (e.getX() / scale);
				yIni = (int) (e.getY() / scale);
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				if (position == null) {
					return;
				}
				int xEvt = (int) (e.getX() / scale);
				int yEvt = (int) (e.getY() / scale);
				if (rectangle == null) {
					position.x = x + xEvt - xIni;
					if (position.x < 0) {
						position.x = 0;
					}
					position.y = y + yEvt - yIni;
					if (position.y < 0) {
						position.y = 0;
					}
				} else {
					int d = xEvt - xIni;
					int dx, dy;
					if (d == 0 || d > 0 && d < yEvt - y || d < 0 && d > yEvt - y) {
						d = yEvt - yIni;
						dy = d;
						dx = d * position.w / position.h;
					} else {
						dx = d;
						dy = d * position.h / position.w;
					}
					position.w = x + dx;
					position.h = y + dy;
				}
				update();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				rectangle = null;
				position = null;
				editing = null;
				repaint();
			}
		};
		addMouseListener(ma);
		addMouseMotionListener(ma);
		setFocusable(true);
		setBackground(Color.WHITE);
		SwingUtilities.invokeLater(() -> requestFocus());
	}

	private void update() {
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (H == 0 || W == 0) {
			W = getWidth();
			H = getHeight();
		}
		int mW = 0, mH = 0;
		for (ImgPos ip : positions) {
			if (ip == null) {
				continue;
			}
			if (ip.x + ip.w > mW) {
				mW = ip.x + ip.w;
			}
			if (ip.y + ip.h > mH) {
				mH = ip.y + ip.h;
			}
		}
		scale = 1.0f;
		if (scale > (float) W / mW) {
			scale = (float) W / mW;
		}
		if (scale > (float) H / mH) {
			scale = (float) H / mH;
		}
		for (int i = 0; i < images.length; i++) {
			if (images[i] != null) {
				handles[i] = drawImage(g, images[i], positions[i].x, positions[i].y, positions[i].getXF(), positions[i].getYF(),
						0, 0, images[i].getWidth(), images[i].getHeight());
			}
		}
		if (editing != null) {
			g.setColor(Color.RED);
			g.drawRect((int) (editing.x * scale + .5f), (int) (editing.y * scale + .5f), (int) (editing.w * scale + .5f), (int) (editing.h * scale + .5f));
		}
	}

	private Rectangle drawImage(Graphics g, BufferedImage image, int dx1, int dy1, int dx2, int dy2, int sx1, int sy1, int sx2, int sy2) {
		g.drawImage(image, (int) (dx1 * scale + .5f), (int) (dy1 * scale + .5f), (int) (dx2 * scale + .5f), (int) (dy2 * scale + .5f), sx1, sy1, sx2, sy2, null);
		g.setColor(Color.BLACK);
		g.drawRect((int) (dx1 * scale + .5f), (int) (dy1 * scale + .5f), (int) ((dx2 - dx1) * scale + .5f), (int) ((dy2 - dy1) * scale + .5f));
		g.setColor(Color.CYAN);
		g.drawRect((int) (dx2 * scale + .5f) - 10, (int) (dy2 * scale + .5f) - 10, 10, 10);
		return new Rectangle((int) (dx2 * scale + .5f) - 10, (int) (dy2 * scale + .5f) - 10, 10, 10);
	}

	public BufferedImage getImage() {
		int mW = 0, mH = 0;
		for (ImgPos ip : positions) {
			if (ip == null) {
				continue;
			}
			if (ip.getXF() > mW) {
				mW = ip.getXF();
			}
			if (ip.getYF() > mH) {
				mH = ip.getYF();
			}
		}
		BufferedImage result = new BufferedImage(mW, mH, BufferedImage.TYPE_INT_ARGB);
		Graphics g = result.getGraphics();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, mW, mH);
		for (int i = 0; i < images.length; i++) {
			if (images[i] != null) {
				ImgPos p = positions[i];
				BufferedImage img = Thumbnailator.createThumbnail(images[i], p.w, p.h);
				g.drawImage(img, p.x, p.y, p.getXF(), p.getYF(), 0, 0, img.getWidth(), img.getHeight(), null);
			}
		}
		return result;
	}

	protected void setPositions(MapPrintSaver mps) {
		mps.setMapX(mapPos.x);
		mps.setMapY(mapPos.y);
		mps.setMapW(mapPos.w);
		mps.setMapH(mapPos.h);
		mps.setTitleX(titlePos.x);
		mps.setTitleY(titlePos.y);
		mps.setTitleW(titlePos.w);
		mps.setTitleH(titlePos.h);
		mps.setCrX(copyrightPos.x);
		mps.setCrY(copyrightPos.y);
		mps.setCrW(copyrightPos.w);
		mps.setCrH(copyrightPos.h);
		if (labelsPos != null) {
			mps.setLegX(labelsPos.x);
			mps.setLegY(labelsPos.y);
			mps.setLegW(labelsPos.w);
			mps.setLegH(labelsPos.h);
		}
		if (histogramPos != null) {
			mps.setHisX(histogramPos.x);
			mps.setHisY(histogramPos.y);
			mps.setHisW(histogramPos.w);
			mps.setHisH(histogramPos.h);
		}
		if (questionsPos != null) {
			mps.setQueX(questionsPos.x);
			mps.setQueY(questionsPos.y);
			mps.setQueW(questionsPos.w);
			mps.setQueH(questionsPos.h);
		}
	}

	protected void loadPositions(MapPrintSaver mps) {
		if (mps.getTitleW() * images[1].getHeight() == mps.getTitleH() * images[1].getWidth()) {
			titlePos.setPos(mps.getTitleX(), mps.getTitleY(), mps.getTitleW(), mps.getTitleH());
		} else {
			titlePos.setPos(mps.getTitleX(), mps.getTitleY(), mps.getTitleW(), mps.getTitleW() * images[1].getHeight() / images[1].getWidth());
		}
		mapPos.setPos(mps.getMapX(), mps.getMapY(), mps.getMapW(), mps.getMapH());
		copyrightPos.setPos(mps.getCrX(), mps.getCrY(), mps.getCrW(), mps.getCrH());
		if (labels != null) {
			if (mps.getLegW() * labels.getHeight() == mps.getLegH() * labels.getWidth()) {
				labelsPos.setPos(mps.getLegX(), mps.getLegY(), mps.getLegW(), mps.getLegH());
			} else {
				labelsPos.setPos(mps.getLegX(), mps.getLegY(), mps.getLegW(), mps.getLegW() * labels.getHeight() / labels.getWidth());
			}
		}
		if (histogram != null) {
			histogramPos.setPos(mps.getHisX(), mps.getHisY(), mps.getHisW(), mps.getHisH());
		}
		if (questions != null) {
			questionsPos.setPos(mps.getQueX(), mps.getQueY(), mps.getQueW(), mps.getQueH());
		}
		update();
	}

	private class ImgPos {

		public int x;
		public int y;
		public int w;
		public int h;

		ImgPos(int x, int y, int w, int h) {
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
		}

		void setPos(int x, int y, int w, int h) {
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
		}

		Rectangle getRect() {
			return new Rectangle(x, y, w, h);
		}

		int getXF() {
			return x + w;
		}

		int getYF() {
			return y + h;
		}
	}

}
