/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.SplashScreen;
import java.io.IOException;
import java.util.logging.Level;
import sgvclin.UI.MainFrame;
import sgvclin.processors.DBController;
import sgvclin.processors.LoggerManager;

/**
 * @author Nathan Oliveira
 */
public class MainController {

	private static MainController instance = null;

	public static MainController getInstance() {
		if (instance == null) {
			try {
				instance = new MainController();
			} catch (FontFormatException | IOException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		}
		return instance;
	}
	private final MainFrame mainFrame;
	private final Font variantFont;

	private MainController() throws FontFormatException, IOException {
		variantFont = Font.createFont(Font.TRUETYPE_FONT, MainController.class.getResourceAsStream("/fonte_vars.ttf"));
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		ge.registerFont(variantFont);
		mainFrame = new MainFrame();
		mainFrame.setVisible(true);
		SplashScreen splashScreen = SplashScreen.getSplashScreen();
		if (splashScreen != null) {
			splashScreen.close();
		}
	}

	public MainFrame getMainFrame() {
		return mainFrame;
	}

	public Font getVariantFont(Font orig) {
		return new Font(variantFont.getFontName(), orig.getStyle(), orig.getSize());
	}

	public void finishExecution() {
		if (DBController.getInstance() != null) {
			DBController.getInstance().closeConnection();
		}
		System.exit(0);
	}
}
