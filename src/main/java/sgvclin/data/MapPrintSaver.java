/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import sgvclin.processors.DBController;

/**
 * @author Nathan Oliveira
 */
public class MapPrintSaver {

	public static void saveMapPrintSaver(MapPrintSaver mps) throws SQLException {
		try (PreparedStatement smps = DBController.getInstance().getConnection().prepareStatement("INSERT INTO MapPrintSaver"
				+ "(mapSaver, mapX, mapY, mapW, mapH, titleX, titleY, titleW, titleH, crX, crY, crW, crH"
				+ ", legX, legY, legW, legH, hisX, hisY, hisW, hisH, queX, queY, queW, queH) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
			smps.setInt(1, mps.getMs().getId());
			smps.setInt(2, mps.getMapX());
			smps.setInt(3, mps.getMapY());
			smps.setInt(4, mps.getMapW());
			smps.setInt(5, mps.getMapH());
			smps.setInt(6, mps.getTitleX());
			smps.setInt(7, mps.getTitleY());
			smps.setInt(8, mps.getTitleW());
			smps.setInt(9, mps.getTitleH());
			smps.setInt(10, mps.getCrX());
			smps.setInt(11, mps.getCrY());
			smps.setInt(12, mps.getCrW());
			smps.setInt(13, mps.getCrH());
			smps.setInt(14, mps.getLegX());
			smps.setInt(15, mps.getLegY());
			smps.setInt(16, mps.getLegW());
			smps.setInt(17, mps.getLegH());
			smps.setInt(18, mps.getHisX());
			smps.setInt(19, mps.getHisY());
			smps.setInt(20, mps.getHisW());
			smps.setInt(21, mps.getHisH());
			smps.setInt(22, mps.getQueX());
			smps.setInt(23, mps.getQueY());
			smps.setInt(24, mps.getQueW());
			smps.setInt(25, mps.getQueH());
			smps.executeUpdate();
		}
	}

	public static void updateMapPrintSaver(MapPrintSaver mps) throws SQLException {
		try (PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("UPDATE MapPrintSaver"
				+ " SET mapX = ?, mapY = ?, mapW = ?, mapH = ?, titleX = ?, titleY = ?, titleW = ?, titleH = ?,"
				+ " crX = ?, crY = ?, crW = ?, crH = ?, legX = ?, legY = ?, legW = ?, legH = ?,"
				+ " hisX = ?, hisY = ?, hisW = ?, hisH = ?, queX = ?, queY = ?, queW = ?, queH = ?"
				+ " WHERE mapSaver = " + mps.getMs().getId())) {
			pst.setInt(1, mps.getMapX());
			pst.setInt(2, mps.getMapY());
			pst.setInt(3, mps.getMapW());
			pst.setInt(4, mps.getMapH());
			pst.setInt(5, mps.getTitleX());
			pst.setInt(6, mps.getTitleY());
			pst.setInt(7, mps.getTitleW());
			pst.setInt(8, mps.getTitleH());
			pst.setInt(9, mps.getCrX());
			pst.setInt(10, mps.getCrY());
			pst.setInt(11, mps.getCrW());
			pst.setInt(12, mps.getCrH());
			pst.setInt(13, mps.getLegX());
			pst.setInt(14, mps.getLegY());
			pst.setInt(15, mps.getLegW());
			pst.setInt(16, mps.getLegH());
			pst.setInt(17, mps.getHisX());
			pst.setInt(18, mps.getHisY());
			pst.setInt(19, mps.getHisW());
			pst.setInt(20, mps.getHisH());
			pst.setInt(21, mps.getQueX());
			pst.setInt(22, mps.getQueY());
			pst.setInt(23, mps.getQueW());
			pst.setInt(24, mps.getQueH());
			pst.executeUpdate();
		}
	}

	public static MapPrintSaver getMapPrintSaver(int key) throws SQLException {
		MapPrintSaver mps = new MapPrintSaver();
		mps.setMs(MapSaver.getMapSaver(key));
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM `MapPrintSaver` WHERE mapSaver = " + key)) {
			if (!rs.next()) {
				return null;
			}
			mps.setMapX(rs.getInt("mapX"));
			mps.setMapY(rs.getInt("mapY"));
			mps.setMapW(rs.getInt("mapW"));
			mps.setMapH(rs.getInt("mapH"));
			mps.setTitleX(rs.getInt("titleX"));
			mps.setTitleY(rs.getInt("titleY"));
			mps.setTitleW(rs.getInt("titleW"));
			mps.setTitleH(rs.getInt("titleH"));
			mps.setCrX(rs.getInt("crX"));
			mps.setCrY(rs.getInt("crY"));
			mps.setCrW(rs.getInt("crW"));
			mps.setCrH(rs.getInt("crH"));
			mps.setLegX(rs.getInt("legX"));
			mps.setLegY(rs.getInt("legY"));
			mps.setLegW(rs.getInt("legW"));
			mps.setLegH(rs.getInt("legH"));
			mps.setHisX(rs.getInt("hisX"));
			mps.setHisY(rs.getInt("hisY"));
			mps.setHisW(rs.getInt("hisW"));
			mps.setHisH(rs.getInt("hisH"));
			mps.setQueX(rs.getInt("queX"));
			mps.setQueY(rs.getInt("queY"));
			mps.setQueW(rs.getInt("queW"));
			mps.setQueH(rs.getInt("queH"));
		}
		return mps;
	}

	public static void deleteMapPrintSaver(MapPrintSaver mps) throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("DELETE FROM MapPrintSaver WHERE mapSaver = " + mps.getMs().getId());
	}

	public static MapPrintSaver[] getMapPrintSavers(int mapKey) throws SQLException {
		List<MapPrintSaver> list = new ArrayList<>(50);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT title, map, idMapSaver, mapSaver"
				+ " FROM MapSaver, MapPrintSaver WHERE idMapSaver = mapSaver AND map = " + mapKey)) {
			while (rs.next()) {
				list.add(getMapPrintSaver(rs.getInt("mapSaver")));
			}
		}
		return list.toArray(new MapPrintSaver[list.size()]);
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `MapPrintSaver` ("
				+ " `mapSaver` int(11) NOT NULL,"
				+ " `mapX` int(11) NOT NULL,"
				+ " `mapY` int(11) NOT NULL,"
				+ " `mapW` int(11) NOT NULL,"
				+ " `mapH` int(11) NOT NULL,"
				+ " `titleX` int(11) NOT NULL,"
				+ " `titleY` int(11) NOT NULL,"
				+ " `titleW` int(11) NOT NULL,"
				+ " `titleH` int(11) NOT NULL,"
				+ " `crX` int(11) NOT NULL,"
				+ " `crY` int(11) NOT NULL,"
				+ " `crW` int(11) NOT NULL,"
				+ " `crH` int(11) NOT NULL,"
				+ " `legX` int(11) NOT NULL,"
				+ " `legY` int(11) NOT NULL,"
				+ " `legW` int(11) NOT NULL,"
				+ " `legH` int(11) NOT NULL,"
				+ " `hisX` int(11),"
				+ " `hisY` int(11),"
				+ " `hisW` int(11),"
				+ " `hisH` int(11),"
				+ " `queX` int(11),"
				+ " `queY` int(11),"
				+ " `queW` int(11),"
				+ " `queH` int(11),"
				+ " PRIMARY KEY (`mapSaver`),"
				+ " CONSTRAINT `MapPrintSaver_ibfk_1` FOREIGN KEY (`mapSaver`) REFERENCES `MapSaver` (`idMapSaver`) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("MapPrintSaver"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `MapPrintSaver`");
	}

	private MapSaver ms;
	private int mapX;
	private int mapY;
	private int mapW;
	private int mapH;
	private int titleX;
	private int titleY;
	private int titleW;
	private int titleH;
	private int crX;
	private int crY;
	private int crW;
	private int crH;
	private int legX;
	private int legY;
	private int legW;
	private int legH;
	private int hisX;
	private int hisY;
	private int hisW;
	private int hisH;
	private int queX;
	private int queY;
	private int queW;
	private int queH;

	public MapSaver getMs() {
		return ms;
	}

	public void setMs(MapSaver ms) {
		this.ms = ms;
	}

	public int getMapX() {
		return mapX;
	}

	public void setMapX(int mapX) {
		this.mapX = mapX;
	}

	public int getMapY() {
		return mapY;
	}

	public void setMapY(int mapY) {
		this.mapY = mapY;
	}

	public int getMapW() {
		return mapW;
	}

	public void setMapW(int mapW) {
		this.mapW = mapW;
	}

	public int getMapH() {
		return mapH;
	}

	public void setMapH(int mapH) {
		this.mapH = mapH;
	}

	public int getTitleX() {
		return titleX;
	}

	public void setTitleX(int titleX) {
		this.titleX = titleX;
	}

	public int getTitleY() {
		return titleY;
	}

	public void setTitleY(int titleY) {
		this.titleY = titleY;
	}

	public int getTitleW() {
		return titleW;
	}

	public void setTitleW(int titleW) {
		this.titleW = titleW;
	}

	public int getTitleH() {
		return titleH;
	}

	public void setTitleH(int titleH) {
		this.titleH = titleH;
	}

	public int getCrX() {
		return crX;
	}

	public void setCrX(int crX) {
		this.crX = crX;
	}

	public int getCrY() {
		return crY;
	}

	public void setCrY(int crY) {
		this.crY = crY;
	}

	public int getCrW() {
		return crW;
	}

	public void setCrW(int crW) {
		this.crW = crW;
	}

	public int getCrH() {
		return crH;
	}

	public void setCrH(int crH) {
		this.crH = crH;
	}

	public int getLegX() {
		return legX;
	}

	public void setLegX(int legX) {
		this.legX = legX;
	}

	public int getLegY() {
		return legY;
	}

	public void setLegY(int legY) {
		this.legY = legY;
	}

	public int getLegW() {
		return legW;
	}

	public void setLegW(int legW) {
		this.legW = legW;
	}

	public int getLegH() {
		return legH;
	}

	public void setLegH(int legH) {
		this.legH = legH;
	}

	public int getHisX() {
		return hisX;
	}

	public void setHisX(int hisX) {
		this.hisX = hisX;
	}

	public int getHisY() {
		return hisY;
	}

	public void setHisY(int hisY) {
		this.hisY = hisY;
	}

	public int getHisW() {
		return hisW;
	}

	public void setHisW(int hisW) {
		this.hisW = hisW;
	}

	public int getHisH() {
		return hisH;
	}

	public void setHisH(int hisH) {
		this.hisH = hisH;
	}

	public int getQueX() {
		return queX;
	}

	public void setQueX(int queX) {
		this.queX = queX;
	}

	public int getQueY() {
		return queY;
	}

	public void setQueY(int queY) {
		this.queY = queY;
	}

	public int getQueW() {
		return queW;
	}

	public void setQueW(int queW) {
		this.queW = queW;
	}

	public int getQueH() {
		return queH;
	}

	public void setQueH(int queH) {
		this.queH = queH;
	}

	@Override
	public String toString() {
		return ms.toString();
	}
}
