/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import sgvclin.processors.LoggerManager;

/**
 *
 * @author Nathan
 */
public class ColorPattern {

	static private BufferedImage[] textures;
	static public final int DEFAULT_SIZE = 45;

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	static public BufferedImage[] loadTextures() {
		if (textures == null) {
			try {
				textures = new BufferedImage[]{
					ImageIO.read(ColorPattern.class.getResource("/textures/template_plain_color.png")),
					ImageIO.read(ColorPattern.class.getResource("/textures/template_slash.png")),
					ImageIO.read(ColorPattern.class.getResource("/textures/template_backslash.png")),
					ImageIO.read(ColorPattern.class.getResource("/textures/template_dot.png")),
					ImageIO.read(ColorPattern.class.getResource("/textures/template_vertical.png")),
					ImageIO.read(ColorPattern.class.getResource("/textures/template_horizontal.png")),
					ImageIO.read(ColorPattern.class.getResource("/textures/template_times.png")),
					ImageIO.read(ColorPattern.class.getResource("/textures/template_plus.png"))};
			} catch (IOException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				textures = null;
			}
		}
		return textures;
	}

	static public BufferedImage[] paintTextures(BufferedImage[] textures, Color fg, Color bg) {
		BufferedImage[] result = new BufferedImage[textures.length];
		List<Thread> threads = new ArrayList<>(textures.length);
		for (int i = 0; i < textures.length; i++) {
			final int iF = i;
			Thread thread = new Thread(() -> {
				BufferedImage img = paintTexture(textures[iF], fg, bg);
				synchronized (result) {
					result[iF] = img;
				}
			});
			threads.add(thread);
			thread.start();
		}
		while (!threads.isEmpty()) {
			if (!threads.get(0).isAlive()) {
				threads.remove(0);
			}
		}
		return result;
	}

	static private BufferedImage paintTexture(BufferedImage inImg, Color fg, Color bg) {
		BufferedImage img = new BufferedImage(inImg.getWidth(), inImg.getHeight(), BufferedImage.TYPE_INT_ARGB);
		int[] rgb = inImg.getRGB(0, 0, img.getWidth(), img.getHeight(), null, 0, img.getWidth());
		for (int i = 0; i < rgb.length; i++) {
			if (rgb[i] == Color.WHITE.getRGB()) {
				rgb[i] = fg.getRGB();
			} else {
				rgb[i] = bg.getRGB();
			}
		}
		img.setRGB(0, 0, img.getWidth(), img.getHeight(), rgb, 0, img.getWidth());
		return img;
	}

	static public Color[] toColors(ColorPattern[] patterns) {
		Color[] colors = new Color[patterns.length];
		for (int i = 0; i < patterns.length; i++) {
			colors[i] = patterns[i].getForeground();
		}
		return colors;
	}

	static public ColorPattern[] toPatterns(Color[] colors) {
		ColorPattern[] patterns = new ColorPattern[colors.length];
		for (int i = 0; i < colors.length; i++) {
			patterns[i] = new ColorPattern(colors[i], Color.BLACK, Pattern.plain_color, DEFAULT_SIZE);
		}
		return patterns;
	}

	private Color foreground;
	private Color background;
	private Pattern pattern;
	private int size;

	public ColorPattern() {
	}

	public ColorPattern(Color foreground, Color background, Pattern pattern, int size) {
		this.foreground = foreground;
		this.background = background;
		this.pattern = pattern;
		this.size = size;
	}

	public Color getForeground() {
		return foreground;
	}

	public void setForeground(Color foreground) {
		this.foreground = foreground;
	}

	public Color getBackground() {
		return background;
	}

	public void setBackground(Color background) {
		this.background = background;
	}

	public Pattern getPattern() {
		return pattern;
	}

	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public BufferedImage getTexture() {
		Color fg = new Color(foreground.getRGB() & 0x00ffffff | 0xc8000000);
		Color bg = new Color(background.getRGB() & 0x00ffffff | 0xc8000000);
		switch (pattern) {
			case plain_color:
				return paintTexture(loadTextures()[0], fg, bg);
			case slash:
				return paintTexture(loadTextures()[1], fg, bg);
			case backslash:
				return paintTexture(loadTextures()[2], fg, bg);
			case dot:
				return paintTexture(loadTextures()[3], fg, bg);
			case vertical:
				return paintTexture(loadTextures()[4], fg, bg);
			case horizontal:
				return paintTexture(loadTextures()[5], fg, bg);
			case times:
				return paintTexture(loadTextures()[6], fg, bg);
			case plus:
				return paintTexture(loadTextures()[7], fg, bg);
			default:
				return paintTexture(loadTextures()[0], fg, bg);
		}
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 59 * hash + Objects.hashCode(this.foreground);
		hash = 59 * hash + Objects.hashCode(this.background);
		hash = 59 * hash + Objects.hashCode(this.pattern);
		hash = 59 * hash + this.size;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ColorPattern other = (ColorPattern) obj;
		if (this.size != other.size) {
			return false;
		}
		if (!Objects.equals(this.foreground, other.foreground)) {
			return false;
		}
		if (!Objects.equals(this.background, other.background)) {
			return false;
		}
		return this.pattern == other.pattern;

	}

	@SuppressWarnings("PublicInnerClass")
	public enum Pattern {
		plain_color,
		slash,
		backslash,
		dot,
		vertical,
		horizontal,
		times,
		plus
	}
}
