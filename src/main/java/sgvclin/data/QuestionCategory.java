/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import sgvclin.processors.DBController;
import sgvclin.processors.LoggerManager;

/**
 * @author Nathan Oliveira
 */
public class QuestionCategory {

	public static QuestionCategory getQuestionCategory(int key) throws SQLException, NullPointerException {
		QuestionCategory cq = new QuestionCategory();
		try (ResultSet rs = DBController.getInstance().getResultSet("QuestionCategory", key)) {
			rs.next();
			cq.setId(rs.getInt("idQuestionCategory"));
			cq.setDescription(rs.getString("description"));
		}
		return cq;
	}

	public static QuestionCategory getQuestionCategory(String description) throws SQLException {
		int k;
		try {
			try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idQuestionCategory, description"
					+ " FROM QuestionCategory WHERE description = '" + description + "'")) {
				if (!rs.next()) {
					return null;
				}
				k = rs.getInt("idQuestionCategory");
			}
		} catch (SQLException ex) {
			throw ex;
		}
		return getQuestionCategory(k);
	}

	public static void removeQuestionCategory(QuestionCategory qc) {
		try {
			DBController.getInstance().getStatement().executeUpdate("DELETE FROM QuestionCategory WHERE idQuestionCategory = " + qc.getId());
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
		}
	}

	public static void saveQuestionCategory(String description) throws SQLException {
		QuestionCategory cq = new QuestionCategory();
		cq.setId(DBController.getInstance().getNewPrimaryKeyFor("QuestionCategory"));
		cq.setDescription(description);
		try (PreparedStatement scq = DBController.getInstance().getConnection().prepareStatement("INSERT INTO QuestionCategory"
				+ "(idQuestionCategory, description, description_hash) VALUES(?, ?, ?)")) {
			scq.setInt(1, cq.getId());
			scq.setString(2, cq.getDescription());
			scq.setString(3, DBController.getMD5(cq.getDescription()));
			scq.executeUpdate();
		}
	}

	public static void updateQuestionCategory(QuestionCategory qc) throws SQLException {
		try (PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("UPDATE QuestionCategory "
				+ "SET description=?, description_hash=? WHERE idQuestionCategory=" + qc.getId())) {
			pst.setString(1, qc.getDescription());
			pst.setString(2, DBController.getMD5(qc.getDescription()));
			pst.executeUpdate();
		}
	}

	public static QuestionCategory[] getQuestionCategories() throws SQLException {
		List<QuestionCategory> list = new ArrayList<>(5);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idQuestionCategory FROM QuestionCategory")) {
			while (rs.next()) {
				list.add(getQuestionCategory(rs.getInt("idQuestionCategory")));
			}
		}
		return list.toArray(new QuestionCategory[list.size()]);
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `QuestionCategory` ("
				+ "  `idQuestionCategory` int(11) NOT NULL,"
				+ "  `description` varchar(5000) NOT NULL,"
				+ "  `description_hash` char(32) NOT NULL,"
				+ "  PRIMARY KEY (`idQuestionCategory`),"
				+ "  UNIQUE (`description_hash`)"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("QuestionCategory"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `QuestionCategory`");
	}

	private int id;
	private String description;

	public String getDescription() {
		return description;
	}

	public int getId() {
		return id;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isPhonetic() {
		return getDescription().startsWith(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PPQ"));
	}

	public String getTooltip() {
		if (getDescription().equals(java.util.ResourceBundle.getBundle("texts/Bundle").getString("PPQ"))) {
			return java.util.ResourceBundle.getBundle("texts/Bundle").getString("PPQ_TOOLTIP");
		} else if (getDescription().equals(java.util.ResourceBundle.getBundle("texts/Bundle").getString("MSQ"))) {
			return java.util.ResourceBundle.getBundle("texts/Bundle").getString("MSQ_TOOLTIP");
		} else if (getDescription().equals(java.util.ResourceBundle.getBundle("texts/Bundle").getString("LSQ"))) {
			return java.util.ResourceBundle.getBundle("texts/Bundle").getString("LSQ_TOOLTIP");
		} else {
			return getDescription();
		}
	}

	@Override
	public String toString() {
		return description;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 79 * hash + this.id;
		hash = 79 * hash + Objects.hashCode(this.description);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final QuestionCategory other = (QuestionCategory) obj;
		return this.id == other.id
				&& Objects.equals(this.description, other.description);
	}
}
