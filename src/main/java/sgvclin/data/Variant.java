/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import sgvclin.processors.DBController;

/**
 * @author Nathan Oliveira
 */
public class Variant {

	public static Variant getVariant(int key) throws SQLException, NullPointerException {
		Variant v = new Variant();
		try (ResultSet rs = DBController.getInstance().getResultSet("Variant", key)) {
			rs.next();
			v.setId(rs.getInt("idVariant"));
			v.setAnswer(rs.getString("answer"));
			int k = rs.getInt("question");
			Question q = Question.getQuestion(k);
			v.setQuestion(q);
		}
		return v;
	}

	public static Variant saveVariant(Question question, String variant) throws SQLException {
		Variant v = new Variant();
		v.setQuestion(question);
		v.setAnswer(variant);
		v.setId(DBController.getInstance().getNewPrimaryKeyFor("Variant"));
		try (PreparedStatement sv = DBController.getInstance().getConnection().prepareStatement("INSERT INTO Variant("
				+ "idVariant, question, answer, answer_hash) VALUES(?, ?, ?, ?)")) {
			sv.setInt(1, v.getId());
			sv.setInt(2, v.getQuestion().getId());
			sv.setString(3, v.getAnswer());
			sv.setString(4, DBController.getMD5(v.getAnswer()));
			sv.executeUpdate();
		}
		return v;
	}

	public static Variant getVariant(Question q, String answer) throws SQLException {
		int k;
		try {
			try (PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("SELECT * FROM Variant WHERE question=" + q.getId()
					+ " AND answer=? COLLATE BINARY")) {
				pst.setString(1, answer);
				ResultSet rs = pst.executeQuery();
				if (!rs.next()) {
					throw new NullPointerException(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DATABASE ERROR"));
				}
				k = rs.getInt("idVariant");
			}
		} catch (NullPointerException ex) {
			return saveVariant(q, answer);
		}
		return getVariant(k);
	}

	public static void deleteVariant(Variant variant) throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("DELETE FROM Variant WHERE idVariant=" + variant.getId());
	}

	public static void deleteVariant(Question question, String variant) throws SQLException {
		deleteVariant(getVariant(question, variant));
	}

	public static void swapVariant(Variant v, String answer) throws SQLException {
		try (PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("UPDATE Variant SET answer=?, answer_hash=? WHERE idVariant=?")) {
			pst.setString(1, answer);
			pst.setString(2, DBController.getMD5(answer));
			pst.setInt(3, v.getId());
			pst.executeUpdate();
		}
		v.setAnswer(answer);
	}

	public static List<Variant> getListOfVariants(Question question) throws SQLException {
		List<Variant> l = new ArrayList<>(20);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM Variant WHERE question = " + question.getId()
				+ " ORDER BY answer")) {
			while (rs.next()) {
				l.add(getVariant(rs.getInt("idVariant")));
			}
		}
		return l;
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `Variant` ("
				+ "  `idVariant` int(11) NOT NULL,"
				+ "  `question` int(11) NOT NULL,"
				+ "  `answer` varchar(5000) NOT NULL,"
				+ "  `answer_hash` char(32) NOT NULL,"
				+ "  PRIMARY KEY (`idVariant`),"
				+ "  UNIQUE (`question`,`answer_hash`),"
				+ "  CONSTRAINT `Variant_ibfk_1` FOREIGN KEY (`question`) REFERENCES `Question` (`idQuestion`) ON DELETE CASCADE ON UPDATE CASCADE"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("Variant"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `Variant`");
	}

	private int id;
	private Question question;
	private String answer;

	public int getId() {
		return id;
	}

	public Question getQuestion() {
		return question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Override
	public String toString() {
		return answer;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 61 * hash + this.id;
		hash = 61 * hash + Objects.hashCode(this.question);
		hash = 61 * hash + Objects.hashCode(this.answer);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Variant other = (Variant) obj;
		return this.id == other.id
				&& Objects.equals(this.answer, other.answer)
				&& Objects.equals(this.question, other.question);
	}

	public boolean equalsByAnswer(Variant var) {
		return getAnswer().equals(var.getAnswer());
	}
}
