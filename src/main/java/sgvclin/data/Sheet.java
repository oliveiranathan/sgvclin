/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.io.Serializable;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import sgvclin.processors.DBController;

/**
 * @author Nathan Oliveira
 */
public class Sheet implements Serializable {

	private static final long serialVersionUID = 1L;

	public static Sheet getSheet(int key) throws SQLException, NullPointerException {
		Sheet s = new Sheet();
		try (ResultSet rs = DBController.getInstance().getResultSet("Sheet", key)) {
			rs.next();
			s.setId(rs.getInt("idSheet"));
			s.setName(rs.getString("name"));
		}
		return s;
	}

	public static Sheet saveSheet(String name) throws SQLException {
		Sheet f = new Sheet();
		f.setName(name);
		f.setId(DBController.getInstance().getNewPrimaryKeyFor("Sheet"));
		try (PreparedStatement sf = DBController.getInstance().getConnection().prepareStatement("INSERT INTO Sheet(idSheet, name, name_hash) VALUES(?, ?, ?)")) {
			sf.setInt(1, f.getId());
			sf.setString(2, f.getName());
			sf.setString(3, DBController.getMD5(f.getName()));
			sf.executeUpdate();
		}
		return f;
	}

	public static void updateSheet(Sheet sheet) throws SQLException {
		try {
			getSheet(sheet.getId());
		} catch (NullPointerException | SQLException e) {
			return;
		}
		try (PreparedStatement ps = DBController.getInstance().getConnection().prepareStatement("UPDATE Sheet SET name=?, name_hash=? WHERE idSheet = " + sheet.getId())) {
			ps.setString(1, sheet.getName());
			ps.setString(2, DBController.getMD5(sheet.getName()));
			ps.executeUpdate();
		}
	}

	public static void deleteSheet(Sheet sheet) throws SQLException {
		DBController.getInstance().getStatement().execute("DELETE FROM Sheet WHERE idSheet=" + sheet.getId());
	}

	public static Sheet getSheet(String name) throws SQLException, NullPointerException {
		int k;
		try {
			try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idSheet, name FROM Sheet WHERE name = '" + name + "'")) {
				if (!rs.next()) {
					throw new Exception(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DATABASE ERROR"));
				}
				k = rs.getInt("idSheet");
			}
		} catch (Exception ex) {
			throw new NullPointerException(ex.getMessage());
		}
		return getSheet(k);
	}

	public static List<Sheet> getListOfSheets() throws SQLException {
		List<Sheet> list = new ArrayList<>(5);
		for (String str : getListOfSheetsNames()) {
			list.add(getSheet(str));
		}
		return list;
	}

	public static String[] getListOfSheetsNames() throws SQLException {
		List<String> list = new ArrayList<>(5);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT name FROM Sheet ORDER BY name")) {
			while (rs.next()) {
				list.add(rs.getString("name"));
			}
		}
		return list.toArray(new String[list.size()]);
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `Sheet` ("
				+ "  `idSheet` int(11) NOT NULL,"
				+ "  `name` varchar(2000) NOT NULL,"
				+ "  `name_hash` varchar(32) NOT NULL,"
				+ "  PRIMARY KEY (`idSheet`),"
				+ "  UNIQUE (`name_hash`)"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("Sheet"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `Sheet`");
	}

	private int id;
	private String name;

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && (obj instanceof Sheet) && ((Sheet) obj).getId() == id && ((Sheet) obj).getName().equals(name);
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 37 * hash + this.id;
		hash = 37 * hash + Objects.hashCode(this.name);
		return hash;
	}

	@Override
	public String toString() {
		return name;
	}
}
