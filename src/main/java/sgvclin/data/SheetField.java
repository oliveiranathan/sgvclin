/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.io.Serializable;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import sgvclin.processors.DBController;

/**
 * @author Nathan Oliveira
 */
public class SheetField implements Serializable {

	private static final long serialVersionUID = 1L;

	public static SheetField getSheetField(int key) throws SQLException, NullPointerException {
		SheetField cf = new SheetField();
		try (ResultSet rs = DBController.getInstance().getResultSet("SheetField", key)) {
			rs.next();
			cf.setId(rs.getInt("idSheetField"));
			cf.setName(rs.getString("name"));
			cf.setType(SheetField.Type.valueOf(rs.getString("type")));
			int k = rs.getInt("sheet");
			Sheet f = Sheet.getSheet(k);
			cf.setSheet(f);
			cf.setGroupable(rs.getBoolean("groupable"));
		}
		return cf;
	}

	public static void saveSheetField(SheetField sf) throws SQLException {
		sf.setId(DBController.getInstance().getNewPrimaryKeyFor("SheetField"));
		try (PreparedStatement scf = DBController.getInstance().getConnection().prepareStatement("INSERT INTO SheetField("
				+ "idSheetField, sheet, type, name, name_hash, groupable) VALUES(?, ?, ?, ?, ?, ?)")) {
			scf.setInt(1, sf.getId());
			scf.setInt(2, sf.getSheet().getId());
			scf.setString(3, sf.getType().name());
			scf.setString(4, sf.getName());
			scf.setString(5, DBController.getMD5(sf.getName()));
			scf.setBoolean(6, sf.isGroupable());
			scf.executeUpdate();
		}
	}

	public static void updateSheetField(SheetField sf) throws SQLException {
		try {
			getSheetField(sf.getId());
		} catch (NullPointerException | SQLException e) {
			return;
		}
		try (PreparedStatement ps = DBController.getInstance().getConnection().prepareStatement("UPDATE SheetField SET name=?, name_hash=?, groupable=? WHERE idSheetField = " + sf.getId())) {
			ps.setString(1, sf.getName());
			ps.setString(2, DBController.getMD5(sf.getName()));
			ps.setBoolean(3, sf.isGroupable());
			ps.executeUpdate();
		}
	}

	public static void deleteSheetField(SheetField field) throws SQLException {
		DBController.getInstance().getStatement().execute("DELETE FROM SheetField WHERE idSheetField=" + field.getId());
	}

	public static List<SheetField> getListOfFields(Sheet s) throws SQLException {
		List<Integer> list = new ArrayList<>(10);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idSheetField, sheet FROM SheetField WHERE sheet = " + s.getId()
				+ " ORDER BY idSheetField")) {
			while (rs.next()) {
				list.add(rs.getInt("idSheetField"));
			}
		}
		List<SheetField> l = new ArrayList<>(list.size());
		for (int k : list) {
			l.add(getSheetField(k));
		}
		return l;
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `SheetField` ("
				+ "  `idSheetField` int(11) NOT NULL,"
				+ "  `sheet` int(11) NOT NULL,"
				+ "  `type` varchar(500) NOT NULL,"
				+ "  `name` varchar(500) NOT NULL,"
				+ "  `name_hash` char(32) NOT NULL,"
				+ "  `groupable` bool NOT NULL,"
				+ "  PRIMARY KEY (`idSheetField`),"
				+ "  UNIQUE (`sheet`,`name_hash`),"
				+ "  CONSTRAINT `SheetField_ibfk_1` FOREIGN KEY (`sheet`) REFERENCES `Sheet` (`idSheet`) ON DELETE CASCADE ON UPDATE CASCADE"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("SheetField"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `SheetField`");
	}

	private int id;
	private Sheet sheet;
	private Type type;
	private String name;
	private boolean groupable;

	public Sheet getSheet() {
		return sheet;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}

	public boolean isGroupable() {
		return groupable;
	}

	public void setSheet(Sheet sheet) {
		this.sheet = sheet;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public void setGroupable(boolean groupable) {
		this.groupable = groupable;
	}

	public boolean isText() {
		return type == Type.TEXT;
	}

	public boolean isDate() {
		return type == Type.DATE;
	}

	public boolean isInteger() {
		return type == Type.INTEGER;
	}

	public boolean isDouble() {
		return type == Type.DOUBLE;
	}

	public boolean isMultivaluedAndMultiselectable() {
		return type == Type.MULTIVALUED_MULTIPLE;
	}

	public boolean isMultivaluedButUniqueSelectable() {
		return type == Type.MULTIVALUED_UNIQUE;
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && (obj instanceof SheetField) && ((SheetField) obj).getId() == id && ((SheetField) obj).isGroupable() == groupable
				&& ((SheetField) obj).getName().equals(name) && ((SheetField) obj).getType().equals(type) && ((SheetField) obj).getSheet().equals(sheet);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 53 * hash + this.id;
		hash = 53 * hash + Objects.hashCode(this.sheet);
		hash = 53 * hash + Objects.hashCode(this.type);
		hash = 53 * hash + Objects.hashCode(this.name);
		hash = 53 * hash + (this.groupable ? 1 : 0);
		return hash;
	}

	@Override
	public String toString() {
		return name;
	}

	@SuppressWarnings("PublicInnerClass")
	public enum Type {

		TEXT,
		DATE,
		INTEGER,
		DOUBLE,
		MULTIVALUED_UNIQUE,
		MULTIVALUED_MULTIPLE
	}
}
