/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.awt.geom.Point2D;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import sgvclin.processors.DBController;
import sgvclin.processors.LoggerManager;

/**
 * @author Nathan Oliveira
 */
public class GeographicPoint {

	public static GeographicPoint getGeoPoint(int key) throws SQLException, NullPointerException {
		GeographicPoint pg = new GeographicPoint();
		try (ResultSet rs = DBController.getInstance().getResultSet("GeographicPoint", key)) {
			rs.next();
			pg.setId(rs.getInt("idGeographicPoint"));
			pg.setName(rs.getString("name"));
			pg.setCoordinates(new Point2D.Double(rs.getDouble("x"), rs.getDouble("y")));
			pg.setCode(rs.getInt("code"));
			pg.setState(rs.getString("state"));
		}
		return pg;
	}

	public static GeographicPoint getGeoPointByCode(int codePG, String name, String state) throws SQLException {
		int k;
		try {
			try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idGeographicPoint, code FROM GeographicPoint WHERE code = " + codePG)) {
				rs.next();
				k = rs.getInt("idGeographicPoint");
			}
		} catch (SQLException ex) {
			k = DBController.getInstance().getNewPrimaryKeyFor("GeographicPoint");
			saveGeoPoint(k, name, state, codePG);
		}
		return getGeoPoint(k);
	}

	public static GeographicPoint getGeoPointByCode(int codePG) throws SQLException {
		int k;
		try {
			try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idGeographicPoint, code FROM GeographicPoint WHERE code = " + codePG)) {
				if (!rs.next()) {
					throw new Exception(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DATABASE ERROR"));
				}
				k = rs.getInt("idGeographicPoint");
			}
		} catch (Exception ex) {
			k = DBController.getInstance().getNewPrimaryKeyFor("GeographicPoint");
			GeographicPoint pg = new GeographicPoint();
			pg.setId(k);
			return pg;
		}
		return getGeoPoint(k);
	}

	private static void saveGeoPoint(int id, String name, String state, int codePG) throws SQLException {
		GeographicPoint pg = new GeographicPoint();
		pg.setCode(codePG);
		pg.setId(id);
		pg.setName(name);
		pg.setState(state);
		saveGeoPoint(pg);
	}

	public static GeographicPoint getFirstGeoPoint(Map map) throws SQLException {
		int k;
		GeographicPoint pg;
		try {
			try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT MIN(geographicPoint) AS min FROM MapPoint"
					+ " WHERE map = " + map.getId())) {
				if (!rs.next()) {
					k = 0;
				} else {
					k = rs.getInt("min");
				}
			}
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			return null;
		}
		if (k == 0) {
			pg = new GeographicPoint();
			pg.setId(1);
			return pg;
		} else {
			return getGeoPoint(k);
		}
	}

	public static void saveGeoPoint(GeographicPoint pg) throws SQLException {
		boolean update = true;
		try {
			getGeoPoint(pg.getId());
		} catch (NullPointerException | SQLException e) {
			update = false;
		}
		if (update) {
			try (PreparedStatement spg = DBController.getInstance().getConnection().prepareStatement("UPDATE GeographicPoint SET name=?, x=?, y=?, code=?, state=?, "
					+ "name_hash=? WHERE idGeographicPoint = " + pg.getId())) {
				spg.setString(1, pg.getName());
				if (pg.getCoordinates() != null) {
					spg.setDouble(2, pg.getCoordinates().x);
					spg.setDouble(3, pg.getCoordinates().y);
				} else {
					spg.setDouble(2, 0.0);
					spg.setDouble(3, 0.0);
				}
				spg.setInt(4, pg.getCode());
				spg.setString(5, pg.getState());
				spg.setString(6, DBController.getMD5(pg.getName()));
				spg.executeUpdate();
			}
		} else {
			try (PreparedStatement spg = DBController.getInstance().getConnection().prepareStatement("INSERT INTO GeographicPoint("
					+ "idGeographicPoint, name, x, y, code, state, name_hash) VALUES(?, ?, ? ,? ,?, ?, ?)")) {
				spg.setInt(1, pg.getId());
				spg.setString(2, pg.getName());
				if (pg.getCoordinates() != null) {
					spg.setDouble(3, pg.getCoordinates().x);
					spg.setDouble(4, pg.getCoordinates().y);
				} else {
					spg.setDouble(3, 0.0);
					spg.setDouble(4, 0.0);
				}
				spg.setInt(5, pg.getCode());
				spg.setString(6, pg.getState());
				spg.setString(7, DBController.getMD5(pg.getName()));
				spg.executeUpdate();
			}
		}
	}

	public static GeographicPoint getLastGeoPoint(Map map) throws SQLException {
		int k;
		try (ResultSet rs = DBController.getInstance().getConnection().createStatement().executeQuery("SELECT MAX(geographicPoint) AS max FROM MapPoint"
				+ " WHERE map = " + map.getId())) {
			if (!rs.next()) {
				k = 0;
			} else {
				k = rs.getInt("max");
			}
		}
		return (k == 0) ? getNewGeoPoint() : getGeoPoint(k);
	}

	public static GeographicPoint getNewGeoPoint() throws SQLException {
		int k = DBController.getInstance().getNewPrimaryKeyFor("GeographicPoint");
		GeographicPoint pg = new GeographicPoint();
		pg.setId(k);
		return pg;
	}

	public static GeographicPoint getPreviousGeoPoint(GeographicPoint pg, Map map) throws SQLException {
		int k;
		try (ResultSet rs = DBController.getInstance().getConnection().createStatement().executeQuery("SELECT MAX(lessthan) AS max FROM "
				+ "(SELECT GeographicPoint AS lessthan FROM MapPoint WHERE map = " + map.getId()
				+ " AND geographicPoint < " + pg.getId() + ") AS temp")) {
			rs.next();
			k = rs.getInt("max");
		}
		if (k < getFirstGeoPoint(map).getId()) {
			k = getFirstGeoPoint(map).getId();
		}
		return (k == 0) ? getNewGeoPoint() : getGeoPoint(k);
	}

	public static GeographicPoint getNextGeoPoint(GeographicPoint pg, Map map) throws SQLException {
		int k;
		try (ResultSet rs = DBController.getInstance().getConnection().createStatement().executeQuery("SELECT MIN(morethan) AS min FROM "
				+ "(SELECT geographicPoint AS morethan FROM MapPoint WHERE map = " + map.getId()
				+ " AND geographicPoint > " + pg.getId() + ") AS temp")) {
			rs.next();
			k = rs.getInt("min");
		}
		if (k > getLastGeoPoint(map).getId() || k == 0) {
			k = getLastGeoPoint(map).getId();
		}
		return (k == 0) ? getNewGeoPoint() : getGeoPoint(k);
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `GeographicPoint` ("
				+ "  `idGeographicPoint` int(11) NOT NULL,"
				+ "  `name` varchar(2000) NOT NULL,"
				+ "  `name_hash` char(32) NOT NULL,"
				+ "  `x` double DEFAULT NULL,"
				+ "  `y` double DEFAULT NULL,"
				+ "  `code` int(11) NOT NULL,"
				+ "  `state` varchar(200) DEFAULT NULL,"
				+ "  PRIMARY KEY (`idGeographicPoint`),"
				+ "  UNIQUE (`code`)"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("GeographicPoint"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `GeographicPoint`");
	}

	private int id;
	private String name;
	private String state;
	private Point2D.Double coordinates;
	private int code;

	public int getCode() {
		return code;
	}

	public Point2D.Double getCoordinates() {
		return coordinates;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public String getState() {
		return state;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setCoordinates(Point2D.Double coordinates) {
		this.coordinates = coordinates;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return code + " - " + name;
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null
				&& (obj instanceof GeographicPoint)
				&& ((GeographicPoint) obj).getId() == id
				&& ((GeographicPoint) obj).getCode() == code
				&& (((GeographicPoint) obj).getCoordinates() == null || ((GeographicPoint) obj).getCoordinates().equals(coordinates))
				&& ((GeographicPoint) obj).getName().equals(name)
				&& ((GeographicPoint) obj).getState().equals(state);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 41 * hash + this.id;
		hash = 41 * hash + Objects.hashCode(this.name);
		hash = 41 * hash + Objects.hashCode(this.state);
		hash = 41 * hash + Objects.hashCode(this.coordinates);
		hash = 41 * hash + this.code;
		return hash;
	}
}
