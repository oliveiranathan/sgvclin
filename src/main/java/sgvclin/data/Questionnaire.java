/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import sgvclin.processors.DBController;

/**
 * @author Nathan Oliveira
 */
public class Questionnaire {

	public static Questionnaire getQuestionnaire(int key) throws SQLException, NullPointerException {
		Questionnaire q = new Questionnaire();
		try (ResultSet rs = DBController.getInstance().getResultSet("Questionnaire", key)) {
			rs.next();
			q.setId(rs.getInt("idQuestionnaire"));
			q.setName(rs.getString("name"));
		}
		return q;
	}

	public static Questionnaire saveQuestionnaire(String name) throws SQLException, NullPointerException {
		Questionnaire q = new Questionnaire();
		q.setName(name);
		q.setId(DBController.getInstance().getNewPrimaryKeyFor("Questionnaire"));
		try (PreparedStatement sq = DBController.getInstance().getConnection().prepareStatement("INSERT INTO Questionnaire(idQuestionnaire,"
				+ " name, name_hash) VALUES(?, ?, ?)")) {
			sq.setInt(1, q.getId());
			sq.setString(2, q.getName());
			sq.setString(3, DBController.getMD5(q.getName()));
			sq.executeUpdate();
		}
		return q;
	}

	public static Questionnaire updateQuestionnaire(Questionnaire q) throws SQLException, NullPointerException {
		try (PreparedStatement sq = DBController.getInstance().getConnection().prepareStatement("UPDATE Questionnaire SET "
				+ " name=?, name_hash=? WHERE idQuestionnaire=" + q.getId())) {
			sq.setString(1, q.getName());
			sq.setString(2, DBController.getMD5(q.getName()));
			sq.executeUpdate();
		}
		return q;
	}

	public static void removeQuestionnaire(Questionnaire q) throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("DELETE FROM Questionnaire WHERE idQuestionnaire = " + q.getId());
	}

	public static Questionnaire getQuestionnaire(String name) throws SQLException {
		int k;
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM Questionnaire WHERE name = '" + name + "'")) {
			if (!rs.next()) {
				return null;
			}
			k = rs.getInt("idQuestionnaire");
		}
		return getQuestionnaire(k);
	}

	public static List<Questionnaire> getQuestionnaires() throws SQLException {
		List<Questionnaire> l = new ArrayList<>(5);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idQuestionnaire FROM Questionnaire")) {
			while (rs.next()) {
				l.add(getQuestionnaire(rs.getInt("idQuestionnaire")));
			}
		}
		return l;
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `Questionnaire`("
				+ " `idQuestionnaire` int(11) NOT NULL,"
				+ " `name` varchar(5000) NOT NULL,"
				+ " `name_hash` char(32) NOT NULL,"
				+ " PRIMARY KEY (`idQuestionnaire`),"
				+ " UNIQUE (`name_hash`)"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("Questionnaire"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `Questionnaire`");
	}

	private int id;
	private String name;

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && (obj instanceof Questionnaire) && ((Questionnaire) obj).getId() == getId() && ((Questionnaire) obj).getName().equals(getName());
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 71 * hash + this.id;
		hash = 71 * hash + Objects.hashCode(this.name);
		return hash;
	}
}
