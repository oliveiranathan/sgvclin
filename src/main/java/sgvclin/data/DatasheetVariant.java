/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.io.InvalidClassException;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import sgvclin.processors.DBController;

/**
 * @author Nathan Oliveira
 */
public class DatasheetVariant {

	private static final String SHEET_QUESTION_PREFIX = "SHEET::QUESTION::";
	private static final String SHEET_VARIANT_PREFIX = "SHEET::VARIANT::";

	public static DatasheetVariant getDatasheetVariant(Question q, Datasheet ds) throws SQLException {
		DatasheetVariant vfd = new DatasheetVariant();
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM `" + SHEET_QUESTION_PREFIX + ds.getName() + "`"
				+ " WHERE sheet=" + ds.getId() + " AND question=" + q.getId())) {
			if (!rs.next()) {
				return null;
			}
			try {
				vfd.setSheet(Datasheet.getDatasheet(rs.getInt("sheet"), Sheet.getSheet(ds.getName())));
			} catch (SQLException | NullPointerException | InvalidClassException ex) {
				throw new SQLException(ex.getMessage());
			}
			vfd.setQuestion(Question.getQuestion(rs.getInt("question")));
			vfd.setNotKnown(rs.getBoolean("nk"));
			vfd.setTechnicalProblem(rs.getBoolean("tp"));
			vfd.setObservation(rs.getString("observation"));
		}
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM `" + SHEET_VARIANT_PREFIX
				+ ds.getName() + "` WHERE sheet=" + ds.getId() + " AND question=" + q.getId() + " ORDER BY `index`")) {
			while (rs.next()) {
				vfd.addVariant(Variant.getVariant(rs.getInt("variant")));

			}
		}
		return vfd;
	}

	public static void saveDatasheetVariant(DatasheetVariant dsv) throws SQLException {
		DBController.getInstance().getStatement().execute("DELETE FROM `" + SHEET_VARIANT_PREFIX + dsv.getSheet().getName() + "` WHERE"
				+ " sheet=" + dsv.getSheet().getId() + " AND question=" + dsv.getQuestion().getId());
		DBController.getInstance().getStatement().execute("DELETE FROM `" + SHEET_QUESTION_PREFIX + dsv.getSheet().getName() + "` WHERE"
				+ " sheet=" + dsv.getSheet().getId() + " AND question=" + dsv.getQuestion().getId());
		try (PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("INSERT INTO `" + SHEET_QUESTION_PREFIX
				+ dsv.getSheet().getName() + "`(sheet, question, nk, tp, observation) VALUES(?, ?, ?, ?, ?)")) {
			pst.setInt(1, dsv.getSheet().getId());
			pst.setInt(2, dsv.getQuestion().getId());
			pst.setBoolean(3, dsv.isNotKnown());
			pst.setBoolean(4, dsv.isTechnicalProblem());
			pst.setString(5, dsv.getObservation());
			pst.executeUpdate();
		} catch (SQLException ex) {
			if (ex.getErrorCode() != 1062) {
				throw ex;
			}
		}
		try (PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("INSERT INTO `" + SHEET_VARIANT_PREFIX
				+ dsv.getSheet().getName() + "`(sheet, question, variant, `index`) VALUES(?, ?, ?, ?)")) {
			pst.setInt(1, dsv.getSheet().getId());
			pst.setInt(2, dsv.getQuestion().getId());
			for (int i = 0; i < dsv.getVariants().size(); i++) {
				pst.setInt(3, dsv.getVariants().get(i).getId());
				pst.setInt(4, i);
				pst.executeUpdate();
			}
		}
	}

	public static List<Variant> getListOfVariants(List<Datasheet> dss, Question q) throws SQLException {
		List<Variant> list = new ArrayList<>(20);
		String fdsString = "";
		boolean had = false;
		for (Datasheet fd : dss) {
			if (had) {
				fdsString += " OR ";
			} else {
				had = true;
			}
			fdsString += " sheet = " + fd.getId();
		}
		String sql = "SELECT * FROM Variant, `" + SHEET_VARIANT_PREFIX + dss.get(0).getName() + "` WHERE (" + fdsString + ") AND idVariant=variant"
				+ " AND question = " + q.getId();
		Variant v;
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery(sql)) {
			while (rs.next()) {
				v = Variant.getVariant(rs.getInt("variant"));
				if (!list.contains(v)) {
					list.add(v);
				}
			}
		}
		return list;
	}

	public static boolean hasDynamicVariantTables(String sheet) throws SQLException {
		DatabaseMetaData dmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(20);
		try (ResultSet rs = dmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, null, null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		boolean b = tables.stream().anyMatch((table)
				-> (table.toLowerCase(Locale.ENGLISH).equals((SHEET_QUESTION_PREFIX + sheet).toLowerCase(Locale.ENGLISH))));
		if (!b) {
			return false;
		}
		b = tables.stream().anyMatch((table)
				-> (table.toLowerCase(Locale.ENGLISH).equals((SHEET_VARIANT_PREFIX + sheet).toLowerCase(Locale.ENGLISH))));
		return b;
	}

	public static void renameSheetTables(String oldName, String newName) throws SQLException {
		DBController.getInstance().getStatement().execute("ALTER TABLE `" + SHEET_QUESTION_PREFIX + oldName + "` RENAME TO `" + SHEET_QUESTION_PREFIX + newName + "`");
		DBController.getInstance().getStatement().execute("ALTER TABLE `" + SHEET_VARIANT_PREFIX + oldName + "` RENAME TO `" + SHEET_VARIANT_PREFIX + newName + "`");
	}

	public static void createDynamicVariantTables(String sheet) throws SQLException {
		if (hasDynamicVariantTables(sheet)) {
			return;
		}
		String sql = "CREATE TABLE `" + SHEET_QUESTION_PREFIX + sheet + "`("
				+ " sheet int not null,"
				+ " question int not null, "
				+ " observation varchar(21000),"
				+ " nk BOOL not null,"
				+ " tp BOOL not null,"
				+ " PRIMARY KEY(sheet, question),"
				+ " CONSTRAINT `" + SHEET_QUESTION_PREFIX + sheet + "_ibfk_1` FOREIGN KEY(question) REFERENCES Question(idQuestion) ON UPDATE CASCADE ON DELETE CASCADE,"
				+ " CONSTRAINT `" + SHEET_QUESTION_PREFIX + sheet + "_ibfk_2` FOREIGN KEY(sheet) REFERENCES `" + Datasheet.SHEET_PREFIX + sheet
				+ "`(idSheet) ON UPDATE CASCADE ON DELETE CASCADE)";
		String sql2 = "CREATE TABLE `" + SHEET_VARIANT_PREFIX + sheet + "`("
				+ " sheet int not null,"
				+ " question int not null,"
				+ " variant int not null,"
				+ " `index` int not null,"
				+ " PRIMARY KEY(sheet, question, variant),"
				+ " CONSTRAINT `" + SHEET_VARIANT_PREFIX + sheet + "_ibfk_1` FOREIGN KEY(sheet) REFERENCES `" + Datasheet.SHEET_PREFIX + sheet
				+ "`(idSheet) ON UPDATE CASCADE ON DELETE CASCADE,"
				+ " CONSTRAINT `" + SHEET_VARIANT_PREFIX + sheet + "_ibfk_2` FOREIGN KEY(question) REFERENCES Question(idQuestion) ON UPDATE CASCADE ON DELETE CASCADE,"
				+ " CONSTRAINT `" + SHEET_VARIANT_PREFIX + sheet + "_ibfk_3` FOREIGN KEY(variant) REFERENCES Variant(idVariant) ON UPDATE CASCADE ON DELETE CASCADE)";
		DBController.getInstance().getStatement().execute(sql);
		DBController.getInstance().getStatement().execute(sql2);
	}

	public static void copyTables(String from, String to) throws SQLException {
		DBController.getInstance().getStatement().execute("INSERT INTO `" + SHEET_QUESTION_PREFIX + to + "`(sheet, question, observation, nk, tp) SELECT sheet, question, observation, nk, tp "
				+ "FROM `" + SHEET_QUESTION_PREFIX + from + "`");
		DBController.getInstance().getStatement().execute("INSERT INTO `" + SHEET_VARIANT_PREFIX + to + "`(sheet, question, variant, `index`) SELECT sheet, question, variant, `index` "
				+ "FROM `" + SHEET_VARIANT_PREFIX + from + "`");
	}

	public static void deleteDynamicVariantTables(String sheet) throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE `" + SHEET_VARIANT_PREFIX + sheet + "`");
		DBController.getInstance().getStatement().execute("DROP TABLE `" + SHEET_QUESTION_PREFIX + sheet + "`");
	}

	public static void createTables() {
	}

	public static boolean verifyTables() {
		return true;
	}

	public static void deleteTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		tables.stream().filter((table)
				-> (table.toLowerCase(Locale.ENGLISH).startsWith(SHEET_QUESTION_PREFIX.toLowerCase(Locale.ENGLISH))
				|| table.toLowerCase(Locale.ENGLISH).startsWith(SHEET_VARIANT_PREFIX.toLowerCase(Locale.ENGLISH)))).forEach((table) -> {
			try {
				DBController.getInstance().getStatement().executeUpdate("DROP TABLE IF EXISTS `" + table + "`");
			} catch (SQLException ex) {
			}
		});
	}

	private List<Variant> variants;
	private Datasheet sheet;
	private boolean technicalProblem = false, notKnown = false;
	private String observation;
	private Question question;

	public DatasheetVariant() {
		variants = new ArrayList<>(5);
	}

	public void clearVariants() {
		variants = new ArrayList<>(5);
	}

	public void setSheet(Datasheet sheet) {
		this.sheet = sheet;
	}

	public void addVariant(Variant variant) {
		this.variants.add(variant);
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public void setTechnicalProblem(boolean technicalProblem) {
		this.technicalProblem = technicalProblem;
	}

	public void setNotKnown(boolean notKnown) {
		this.notKnown = notKnown;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Datasheet getSheet() {
		return sheet;
	}

	public List<Variant> getVariants() {
		return Collections.unmodifiableList(variants);
	}

	public String getObservation() {
		return observation;
	}

	public Question getQuestion() {
		return question;
	}

	public boolean isNotKnown() {
		return notKnown;
	}

	public boolean isTechnicalProblem() {
		return technicalProblem;
	}
}
