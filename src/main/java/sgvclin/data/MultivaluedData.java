/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.io.Serializable;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import sgvclin.processors.DBController;
import sgvclin.processors.LoggerManager;

/**
 * @author Nathan Oliveira
 */
public class MultivaluedData implements Serializable {

	private static final long serialVersionUID = 1L;

	public static MultivaluedData getMultivaluedData(int key) throws SQLException, NullPointerException {
		MultivaluedData dm = new MultivaluedData();
		try (ResultSet rs = DBController.getInstance().getResultSet("MultivaluedData", key)) {
			if (!rs.next()) {
				return null;
			}
			dm.setId(rs.getInt("idMultivaluedData"));
			dm.setText(rs.getString("text"));
			dm.setSheetField(SheetField.getSheetField(rs.getInt("sheetField")));
		}
		return dm;
	}

	public static void saveMultivaluatedData(MultivaluedData md) throws SQLException {
		md.setId(DBController.getInstance().getNewPrimaryKeyFor("MultivaluedData"));
		try (PreparedStatement sdm = DBController.getInstance().getConnection().prepareStatement("INSERT INTO MultivaluedData"
				+ "(idMultivaluedData, sheetField, text, text_hash) VALUES(?, ?, ?, ?)")) {
			sdm.setInt(1, md.getId());
			sdm.setInt(2, md.getSheetField().getId());
			sdm.setString(3, md.getText());
			sdm.setString(4, DBController.getMD5(md.getText()));
			sdm.executeUpdate();
		}
	}

	public static List<MultivaluedData> getListOfMultivaluedData(SheetField sf) throws SQLException {
		List<Integer> list = new ArrayList<>(10);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idMultivaluedData, sheetField"
				+ " FROM MultivaluedData WHERE sheetField = " + sf.getId() + " ORDER BY idMultivaluedData")) {
			while (rs.next()) {
				list.add(rs.getInt("idMultivaluedData"));
			}
		}
		List<MultivaluedData> l = new ArrayList<>(list.size());
		for (int k : list) {
			l.add(getMultivaluedData(k));
		}
		return l;
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `MultivaluedData` ("
				+ "  `idMultivaluedData` int(11) NOT NULL,"
				+ "  `sheetField` int(11) NOT NULL,"
				+ "  `text` varchar(2500) NOT NULL,"
				+ "  `text_hash` char(32) NOT NULL,"
				+ "  PRIMARY KEY (`idMultivaluedData`),"
				+ "  UNIQUE (`sheetField`,`text_hash`),"
				+ "  CONSTRAINT `sheetField` FOREIGN KEY (`sheetField`) REFERENCES `SheetField` (`idSheetField`) ON DELETE CASCADE ON UPDATE CASCADE"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("MultivaluedData"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `MultivaluedData`");
	}

	private int id;
	private int sheetField;
	private String text;

	public SheetField getSheetField() throws SQLException {
		return SheetField.getSheetField(sheetField);
	}

	public int getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public void setSheetField(SheetField sheetField) {
		this.sheetField = sheetField.getId();
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}

	@Override
	public boolean equals(Object obj) {
		try {
			return obj != null && (obj instanceof MultivaluedData) && ((MultivaluedData) obj).getId() == id && ((MultivaluedData) obj).getText().equals(text)
					&& ((MultivaluedData) obj).getSheetField().getId() == sheetField;
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 37 * hash + this.id;
		hash = 37 * hash + Objects.hashCode(this.sheetField);
		hash = 37 * hash + Objects.hashCode(this.text);
		return hash;
	}
}
