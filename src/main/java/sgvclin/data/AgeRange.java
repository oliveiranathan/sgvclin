/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import sgvclin.processors.DBController;

/**
 * @author Nathan Oliveira
 */
public class AgeRange {

	public static AgeRange[] getListOfAgeRanges(AgeGroup ag) throws SQLException {
		List<AgeRange> lfes = new ArrayList<>(5);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM AgeRange WHERE ageGroup = " + ag.getId())) {
			while (rs.next()) {
				AgeRange f = new AgeRange();
				f.setAgeGroups(ag);
				f.setName(rs.getString("name"));
				f.setBegin(rs.getInt("begin"));
				f.setEnd(rs.getInt("end"));
				lfes.add(f);
			}
		}
		return lfes.toArray(new AgeRange[lfes.size()]);
	}

	public static void deleteAllAgeRanges(AgeGroup ag) throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("DELETE FROM AgeRange WHERE ageGroup = " + ag.getId());
	}

	public static void saveAllAgeRanges(AgeRange[] ars) throws SQLException {
		PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("INSERT INTO AgeRange("
				+ "ageGroup, name, begin, end, name_hash) VALUES (?, ?, ?, ?, ?)");
		for (AgeRange ar : ars) {
			pst.setInt(1, ar.getAgeGroups().getId());
			pst.setString(2, ar.getName());
			pst.setInt(3, ar.getBegin());
			pst.setInt(4, ar.getEnd());
			pst.setString(5, DBController.getMD5(ar.getName()));
			pst.executeUpdate();
		}
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `AgeRange` ("
				+ " `ageGroup` int(11) NOT NULL,"
				+ " `name` varchar(2500) NOT NULL,"
				+ " `name_hash` char(32) NOT NULL,"
				+ " `begin` int(11) NOT NULL,"
				+ " `end` int(11) NOT NULL,"
				+ " PRIMARY KEY (`ageGroup`, `begin`, `end`),"
				+ " UNIQUE (`ageGroup`, `name_hash`),"
				+ " CONSTRAINT `ageGroup` FOREIGN KEY (`ageGroup`) REFERENCES `AgeGroup` (`idAgeGroup`) ON DELETE CASCADE ON UPDATE CASCADE"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("AgeRange"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `AgeRange`");
	}

	private AgeGroup ageGroups;
	private String name;
	private int begin, end;

	public AgeGroup getAgeGroups() {
		return ageGroups;
	}

	public void setAgeGroups(AgeGroup ageGroups) {
		this.ageGroups = ageGroups;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBegin() {
		return begin;
	}

	public void setBegin(int begin) {
		this.begin = begin;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	@Override
	public String toString() {
		return name;
	}
}
