/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.awt.Image;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StreamCorruptedException;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import sgvclin.processors.CBDUtils;
import sgvclin.processors.DBController;
import sgvclin.processors.LoggerManager;
import sgvclin.processors.TemplateWorker;

/**
 * @author Nathan Oliveira
 */
public class Map {

	public static Map getMap(int key) throws SQLException, IOException, NullPointerException, ClassNotFoundException {
		Map m = new Map();
		try (ResultSet rs = DBController.getInstance().getResultSet("Map", key)) {
			rs.next();
			m.setId(rs.getInt("idMap"));
			m.setName(rs.getString("name"));
			m.setInvalid(rs.getBoolean("invalid"));
		}
		return m;
	}

	public static Map saveMap(String name, Image img) throws SQLException, NullPointerException, IOException, ClassNotFoundException {
		Map m = new Map();
		m.setId(DBController.getInstance().getNewPrimaryKeyFor("Map"));
		m.setName(name);
		m.setMapImage((BufferedImage) img);
		try (PreparedStatement sm = DBController.getInstance().getConnection().prepareStatement("INSERT INTO Map("
				+ "idMap, name, width, height, image, template, invalid, name_hash) VALUES(?, ?, ?, ?, ?, ?, ?, ?)")) {
			sm.setInt(1, m.getId());
			sm.setString(2, m.getName());
			sm.setDouble(3, m.mapImage.getWidth());
			sm.setDouble(4, m.mapImage.getHeight());
			ByteArrayOutputStream blob = new ByteArrayOutputStream();
			ImageIO.write(m.mapImage, "png", blob);
			sm.setBytes(5, blob.toByteArray());
			if (m.template == null) {
				sm.setBytes(6, null);
			} else {
				sm.setBytes(6, CBDUtils.getBytes(m.template));
			}
			sm.setBoolean(7, m.isInvalid());
			sm.setString(8, DBController.getMD5(m.getName()));
			sm.executeUpdate();
		}
		return m;
	}

	public static Map getMap(String name) throws SQLException, IOException, NullPointerException, ClassNotFoundException {
		int k;
		try (ResultSet rs = DBController.getInstance().getConnection().createStatement().executeQuery("SELECT idMap, name FROM Map WHERE name = '" + name + "'")) {
			if (!rs.next()) {
				throw new Exception(java.util.ResourceBundle.getBundle("texts/Bundle").getString("DATABASE ERROR"));
			}
			k = rs.getInt("idMap");
		} catch (Exception ex) {
			throw new NullPointerException(ex.getMessage());
		}
		return Map.getMap(k);
	}

	public static void updateMapTemplate(Map m) throws SQLException, IOException {
		try (PreparedStatement sm = DBController.getInstance().getConnection().prepareStatement("UPDATE Map SET template=?, invalid=? WHERE idMap=" + m.getId())) {
			if (m.template == null) {
				sm.setBytes(1, null);
			} else {
				sm.setBytes(1, CBDUtils.getBytes(m.template));
			}
			sm.setBoolean(2, true);
			sm.executeUpdate();
		}
	}

	public static void updateIsMapInvalid(Map m) throws SQLException {
		try (PreparedStatement sm = DBController.getInstance().getConnection().prepareStatement("UPDATE Map SET invalid=? WHERE idMap=" + m.getId())) {
			sm.setBoolean(1, m.isInvalid());
			sm.executeUpdate();
		}
		if (m.isInvalid()) {
			MapSaver.deleteMapSaversFrom(m);
		}
	}

	public static void deleteMap(Map map) throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("DELETE FROM Map WHERE idMap = " + map.getId());
	}

	public static List<Map> getMaps() throws SQLException, IOException, NullPointerException, ClassNotFoundException {
		List<Map> list = new ArrayList<>(5);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idMap FROM Map")) {
			while (rs.next()) {
				list.add(getMap(rs.getInt("idMap")));
			}
		}
		return list;
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `Map` ("
				+ "  `idMap` int(11) NOT NULL,"
				+ "  `name` varchar(200) NOT NULL,"
				+ "  `name_hash` char(32) NOT NULL,"
				+ "  `width` double NOT NULL,"
				+ "  `height` double NOT NULL,"
				+ "  `image` longblob NOT NULL,"
				+ "  `template` longblob DEFAULT NULL,"
				+ "  `invalid` bool NOT NULL DEFAULT TRUE,"
				+ "  PRIMARY KEY (`idMap`),"
				+ "  UNIQUE (`name_hash`)"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("Map"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `Map`");
	}

	private int id;
	private BufferedImage mapImage;
	private String name;
	private Area template = null;
	private boolean invalid = true;
	private Thread worker = null;

	private void loadMapImageFromDB() throws SQLException, IOException {
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT image FROM Map WHERE idMap = " + id)) {
			rs.next();
			ByteArrayInputStream blob = new ByteArrayInputStream(rs.getBytes("image"));
			BufferedImage img = ImageIO.read(blob);
			setMapImage(img);
		}
	}

	private void loadMapTemplateFromDB() throws SQLException, IOException, ClassNotFoundException {
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT template, invalid FROM Map WHERE idMap = " + id)) {
			rs.next();
			byte[] bytes = rs.getBytes("template");
			setInvalid(rs.getBoolean("invalid"));
			if (bytes == null) {
				setTemplate(null);
			} else {
				try {
					setTemplate(CBDUtils.getArea(bytes));
				} catch (StreamCorruptedException ex) {
					LoggerManager.getInstance().log(Level.INFO, "THIS SHALL FAIL ON THE FIRST RUN OF EVERY \"OLD\" MAP...", null);
					setTemplate(TemplateWorker.getArea(CBDUtils.decompress(bytes)));
					setInvalid(true);
					Map.updateMapTemplate(this);
				}
			}
		}
	}

	public BufferedImage getMapImage() {
		if (mapImage == null) {
			try {
				loadMapImageFromDB();
			} catch (IOException | SQLException ex) {
				//Should never happen
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		}
		return mapImage;
	}

	public void setWorker(Thread worker) {
		this.worker = worker;
	}

	public Thread getWorker() {
		return worker;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public boolean isInvalid() {
		return invalid;
	}

	public Area getTemplate() {
		if (template == null) {
			try {
				loadMapTemplateFromDB();
			} catch (ClassNotFoundException | IOException | SQLException ex) {
				//Should never happen
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			}
		}
		return template;
	}

	public void setMapImage(BufferedImage mapImage) throws IOException {
		this.mapImage = mapImage;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setInvalid(boolean invalid) {
		this.invalid = invalid;
	}

	public void setTemplate(Area template) {
		this.template = template;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 29 * hash + this.id;
		hash = 29 * hash + Objects.hashCode(this.mapImage);
		hash = 29 * hash + Objects.hashCode(this.name);
		hash = 29 * hash + Objects.hashCode(this.template);
		hash = 29 * hash + (this.invalid ? 1 : 0);
		hash = 29 * hash + Objects.hashCode(this.worker);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Map other = (Map) obj;
		return this.id == other.id
				&& this.invalid == other.invalid
				&& Objects.equals(this.name, other.name)
				&& Objects.equals(this.mapImage, other.mapImage)
				&& Objects.equals(this.template, other.template)
				&& Objects.equals(this.worker, other.worker);
	}
}
