/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.io.InvalidClassException;
import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Nathan Oliveira
 */
public class Data implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String DATE_PATTERN = "yyyy/MM/dd";

	private final SheetField.Type type;
	private final Object data;

	public Data(SheetField.Type tipo, Object dado) {
		this.type = tipo;
		this.data = dado;
	}

	public SheetField.Type getType() {
		return type;
	}

	public String getText() throws InvalidClassException {
		if (type == SheetField.Type.TEXT) {
			return (String) data;
		}
		throw new InvalidClassException(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INVALID TYPE."));
	}

	public Date getDate() throws InvalidClassException {
		if (type == SheetField.Type.DATE) {
			return (Date) data;
		}
		throw new InvalidClassException(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INVALID TYPE."));
	}

	public int getInt() throws InvalidClassException {
		if (type == SheetField.Type.INTEGER) {
			return (Integer) data;
		}
		throw new InvalidClassException(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INVALID TYPE."));
	}

	public double getDouble() throws InvalidClassException {
		if (type == SheetField.Type.DOUBLE) {
			return (Double) data;
		}
		throw new InvalidClassException(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INVALID TYPE."));
	}

	@SuppressWarnings("unchecked")
	public List<MultivaluedData> getSelectedValues() throws InvalidClassException {
		if (type == SheetField.Type.MULTIVALUED_MULTIPLE) {
			return (List<MultivaluedData>) data;
		}
		throw new InvalidClassException(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INVALID TYPE."));
	}

	public MultivaluedData getSelected() throws InvalidClassException {
		if (type == SheetField.Type.MULTIVALUED_UNIQUE) {
			return (MultivaluedData) data;
		}
		throw new InvalidClassException(java.util.ResourceBundle.getBundle("texts/Bundle").getString("INVALID TYPE."));
	}

	@Override
	public String toString() {
		switch (type) {
			case TEXT:
				return (String) data;
			case DATE:
				SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN, Locale.getDefault());
				return sdf.format((java.util.Date) data);
			case INTEGER:
				return Integer.toString((int) data);
			case DOUBLE:
				return Double.toString((double) data);
			case MULTIVALUED_UNIQUE:
				return ((MultivaluedData) data).getText();
			case MULTIVALUED_MULTIPLE:
				@SuppressWarnings("unchecked") List<MultivaluedData> list = (List<MultivaluedData>) data;
				return "[" + StringUtils.join(list, ",") + "]";
			default:
				throw new AssertionError(type.name());
		}
	}
}
