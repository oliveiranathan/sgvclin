/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import sgvclin.processors.DBController;

/**
 * @author Nathan Oliveira
 */
public class Question {

	public static Question getQuestion(int key) throws SQLException, NullPointerException {
		Question q = new Question();
		try (ResultSet rs = DBController.getInstance().getResultSet("Question", key)) {
			rs.next();
			q.setId(rs.getInt("idQuestion"));
			q.setQuestion(rs.getString("question"));
			q.setNumber(rs.getInt("number"));
			int k = rs.getInt("questionnaire");
			Questionnaire qq = Questionnaire.getQuestionnaire(k);
			q.setQuestionnaire(qq);
			k = rs.getInt("questionCategory");
			QuestionCategory cq = QuestionCategory.getQuestionCategory(k);
			q.setCategory(cq);
		}
		return q;
	}

	public static Question saveQuestion(Question question) throws SQLException {
		boolean update = true;
		try {
			getQuestion(question.getId());
		} catch (NullPointerException | SQLException ex) {
			update = false;
		}
		if (update) {
			try (PreparedStatement ps = DBController.getInstance().getConnection().prepareStatement("UPDATE Question SET question=?,"
					+ " questionCategory=?, number=?, question_hash=? WHERE idQuestion = " + question.getId())) {
				ps.setString(1, question.getQuestion());
				ps.setInt(2, question.getCategory().getId());
				ps.setInt(3, question.getNumber());
				ps.setString(4, DBController.getMD5(question.getQuestion()));
				ps.executeUpdate();
			}
		} else {
			question.setId(DBController.getInstance().getNewPrimaryKeyFor("Question"));
			try (PreparedStatement sq = DBController.getInstance().getConnection().prepareStatement("INSERT INTO Question(idQuestion,"
					+ " questionnaire, question, questionCategory, number, question_hash) VALUES(?, ?, ?, ?, ?, ?)")) {
				sq.setInt(1, question.getId());
				sq.setInt(2, question.getQuestionnaire().getId());
				sq.setString(3, question.getQuestion());
				sq.setInt(4, question.getCategory().getId());
				sq.setInt(5, question.getNumber());
				sq.setString(6, DBController.getMD5(question.getQuestion()));
				sq.executeUpdate();
			}
		}
		return question;
	}

	public static Question getFirstQuestion(Questionnaire questionnaire) throws SQLException {
		int k;
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT MIN(idQuestion) AS min FROM Question WHERE questionnaire = "
				+ questionnaire.getId())) {
			if (rs.next()) {
				k = rs.getInt("min");
			} else {
				k = 0;
			}
		}
		return (k == 0) ? getNewQuestion(questionnaire) : getQuestion(k);
	}

	public static Question getLastQuestion(Questionnaire questionnaire) throws SQLException {
		int k;
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT MAX(idQuestion) AS max FROM Question WHERE questionnaire = "
				+ questionnaire.getId())) {
			if (rs.next()) {
				k = rs.getInt("max");
			} else {
				k = 0;
			}
		}
		return (k == 0) ? getNewQuestion(questionnaire) : getQuestion(k);
	}

	public static Question getNextQuestion(Questionnaire questionnaire, Question question) throws SQLException {
		int k;
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT MIN(morethan) AS min FROM "
				+ "(SELECT idQuestion AS morethan FROM Question WHERE idQuestion > " + question.getId() + " AND questionnaire = "
				+ question.getQuestionnaire().getId() + ") AS temp")) {
			if (rs.next()) {
				k = rs.getInt("min");
			} else {
				k = 0;
			}

		}
		if (k > getLastQuestion(questionnaire).getId() || k == 0) {
			k = getLastQuestion(questionnaire).getId();
		}
		return (k == 0) ? getNewQuestion(questionnaire) : getQuestion(k);
	}

	public static Question getPreviousQuestion(Questionnaire questionnaire, Question question) throws SQLException {
		int k;
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT MAX(lessthan) AS max FROM "
				+ "(SELECT idQuestion AS lessthan FROM Question WHERE idQuestion < " + question.getId() + " AND questionnaire = "
				+ question.getQuestionnaire().getId() + ") AS temp")) {
			if (rs.next()) {
				k = rs.getInt("max");
			} else {
				k = 0;
			}
		}
		if (k < getFirstQuestion(questionnaire).getId()) {
			k = getFirstQuestion(questionnaire).getId();
		}
		return (k == 0) ? getNewQuestion(questionnaire) : getQuestion(k);
	}

	public static Question getNewQuestion(Questionnaire questionnaire) throws SQLException {
		int k = DBController.getInstance().getNewPrimaryKeyFor("Question");
		if (k == 0) {
			k = 1;
		}
		Question q = new Question();
		q.setId(k);
		q.setQuestionnaire(questionnaire);
		return q;
	}

	public static Question[] getQuestion(Questionnaire questionnaire, String question) throws SQLException {
		List<Integer> l = new ArrayList<>(2);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM Question WHERE question = '" + question + "'"
				+ " AND questionnaire = " + questionnaire.getId())) {
			while (rs.next()) {
				l.add(rs.getInt("idQuestion"));
			}
		}
		Question[] ret = new Question[l.size()];
		for (int i = 0; i < l.size(); i++) {
			ret[i] = getQuestion(l.get(i));
		}
		return ret;
	}

	public static Question deleteQuestion(Questionnaire questionnaire, Question question) throws SQLException {
		Question q = getNextQuestion(questionnaire, question);
		if (q.equals(question)) {
			q = getPreviousQuestion(questionnaire, question);
			if (q.equals(question)) {
				q = getNewQuestion(questionnaire);
			}
		}
		DBController.getInstance().getStatement().executeUpdate("DELETE FROM Question WHERE idQuestion=" + question.getId());
		return q;
	}

	public static List<Question> getListOfQuestions(Questionnaire questionnaire) throws SQLException {
		List<Question> l = new ArrayList<>(20);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM Question WHERE questionnaire = "
				+ questionnaire.getId() + " ORDER BY idQuestion")) {
			while (rs.next()) {
				l.add(getQuestion(rs.getInt("idQuestion")));
			}
		}
		return l;
	}

	public static int getCountOfQuestionsOf(QuestionCategory qc) throws SQLException {
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT COUNT(*) as c FROM Question WHERE questionCategory=" + qc.getId())) {
			if (rs.next()) {
				return rs.getInt("c");
			}
		}
		return 0;
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `Question` ("
				+ "  `idQuestion` int(11) NOT NULL,"
				+ "  `questionnaire` int(11) NOT NULL,"
				+ "  `question` varchar(15000) NOT NULL,"
				+ "  `question_hash` char(32) NOT NULL,"
				+ "  `questionCategory` int(11) NOT NULL,"
				+ "  `number` int(11) NOT NULL,"
				+ "  PRIMARY KEY (`idQuestion`),"
				+ "  UNIQUE (`questionnaire`,`question_hash`,`questionCategory`),"
				+ "  UNIQUE (`questionnaire`,`number`,`questionCategory`),"
				+ "  CONSTRAINT `Question_ibfk_1` FOREIGN KEY (`questionnaire`) REFERENCES `Questionnaire` (`idQuestionnaire`) ON DELETE CASCADE ON UPDATE CASCADE,"
				+ "  CONSTRAINT `Question_ibfk_2` FOREIGN KEY (`questionCategory`) REFERENCES `QuestionCategory` (`idQuestionCategory`) ON DELETE CASCADE ON UPDATE CASCADE"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("Question"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `Question`");
	}

	private int id;
	private int number;
	private Questionnaire questionnaire;
	private String question;
	private QuestionCategory category;

	public QuestionCategory getCategory() {
		return category;
	}

	public int getId() {
		return id;
	}

	public String getQuestion() {
		return question;
	}

	public Questionnaire getQuestionnaire() {
		return questionnaire;
	}

	public int getNumber() {
		return number;
	}

	public void setCategory(QuestionCategory category) {
		this.category = category;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return question;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 97 * hash + this.id;
		hash = 97 * hash + this.number;
		hash = 97 * hash + Objects.hashCode(this.questionnaire);
		hash = 97 * hash + Objects.hashCode(this.question);
		hash = 97 * hash + Objects.hashCode(this.category);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Question other = (Question) obj;
		return this.id == other.id
				&& this.number == other.number
				&& Objects.equals(this.question, other.question)
				&& Objects.equals(this.questionnaire, other.questionnaire)
				&& Objects.equals(this.category, other.category);
	}
}
