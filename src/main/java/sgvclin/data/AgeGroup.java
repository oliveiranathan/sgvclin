/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import sgvclin.processors.DBController;

/**
 * @author Nathan Oliveira
 */
public class AgeGroup {

	public static AgeGroup getAgeGroup(int id) throws SQLException {
		AgeGroup fE = new AgeGroup();
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM AgeGroup WHERE idAgeGroup = " + id)) {
			if (rs.next()) {
				fE.setId(rs.getInt("idAgeGroup"));
				fE.setName(rs.getString("name"));
				return fE;
			}
		}
		return null;
	}

	public static AgeGroup getAgeGroup(String name) throws SQLException {
		AgeGroup fE = new AgeGroup();
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM AgeGroup WHERE name = '" + name + "'")) {
			if (rs.next()) {
				fE.setId(rs.getInt("idAgeGroup"));
				fE.setName(rs.getString("name"));
				return fE;
			}
		}
		return null;
	}

	public static void deleteAgeGroup(AgeGroup ag) throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("DELETE FROM AgeGroup WHERE idAgeGroup = " + ag.getId());
	}

	public static void saveAgeGroup(AgeGroup ag) throws SQLException {
		boolean update = false;
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM AgeGroup WHERE idAgeGroup = " + ag.getId())) {
			if (rs.next()) {
				update = true;
			}
		}
		if (update) {
			PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("UPDATE AgeGroup SET name=?, name_hash=? WHERE idAgeGroup = " + ag.getId());
			pst.setString(1, ag.getName());
			pst.setString(2, DBController.getMD5(ag.getName()));
			pst.executeUpdate();
		} else {
			PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("INSERT INTO AgeGroup(idAgeGroup, name, name_hash) VALUES (?, ?, ?)");
			pst.setInt(1, ag.getId());
			pst.setString(2, ag.getName());
			pst.setString(3, DBController.getMD5(ag.getName()));
			pst.executeUpdate();
		}
	}

	public static AgeGroup createAgeGroup(String name) throws SQLException {
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM AgeGroup WHERE name = '" + name + "'")) {
			if (rs.next()) {
				return null;
			}
		}
		AgeGroup fE = new AgeGroup();
		fE.setName(name);
		fE.setId(DBController.getInstance().getNewPrimaryKeyFor("AgeGroup"));
		saveAgeGroup(fE);
		return fE;
	}

	public static AgeGroup[] getAgeGroups() throws SQLException {
		List<AgeGroup> ageGroup = new ArrayList<>(5);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idAgeGroup FROM AgeGroup")) {
			while (rs.next()) {
				ageGroup.add(getAgeGroup(rs.getInt("idAgeGroup")));
			}
		}
		return ageGroup.toArray(new AgeGroup[ageGroup.size()]);
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `AgeGroup` ("
				+ " `idAgeGroup` int(11) NOT NULL,"
				+ " `name` varchar(2500) NOT NULL,"
				+ " `name_hash` char(32) NOT NULL,"
				+ " PRIMARY KEY (`idAgeGroup`),"
				+ " UNIQUE (`name_hash`)"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("AgeGroup"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `AgeGroup`");
	}

	private int id;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
