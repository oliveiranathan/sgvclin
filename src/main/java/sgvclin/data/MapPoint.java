/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.awt.Point;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.StreamCorruptedException;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import sgvclin.processors.CBDUtils;
import sgvclin.processors.DBController;
import sgvclin.processors.LoggerManager;

/**
 * @author Nathan Oliveira
 */
public class MapPoint {

	public static MapPoint getMapPoint(int key) throws SQLException, NullPointerException, IOException, ClassNotFoundException {
		MapPoint pm = new MapPoint();
		try (ResultSet rs = DBController.getInstance().getResultSet("MapPoint", key)) {
			rs.next();
			pm.setId(rs.getInt("idMapPoint"));
			pm.setPosition(new Point2D.Double(rs.getDouble("x"), rs.getDouble("y")));
			int k = rs.getInt("map");
			Map m = Map.getMap(k);
			pm.setMap(m);
			k = rs.getInt("geographicPoint");
			GeographicPoint pg = GeographicPoint.getGeoPoint(k);
			pm.setGeographicPoint(pg);
			byte[] bs = rs.getBytes("area");
			if (bs == null) {
				pm.setArea(null);
			} else {
				try {
					pm.setArea(CBDUtils.getArea(bs));
				} catch (StreamCorruptedException ex) {
					LoggerManager.getInstance().log(Level.INFO, "THIS SHALL FAIL ON THE FIRST RUN OF EVERY \"OLD\" MAP...", null);
					m.setInvalid(true);
					Map.updateIsMapInvalid(m);
				}
			}
		}
		return pm;
	}

	public static void saveMapPoint(Map map, Point point, int codeGP, String name, String state) throws SQLException, IOException, ClassNotFoundException {
		GeographicPoint pg = GeographicPoint.getGeoPointByCode(codeGP, name, state);
		MapPoint pm = null;
		try {
			pm = getMapPoint(codeGP, map);
		} catch (IOException ex) {
			throw new SQLException(ex.getMessage());
		}
		boolean isNew = false;
		if (pm == null) {
			isNew = true;
			pm = new MapPoint();
			pm.setId(DBController.getInstance().getNewPrimaryKeyFor("MapPoint"));
			pm.setMap(map);
			pm.setGeographicPoint(pg);
		}
		pm.setPosition(new Point2D.Double(point.x, point.y));
		if (isNew) {
			try (PreparedStatement spm = DBController.getInstance().getConnection().prepareStatement("INSERT INTO MapPoint"
					+ "(idMapPoint, map, x, y, geographicPoint, area) VALUES(?, ?, ?, ?, ?, ?)")) {
				spm.setInt(1, pm.getId());
				spm.setInt(2, pm.getMap().getId());
				spm.setDouble(3, pm.getPosition().x);
				spm.setDouble(4, pm.getPosition().y);
				spm.setInt(5, pm.getGeographicPoint().getId());
				if (pm.getArea() == null) {
					spm.setBytes(6, null);
				} else {
					spm.setBytes(6, CBDUtils.getBytes(pm.getArea()));
				}
				spm.executeUpdate();
			}
		} else {
			try (PreparedStatement spm = DBController.getInstance().getConnection().prepareStatement("UPDATE MapPoint SET x=?, y=? WHERE idMapPoint="
					+ pm.getId())) {
				spm.setDouble(1, pm.getPosition().x);
				spm.setDouble(2, pm.getPosition().y);
				spm.executeUpdate();
			}
		}
		pm.getMap().setInvalid(true);
		Map.updateIsMapInvalid(pm.getMap());
	}

	public static MapPoint getMapPoint(int codeGP, Map map) throws SQLException, IOException, ClassNotFoundException {
		int k;
		try (ResultSet rs = DBController.getInstance().getConnection().createStatement().executeQuery("SELECT * FROM MapPoint, GeographicPoint WHERE map=" + map.getId()
				+ " AND code = " + codeGP + " AND idGeographicPoint = geographicPoint")) {
			if (!rs.next()) {
				return null;
			}
			k = rs.getInt("idMapPoint");
		}
		return getMapPoint(k);
	}

	public static void updateMapPointArea(MapPoint pm) throws SQLException, IOException {
		try (PreparedStatement spm = DBController.getInstance().getConnection().prepareStatement("UPDATE MapPoint SET area=? WHERE idMapPoint=" + pm.getId())) {
			if (pm.getArea() == null) {
				spm.setBytes(1, null);
			} else {
				spm.setBytes(1, CBDUtils.getBytes(pm.getArea()));
			}
			spm.executeUpdate();
		}
	}

	public static List<GeographicPoint> getMapPointsOfMap(Map map) throws SQLException {
		List<GeographicPoint> list = new ArrayList<>(50);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM MapPoint WHERE map=" + map.getId())) {
			while (rs.next()) {
				list.add(GeographicPoint.getGeoPoint(rs.getInt("geographicPoint")));
			}
		}
		return list;
	}

	public static List<Point2D.Double> getPointsPositionsOfMap(Map map) throws SQLException {
		List<Point2D.Double> list = new ArrayList<>(50);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM MapPoint WHERE map=" + map.getId())) {
			while (rs.next()) {
				list.add(new Point2D.Double(rs.getDouble("x"), rs.getDouble("y")));
			}
		}
		return list;
	}

	public static List<String> getCodesOfPointsInMap(Map map) throws SQLException {
		List<Integer> list = new ArrayList<>(50);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM MapPoint WHERE map=" + map.getId())) {
			while (rs.next()) {
				list.add(rs.getInt("geographicPoint"));
			}
		}
		List<String> strs = new ArrayList<>(list.size());
		for (int i : list) {
			strs.add("" + GeographicPoint.getGeoPoint(i).getCode());
		}
		return strs;
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `MapPoint` ("
				+ "  `idMapPoint` int(11) NOT NULL,"
				+ "  `map` int(11) NOT NULL,"
				+ "  `x` double NOT NULL,"
				+ "  `y` double NOT NULL,"
				+ "  `geographicPoint` int(11) NOT NULL,"
				+ "  `area` longblob DEFAULT NULL,"
				+ "  PRIMARY KEY (`idMapPoint`),"
				+ "  UNIQUE (`map`,`geographicPoint`),"
				+ "  CONSTRAINT `MapPoint_ibfk_1` FOREIGN KEY (`map`) REFERENCES `Map` (`idMap`) ON UPDATE CASCADE ON DELETE CASCADE,"
				+ "  CONSTRAINT `MapPoint_ibfk_2` FOREIGN KEY (`geographicPoint`) REFERENCES `GeographicPoint` (`idGeographicPoint`) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("MapPoint"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `MapPoint`");
	}

	private int id;
	private Map map;
	private Point2D.Double position;
	private GeographicPoint geographicPoint;
	private Area area;

	public Map getMap() {
		return map;
	}

	public GeographicPoint getGeographicPoint() {
		return geographicPoint;
	}

	public Point2D.Double getPosition() {
		return position;
	}

	public int getId() {
		return id;
	}

	public Area getArea() {
		return area;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public void setGeographicPoint(GeographicPoint geographicPoint) {
		this.geographicPoint = geographicPoint;
	}

	public void setPosition(Point2D.Double position) {
		this.position = position;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 73 * hash + this.id;
		hash = 73 * hash + Objects.hashCode(this.map);
		hash = 73 * hash + Objects.hashCode(this.position);
		hash = 73 * hash + Objects.hashCode(this.geographicPoint);
		hash = 73 * hash + Objects.hashCode(this.area);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final MapPoint other = (MapPoint) obj;
		return this.id == other.id
				&& Objects.equals(this.map, other.map)
				&& Objects.equals(this.position, other.position)
				&& Objects.equals(this.geographicPoint, other.geographicPoint)
				&& Objects.equals(this.area, other.area);
	}
}
