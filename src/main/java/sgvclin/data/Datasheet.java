/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.io.InvalidClassException;
import java.io.Serializable;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import sgvclin.UI.SheetFieldPanel;
import sgvclin.processors.DBController;
import sgvclin.processors.LoggerManager;
import sgvclin.util.Pair;
import sgvclin.util.Triplet;

/**
 * @author Nathan Oliveira
 */
public class Datasheet implements Serializable {

	private static final long serialVersionUID = 1L;

	protected static final String SHEET_PREFIX = "SHEET::";

	public static Datasheet getFirstDatasheet(Sheet sheet) throws SQLException, Exception {
		int k;
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT MIN(idSheet) AS min FROM `" + SHEET_PREFIX + sheet.getName() + "`")) {
			if (!rs.next()) {
				k = 0;
			} else {
				k = rs.getInt("min");
			}
		}
		if (k == 0) {
			Datasheet fd = new Datasheet();
			fd.setId(1);
			fd.setName(sheet.getName());
			return fd;
		} else {
			return getDatasheet(k, sheet);
		}
	}

	public static Datasheet getPreviousDatasheet(Datasheet ds) throws SQLException, Exception {
		int k;
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT MAX(lessthan) AS max FROM "
				+ "(SELECT idSheet AS lessthan FROM `" + SHEET_PREFIX + ds.getName() + "` WHERE idSheet < " + ds.getId() + ") AS temp")) {
			rs.next();
			k = rs.getInt("max");
		}
		if (k < getFirstDatasheet(Sheet.getSheet(ds.getName())).getId()) {
			k = getFirstDatasheet(Sheet.getSheet(ds.getName())).getId();
		}
		return (k == 0) ? getNewDatasheet(Sheet.getSheet(ds.getName())) : getDatasheet(k, Sheet.getSheet(ds.getName()));
	}

	public static Datasheet getNextDatasheet(Datasheet ds) throws SQLException, Exception {
		int k;
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT MIN(morethan) AS min FROM "
				+ "(SELECT idSheet AS morethan FROM `" + SHEET_PREFIX + ds.getName() + "` WHERE idSheet > " + ds.getId() + ") AS temp")) {
			rs.next();
			k = rs.getInt("min");
		}
		if (k > getLastDatasheet(Sheet.getSheet(ds.getName())).getId() || k == 0) {
			k = getLastDatasheet(Sheet.getSheet(ds.getName())).getId();
		}
		return (k == 0) ? getNewDatasheet(Sheet.getSheet(ds.getName())) : getDatasheet(k, Sheet.getSheet(ds.getName()));
	}

	public static Datasheet getLastDatasheet(Sheet sheet) throws SQLException, Exception {
		int k = DBController.getInstance().maxValueOfColumn(SHEET_PREFIX + sheet.getName(), "idSheet");
		return (k == 0) ? getNewDatasheet(sheet) : getDatasheet(k, sheet);
	}

	public static Datasheet getNewDatasheet(Sheet sheet) throws SQLException {
		int k = DBController.getInstance().maxValueOfColumn(SHEET_PREFIX + sheet.getName(), "idSheet") + 1;
		Datasheet fd = new Datasheet();
		fd.setId(k);
		fd.setName(sheet.getName());
		return fd;
	}

	public static Datasheet getDatasheet(int key, Sheet sheet) throws SQLException, InvalidClassException {
		Datasheet ds = new Datasheet();
		ds.setName(sheet.getName());
		List<SheetField> cfs_dmvs = new ArrayList<>(50);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM `" + SHEET_PREFIX + sheet.getName() + "` WHERE idSheet = " + key)) {
			rs.next();
			ds.setId(rs.getInt("idSheet"));
			ds.setInformant(rs.getInt("informant"));
			ds.setLocality(GeographicPoint.getGeoPoint(rs.getInt("geographicPoint")));
			for (SheetField cf : SheetField.getListOfFields(sheet)) {
				if (cf.isText()) {
					ds.addProperty(cf.getName(), SheetField.Type.TEXT, rs.getString(cf.getName()));
				} else if (cf.isDate()) {
					String[] tokens = rs.getString(cf.getName()).split("-");
					Date d = new Date(new GregorianCalendar(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]) - 1, Integer.parseInt(tokens[2])).getTimeInMillis());
					ds.addProperty(cf.getName(), SheetField.Type.DATE, d);
				} else if (cf.isInteger()) {
					ds.addProperty(cf.getName(), SheetField.Type.INTEGER, rs.getInt(cf.getName()));
				} else if (cf.isDouble()) {
					ds.addProperty(cf.getName(), SheetField.Type.DOUBLE, rs.getDouble(cf.getName()));
				} else if (cf.isMultivaluedButUniqueSelectable()) {
					for (MultivaluedData md : MultivaluedData.getListOfMultivaluedData(cf)) {
						if (md.toString().equals(rs.getString(cf.getName()))) {
							ds.addProperty(cf.getName(), SheetField.Type.MULTIVALUED_UNIQUE, md);
						}
					}
				} else if (cf.isMultivaluedAndMultiselectable()) {
					cfs_dmvs.add(cf);
				}
			}
		}
		for (SheetField cf : cfs_dmvs) {
			try (ResultSet rs = DBController.getInstance().getStatement()
					.executeQuery("SELECT * FROM `" + SHEET_PREFIX + ds.getName() + "::" + cf.getName() + "` WHERE `" + ds.getName() + "` = " + ds.getId())) {
				List<MultivaluedData> dados = new ArrayList<>(10);
				while (rs.next()) {
					for (MultivaluedData md : MultivaluedData.getListOfMultivaluedData(cf)) {
						if (md.toString().equals(rs.getString(cf.getName()))) {
							dados.add(md);
						}
					}
				}
				ds.addProperty(cf.getName(), SheetField.Type.MULTIVALUED_MULTIPLE, dados);
			}
		}
		return ds;
	}

	public static void saveDatasheet(Datasheet ds) throws SQLException, Exception {
		try {
			saveDS(ds);
		} catch (Exception e) {
			updateDatasheet(ds);
		}
	}

	private static void updateDatasheet(Datasheet ds) throws SQLException, Exception {
		String stmt = "UPDATE `" + SHEET_PREFIX + ds.getName() + "` SET informant=" + ds.getInformant() + ", geographicPoint=" + ds.getLocality().getId() + ", ";
		List<String> stmts = new ArrayList<>(10);
		for (SheetField cf : SheetField.getListOfFields(Sheet.getSheet(ds.getName()))) {
			if (cf.isText()) {
				stmt += "`" + cf.getName() + "`='" + ds.getProperty(cf.getName()).getText() + "', ";
			} else if (cf.isDate()) {
				Date d = ds.getProperty(cf.getName()).getDate();
				stmt += "`" + cf.getName() + "`='" + d.toString() + "', ";
			} else if (cf.isInteger()) {
				stmt += "`" + cf.getName() + "`=" + ds.getProperty(cf.getName()).getInt() + ", ";
			} else if (cf.isDouble()) {
				stmt += "`" + cf.getName() + "`=" + ds.getProperty(cf.getName()).getDouble() + ", ";
			} else if (cf.isMultivaluedButUniqueSelectable()) {
				stmt += "`" + cf.getName() + "`='" + ds.getProperty(cf.getName()).getSelected() + "', ";
			} else if (cf.isMultivaluedAndMultiselectable()) {
				List<MultivaluedData> ms = ds.getProperty(cf.getName()).getSelectedValues();
				stmts.add("DELETE FROM `" + SHEET_PREFIX + ds.getName() + "::" + cf.getName() + "` WHERE `" + ds.getName() + "` = " + ds.getId());
				ms.stream().map((m) -> m.toString()).forEach((m) -> {
					stmts.add("INSERT INTO `" + SHEET_PREFIX + ds.getName() + "::" + cf.getName() + "`(`" + ds.getName() + "`, `" + cf.getName() + "`) "
							+ "VALUES(" + ds.getId() + ", '" + m + "')");
				});
			}
		}
		if (stmt.endsWith(", ")) {
			stmt = stmt.substring(0, stmt.length() - 2);
		}
		stmt += " WHERE idSheet = " + ds.getId();
		Statement statement = DBController.getInstance().getStatement();
		statement.clearBatch();
		statement.addBatch(stmt);
		for (String str : stmts) {
			statement.addBatch(str);
		}
		statement.executeBatch();
	}

	private static void saveDS(Datasheet ds) throws SQLException, Exception {
		String stmt = "INSERT INTO `" + SHEET_PREFIX + ds.getName() + "` (informant, geographicPoint";
		String values = "VALUES (" + ds.getInformant() + ", " + ds.getLocality().getId();
		List<String> stmts = new ArrayList<>(10);
		for (SheetField cf : SheetField.getListOfFields(Sheet.getSheet(ds.getName()))) {
			if (cf.isText()) {
				stmt += ", `" + cf.getName() + "`";
				values += ", '" + ds.getProperty(cf.getName()).getText() + "'";
			} else if (cf.isDate()) {
				Date d = ds.getProperty(cf.getName()).getDate();
				stmt += ", `" + cf.getName() + "`";
				values += ", '" + d.toString() + "'";
			} else if (cf.isInteger()) {
				stmt += ", `" + cf.getName() + "`";
				values += ", " + ds.getProperty(cf.getName()).getInt();
			} else if (cf.isDouble()) {
				stmt += ", `" + cf.getName() + "`";
				values += ", " + ds.getProperty(cf.getName()).getDouble();
			} else if (cf.isMultivaluedButUniqueSelectable()) {
				stmt += ", `" + cf.getName() + "`";
				values += ", '" + ds.getProperty(cf.getName()).getSelected() + "'";
			} else if (cf.isMultivaluedAndMultiselectable()) {
				List<MultivaluedData> ms = ds.getProperty(cf.getName()).getSelectedValues();
				stmts.add("DELETE FROM `" + SHEET_PREFIX + ds.getName() + "::" + cf.getName() + "` WHERE `" + ds.getName() + "` = " + ds.getId());
				ms.stream().map((m) -> m.toString()).forEach((m) -> {
					stmts.add("INSERT INTO `" + SHEET_PREFIX + ds.getName() + "::" + cf.getName() + "`(`" + ds.getName() + "`, `" + cf.getName() + "`) "
							+ "VALUES(" + ds.getId() + ", '" + m + "')");
				});
			}
		}
		stmt += ", idSheet) ";
		values += ", " + ds.getId() + ")";
		stmt += values;
		Statement statement = DBController.getInstance().getStatement();
		statement.clearBatch();
		statement.addBatch(stmt);
		for (String str : stmts) {
			statement.addBatch(str);
		}
		statement.executeBatch();
	}

	public static List<Datasheet> getListDatasheets(Sheet sheet) throws SQLException, InvalidClassException {
		List<Integer> li = new ArrayList<>(10);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idSheet, geographicPoint, informant, "
				+ "idGeographicPoint, code FROM `" + SHEET_PREFIX + sheet.getName() + "`, GeographicPoint WHERE geographicPoint=idGeographicPoint"
				+ " ORDER BY code, informant")) {
			while (rs.next()) {
				int i = rs.getInt("idSheet");
				li.add(i);
			}
		}
		List<Datasheet> l = new ArrayList<>(li.size());
		for (int i : li) {
			l.add(getDatasheet(i, sheet));
		}
		return l;
	}

	public static List<Datasheet> getListDatasheets(List<GeographicPoint> points, Sheet sheet) throws SQLException, InvalidClassException {
		List<Datasheet> list = new ArrayList<>(points.size());
		String pontos = "";
		boolean had = false;
		for (GeographicPoint gp : points) {
			if (had) {
				pontos += " OR ";
			} else {
				had = true;
			}
			pontos += " geographicPoint = " + gp.getId();
		}
		if (!had) { // there are no points in the map, so it will fail
			throw new IllegalStateException("Create the points of the map first.");
		}
		String sql = "SELECT idSheet, geographicPoint, informant, idGeographicPoint, code FROM `" + SHEET_PREFIX + sheet.getName() + "`, GeographicPoint"
				+ " WHERE geographicPoint=idGeographicPoint AND (" + pontos + ") ORDER BY code, informant";
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery(sql)) {
			while (rs.next()) {
				list.add(getDatasheet(rs.getInt("idSheet"), sheet));
			}
		}
		return list;
	}

	public static Datasheet deleteDatasheet(Datasheet ds) throws SQLException {
		Datasheet fd_prx;
		try {
			fd_prx = getNextDatasheet(ds);
			if (fd_prx.equals(ds)) {
				fd_prx = getPreviousDatasheet(fd_prx);
				if (fd_prx.equals(ds)) {
					fd_prx = getNewDatasheet(Sheet.getSheet(ds.getName()));
				}
			}
		} catch (Exception ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			throw new SQLException(ex.getMessage());
		}
		try (PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("DELETE FROM `" + SHEET_PREFIX + ds.getName() + "`"
				+ " WHERE idSheet=?")) {
			pst.setInt(1, ds.getId());
			pst.executeUpdate();
		}
		return fd_prx;
	}

	public static void updateSheet(Sheet selected, String newName, List<SheetField> oldFields, List<SheetFieldPanel> renamedIn,
			List<SheetField> removed, List<SheetFieldPanel> addedIn, List<SheetFieldPanel> panels) throws SQLException {
		DBController.getInstance().getConnection().setAutoCommit(false);
		try {
			updateGroupables(oldFields, panels);
			if (newName == null && renamedIn.isEmpty() && removed.isEmpty() && addedIn.isEmpty()) {
				DBController.getInstance().getConnection().commit();
				DBController.getInstance().getConnection().setAutoCommit(true);
				return;
			}
			List<SheetField> unchanged = new ArrayList<>(oldFields);
			List<Pair<String, SheetField.Type>> added = new ArrayList<>(addedIn.size());
			List<Triplet<String, String, SheetField.Type>> renamed = new ArrayList<>(renamedIn.size());
			addedIn.stream().forEach((add) -> {
				SheetField sf = add.saveSheetField(selected);
				if (sf != null) {
					added.add(new Pair<>(sf.getName(), sf.getType()));
				}
			});
			for (SheetField field : removed) {
				unchanged.remove(field);
				SheetField.deleteSheetField(field);
			}
			for (SheetFieldPanel panel : renamedIn) {
				SheetField sf = null;
				for (SheetField field : oldFields) {
					if (field.getName().equals(panel.getOldName())) {
						sf = field;
					}
				}
				if (sf != null) {
					sf.setName(panel.getFieldName());
					SheetField.updateSheetField(sf);
					renamed.add(new Triplet<>(panel.getOldName(), panel.getFieldName(), sf.getType()));
					unchanged.remove(sf);
				}
			}
			updateTable(selected, newName, unchanged, added, renamed, removed);
			DBController.getInstance().getConnection().commit();
		} catch (SQLException ex) {
			LoggerManager.getInstance().log(Level.SEVERE, null, ex);
			DBController.getInstance().getConnection().rollback();
		}
		DBController.getInstance().getConnection().setAutoCommit(true);
	}

	private static void updateGroupables(List<SheetField> fields, List<SheetFieldPanel> panels) throws SQLException {
		for (SheetField field : fields) {
			for (SheetFieldPanel panel : panels) {
				if (field.getName().equals(panel.getOldName())) {
					if (field.isGroupable() != panel.isGroupable()) {
						field.setGroupable(panel.isGroupable());
						SheetField.updateSheetField(field);
					}
					break;
				}
			}
		}
	}

	@SuppressWarnings("AssignmentToMethodParameter")
	private static void updateTable(Sheet sheet, String newName, List<SheetField> unchanged, List<Pair<String, SheetField.Type>> added,
			List<Triplet<String, String, SheetField.Type>> renamed, List<SheetField> deleted) throws SQLException {
		String oldName = sheet.getName();
		String tmpTablePrefix = "";
		// check if needs renaming
		if (newName == null) {
			tmpTablePrefix = "TMP::";
			renameSheetTable(oldName, tmpTablePrefix + oldName);
			for (SheetField sf : unchanged) {
				if (sf.isMultivaluedAndMultiselectable()) {
					renameMultivaluedTable(oldName, tmpTablePrefix + oldName, sf.getName());
				}
			}
			newName = oldName;
		} else {
			sheet.setName(newName);
			Sheet.updateSheet(sheet);
		}
		// create new tables
		createSheetOnDB(sheet);
		// copy data from the old tables
		{
			String sqlPt1 = "INSERT INTO `" + SHEET_PREFIX + newName + "` (`idSheet`, `geographicPoint`, `informant`, ";
			String sqlPt2 = ") SELECT `idSheet`, `geographicPoint`, `informant`, ";
			String sqlPt3 = " FROM `" + SHEET_PREFIX + tmpTablePrefix + oldName + "`";
			for (Triplet<String, String, SheetField.Type> trip : renamed) {
				sqlPt1 += "`" + trip.b + "`, ";
				sqlPt2 += "`" + trip.a + "`, ";
			}
			for (SheetField sf : unchanged) {
				sqlPt1 += "`" + sf.getName() + "`, ";
				sqlPt2 += "`" + sf.getName() + "`, ";
			}
			String sql = sqlPt1.substring(0, sqlPt1.length() - 2) + sqlPt2.substring(0, sqlPt2.length() - 2) + sqlPt3;
			DBController.getInstance().getStatement().execute(sql);
			for (Triplet<String, String, SheetField.Type> trip : renamed) {
				if (trip.c == SheetField.Type.MULTIVALUED_MULTIPLE) {
					DBController.getInstance().getStatement().execute("INSERT INTO `" + SHEET_PREFIX + newName + "::" + trip.b + "`(" + newName + ", " + trip.b + ")"
							+ " SELECT " + oldName + ", " + trip.a + " FROM `" + SHEET_PREFIX + tmpTablePrefix + oldName + "::" + trip.a + "`");
				}
			}
			for (SheetField sf : unchanged) {
				if (sf.isMultivaluedAndMultiselectable()) {
					DBController.getInstance().getStatement().execute("INSERT INTO `" + SHEET_PREFIX + newName + "::" + sf.getName() + "`(" + newName + ", " + sf.getName() + ")"
							+ " SELECT " + oldName + ", " + sf.getName() + " FROM `" + SHEET_PREFIX + tmpTablePrefix + oldName + "::" + sf.getName() + "`");
				}
			}
			DatasheetVariant.copyTables(tmpTablePrefix + oldName, newName);
		}
		// delete the old tables
		for (SheetField field : deleted) {
			if (field.isMultivaluedAndMultiselectable()) {
				DBController.getInstance().getStatement().execute("DROP TABLE `" + SHEET_PREFIX + tmpTablePrefix + oldName + "::" + field.getName() + "`");
			}
		}
		for (SheetField field : unchanged) {
			if (field.isMultivaluedAndMultiselectable()) {
				DBController.getInstance().getStatement().execute("DROP TABLE `" + SHEET_PREFIX + tmpTablePrefix + oldName + "::" + field.getName() + "`");
			}
		}
		for (Triplet<String, String, SheetField.Type> trip : renamed) {
			if (trip.c == SheetField.Type.MULTIVALUED_MULTIPLE) {
				DBController.getInstance().getStatement().execute("DROP TABLE `" + SHEET_PREFIX + tmpTablePrefix + oldName + "::" + trip.a + "`");
			}
		}
		DatasheetVariant.deleteDynamicVariantTables(tmpTablePrefix + oldName);
		deleteSheetTable(tmpTablePrefix + oldName);
		DBController.getInstance().getConnection().commit();
	}

	public static void deleteSheetTables(Sheet sheet) throws SQLException {
		List<SheetField> fields = SheetField.getListOfFields(sheet);
		for (SheetField field : fields) {
			if (field.isMultivaluedAndMultiselectable()) {
				DBController.getInstance().getStatement().execute("DROP TABLE `" + SHEET_PREFIX + sheet.getName() + "::" + field.getName() + "`");
			}
		}
		DatasheetVariant.deleteDynamicVariantTables(sheet.getName());
		deleteSheetTable(sheet.getName());
	}

	private static void deleteSheetTable(String sheet) throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE `" + SHEET_PREFIX + sheet + "`");
	}

	private static void renameSheetTable(String oldName, String newName) throws SQLException {
		DBController.getInstance().getStatement().execute("ALTER TABLE `" + SHEET_PREFIX + oldName + "` RENAME TO `" + SHEET_PREFIX + newName + "`");
		DatasheetVariant.renameSheetTables(oldName, newName);
	}

	private static void renameMultivaluedTable(String oldName, String newName, String fieldName) throws SQLException {
		DBController.getInstance().getStatement().execute("ALTER TABLE `" + SHEET_PREFIX + oldName + "::" + fieldName + "` RENAME TO `"
				+ SHEET_PREFIX + newName + "::" + fieldName + "`");
	}

	public static void createSheetOnDB(Sheet sheet) throws SQLException {
		String stmt = "CREATE TABLE `" + SHEET_PREFIX + sheet.getName() + "`("
				+ " `idSheet` int primary key,"
				+ " `geographicPoint` int not null,"
				+ " `informant` int not null, ";
		List<String> tables = new ArrayList<>(10);
		for (SheetField sf : SheetField.getListOfFields(sheet)) {
			switch (sf.getType()) {
				case TEXT:
					stmt += "`" + sf.getName() + "` varchar(1000) not null default '', ";
					break;
				case DATE:
					stmt += "`" + sf.getName() + "` date not null default '2015-01-01', ";
					break;
				case INTEGER:
					stmt += "`" + sf.getName() + "` int not null default 0, ";
					break;
				case DOUBLE:
					stmt += "`" + sf.getName() + "` double not null default 0.0, ";
					break;
				case MULTIVALUED_UNIQUE: {
					stmt += "`" + sf.getName() + "` TEXT CHECK (`" + sf.getName() + "` IN(";
					for (MultivaluedData md : MultivaluedData.getListOfMultivaluedData(sf)) {
						stmt += "'" + md.getText() + "', ";
					}
					if (stmt.endsWith(", ")) {
						stmt = stmt.substring(0, stmt.length() - 2);
					}
					stmt += ")";
					stmt += ") not null default '" + MultivaluedData.getListOfMultivaluedData(sf).get(0).getText() + "', ";
					break;
				}
				case MULTIVALUED_MULTIPLE: {
					String stmt2 = "CREATE TABLE `" + SHEET_PREFIX + sheet.getName() + "::" + sf.getName() + "`("
							+ "`" + sheet.getName() + "` int not null,";
					stmt2 += " `" + sf.getName() + "` TEXT CHECK(`" + sf.getName() + "` IN(";
					for (MultivaluedData md : MultivaluedData.getListOfMultivaluedData(sf)) {
						stmt2 += "'" + md.getText() + "', ";
					}
					if (stmt2.endsWith(", ")) {
						stmt2 = stmt2.substring(0, stmt2.length() - 2);
					}
					stmt2 += ")";
					stmt2 += ") not null,"
							+ " FOREIGN KEY (`" + sheet.getName() + "`) REFERENCES `" + SHEET_PREFIX + sheet.getName() + "`(`idSheet`) ON UPDATE CASCADE ON DELETE CASCADE)";
					tables.add(stmt2);
					break;
				}
			}
		}
		stmt += "FOREIGN KEY (`geographicPoint`) REFERENCES GeographicPoint(idGeographicPoint) ON DELETE CASCADE ON UPDATE CASCADE, UNIQUE(`geographicPoint`, `informant`))";
		try {
			DBController.getInstance().getStatement().execute(stmt);
		} catch (SQLException ex) {
			DBController.getInstance().getStatement().execute("DELETE FROM Sheet WHERE idSheet = " + sheet.getId());
			throw ex;
		}
		try {
			for (String str : tables) {
				DBController.getInstance().getStatement().execute(str);
			}
		} catch (SQLException ex) {
			SheetField.getListOfFields(sheet).stream().filter((sf) -> sf.isMultivaluedAndMultiselectable()).forEach((sf) -> {
				try {
					DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `" + SHEET_PREFIX + sheet.getName() + "::" + sf.getName() + "`");
				} catch (SQLException ex_) {
				}
			});
			DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `" + SHEET_PREFIX + sheet.getName() + "`");
			DBController.getInstance().getStatement().execute("DELETE FROM Sheet WHERE idSheet = " + sheet.getId());
			throw ex;
		}
		DatasheetVariant.createDynamicVariantTables(sheet.getName());
	}

	public static void createTables() {
	}

	public static boolean verifyTables() {
		return true;
	}

	public static void deleteTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		tables.stream().filter((table)
				-> (table.toLowerCase(Locale.ENGLISH).startsWith(SHEET_PREFIX.toLowerCase(Locale.ENGLISH)))).forEach((table) -> {
			try {
				DBController.getInstance().getStatement().executeUpdate("DROP TABLE IF EXISTS `" + table + "`");
			} catch (SQLException ex) {
			}
		});
	}

	private int id;
	private String name;
	private int locality;
	private int informant;
	private final ConcurrentHashMap<String, Data> fields;

	public Datasheet() {
		fields = new ConcurrentHashMap<>(10);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLocality(GeographicPoint locality) {
		this.locality = locality.getId();
	}

	public void setInformant(int informant) {
		this.informant = informant;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public GeographicPoint getLocality() throws SQLException {
		return GeographicPoint.getGeoPoint(locality);
	}

	public int getInformant() {
		return informant;
	}

	public void addProperty(String name, SheetField.Type type, Object value) throws InvalidClassException {
		fields.put(name, new Data(type, value));
	}

	public Data getProperty(String name) {
		return fields.get(name);
	}

	public boolean haveData() {
		return !fields.isEmpty();
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 41 * hash + this.id;
		hash = 41 * hash + Objects.hashCode(this.name);
		hash = 41 * hash + this.locality;
		hash = 41 * hash + this.informant;
		hash = 41 * hash + Objects.hashCode(this.fields);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Datasheet other = (Datasheet) obj;
		return this.id == other.id
				&& this.locality == other.locality
				&& this.informant == other.informant
				&& Objects.equals(this.name, other.name)
				&& Objects.equals(this.fields, other.fields);
	}
}
