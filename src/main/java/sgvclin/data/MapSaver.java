/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.data;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import sgvclin.processors.DBController;
import sgvclin.processors.LoggerManager;
import sgvclin.processors.dataprocessor.Filter;
import sgvclin.processors.dataprocessor.VariantGroup;

/**
 * @author Nathan Oliveira
 */
public class MapSaver {

	@SuppressWarnings("fallthrough")
	public static void saveMapSaver(MapSaver ms) throws SQLException {
		{
			PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("INSERT INTO MapSaver(idMapSaver, type, map, sheet, question,"
					+ " title, radius, qttsVariants, title_hash) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
			pst.setInt(1, ms.getId());
			pst.setInt(2, ms.getType().ordinal());
			pst.setInt(3, ms.getMap().getId());
			pst.setInt(4, ms.getSheet().getId());
			pst.setInt(5, ms.getQuestions().get(0).getId());//FUTURE remove... It saves to the field in order to not break the tables...
			pst.setString(6, ms.getTitle());
			pst.setInt(7, ms.getRadius());
			pst.setInt(8, ms.getQttsVariants());
			pst.setString(9, DBController.getMD5(ms.getTitle()));
			pst.executeUpdate();
		}
		{
			PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("INSERT INTO QuestionList(mapSaver, question, `index`) VALUES (?, ?, ?)");
			int i = 0;
			for (Question q : ms.getQuestions()) {
				pst.setInt(1, ms.getId());
				pst.setInt(2, q.getId());
				pst.setInt(3, i);
				i++;
				pst.executeUpdate();
			}
		}
		switch (ms.getType()) {
			case AREALITY:
			case AREALITY_GRADUAL: {
				PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("INSERT INTO Selected(mapSaver, `index`, selected) VALUES (?, ?, ?)");
				boolean[] sel = ms.getSelected();
				for (int i = 0; i < sel.length; i++) {
					pst.setInt(1, ms.getId());
					pst.setInt(2, i);
					pst.setBoolean(3, sel[i]);
					pst.executeUpdate();
				}
			}
			 {
				PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("INSERT INTO Color(mapSaver, `index`,color, bg, pattern, size) VALUES (?, ?, ?, ?, ?, ?)");
				ColorPattern[] cs = ms.getColors();
				for (int i = 0; i < cs.length; i++) {
					pst.setInt(1, ms.getId());
					pst.setInt(2, i);
					pst.setInt(3, cs[i].getForeground().getRGB());
					pst.setInt(4, cs[i].getBackground().getRGB());
					pst.setInt(5, cs[i].getPattern().ordinal());
					pst.setInt(6, cs[i].getSize());
					pst.executeUpdate();
				}
			}
			break;
			case PIE_SELECTION:
			case HISTOGRAM_SELECTION: {
				PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("INSERT INTO Selected(mapSaver, `index`, selected) VALUES (?, ?, ?)");
				boolean[] sel = ms.getSelected();
				for (int i = 0; i < sel.length; i++) {
					pst.setInt(1, ms.getId());
					pst.setInt(2, i);
					pst.setBoolean(3, sel[i]);
					pst.executeUpdate();
				}
			}
			case PIE:
			case HISTOGRAM: {
				PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("INSERT INTO Color(mapSaver, `index`,color, bg, pattern, size) VALUES (?, ?, ?, ?, ?, ?)");
				ColorPattern[] cs = ms.getColors();
				for (int i = 0; i < cs.length; i++) {
					pst.setInt(1, ms.getId());
					pst.setInt(2, i);
					pst.setInt(3, cs[i].getForeground().getRGB());
					pst.setInt(4, cs[i].getBackground().getRGB());
					pst.setInt(5, cs[i].getPattern().ordinal());
					pst.setInt(6, cs[i].getSize());
					pst.executeUpdate();
				}
			}
			 {
				PreparedStatement pst = DBController.getInstance().getConnection().prepareStatement("INSERT INTO Delta(mapSaver, x, y, dx, dy) VALUES (?, ?, ? ,?, ?)");
				HashMap<Point2D.Double, Point2D.Double> deltas = ms.getDeltas();
				for (Point2D.Double p : deltas.keySet()) {
					pst.setInt(1, ms.getId());
					pst.setDouble(2, p.x);
					pst.setDouble(3, p.y);
					pst.setDouble(4, deltas.get(p).x);
					pst.setDouble(5, deltas.get(p).y);
					pst.executeUpdate();
				}
			}
			break;
			default:
				throw new AssertionError(ms.getType().name());
		}
		for (VariantGroup vg : ms.getVariantGroups()) {
			int id = DBController.getInstance().getNewPrimaryKeyFor("VariantGroup");
			PreparedStatement pstVG = DBController.getInstance().getConnection().prepareStatement("INSERT INTO VariantGroup(idVariantGroup, mapSaver,"
					+ " `index`, `representation`) VALUES(?, ?, ?, ?)");
			pstVG.setInt(1, id);
			pstVG.setInt(2, ms.getId());
			pstVG.setInt(3, ms.getVariantGroups().indexOf(vg));
			pstVG.setString(4, vg.getRepresentation());
			pstVG.executeUpdate();
			PreparedStatement pstVL = DBController.getInstance().getConnection().prepareStatement("INSERT INTO VariantList(variantGroup, variant, `index`) VALUES (?, ?, ?)");
			for (Variant v : vg.getVariants()) {
				pstVL.setInt(1, id);
				pstVL.setInt(2, v.getId());
				pstVL.setInt(3, vg.getVariants().indexOf(v));
				pstVL.executeUpdate();
			}
		}
		if (ms.getFilters() != null) {
			for (Filter f : ms.getFilters()) {
				int idFilter = DBController.getInstance().getNewPrimaryKeyFor("Filter");
				PreparedStatement pstF = DBController.getInstance().getConnection().prepareStatement("INSERT INTO `Filter`(`idFilter`, `mapSaver`, `index`)"
						+ " VALUES(?, ?, ?)");
				pstF.setInt(1, idFilter);
				pstF.setInt(2, ms.getId());
				pstF.setInt(3, ms.getFilters().indexOf(f));
				pstF.executeUpdate();
				for (SheetField sf : f.getOrdenation()) {
					int idFilterSheetField = DBController.getInstance().getNewPrimaryKeyFor("FilterSheetField");
					PreparedStatement pstFSF = DBController.getInstance().getConnection().prepareStatement("INSERT INTO `FilterSheetField`(`idFilterSheetField`, `filter`,"
							+ " `index`, `sheetField`) VALUES (?, ?, ?, ?)");
					pstFSF.setInt(1, idFilterSheetField);
					pstFSF.setInt(2, idFilter);
					pstFSF.setInt(3, f.getOrdenation().indexOf(sf));
					pstFSF.setInt(4, sf.getId());
					pstFSF.executeUpdate();
					for (Object obj : f.getDetails(sf)) {
						PreparedStatement pstFSFL = DBController.getInstance().getConnection().prepareStatement("INSERT INTO `FilterSheetFieldList`("
								+ "`filterSheetField`, `index`, `object`) VALUES (?, ?, ?)");
						pstFSFL.setInt(1, idFilterSheetField);
						pstFSFL.setInt(2, f.getDetails(sf).indexOf(obj));
						String str = null;
						if (obj instanceof String) {
							str = "STRING:::" + obj;
						} else if (obj instanceof MultivaluedData) {
							str = "MULTIVALUEDDATA:::" + ((MultivaluedData) obj).getText();
						} else if (obj instanceof AgeRange) {
							str = "AGERANGE:::" + ((AgeRange) obj).getAgeGroups().getName() + ":::" + ((AgeRange) obj).getName();
						} else if (obj instanceof Date) {
							str = "DATE:::" + new SimpleDateFormat(Data.DATE_PATTERN, Locale.getDefault()).format((Date) obj);
						} else if (obj instanceof Integer) {
							str = "INTEGER:::" + Integer.toString((int) obj);
						} else if (obj instanceof Double) {
							str = "DOUBLE:::" + Double.toString((double) obj);
						}
						if (str != null) {
							pstFSFL.setString(3, str);
							pstFSFL.executeUpdate();
						}
					}
				}
			}
		}
	}

	@SuppressWarnings("fallthrough")
	public static MapSaver getMapSaver(int id) throws SQLException {
		MapSaver ms;
		try (ResultSet rs = DBController.getInstance().getResultSet("MapSaver", id)) {
			if (!rs.next()) {
				return null;
			}
			ms = new MapSaver();
			ms.setId(id);
			try {
				ms.setType(MapSaver.Type.values()[rs.getInt("type")]);
			} catch (IllegalArgumentException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, "WTF: MapSaver tem valor errado de classe...", ex);
				return null;
			}
			try {
				ms.setMap(Map.getMap(rs.getInt("map")));
			} catch (ClassNotFoundException | IOException | NullPointerException ex) {
				LoggerManager.getInstance().log(Level.SEVERE, null, ex);
				return null;
			}
			ms.setSheet(Sheet.getSheet(rs.getInt("sheet")));
			List<Question> questions = new ArrayList<>(1);
			try (ResultSet qrs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM QuestionList WHERE mapSaver = " + ms.getId())) {
				ConcurrentHashMap<Integer, Question> map = new ConcurrentHashMap<>(5);
				while (qrs.next()) {
					map.put(qrs.getInt("index"), Question.getQuestion(qrs.getInt("question")));
				}
				for (int i = 0; i < map.size(); i++) {
					questions.add(map.get(i));
				}
			}
			if (questions.isEmpty()) {
				questions.add(Question.getQuestion(rs.getInt("question")));
			}
			ms.setQuestions(questions);
			ms.setTitle(rs.getString("title"));
			ms.setRadius(rs.getInt("radius"));
			ms.setQttsVariants(rs.getInt("qttsVariants"));
		}
		switch (ms.getType()) {
			case AREALITY:
			case AREALITY_GRADUAL:
				try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM Selected WHERE mapSaver = " + ms.getId())) {
					ConcurrentHashMap<Integer, Boolean> bools = new ConcurrentHashMap<>(20);
					while (rs.next()) {
						bools.put(rs.getInt("index"), rs.getBoolean("selected"));
					}
					boolean[] sels = new boolean[bools.size()];
					for (int i = 0; i < bools.size(); i++) {
						sels[i] = bools.get(i);
					}
					ms.setSelected(sels);
				}
				try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM Color WHERE mapSaver = " + ms.getId())) {
					ConcurrentHashMap<Integer, ColorPattern> colrs = new ConcurrentHashMap<>(20);
					while (rs.next()) {
						colrs.put(rs.getInt("index"), new ColorPattern(new Color(rs.getInt("color"), true), new Color(rs.getInt("bg"), true), ColorPattern.Pattern.values()[rs.getInt("pattern")], rs.getInt("size")));
					}
					ColorPattern[] colors = new ColorPattern[colrs.size()];
					for (int i = 0; i < colors.length; i++) {
						colors[i] = colrs.get(i);
					}
					ms.setColors(colors);
				}
				break;
			case PIE_SELECTION:
			case HISTOGRAM_SELECTION:
				try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM Selected WHERE mapSaver = " + ms.getId())) {
					ConcurrentHashMap<Integer, Boolean> bools = new ConcurrentHashMap<>(20);
					while (rs.next()) {
						bools.put(rs.getInt("index"), rs.getBoolean("selected"));
					}
					boolean[] sels = new boolean[bools.size()];
					for (int i = 0; i < bools.size(); i++) {
						sels[i] = bools.get(i);
					}
					ms.setSelected(sels);
				}
			case PIE:
			case HISTOGRAM:
				try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM Color WHERE mapSaver = " + ms.getId())) {
					ConcurrentHashMap<Integer, ColorPattern> colrs = new ConcurrentHashMap<>(20);
					while (rs.next()) {
						colrs.put(rs.getInt("index"), new ColorPattern(new Color(rs.getInt("color"), true), new Color(rs.getInt("bg"), true), ColorPattern.Pattern.values()[rs.getInt("pattern")], rs.getInt("size")));
					}
					ColorPattern[] colors = new ColorPattern[colrs.size()];
					for (int i = 0; i < colors.length; i++) {
						colors[i] = colrs.get(i);
					}
					ms.setColors(colors);
				}
				try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM Delta WHERE mapSaver = " + ms.getId())) {
					HashMap<Point2D.Double, Point2D.Double> deltas = new HashMap<>(50);
					while (rs.next()) {
						deltas.put(new Point2D.Double(rs.getDouble("x"), rs.getDouble("y")), new Point2D.Double(rs.getDouble("dx"), rs.getDouble("dy")));
					}
					ms.setDeltas(deltas);
				}
				break;
			default:
				throw new AssertionError(ms.getType().name());
		}
		List<VariantGroup> groups = new ArrayList<>(5);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM VariantGroup WHERE mapSaver = " + ms.getId() + " ORDER BY `index`")) {
			while (rs.next()) {
				try (ResultSet rs2 = DBController.getInstance().getStatement().executeQuery("SELECT * FROM VariantList WHERE variantGroup = "
						+ rs.getInt("idVariantGroup") + " ORDER BY `index`")) {
					List<Variant> list = new ArrayList<>(5);
					while (rs2.next()) {
						list.add(Variant.getVariant(rs2.getInt("variant")));
					}
					groups.add(new VariantGroup(list, rs.getString("representation")));
				}
			}
		}
		ms.setVariantGroups(groups);
		List<Filter> filters = new ArrayList<>(5);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT * FROM Filter WHERE mapSaver = " + ms.getId() + " ORDER BY `index`")) {
			while (rs.next()) {
				Filter f = new Filter();
				try (ResultSet rs2 = DBController.getInstance().getStatement().executeQuery("SELECT * FROM FilterSheetField WHERE filter = " + rs.getInt("idFilter")
						+ " ORDER BY `index`")) {
					while (rs2.next()) {
						SheetField sf = SheetField.getSheetField(rs2.getInt("sheetField"));
						try (ResultSet rs3 = DBController.getInstance().getStatement().executeQuery("SELECT * FROM FilterSheetFieldList WHERE "
								+ "filterSheetField = " + rs2.getInt("idFilterSheetField") + " ORDER BY `index`")) {
							while (rs3.next()) {
								String str = rs3.getString("object");
								String[] tokens = str.split(":::");
								String data = null;
								AgeGroup group = null;
								switch (tokens[0]) {
									case "STRING":
									case "MULTIVALUEDDATA":
									case "DATE":
									case "INTEGER":
									case "DOUBLE":
										data = tokens[1];
										break;
									case "AGERANGE":
										group = AgeGroup.getAgeGroup(tokens[1]);
										data = tokens[2];
										break;
									default:
										throw new AssertionError(tokens[0]);
								}
								if (data != null) {
									f.addField(sf, data, group);
								}
							}
						}
					}
				}
				filters.add(f);
			}
		}
		ms.setFilters(filters);
		return ms;
	}

	static void deleteMapSaversFrom(Map m) throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("DELETE FROM MapSaver WHERE map = " + m.getId());
	}

	public static MapSaver getMapSaver(String title) throws SQLException {
		int k;
		try {
			try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idMapSaver, title FROM MapSaver WHERE title = '" + title + "'")) {
				if (!rs.next()) {
					LoggerManager.getInstance().log(Level.SEVERE, "MapSaver title not found.", null);
					throw new NoSuchFieldException("Does not have the map.");
				}
				k = rs.getInt("idMapSaver");
			}
		} catch (NoSuchFieldException ex) {
			return null;
		}
		return getMapSaver(k);
	}

	public static void deleteMapSaver(MapSaver ms) throws SQLException {
		if (ms == null) {
			return;
		}
		DBController.getInstance().getStatement().executeUpdate("DELETE FROM MapSaver WHERE idMapSaver = " + ms.getId());
	}

	public static int getNewMapSaverId() throws SQLException {
		return DBController.getInstance().getNewPrimaryKeyFor("MapSaver");
	}

	public static MapSaver[] getMapSavers() throws SQLException {
		List<MapSaver> list = new ArrayList<>(50);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idMapSaver FROM MapSaver")) {
			while (rs.next()) {
				list.add(getMapSaver(rs.getInt("idMapSaver")));
			}
		}
		return list.toArray(new MapSaver[list.size()]);
	}

	public static MapSaver[] getDiatopicMapSavers() throws SQLException {
		List<MapSaver> list = new ArrayList<>(50);
		try (ResultSet rs = DBController.getInstance().getStatement().executeQuery("SELECT idMapSaver FROM MapSaver WHERE type BETWEEN 2 AND 3")) {
			while (rs.next()) {
				list.add(getMapSaver(rs.getInt("idMapSaver")));
			}
		}
		return list.toArray(new MapSaver[list.size()]);
	}

	public static void createTables() throws SQLException {
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `MapSaver` ("
				+ " `idMapSaver` int(11) NOT NULL,"
				+ " `type` int(11) NOT NULL,"
				+ " `map` int(11) NOT NULL,"
				+ " `sheet` int(11) NOT NULL,"
				+ " `question` int(11) NOT NULL," ///FUTURE this field should be deleted, in favour of the QuestionList table, but I did not delete it yet because of the risks involved in doing so on a SQLite base
				+ " `title` varchar(10000) NOT NULL,"
				+ " `title_hash` char(32) NOT NULL,"
				+ " `radius` int(11) NOT NULL,"
				+ " `qttsVariants` int(11) NOT NULL,"
				+ " PRIMARY KEY (`idMapSaver`),"
				+ " UNIQUE (`title_hash`),"
				+ " CONSTRAINT `MapSaver_ibfk_1` FOREIGN KEY (`map`) REFERENCES `Map` (`idMap`) ON UPDATE CASCADE ON DELETE CASCADE,"
				+ " CONSTRAINT `MapSaver_ibfk_2` FOREIGN KEY (`sheet`) REFERENCES `Sheet` (`idSheet`) ON UPDATE CASCADE ON DELETE CASCADE,"
				+ " CONSTRAINT `MapSaver_ibfk_3` FOREIGN KEY (`question`) REFERENCES `Question` (`idQuestion`) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `Selected` ("
				+ " `mapSaver` int(11) NOT NULL,"
				+ " `index` int(11) NOT NULL,"
				+ " `selected` bool NOT NULL,"
				+ " PRIMARY KEY (`mapSaver`, `index`),"
				+ " CONSTRAINT `Selected_ibfk_1` FOREIGN KEY (`mapSaver`) REFERENCES `MapSaver` (`idMapSaver`) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `Color` ("
				+ " `mapSaver` int(11) NOT NULL,"
				+ " `index` int(11) NOT NULL,"
				+ " `color` int(11) NOT NULL,"
				+ " `bg` int(11) NOT NULL,"
				+ " `pattern` int(11) NOT NULL,"
				+ " `size` int(11) NOT NULL,"
				+ " PRIMARY KEY (`mapSaver`, `index`),"
				+ " CONSTRAINT `Color_ibfk_1` FOREIGN KEY (`mapSaver`) REFERENCES `MapSaver` (`idMapSaver`) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `Delta` ("
				+ " `mapSaver` int(11) NOT NULL,"
				+ " `x` double NOT NULL,"
				+ " `y` double NOT NULL,"
				+ " `dx` double NOT NULL,"
				+ " `dy` double NOT NULL,"
				+ " CONSTRAINT `Delta_ibfk_1` FOREIGN KEY (`mapSaver`) REFERENCES `MapSaver` (`idMapSaver`) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `VariantGroup` ("
				+ " `idVariantGroup` int(11) NOT NULL,"
				+ " `mapSaver` int(11) NOT NULL,"
				+ " `index` int(11) NOT NULL,"
				+ " `representation` varchar(500) NOT NULL,"
				+ " PRIMARY KEY (`idVariantGroup`),"
				+ " CONSTRAINT `VariantGroup_ibfk_1` FOREIGN KEY (`mapSaver`) REFERENCES `MapSaver` (`idMapSaver`) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `VariantList` ("
				+ " `variantGroup` int(11) NOT NULL,"
				+ " `variant` int(11) NOT NULL,"
				+ " `index` int(11) NOT NULL,"
				+ " PRIMARY KEY (`variantGroup`, `variant`),"
				+ " CONSTRAINT `VariantList_ibfk_1` FOREIGN KEY (`variantGroup`) REFERENCES `VariantGroup` (`idVariantGroup`) ON UPDATE CASCADE ON DELETE CASCADE,"
				+ " CONSTRAINT `VariantList_ibfk_2` FOREIGN KEY (`variant`) REFERENCES `Variant` (`idVariant`) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `Filter` ("
				+ " `idFilter` int(11) NOT NULL,"
				+ " `mapSaver` int(11) NOT NULL,"
				+ " `index` int(11) NOT NULL,"
				+ " PRIMARY KEY (`idFilter`),"
				+ " CONSTRAINT `Filter_ibfk_1` FOREIGN KEY (`mapSaver`) REFERENCES `MapSaver` (`idMapSaver`) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `FilterSheetField` ("
				+ " `idFilterSheetField` int(11) NOT NULL,"
				+ " `filter` int(11) NOT NULL,"
				+ " `index` int(11) NOT NULL,"
				+ " `sheetField` int(11) NOT NULL,"
				+ " PRIMARY KEY (`idFilterSheetField`),"
				+ " CONSTRAINT `FilterSheetField_ibfk_1` FOREIGN KEY (`filter`) REFERENCES `Filter` (`idFilter`) ON UPDATE CASCADE ON DELETE CASCADE,"
				+ " CONSTRAINT `FilterSheetField_ibfk_2` FOREIGN KEY (`sheetField`) REFERENCES `SheetField` (`idSheetField`) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `FilterSheetFieldList` ("
				+ " `filterSheetField` int(11) NOT NULL,"
				+ " `object` varchar(1000) NOT NULL,"
				+ " `index` int(11) NOT NULL,"
				+ " CONSTRAINT `FilterSheetFieldList_ibfk_1` FOREIGN KEY (`filterSheetField`) REFERENCES `FilterSheetField` (`idFilterSheetField`)"
				+ " ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
		DBController.getInstance().getStatement().executeUpdate("CREATE TABLE `QuestionList` ("
				+ " `mapSaver` int(11) NOT NULL,"
				+ " `question` int(11) NOT NULL,"
				+ " `index` int(11) NOT NULL,"
				+ " PRIMARY KEY (`mapSaver`, `question`),"
				+ " CONSTRAINT `QuestionList_ibfk_1` FOREIGN KEY (`mapSaver`) REFERENCES `MapSaver` (`idMapSaver`) ON UPDATE CASCADE ON DELETE CASCADE,"
				+ " CONSTRAINT `QuestionList_ibfk_2` FOREIGN KEY (`question`) REFERENCES `Question` (`idQuestion`) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ")");
	}

	public static boolean verifyTables() throws SQLException {
		DatabaseMetaData dbmd = DBController.getInstance().getConnection().getMetaData();
		List<String> tables = new ArrayList<>(15);
		try (ResultSet rs = dbmd.getTables(DBController.getInstance().getConnection().getCatalog(), null, "%", null)) {
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		}
		return tables.stream().anyMatch((table) -> table.equalsIgnoreCase("QuestionList"))
				&& tables.stream().anyMatch((table) -> table.equalsIgnoreCase("filterSheetFieldList"))
				&& tables.stream().anyMatch((table) -> table.equalsIgnoreCase("FilterSheetField"))
				&& tables.stream().anyMatch((table) -> table.equalsIgnoreCase("Filter"))
				&& tables.stream().anyMatch((table) -> table.equalsIgnoreCase("VariantList"))
				&& tables.stream().anyMatch((table) -> table.equalsIgnoreCase("VariantGroup"))
				&& tables.stream().anyMatch((table) -> table.equalsIgnoreCase("Delta"))
				&& tables.stream().anyMatch((table) -> table.equalsIgnoreCase("Color"))
				&& tables.stream().anyMatch((table) -> table.equalsIgnoreCase("Selected"))
				&& tables.stream().anyMatch((table) -> table.equalsIgnoreCase("MapSaver"));
	}

	public static void deleteTables() throws SQLException {
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `QuestionList`");
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `FilterSheetFieldList`");
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `FilterSheetField`");
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `Filter`");
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `VariantList`");
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `VariantGroup`");
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `Delta`");
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `Color`");
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `Selected`");
		DBController.getInstance().getStatement().execute("DROP TABLE IF EXISTS `MapSaver`");
	}

	private int id;
	private Type type;
	private Map map;
	private Sheet sheet;
	private List<Question> questions;
	private String title;
	private List<Filter> filters;
	private List<VariantGroup> variantGroups;
	private boolean[] selected;
	private ColorPattern[] colors;
	private HashMap<Point2D.Double, Point2D.Double> deltas;
	private int radius;
	private int qttsVariants;

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setType(Type type) throws IllegalArgumentException {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public Map getMap() {
		return map;
	}

	public void setSheet(Sheet sheet) {
		this.sheet = sheet;
	}

	public Sheet getSheet() {
		return sheet;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = new ArrayList<>(questions);
	}

	public List<Question> getQuestions() {
		return Collections.unmodifiableList(questions);
	}

	public HashMap<Point2D.Double, Point2D.Double> getDeltas() {
		return new HashMap<>(deltas);
	}

	public List<VariantGroup> getVariantGroups() {
		List<VariantGroup> list = new ArrayList<>(variantGroups.size());
		variantGroups.stream().forEach((vg) -> list.add(new VariantGroup(vg)));
		return list;
	}

	public List<Filter> getFilters() {
		if (filters == null) {
			return null;
		}
		List<Filter> list = new ArrayList<>(filters.size());
		filters.stream().forEach((filter) -> list.add(new Filter(filter)));
		return list;
	}

	public int getQttsVariants() {
		return qttsVariants;
	}

	public int getRadius() {
		return radius;
	}

	public boolean[] getSelected() {
		if (selected == null) {
			return null;
		}
		return Arrays.copyOf(selected, selected.length);
	}

	public ColorPattern[] getColors() {
		if (colors == null) {
			return null;
		}
		return Arrays.copyOf(colors, colors.length);
	}

	public String getTitle() {
		return title;
	}

	public void setSelected(boolean[] selected) {
		if (selected == null) {
			this.selected = null;
		} else {
			this.selected = Arrays.copyOf(selected, selected.length);
		}
	}

	public void setColors(ColorPattern[] colors) {
		if (colors == null) {
			this.colors = null;
		} else {
			this.colors = Arrays.copyOf(colors, colors.length);
		}
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public void setQttsVariants(int qttsVariants) {
		this.qttsVariants = qttsVariants;
	}

	public void setFilters(List<Filter> fs) {
		filters = new ArrayList<>(fs.size());
		fs.stream().forEach((filter) -> filters.add(new Filter(filter)));
	}

	public void setVariantGroups(List<VariantGroup> vgs) {
		variantGroups = new ArrayList<>(vgs.size());
		vgs.stream().forEach((vg) -> variantGroups.add(new VariantGroup(vg)));
	}

	public void setDeltas(HashMap<Point2D.Double, Point2D.Double> deltas) {
		this.deltas = new HashMap<>(deltas);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return title;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 53 * hash + this.id;
		hash = 53 * hash + Objects.hashCode(this.type);
		hash = 53 * hash + Objects.hashCode(this.map);
		hash = 53 * hash + Objects.hashCode(this.sheet);
		hash = 53 * hash + Objects.hashCode(this.questions);
		hash = 53 * hash + Objects.hashCode(this.title);
		hash = 53 * hash + Objects.hashCode(this.filters);
		hash = 53 * hash + Objects.hashCode(this.variantGroups);
		hash = 53 * hash + Arrays.hashCode(this.selected);
		hash = 53 * hash + Arrays.deepHashCode(this.colors);
		hash = 53 * hash + Objects.hashCode(this.deltas);
		hash = 53 * hash + this.radius;
		hash = 53 * hash + this.qttsVariants;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final MapSaver other = (MapSaver) obj;
		return this.id == other.id
				&& this.radius == other.radius
				&& this.qttsVariants == other.qttsVariants
				&& Objects.equals(this.title, other.title)
				&& this.type == other.type
				&& Objects.equals(this.map, other.map)
				&& Objects.equals(this.sheet, other.sheet)
				&& Objects.equals(this.questions, other.questions)
				&& Objects.equals(this.filters, other.filters)
				&& Objects.equals(this.variantGroups, other.variantGroups)
				&& Arrays.equals(this.selected, other.selected)
				&& Arrays.deepEquals(this.colors, other.colors)
				&& Objects.equals(this.deltas, other.deltas);
	}

	@SuppressWarnings("PublicInnerClass")
	public enum Type {

		AREALITY,
		AREALITY_GRADUAL,
		PIE,
		PIE_SELECTION,
		HISTOGRAM,
		HISTOGRAM_SELECTION
	}
}
