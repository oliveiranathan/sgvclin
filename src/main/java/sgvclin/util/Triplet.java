/*
 * Copyright 2014 Rodrigo Duarte Seabra, Valter Pereira Romano, Nathan Oliveira
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sgvclin.util;

import java.util.Objects;

/**
 *
 * @author Nathan Oliveira
 * @param <T>
 * @param <U>
 * @param <V>
 */
public class Triplet<T, U, V> extends Pair<T, U> {

	public final V c;

	public Triplet(T a, U b, V c) {
		super(a, b);
		this.c = c;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 89 * hash + Objects.hashCode(this.a);
		hash = 89 * hash + Objects.hashCode(this.b);
		hash = 89 * hash + Objects.hashCode(this.c);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Triplet<?, ?, ?> other = (Triplet<?, ?, ?>) obj;
		if (!Objects.equals(this.a, other.a)) {
			return false;
		}
		if (!Objects.equals(this.b, other.b)) {
			return false;
		}
		return Objects.equals(this.c, other.c);
	}

	@Override
	public String toString() {
		return "(" + a.toString() + ", " + b.toString() + ", " + c.toString() + ")";
	}
}
