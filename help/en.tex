\documentclass[12pt,a4paper]{book}
\raggedbottom
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{indentfirst}
\usepackage{graphicx} 
\author{Rodrigo Duarte Seabra\\Valter Pereira Romano\\Nathan Oliveira}
\title{Operational Manual - SGVCLin}
\setcounter{secnumdepth}{-2}
\setcounter{tocdepth}{5}
\makeindex
\begin{document}
\frontmatter
\maketitle
\tableofcontents
\newpage
\chapter{Introduction}
This document presents the available functionalities in [SGVCLin] – Software for Generation and Visualization of Linguistic Maps. The software's goal is to facilitate the query process to linguistic data transcript and stored in a general purpose database which can be used in any project. Thus the results obtained from the software can be consulted by various report types and by the creation of linguistic productivity maps and areality maps.

The main motivation for the development of the software was the creation of an environment in which the linguist professional can have freedom in using the tool. However, given the fact that geolinguistics have both linguistics and geography as its basis, the user has to, before using the software, obtain the maps to be used in the software from a cartographer or cartographical base. This map has to have any element the linguist may need, such as graphical scale, political division and the location of the linguistic points. After getting the map in image format (the software accepts most common formats, including PNG, JPEG and BMP), the linguist then uploads it to the software and can then represent various linguistic facts (phonetics, lexical, morphosyntactic).

\mainmatter
\chapter{Installing and configuring the software}
To obtain the SGVCLin installer, access the site: http://sgvclin.altervista.org/

To install the software, just execute the installer and follow the steps to end the installation. The installer will install and configure SGVCLin and all its dependencies.

\chapter{Using the Software}

\section{Home screen}
\noindent\includegraphics[scale=0.5]{images/tela_principal.png}

The [SGVCLin] home screen has four important fields:

\subsection{Workspace}
It's the center portion of the software. It is where every window and dialog from the software appears. When we open the software, it shows only the background image.

\subsection{Status bar}
Displays relevant information during the software operation. It's located on the bottom of the screen.

\subsection{Connection monitor}
It's the small red box on the lower right. The monitor is green when the database connection is open and red when it fails.

\subsection{Main menu}
Located on the top, it contains all actions that the user can do. Every operation will be described hereafter.

\section{Main menu}
It has five menus with several submenus. Some options are only available after establishing the database connection. Hereafter, each menu is listed, and its functionalities described and explained.

\subsection{File}
\noindent\includegraphics[scale=1]{images/menu_arquivo.png}

\subsubsection{Clear database}
Clear the database, deleting all data in it. Used to start from scratch. If there is data on the database, the software will warn the user to export the data first.

\subsubsection{Export}
Exports the data in the database to a file selected by the user. Can be used as a backup, as means of transferring work from one computer to another or to save and then change the dataset being used in the software. The data are saved in a file with the '.sgvclin' file type. It can only be opened with this software.

\subsubsection{Import}
Read a file exported by [SGVCLin] and puts its contents on the database. This process deletes the data in the database first. If there is data on the database, the software will warn the user to export it first.

\subsubsection{Print}
After the creation of a map, this is where we generate the images to print. When we click on it, if there is more than one map created on the software, the user can choose between them and, after choosing the map, a dialogue opens to select the file to save the map. The print process creates a PDF file that can then be used to copy the image, send it via email or printed using the system's PDF reader.

\subsubsection{Quit}
Closes the software. Since we are using a database, there is no need to save before closing.

\subsection{Create}
\noindent\includegraphics[scale=1]{images/menu_criar.png}

\subsubsection{Questionnaires}
In this window, we insert the questionnaires subcategories.

\noindent\includegraphics[scale=1]{images/criar_questionarios.png}

To add a subcategory, select the category which it is derived, type the subcategory name in the text field and click in "Add subcategory". Please note that this button is only activated after selecting the category.

\subsubsection{Questions and Variants}
In this window, we create question groups, its questions and the variants of the questions answers.

\noindent\includegraphics[scale=1]{images/criar_questoes.png}

To create a question group, use the left panel, enter with the group name and click in "Add". The group will be created and the questions panel will receive the questions of the created group. To insert or alter questions of other group, simply select it on the left list.

To insert a question, fill in the fields on the right panel. Enter with the question number and its description and select the question category on the drop down menu. These categories are the ones created on the "Questionnaires" window.

To insert a variant, enter with its name in the correspondent field and click in \includegraphics[scale=1]{images/adicionar.png}. To remove a variant, select it on the list and click in \includegraphics[scale=1]{images/nao.png}.

To type a phonetic variant, you can use the virtual keyboard (click in \includegraphics[scale=1]{images/teclado.png}). A window with the phonetic keyboard will be displayed. Click on the button corresponding to a phoneme to insert it on the variant text field.

\noindent\includegraphics[scale=0.9]{images/virtual_keyboard.png}

To understand how to use the navigation menu in the bottom of the right panel, see the navigation menu entry in the appendix.

\subsubsection{Maps}
In this window, we insert the map image for the software to work with. The map must be an image file in one of the following formats: PNG, BMP or JPEG. Other formats may be read, but are not supported.

\noindent\includegraphics[scale=0.8]{images/criar_mapa_v2.png}

To create a new map, click in "Insert new". A dialogue will open to select the image file, and, once selected, a new one will ask for the map name. To edit the data of a map already in the software, click on "Load existing" and a dialogue will ask the name of the map to edit.

"Edit map" is used to select the area to be coloured on the areality maps. For more information, check this item entry hereafter.

In this window, it is necessary to input to the program which and where the linguistic points on the map are. This is done by typing the Point number, Locality and State in the fields on the left panel and then clicking on the point in the map image. The arrows in the "Point number" field can be used to increase and decrease the point number and, if this point was already entered, it will display its Locality and State on the respective fields.

If you need to alter the Locality name or the State, use the point grid editor. If you need to change the point on the image, enter with the point number and click on the new position in the map. The last position will be discarded in the process.

\subsubsection{Edit Map}
\noindent\includegraphics[scale=0.8]{images/criar_template.png}

In this window, we create a template of the area that will be coloured in the areality maps. The map is shown as a guide to the process, but is not changed by it. The window has a central panel where we edit the map and controls at the bottom with tools that will enable us to create the template. The goal of the process is to define the area that will be used, painting it in red. For that we have the following tools:

\begin{tabular}{|c|c|c|}
\hline 
\textbf{Icon} & \textbf{Name} & \textbf{Description} \\ 
\hline 
\includegraphics[scale=1]{images/bordas.png}  & \parbox[t]{2cm}{Detect borders} & \parbox[t]{11.5cm}{Detects the image borders, so that the user does not need to do it by hand. Note that in some images it may not work or even make the process more difficult.} \\ 
\hline 
\includegraphics[scale=1]{images/lapis.png}  & \parbox[t]{2cm}{Pencil} & \parbox[t]{11.5cm}{Draws a free-form line on the image. To use it, simply click and drag in the image to draw.} \\ 
\hline 
\includegraphics[scale=1]{images/borracha.png}  & \parbox[t]{2cm}{Eraser} & \parbox[t]{11.5cm}{Erases a free-form line on the image. To use it, simply click and drag in the image to erase.} \\ 
\hline 
\includegraphics[scale=1]{images/linha.png}  & \parbox[t]{2cm}{Line} & \parbox[t]{11.5cm}{Draws a line segment in the image. To use it, press the mouse button on the start of the line, move the mouse to the end of the line and release.} \\ 
\hline 
\includegraphics[scale=1]{images/balde.png}  & \parbox[t]{2cm}{Bucket} & \parbox[t]{11.5cm}{Fills a closed area of the image. To use it simply click on the area to be filled. If there is a "hole" in the border, it will fill more than intended. In this case, undo the fill, find and close the "hole" with the pencil or line tool and try again.} \\ 
\hline 
\includegraphics[scale=1]{images/inverter.png}  & \parbox[t]{2cm}{Invert} & \parbox[t]{11.5cm}{Changes all points so that the selected becomes unselected and vice-versa.} \\ 
\hline 
\includegraphics[scale=1]{images/desfazer.png}  & \parbox[t]{2cm}{Undo} & \parbox[t]{11.5cm}{Undo the last action made. Note that it can only undo one last action.} \\ 
\hline 
\end{tabular}

\subsubsection{Edit point grid}
\noindent\includegraphics[scale=1]{images/criar_editar_pontos.png}

In this window, we can change the Locality and State values of a point, if needed.

Just select the point, make the changes and save it with \includegraphics[scale=1]{images/salvar.png}.

To understand how to use the navigation menu in the bottom of the right panel, see the navigation menu entry in the appendix.

\subsubsection{Data sheets}
\noindent\includegraphics[scale=0.8]{images/criar_ficha.png} 

In this window, we can create the informants data sheets. These data sheets will contain information about them such as age and sex, including anything the user may need. After creating the sheets here, we will fill the data in another window.

We must enter with the sheet name to identify it. Then we can add new fields if needed. Note that some common fields are already created and cannot be removed.

The fields can have the following data types:

\begin{tabular}{|c|c|}
\hline
\textbf{Type} & \textbf{Description} \\
\hline 
Text & \parbox[t]{11.5cm}{The field will receive data as text; "name" and "description" are possible examples.} \\ 
\hline 
Date & \parbox[t]{11.5cm}{The field will receive data as date; "birth date" is an example.} \\ 
\hline 
Integer number & \parbox[t]{11.5cm}{The field will receive data as integer numbers; "age" is an example. Number example: 18} \\ 
\hline 
Real number & \parbox[t]{11.5cm}{The field will receive data as real numbers; "height" is an example. Number example: 3.14} \\ 
\hline 
Selection & \parbox[t]{11.5cm}{The field will receive data selected between the values entered here. The data can be of single selection or multiple selections. To enter with the options, type it on the "Item name" field and click in "Add". To remove one of the values, click on it on the list and click "Delete".} \\ 
\hline 
\end{tabular}
\\\\
After having created all the fields, click on "Create data sheet". This will create the data sheet, and, after creating, will automatically close this window and open the window where we enter with the informants data.

\subsection{Register}
\noindent\includegraphics[scale=1]{images/menu_cadastrar.png} 

\subsubsection{Informant data sheet}
\noindent\includegraphics[scale=0.8]{images/cadastrar_ficha.png} 

In this window, we insert in the software the informant data, to be able to do queries like variants per sex or age, for example. Here we just fill in each field and save each informant data.

To understand how to use the navigation menu in the bottom of the right panel, see the navigation menu entry in the appendix.

\subsubsection{Answers}
\noindent\includegraphics[scale=0.8]{images/cadastrar_respostas.png}

In this window, we insert the data of each question for all informants. Before opening it, we select the map that contains the points we'll be working with and the question group we'll be selecting the answers.

The window has a question selector at the top, where we select the question we are current editing, and a button "Add answer" that adds another answer column, if needed.

The columns "POINT", "STATE", "LOCALITY" and "INFORMANT" aren't editable. To alter the data on these columns, we have to alter them in the previous menus. The columns "\#A ANSWER" represents the variants that the informant answered the question and can only be filled with one of the values entered in the question creation. If necessary to add some answer, go back to the questions menu. To fill in the answers, just click on the cell that you want to fill and select the answer on the list that appears. To remove an answer, select the cell and press Delete. The columns NK and TP can be selected to indicate that the informant did Not Know or there was a Technical Problem that prevented the answers to be available. Note that if one of them are selected, it will not be possible to fill in the answers for that line and, when selected, all answers that may be on the line will be erased. The column "OBSERVATIONS" can be used to add short explanations about the answers.

\subsection{Query}
\noindent\includegraphics[scale=1]{images/menu_consultar.png} 

\subsubsection{Export}
Exports data from the program to Excel files (XLS). It offers two options: export the informant data or their answers to a question.

\subsubsection{Reports}
Creates reports for a question with data grouped by one of the options: overall, by gender, by age, by locality, by state, by state and gender, by state and age, by state, gender and age, by civil state, by schooling, by profession, by religion, and by social category.

\subsubsection{Maps}
Creates maps containing a question’s data over the map image. The options are: diatopic, diatopic-diagenerational, diatopic-diasexual, and areality maps.
\\\\
\textit{Diatopic}:\\
\noindent\includegraphics[scale=0.8]{images/diatopica_histograma.png} 

When the software creates a diatopic map, it displays this window, that contains the map with its variants productivity displayed as pizza graphs, on the right side is shown the labels and a histogram of the overall data. To see more information about one of the pizzas, click on it and the information will be displayed on the right panel. An example is shown below:

\noindent\includegraphics[scale=0.8]{images/diatopica_pizza.png} 

\textit{Diagenerational e Diasexual}:\\
\noindent\includegraphics[scale=0.8]{images/diadupla_pizza.png} 

In the case with two graphics, they are displayed side by side on the map, and when selected, the left graph is shown above the right one in the side panel. In the diagenerational maps, the first group is displayed on the left and in the diasexual maps, the males are shown on the left.
\\\\
\textit{Areality}:\\
\noindent\includegraphics[scale=0.8]{images/isolexica.png} 
\\\\
The areality maps start as a blank map. To show data on the map, select the variants on the list in the right side. The labels will be updated and the map will show the variants' areality.

Note that we can select up to 4 variants on the list. If nothing is displayed check that the map's template is defined as the right area in the map.

To change the labels' colors, right click on the coloured rectangle on the label panel and select "Change color". Then, select the desired color and close the color selection dialogue.
\\\\
\textit{Gradual areality}:\\
\noindent\includegraphics[scale=0.6]{images/isolexica_gradiente.png} 
\\\\
The gradual areality show how many occurrences given variant had in each locality.

\subsubsection{Details about the maps' windows}
The maps can be manipulated with the following controls:

\textit{Using the mouse}\\
With the mouse wheel, the maps' zoom can be changed and, with the middle mouse button, a click and drag movement will drag the map in the display panel.

\textit{Using the keyboard}\\
With the '+' and '-' keys, the maps' zoom can be changed and, with the arrow keys, the map can be moved in the panel.

\textit{Using the graphical menu}\\
When we position the mouse cursor in the top left corner of the maps' panel, a menu shows up with the same options as the keyboard.
\\\\
In the maps with pizza graphs, when you click on one of them, the keyboard controls change to reposition it in the map, using the arrow keys. And the '+' and '-' keys can then be used to change the pizzas' diameters. Note that it is not possible to change only one diameter, all pizzas are update simultaneously.

\subsubsection{Information about queries}
To make a query, some of these dialogues will open for you to select the data you want to use.

\textit{Data sheet selection}\\
\noindent\includegraphics[scale=1]{images/passos_1.png} 

Here you select which informants' data to query.
\\\\
\textit{Questions group selection}\\
\noindent\includegraphics[scale=1]{images/passos_2.png} 

Here you select which questions group to query.
\\\\
\textit{Question selection}\\
\noindent\includegraphics[scale=1]{images/passos_3.png} 

Here you select which question to query.
\\\\
\textit{Map selection}\\
\noindent\includegraphics[scale=1]{images/passos_4.png} 

Here you select which map is used to display the data on.
\\\\
\textit{Equivalence input}\\
\noindent\includegraphics[scale=1]{images/passos_5.png} 

Here you can define equivalence between variants, for the cases where one variant have a morphophonemic variation, but that do not define as another answer. To create equivalences, select the first variant on the left list and the second on the other list and click on "Add". To create equivalences between 3 or more variants, you only need to add one of them with all the others. The software will map all of them as being one variant.
\\\\
\textit{Phonic context input}\\
\noindent\includegraphics[scale=1]{images/selecionar_cont_fonico.png} 

Here you can define the phonic context. If only opens for questions of the PPQ category. First select the phoneme to be processed on the first field. The software will then try to select all correspondent phonemes in the other fields. If one of them is wrong, just select the right phoneme in it.
\\\\
\textit{Maximum number of variants to display}\\
\noindent\includegraphics[scale=1]{images/passos_qtde_vars.png} 

Here you can enter with the maximum number of variants to display on the map. Equivalences counts as one variant. If there is more variants than this number, the less productive ones will be grouped in one category named "others".
\\\\
\subsection{Help}
\subsubsection{Help contents}
Opens the manual.

\subsubsection{About}
Displays information about the software and its developers.

\appendix
\chapter{Appendix}
\section{Navigation menu}
\includegraphics[scale=1]{images/menu_navegacao.png} 

This group of buttons is used to navigate between the items available in some windows of the software.

To move to the first item, click \includegraphics[scale=1]{images/primeiro.png}; to move to the last item, click \includegraphics[scale=1]{images/ultimo.png}.

To move to the next item, click \includegraphics[scale=1]{images/proximo.png};  to move to the previous item, click \includegraphics[scale=1]{images/anterior.png} 

To create a new item, click \includegraphics[scale=1]{images/adicionar_item.png} and to save the current item, click \includegraphics[scale=1]{images/salvar.png}. Note that the software saves the current item when we click on any of the previous buttons. The "save button" is only necessary before closing the window.

In some windows, \includegraphics[scale=1]{images/nao.png} may be available. It deletes the current item. Deleted items cannot be recovered.

If you are having trouble understanding the menu, think of the data as being written in paper sheets. When we want a new item, we get a new sheet of paper. When we want to alter something, first we find it by going through all the sheets until we find the one we want, and then we alter it. When we want to exclude something we again find it and then exclude it.
\end{document}
